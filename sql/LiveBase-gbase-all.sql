DROP VIEW IF EXISTS V_DICT;

DROP TABLE IF EXISTS TAACCESSLOG;

DROP TABLE IF EXISTS TAACCESSSYSTEM;

DROP TABLE IF EXISTS TAAREA;

DROP TABLE IF EXISTS TAAVATAR;

DROP TABLE IF EXISTS TACONFIG;

DROP TABLE IF EXISTS TACUSTOMORG;

DROP TABLE IF EXISTS TACUSTOMORGTYPENAME;

DROP TABLE IF EXISTS TACUSTOMORGUSER;

DROP TABLE IF EXISTS TACUSTOMRESOURCE;

DROP TABLE IF EXISTS TADICT;

DROP TABLE IF EXISTS TAEXTENDSETTING;

DROP TABLE IF EXISTS TALOGINHISTORYLOG;

DROP TABLE IF EXISTS TALOGINSTATLOG;

DROP TABLE IF EXISTS TAOBJECTTAGS;

DROP TABLE IF EXISTS TAONLINELOG;

DROP TABLE IF EXISTS TAONLINESTATLOG;

DROP TABLE IF EXISTS TAORG;

DROP TABLE IF EXISTS TAORGCONSTRAINT;

DROP TABLE IF EXISTS TAORGMG;

DROP TABLE IF EXISTS TAORGOPLOG;

DROP TABLE IF EXISTS TARESOURCE;

DROP TABLE IF EXISTS TARESOURCECATEGORY;

DROP TABLE IF EXISTS TARESOURCEUI;

DROP TABLE IF EXISTS TAROLE;

DROP TABLE IF EXISTS TAROLEAUTHORITY;

DROP TABLE IF EXISTS TAROLEUSER;

DROP TABLE IF EXISTS TASERVEREXCEPTIONLOG;

DROP TABLE IF EXISTS TATAG;

DROP TABLE IF EXISTS TATAGGROUP;

DROP TABLE IF EXISTS TATAGSSTORE;

DROP TABLE IF EXISTS TAUSER;

DROP TABLE IF EXISTS TAUSERORG;


CREATE TABLE taaccesslog (
	logid VARCHAR(36) NOT NULL,
	userid VARCHAR(36) NOT NULL,
	roleid VARCHAR(36) NOT NULL,
	menuid VARCHAR(36) NOT NULL,
	ispermission CHAR(1) NOT NULL,
	accesstime DATETIME YEAR TO FRACTION(5) NOT NULL,
	url LVARCHAR(1024),
	sysflag VARCHAR(50),
	field01 VARCHAR(10),
	field02 VARCHAR(10),
	field03 VARCHAR(10),
	field04 VARCHAR(10),
	field05 VARCHAR(10)
)
 in data ;
CREATE TABLE taaccesssystem (
	id VARCHAR(36) NOT NULL,
	syscode VARCHAR(20) NOT NULL,
	sysname VARCHAR(100) NOT NULL,
	spell VARCHAR(30),
	protocol VARCHAR(10),
	domain VARCHAR(100),
	port VARCHAR(5),
	contextpath VARCHAR(30),
	portalsystem VARCHAR(1),
	menuservice VARCHAR(200),
	provider VARCHAR(10),
	effective VARCHAR(1),
	regtime DATETIME YEAR TO FRACTION(5),
	modifytime DATETIME YEAR TO FRACTION(5),
	backgroundaddress VARCHAR(100),
	PRIMARY KEY (id) CONSTRAINT u108_65
)
 in data ;

CREATE TABLE taarea (
	areaid VARCHAR(36) NOT NULL,
	parentid VARCHAR(36) NOT NULL,
	areaname VARCHAR(100) NOT NULL,
	areacode VARCHAR(20) NOT NULL,
	idpath LVARCHAR(300) NOT NULL,
	namepath LVARCHAR(300) NOT NULL,
	arealevel DECIMAL(2,0),
	spell VARCHAR(100),
	createuser VARCHAR(36) NOT NULL,
	areaorder DECIMAL(20,0),
	effective VARCHAR(1) NOT NULL,
	createtime DATETIME YEAR TO FRACTION(5) NOT NULL,
	modifytime DATETIME YEAR TO FRACTION(5) NOT NULL,
	destory VARCHAR(1),
	PRIMARY KEY (areaid) CONSTRAINT u101_1
)
 in data ;

CREATE INDEX taarea_parentid_idx ON taarea (parentid) ;
CREATE TABLE taavatar (
	userid VARCHAR(36) NOT NULL,
	avatar BYTE,
	updatetime DATETIME YEAR TO FRACTION(5) NOT NULL,
	PRIMARY KEY (userid,updatetime) CONSTRAINT u114_135
)
 in data ;

CREATE TABLE taconfig (
	configid VARCHAR(36) NOT NULL,
	configkey VARCHAR(30) NOT NULL,
	configvalue LVARCHAR(1000) NOT NULL,
	configname VARCHAR(150),
	configdesc LVARCHAR(1000),
	PRIMARY KEY (configid) CONSTRAINT u107_58
)
 in data ;

CREATE TABLE tacustomorg (
	customorgid VARCHAR(36) NOT NULL,
	parentid VARCHAR(36),
	customcode VARCHAR(100),
	customorgtypenameid VARCHAR(36) NOT NULL,
	effective VARCHAR(1) NOT NULL DEFAULT '1',
	orderno DECIMAL(10,0),
	customorgname VARCHAR(128),
	customorgpathid VARCHAR(128),
	customorgpath LVARCHAR(256),
	createuser VARCHAR(20),
	createtime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	updatetime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	destory VARCHAR(20) NOT NULL DEFAULT '0',
	spell VARCHAR(20),
	orgmanager VARCHAR(36),
	orgcode VARCHAR(18),
	contacts VARCHAR(36),
	address LVARCHAR(450),
	tel VARCHAR(20),
	field01 LVARCHAR(1000),
	field02 LVARCHAR(1000),
	field03 LVARCHAR(1000),
	field04 LVARCHAR(1000),
	field05 LVARCHAR(1000),
	field06 LVARCHAR(1000),
	field07 LVARCHAR(1000),
	field08 LVARCHAR(1000),
	field09 LVARCHAR(1000),
	field10 LVARCHAR(1000),
	PRIMARY KEY (customorgid) CONSTRAINT u109_72
)
 in data ;

CREATE INDEX tacustomorg_parentid_idx ON tacustomorg (parentid) ;
CREATE TABLE tacustomorgtypename (
	customorgtypenameid VARCHAR(32),
	customorgtypename VARCHAR(20),
	effective VARCHAR(20) NOT NULL DEFAULT '1',
	createuser VARCHAR(20),
	createtime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	updatetime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	destory VARCHAR(20) NOT NULL DEFAULT '0',
	customorgtypenamecode VARCHAR(20),
	customorgtypenamedesc VARCHAR(20)
)
 in data ;
CREATE TABLE tacustomorguser (
	customorgid VARCHAR(36) NOT NULL,
	userid VARCHAR(36) NOT NULL,
	PRIMARY KEY (customorgid,userid) CONSTRAINT u121_199
)
 in data ;

CREATE TABLE tacustomresource (
	customresourceid VARCHAR(36) NOT NULL,
	resourcename LVARCHAR(450) NOT NULL,
	parentid VARCHAR(32) NOT NULL,
	code VARCHAR(100),
	resourcecontent LVARCHAR(1024),
	resourcecategory VARCHAR(32),
	effective VARCHAR(1),
	addtime DATETIME YEAR TO FRACTION(5),
	modifytime DATETIME YEAR TO FRACTION(5),
	system VARCHAR(36),
	PRIMARY KEY (customresourceid) CONSTRAINT u111_94
)
 in data ;

CREATE TABLE tadict (
	name VARCHAR(50) NOT NULL,
	type VARCHAR(50) NOT NULL,
	label VARCHAR(50) NOT NULL,
	value VARCHAR(6) NOT NULL,
	parentvalue VARCHAR(6),
	sort DECIMAL(4,0) NOT NULL,
	authority VARCHAR(20) NOT NULL,
	cssclass VARCHAR(20),
	cssstyle VARCHAR(128),
	remarks LVARCHAR(256),
	createdate DATETIME YEAR TO FRACTION(5) NOT NULL,
	createuser VARCHAR(36) NOT NULL,
	version VARCHAR(10) NOT NULL,
	status VARCHAR(2) NOT NULL,
	field01 VARCHAR(10),
	field02 VARCHAR(10),
	field03 VARCHAR(10),
	field04 VARCHAR(10),
	field05 VARCHAR(10),
	system VARCHAR(2) NOT NULL,
	newtype VARCHAR(2) NOT NULL,
	PRIMARY KEY (type,value) CONSTRAINT u124_222
)
 in data ;
CREATE TABLE taextendsetting (
	fieldid VARCHAR(10) NOT NULL,
	type VARCHAR(2) NOT NULL,
	effective VARCHAR(1),
	hide VARCHAR(1),
	displaytext VARCHAR(150),
	titext LVARCHAR(1500),
	orderno DECIMAL(10,0),
	required VARCHAR(1),
	unchangeable VARCHAR(1),
	formtype VARCHAR(20),
	contentsize DECIMAL(16,0),
	validreg LVARCHAR(1000),
	connectaa10 VARCHAR(30),
	protectprivacy VARCHAR(1),
	PRIMARY KEY (fieldid,type) CONSTRAINT u135_300
)
 in data ;
CREATE TABLE taloginhistorylog (
	logid VARCHAR(36) NOT NULL,
	userid VARCHAR(36) NOT NULL,
	logintime DATETIME YEAR TO FRACTION(5) NOT NULL,
	logouttime DATETIME YEAR TO FRACTION(5) NOT NULL,
	clientip VARCHAR(200) NOT NULL,
	sessionid VARCHAR(200) NOT NULL,
	serverip VARCHAR(200),
	syspath VARCHAR(50),
	clientsystem VARCHAR(50),
	clientbrowser VARCHAR(50),
	clientscreensize VARCHAR(50),
	PRIMARY KEY (logid) CONSTRAINT taloginhistorylog_pk
)
 in data ;

CREATE TABLE taloginstatlog (
	statdate VARCHAR(20) NOT NULL,
	pointintime VARCHAR(20) NOT NULL,
	loginnum DECIMAL(15,0),
	PRIMARY KEY (statdate,pointintime) CONSTRAINT taloginstatlog_pk
)
 in data ;

CREATE TABLE taobjecttags (
	objectid VARCHAR(36) NOT NULL,
	objecttype VARCHAR(3) NOT NULL,
	tagid VARCHAR(36) NOT NULL,
	PRIMARY KEY (objectid,objecttype,tagid) CONSTRAINT u122_204
)
 in data ;

CREATE TABLE taonlinelog (
	logid VARCHAR(36) NOT NULL,
	userid VARCHAR(36) NOT NULL,
	logintime DATETIME YEAR TO FRACTION(5) NOT NULL,
	curresource LVARCHAR(1000),
	clientip VARCHAR(200) NOT NULL,
	sessionid VARCHAR(200) NOT NULL,
	syspath VARCHAR(50),
	serverip VARCHAR(200),
	clientsystem VARCHAR(50),
	clientbrowser VARCHAR(50),
	clientscreensize VARCHAR(50),
	PRIMARY KEY (logid) CONSTRAINT taonlinelog_pk
)
 in data ;

CREATE TABLE taonlinestatlog (
	statdate VARCHAR(20) NOT NULL,
	pointintime VARCHAR(20) NOT NULL,
	loginnum DECIMAL(15,0),
	PRIMARY KEY (statdate,pointintime) CONSTRAINT taonlinestatlog_pk
)
 in data ;

CREATE TABLE taorg (
	orgid VARCHAR(36) NOT NULL,
	orgname LVARCHAR(300) NOT NULL,
	spell VARCHAR(100) NOT NULL,
	parentid VARCHAR(36) NOT NULL,
	idpath LVARCHAR(750) NOT NULL,
	namepath LVARCHAR(10240) NOT NULL,
	customno VARCHAR(30),
	orderno DECIMAL(10,0) NOT NULL,
	orglevel DECIMAL(16,0),
	area VARCHAR(36),
	effective VARCHAR(1),
	orgtype VARCHAR(2) NOT NULL,
	createuser VARCHAR(36) NOT NULL,
	createtime DATETIME YEAR TO FRACTION(5),
	modifytime DATETIME YEAR TO FRACTION(5),
	orgmanager VARCHAR(36),
	orgcode VARCHAR(18),
	contacts VARCHAR(36),
	address LVARCHAR(450),
	tel VARCHAR(20),
	destory VARCHAR(1),
	field01 LVARCHAR(1000),
	field02 LVARCHAR(1000),
	field03 LVARCHAR(1000),
	field04 LVARCHAR(1000),
	field05 LVARCHAR(1000),
	field06 LVARCHAR(1000),
	field07 LVARCHAR(1000),
	field08 LVARCHAR(1000),
	field09 LVARCHAR(1000),
	field10 LVARCHAR(1000),
	PRIMARY KEY (orgid) CONSTRAINT u134_283
)
 in data ;

CREATE INDEX taorg_destory_index ON taorg (destory) ;
CREATE INDEX taorg_destroy_effective_index ON taorg (effective,destory) ;
CREATE INDEX taorg_effective_index ON taorg (effective) ;
CREATE INDEX taorg_idpath_index ON taorg (idpath) ;
CREATE INDEX taorg_orderno_index ON taorg (orderno) ;
CREATE INDEX taorg_parentid_index ON taorg (parentid) ;
CREATE TABLE taorgconstraint (
	categoryid VARCHAR(2) NOT NULL,
	allowcategoryid VARCHAR(2) NOT NULL,
	PRIMARY KEY (categoryid,allowcategoryid) CONSTRAINT u116_157
)
 in data ;

CREATE TABLE taorgmg (
	roleid VARCHAR(36) NOT NULL,
	orgid VARCHAR(36) NOT NULL,
	effecttime DATETIME YEAR TO FRACTION(5),
	PRIMARY KEY (roleid,orgid) CONSTRAINT u104_30
)
 in data ;

CREATE TABLE taorgoplog (
	logid VARCHAR(36) NOT NULL,
	batchno VARCHAR(36) NOT NULL,
	optype VARCHAR(2) NOT NULL,
	influencebodytype VARCHAR(2) NOT NULL,
	influencebody VARCHAR(36) NOT NULL,
	opbodytype VARCHAR(2),
	opsubject VARCHAR(36),
	changecontent LVARCHAR(2048),
	optime DATETIME YEAR TO FRACTION(5) NOT NULL,
	opuser VARCHAR(36) NOT NULL,
	oprole VARCHAR(36) NOT NULL,
	ispermission VARCHAR(1) NOT NULL,
	PRIMARY KEY (logid) CONSTRAINT u131_266
)
 in data ;

CREATE TABLE taresource (
	resourceid VARCHAR(36) NOT NULL,
	presourceid VARCHAR(36) NOT NULL,
	name VARCHAR(100) NOT NULL,
	code VARCHAR(30),
	syscode VARCHAR(20) NOT NULL,
	url VARCHAR(100),
	orderno DECIMAL(10,0),
	idpath LVARCHAR(1024) NOT NULL,
	namepath LVARCHAR(1024) NOT NULL,
	resourcelevel VARCHAR(2) NOT NULL,
	icon VARCHAR(30),
	iconcolor VARCHAR(7),
	securitypolicy VARCHAR(2) NOT NULL,
	securitylevel DECIMAL(4,0) NOT NULL,
	resourcetype VARCHAR(2) NOT NULL,
	effective VARCHAR(1) NOT NULL,
	isdisplay VARCHAR(1),
	isfiledscontrol VARCHAR(1),
	createdate DATETIME YEAR TO FRACTION(5) NOT NULL,
	createuser VARCHAR(36) NOT NULL,
	uiauthoritypolicy VARCHAR(2),
	field01 VARCHAR(10),
	field02 VARCHAR(10),
	field03 VARCHAR(10),
	field04 VARCHAR(10),
	field05 VARCHAR(10),
	field06 VARCHAR(10),
	field07 VARCHAR(10),
	field08 VARCHAR(10),
	field09 VARCHAR(10),
	field10 VARCHAR(10),
	resturl VARCHAR(100),
	PRIMARY KEY (resourceid) CONSTRAINT u112_101
)
 in data ;

CREATE INDEX taresource_presourceid_idx ON taresource (presourceid) ;
CREATE TABLE taresourcecategory (
	categoryid VARCHAR(36) NOT NULL,
	categoryname LVARCHAR(300) NOT NULL,
	effective VARCHAR(1),
	code VARCHAR(100),
	categorycontent LVARCHAR(1024),
	PRIMARY KEY (categoryid) CONSTRAINT u103_25
)
 in data ;

CREATE TABLE taresourceui (
	pageelementid VARCHAR(36) NOT NULL,
	resourceid VARCHAR(36) NOT NULL,
	elenmentname VARCHAR(20) NOT NULL,
	elementid VARCHAR(30) NOT NULL,
	code VARCHAR(20),
	authoritypolicy VARCHAR(2) NOT NULL,
	createuser VARCHAR(36) NOT NULL,
	createdate DATETIME YEAR TO FRACTION(5) NOT NULL,
	field01 VARCHAR(10),
	field02 VARCHAR(10),
	field03 VARCHAR(10),
	effective VARCHAR(2) NOT NULL,
	PRIMARY KEY (pageelementid) CONSTRAINT u115_140
)
 in data ;

CREATE TABLE tarole (
	roleid VARCHAR(36) NOT NULL,
	rolename VARCHAR(150) NOT NULL,
	isadmin VARCHAR(1),
	rolelevel DECIMAL(32,0),
	orgid VARCHAR(36) NOT NULL,
	roletype VARCHAR(2) NOT NULL,
	effective VARCHAR(1) NOT NULL,
	effectivetime DATETIME YEAR TO FRACTION(5),
	createuser VARCHAR(36),
	createtime DATETIME YEAR TO FRACTION(5),
	roledesc VARCHAR(150),
	rolesign VARCHAR(2),
	subordinate VARCHAR(1),
	PRIMARY KEY (roleid) CONSTRAINT u105_33
)
 in data ;

CREATE TABLE taroleauthority (
	roleid VARCHAR(36) NOT NULL,
	resourceid VARCHAR(36) NOT NULL,
	resourcetype VARCHAR(1) NOT NULL,
	usepermission VARCHAR(1),
	repermission VARCHAR(1),
	reauthrity VARCHAR(1),
	createuser VARCHAR(36),
	createtime DATETIME YEAR TO FRACTION(5),
	effecttime DATETIME YEAR TO FRACTION(5),
	PRIMARY KEY (roleid,resourceid,resourcetype) CONSTRAINT u119_184
)
 in data ;

CREATE TABLE taroleuser (
	userid VARCHAR(36) NOT NULL,
	roleid VARCHAR(36) NOT NULL,
	defaultrole VARCHAR(1),
	createuser VARCHAR(36) NOT NULL,
	createtime DATETIME YEAR TO FRACTION(5) NOT NULL,
	PRIMARY KEY (roleid,userid) CONSTRAINT u110_85
)
 in data ;

CREATE TABLE taserverexceptionlog (
	logid VARCHAR(50) NOT NULL,
	ipaddress VARCHAR(100),
	port VARCHAR(10),
	exceptionname LVARCHAR(255),
	content BYTE,
	createtime DATETIME YEAR TO FRACTION(5),
	syspath VARCHAR(50),
	clientip VARCHAR(50),
	url VARCHAR(100),
	menuid VARCHAR(50),
	menuname VARCHAR(30),
	useragent VARCHAR(200),
	exceptiontype VARCHAR(2),
	requestparameter BYTE,
	PRIMARY KEY (logid) CONSTRAINT taserverexceptionlog_pk
)
 in data ;

CREATE TABLE tatag (
	tagid VARCHAR(32) NOT NULL,
	tagname VARCHAR(20),
	taggroupid VARCHAR(20),
	createtime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	effective VARCHAR(20) NOT NULL DEFAULT '1',
	createuser VARCHAR(20) NOT NULL DEFAULT 'developer',
	destory VARCHAR(20) NOT NULL DEFAULT '0',
	updatetime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	PRIMARY KEY (tagid) CONSTRAINT u118_171
)
 in data ;

CREATE TABLE tataggroup (
	taggroupid VARCHAR(20) NOT NULL DEFAULT '001',
	taggroupname VARCHAR(20) NOT NULL DEFAULT '用户',
	effective VARCHAR(20) NOT NULL DEFAULT '1',
	createuser VARCHAR(20) NOT NULL DEFAULT 'developer',
	createtime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	updatetime DATETIME YEAR TO FRACTION(5) NOT NULL DEFAULT current year to fraction(5),
	destory VARCHAR(20) NOT NULL DEFAULT '0'
)
 in data ;
CREATE TABLE tatagsstore (
	tagid VARCHAR(36) NOT NULL,
	tagname VARCHAR(100) NOT NULL,
	tagcode VARCHAR(30) NOT NULL,
	tagcategory VARCHAR(2) NOT NULL,
	PRIMARY KEY (tagid) CONSTRAINT u117_162
)
 in data ;

CREATE TABLE tauser (
	userid VARCHAR(36) NOT NULL,
	loginid VARCHAR(60),
	password VARCHAR(100),
	passworddefaultnum DECIMAL(16,0),
	pwdlastmodifydate DATETIME YEAR TO FRACTION(5),
	islock VARCHAR(1),
	orderno DECIMAL(10,0),
	name LVARCHAR(450),
	sex VARCHAR(2),
	idcardtype VARCHAR(2),
	idcardno VARCHAR(30),
	mobile VARCHAR(20),
	createuser VARCHAR(36),
	createtime DATETIME YEAR TO FRACTION(5),
	modifytime DATETIME YEAR TO FRACTION(5),
	destory VARCHAR(1),
	accountsource VARCHAR(2),
	effective VARCHAR(1),
	effectivetime DATETIME YEAR TO FRACTION(5),
	jobnumber VARCHAR(15),
	state VARCHAR(10),
	birthplace VARCHAR(12),
	address LVARCHAR(450),
	zipcode VARCHAR(10),
	email VARCHAR(100),
	phone VARCHAR(20),
	education VARCHAR(30),
	graduateschool VARCHAR(150),
	workplace LVARCHAR(300),
	field01 LVARCHAR(1000),
	field02 LVARCHAR(1000),
	field03 LVARCHAR(1000),
	field04 LVARCHAR(1000),
	field05 LVARCHAR(1000),
	field06 LVARCHAR(1000),
	field07 LVARCHAR(1000),
	field08 LVARCHAR(1000),
	field09 LVARCHAR(1000),
	field10 LVARCHAR(1000),
	photo BYTE,
	PRIMARY KEY (userid) CONSTRAINT tauser_pk
)
 in data ;

CREATE INDEX tauser_loginid_idx ON tauser (loginid) ;
CREATE TABLE tauserorg (
	userid VARCHAR(32) NOT NULL,
	orgid VARCHAR(32) NOT NULL,
	isdirect VARCHAR(1) NOT NULL,
	PRIMARY KEY (userid,orgid) CONSTRAINT u113_128
)
 in data ;

CREATE INDEX tauserorg_orgid_index ON tauserorg (orgid) ;
CREATE INDEX tauserorg_userid_index ON tauserorg (userid) ;


create view v_dict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) as select x0.name ,x0.type ,x0.label ,x0.value ,x0.parentvalue ,x0.sort ,x0.authority ,x0.cssclass ,x0.cssstyle ,x0.remarks ,x0.createdate ,x0.createuser ,x0.version ,x0.status ,x0.field01 ,x0.field02 ,x0.field03 ,x0.field04 ,x0.field05 ,x0.system ,x0.newtype from "informix".tadict x0 ;

-- 初始化人员表
INSERT INTO tauser (userid,loginid,password,passworddefaultnum,pwdlastmodifydate,islock,orderno,name,sex,idcardtype,idcardno,mobile,createuser,createtime,modifytime,destory,accountsource,effective,effectivetime,jobnumber,state,birthplace,address,zipcode,email,phone,education,graduateschool,workplace,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'1','developer','$2a$10$ZI3ewI1LvnS8bvTDiZk5X.w9/u.LXy8vPTwQFl8SlDtMTYBQHnIEm',0,current,'0',1,'超级管理员','1','0',null,'18011567700','1',current,NULL,'0',NULL,'1',NULL,NULL,NULL,NULL,'成都市/锦江区/久远银海',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化组织表
INSERT INTO taorg (orgid,orgname,spell,parentid,idpath,namepath,customno,orderno,orglevel,area,effective,orgtype,createuser,createtime,modifytime,orgmanager,orgcode,contacts,address,tel,destory,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'fd811ab9c30440088df3e29ea784460a','久远银海','JYYH','0','fd811ab9c30440088df3e29ea784460a','久远银海','',0,0,'67979672000d4c6cbecf422f233213df','1','01','1',NULL,current,'','','','','','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化码表
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','个人角色','02',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','公有角色','01',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','代理角色','03',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','用户名密码','01',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','指纹识别','02',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','人脸识别','03',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'认证方式类型','AUTHMODE','Key盘','04',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','OFF','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','FATAL','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ERROR','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','WARN','3',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','INFO','4',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','DEBUG','5',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','TRACE','6',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ALL','7',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','通用菜单','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','管理菜单','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','经办菜单','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','组织','01',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','人员','02',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','岗位','03',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','权限','07',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增组织','01',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑组织','02',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用组织','03',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增人员','04',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑人员','05',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用人员','06',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','密码重置','07',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位给人员','08',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','设置主岗位给人员','09',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增岗位','10',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑岗位','11',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用岗位','12',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位使用权限','13',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位使用权限','14',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用岗位','15',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用人员','16',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用组织','17',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除组织','18',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人使用权限','19',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人使用权限','20',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人授权权限','21',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人授权权限','22',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除管理员','23',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增管理员','24',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除岗位','25',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除人员','26',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','机构','01',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','部门','02',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','组','03',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','无','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','男','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','女','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','是','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','否','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','不显示','0',NULL,31,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见不可编辑','1','',21,'0','','',NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见可编辑','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','有效','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','居民身份证(户口簿)','0','',10,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','111','11','',10,'0','','',NULL,current,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','无效','0',NULL,0,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','无限制','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','一级','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','二级','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','三级','3',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','四级','4',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','无需登录可访问','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','登陆均可访问','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','授权可访问','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','继承当前菜单权限','0',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','独立授权','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','未同步','0','',0,'0','','',NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','已同步','2','',2,'0','','',NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','脏数据','1','',1,'0','','',NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','123','111','',20,'0','','',NULL,current,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','护照','1','',20,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1','1','',1,'0','','',NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-2','12','',1,'0','','',NULL,current,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1-1U','123','1',1,'0','','',NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','组织账户','2',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','个人账户','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','a','2',NULL,3,'0',NULL,NULL,NULL,current,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','军官证','2','',30,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','其他','3','',40,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'test1','TEST1','a','1','',10,'0',NULL,NULL,NULL,current,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','稽核角色','2',NULL,2,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','业务角色','1',NULL,1,'0',NULL,NULL,NULL,current,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');

-- 初始化功能菜单
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'40337bdecb19484ebeb39d6c21aaca26','0','银海软件','','portal','',0,'40337bdecb19484ebeb39d6c21aaca26','银海软件','0','','','1',1,'0','1','','',current,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'0415d44401b24605a21b589b6aaa349e','40337bdecb19484ebeb39d6c21aaca26','门户菜单','','portal','',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e','银海软件/门户菜单','1','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'7459c1b525934151a1d309a304933644','0415d44401b24605a21b589b6aaa349e','资源管理','','portal','',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644','银海软件/门户菜单/资源管理','2','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'ec56a0a43b09429482632cb61f7c6908','7459c1b525934151a1d309a304933644','功能菜单管理','','portal','sysmg.html#/resourceManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/ec56a0a43b09429482632cb61f7c6908','银海软件/门户菜单/资源管理/功能菜单管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/resourceManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'6b8c6d0a93844985894713e396e7a99f','7459c1b525934151a1d309a304933644','接入系统管理','','portal','sysmg.html#/accessSystem',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/6b8c6d0a93844985894713e396e7a99f','银海软件/门户菜单/资源管理/接入系统管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/accessSystemManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'3df588fc565d4287b3cefcd00a39cd91','7459c1b525934151a1d309a304933644','标签管理','','portal','sysmg.html#/tagManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/3df588fc565d4287b3cefcd00a39cd91','银海软件/门户菜单/资源管理/标签管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/tagManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'c2745b7cae7846acb9bcf8d0f4e836e8','7459c1b525934151a1d309a304933644','授权对象管理','','portal','sysmg.html#/customResource',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/c2745b7cae7846acb9bcf8d0f4e836e8','银海软件/门户菜单/资源管理/授权对象管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'78ad02fdb879406ebc5e7a4faf8f5905','0415d44401b24605a21b589b6aaa349e','资源权限管理','','portal','',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905','银海软件/门户菜单/资源权限管理','2','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'95bb9b749bf54e4692b0b1f14fd1b5ab','78ad02fdb879406ebc5e7a4faf8f5905','角色权限管理','','portal','authority.html#/roleAuthorityManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/95bb9b749bf54e4692b0b1f14fd1b5ab','银海软件/门户菜单/资源权限管理/角色权限管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/roleAuthorityManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'8aa86ed4c7f84183935a262db4a605d3','78ad02fdb879406ebc5e7a4faf8f5905','管理员权限管理','','portal','authority.html#/adminAuthority',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/8aa86ed4c7f84183935a262db4a605d3','银海软件/门户菜单/资源权限管理/管理员权限管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/adminAuthorityManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'1d4e283ad5584e02811f6b188d3592bc','0415d44401b24605a21b589b6aaa349e','系统管理','','portal','',41,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc','银海软件/门户菜单/系统管理','2','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'7c1dabd160974d8f90858c187cefa128','1d4e283ad5584e02811f6b188d3592bc','日志动态配置','','portal','logmg.html#/logConfig',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c1dabd160974d8f90858c187cefa128','银海软件/门户菜单/系统管理/日志动态配置','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'770b2f1e77eb4d9092dc89743b5792c9','1d4e283ad5584e02811f6b188d3592bc','登录日志统计','','portal','logmg.html#/loginLogAnalysis',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/770b2f1e77eb4d9092dc89743b5792c9','银海软件/门户菜单/系统管理/登录日志统计','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'7c2862a57e34459484bd701c19a8de27','1d4e283ad5584e02811f6b188d3592bc','系统异常日志','','portal','logmg.html#/systemExceptionLog',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c2862a57e34459484bd701c19a8de27','银海软件/门户菜单/系统管理/系统异常日志','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'877e407281dd48acb05a77fcb922bc73','78ad02fdb879406ebc5e7a4faf8f5905','权限代理','','portal','authority.html#/authorityAgent',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/877e407281dd48acb05a77fcb922bc73','银海软件/门户菜单/资源权限管理/权限代理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/authorityAgentRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'5e67c7acef914c349d8aff076921f6b5','78ad02fdb879406ebc5e7a4faf8f5905','相似权限管理','','portal','authority.html#/similarAuthority',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/5e67c7acef914c349d8aff076921f6b5','银海软件/门户菜单/资源权限管理/相似权限管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/similarAuthorityManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'48afedddc8f04c668b3c1572c30a7745','0415d44401b24605a21b589b6aaa349e','组织人员管理','','portal','',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745','银海软件/门户菜单/组织人员管理','2','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'1e706f26bc144c1da12022359c238053','48afedddc8f04c668b3c1572c30a7745','组织机构管理','','portal','orguser.html#/orgManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/1e706f26bc144c1da12022359c238053','银海软件/门户菜单/组织人员管理/组织机构管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/orgManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'daceeff8a97b46cb9573b93ba3a5a792','48afedddc8f04c668b3c1572c30a7745','人员管理','','portal','orguser.html#/userManagement',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/daceeff8a97b46cb9573b93ba3a5a792','银海软件/门户菜单/组织人员管理/人员管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/userManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'3dbde33722154503a7d22ac60f6a0e4e','48afedddc8f04c668b3c1572c30a7745','自定义组织管理','','portal','orguser.html#/customOrgManagement',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/3dbde33722154503a7d22ac60f6a0e4e','银海软件/门户菜单/组织人员管理/自定义组织管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/customOrgManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'4b2eee0d7ded4e8094d4acf439fd3a1c','48afedddc8f04c668b3c1572c30a7745','行政区划管理','','portal','orguser.html#/areaManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/4b2eee0d7ded4e8094d4acf439fd3a1c','银海软件/门户菜单/组织人员管理/行政区划管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/areaManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'bd9d0bba145c458e841aa9da0aeeb1d8','7459c1b525934151a1d309a304933644','用户可管理字段配置','','portal','sysmg.html#/userInfoManagement',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/bd9d0bba145c458e841aa9da0aeeb1d8','银海软件/门户菜单/资源管理/用户可管理字段配置','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/manageableFieldsOfUserInfoManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'd0afd42a20a441c5867c93eba60cfdd6','40337bdecb19484ebeb39d6c21aaca26','公共业务菜单','','ggyww','',10,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6','银海软件/公共业务菜单','1','','','2',0,'2','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'ffa74f43e853441dac0ee90c787cb2e6','1d4e283ad5584e02811f6b188d3592bc','字典管理','','portal','sysmg.html#/dictionaryManager',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/ffa74f43e853441dac0ee90c787cb2e6','银海软件/门户菜单/系统管理/字典管理','3','','','2',0,'1','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'58129adf266c4b67bb99e2abb862ddf1','d0afd42a20a441c5867c93eba60cfdd6','单位业务','','ggyww','',0,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/58129adf266c4b67bb99e2abb862ddf1','银海软件/公共业务菜单/单位业务','2','','','2',0,'2','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'395eb1c2a6d84c7c933b9e71c9a3a494','58129adf266c4b67bb99e2abb862ddf1','单位新参保','','ggyww','',0,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/58129adf266c4b67bb99e2abb862ddf1/395eb1c2a6d84c7c933b9e71c9a3a494','银海软件/公共业务菜单/单位业务/单位新参保','3','','','2',0,'2','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'77145930ff8f478c803531b2e06f7750','d0afd42a20a441c5867c93eba60cfdd6','个人业务','','ggyww','',10,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/77145930ff8f478c803531b2e06f7750','银海软件/公共业务菜单/个人业务','2','','','2',0,'2','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'98f50db0ef6d4707844e692d4e5c14b2','77145930ff8f478c803531b2e06f7750','个人新参保','','ggyww','',0,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/77145930ff8f478c803531b2e06f7750/98f50db0ef6d4707844e692d4e5c14b2','银海软件/公共业务菜单/个人业务/个人新参保','3','','','2',0,'2','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'fef08bdf9b4a4c3c9dae02c66e5ed009','40337bdecb19484ebeb39d6c21aaca26','demo菜单','','ggyww','',20,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009','银海软件/demo菜单','1','','','2',0,'0','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'85c224dd0c8b4ec0a1199dbb195765ba','fef08bdf9b4a4c3c9dae02c66e5ed009','模块1',NULL,'ggyww',NULL,0,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009/85c224dd0c8b4ec0a1199dbb195765ba','银海软件/demo菜单/模块1','2',NULL,NULL,'2',0,'0','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'65d3fe6dcba7423688c715aa3a2530e9','85c224dd0c8b4ec0a1199dbb195765ba','part1',NULL,'ggyww','projectModuleOne.html#/modulePart1',0,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009/85c224dd0c8b4ec0a1199dbb195765ba/65d3fe6dcba7423688c715aa3a2530e9','银海软件/demo菜单/模块1/part1','3',NULL,NULL,'2',0,'0','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'b98b1678c10f46c193e79a028c35b7a9','85c224dd0c8b4ec0a1199dbb195765ba','part2',NULL,'ggyww','projectModuleOne.html#/modulePart2',10,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009/85c224dd0c8b4ec0a1199dbb195765ba/b98b1678c10f46c193e79a028c35b7a9','银海软件/demo菜单/模块1/part2','3',NULL,NULL,'2',0,'0','1',NULL,NULL,current,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化角色
INSERT INTO tarole (roleid,rolename,isadmin,rolelevel,orgid,roletype,effective,effectivetime,createuser,createtime,roledesc,rolesign,subordinate) VALUES (
'1','超级管理员',NULL,NULL,'fd811ab9c30440088df3e29ea784460a','04','1',NULL,'1',current,'超级管理员角色',NULL,NULL);

-- 初始化用户组织表
INSERT INTO tauserorg (userid,orgid,isdirect) VALUES (
'1','fd811ab9c30440088df3e29ea784460a','1');
