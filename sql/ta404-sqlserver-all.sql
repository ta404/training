/*==============================================================*/
/* DBMS name:      Microsoft SQL Server 2012                    */
/* Created on:     2018/12/13 12:39:14                          */
/*==============================================================*/

if exists (select 1
            from  sysobjects
           where  id = object_id('v_dict')
            and   type = 'V')
   drop view v_dict
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAACCESSLOG')
            and   type = 'U')
   drop table TAACCESSLOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAACCESSSYSTEM')
            and   type = 'U')
   drop table TAACCESSSYSTEM
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAAREA')
            and   type = 'U')
   drop table TAAREA
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAAVATAR')
            and   type = 'U')
   drop table TAAVATAR
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TACONFIG')
            and   type = 'U')
   drop table TACONFIG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TACUSTOMORG')
            and   type = 'U')
   drop table TACUSTOMORG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TACUSTOMORGTYPENAME')
            and   type = 'U')
   drop table TACUSTOMORGTYPENAME
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TACUSTOMORGUSER')
            and   type = 'U')
   drop table TACUSTOMORGUSER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TACUSTOMRESOURCE')
            and   type = 'U')
   drop table TACUSTOMRESOURCE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TADICT')
            and   type = 'U')
   drop table TADICT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAEXTENDSETTING')
            and   type = 'U')
   drop table TAEXTENDSETTING
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TALOGINHISTORYLOG')
            and   type = 'U')
   drop table TALOGINHISTORYLOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TALOGINSTATLOG')
            and   type = 'U')
   drop table TALOGINSTATLOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAOBJECTTAGS')
            and   type = 'U')
   drop table TAOBJECTTAGS
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAONLINELOG')
            and   type = 'U')
   drop table TAONLINELOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAONLINESTATLOG')
            and   type = 'U')
   drop table TAONLINESTATLOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAORG')
            and   type = 'U')
   drop table TAORG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAORGCONSTRAINT')
            and   type = 'U')
   drop table TAORGCONSTRAINT
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAORGMG')
            and   type = 'U')
   drop table TAORGMG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAORGOPLOG')
            and   type = 'U')
   drop table TAORGOPLOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TARESOURCE')
            and   type = 'U')
   drop table TARESOURCE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TARESOURCECATEGORY')
            and   type = 'U')
   drop table TARESOURCECATEGORY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TARESOURCEUI')
            and   type = 'U')
   drop table TARESOURCEUI
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAROLE')
            and   type = 'U')
   drop table TAROLE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAROLEAUTHORITY')
            and   type = 'U')
   drop table TAROLEAUTHORITY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAROLEUSER')
            and   type = 'U')
   drop table TAROLEUSER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TASERVEREXCEPTIONLOG')
            and   type = 'U')
   drop table TASERVEREXCEPTIONLOG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TATAG')
            and   type = 'U')
   drop table TATAG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TATAGGROUP')
            and   type = 'U')
   drop table TATAGGROUP
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TATAGSSTORE')
            and   type = 'U')
   drop table TATAGSSTORE
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAUSER')
            and   type = 'U')
   drop table TAUSER
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAUSERORG')
            and   type = 'U')
   drop table TAUSERORG
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TARESOURCEURL')
            and   type = 'U')
   drop table TARESOURCEURL
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAROLEURLAUTHORITY')
            and   type = 'U')
   drop table TAROLEURLAUTHORITY
go

if exists (select 1
            from  sysobjects
           where  id = object_id('TAROLEWORKBENCH')
            and   type = 'U')
   drop table TAROLEWORKBENCH
go
if exists (select 1
            from  sysobjects
           where  id = object_id('TAUSERWORKBENCH')
            and   type = 'U')
   drop table TAUSERWORKBENCH
go


/*==============================================================*/
/* Table: TAACCESSLOG                                           */
/*==============================================================*/
create table TAACCESSLOG (
   LOGID                varchar(36)          not null,
   USERID               varchar(36)          not null,
   ROLEID               varchar(36)          not null,
   MENUID               varchar(36)          not null,
   ISPERMISSION         char(1)              not null,
   ACCESSTIME           datetime             not null,
   URL                  varchar(1024)        null,
   SYSFLAG              varchar(50)          null,
   FIELD01              varchar(10)          null,
   FIELD02              varchar(10)          null,
   FIELD03              varchar(10)          null,
   FIELD04              varchar(10)          null,
   FIELD05              varchar(10)          null,
   constraint PK_TAACCESSLOG primary key nonclustered (LOGID)
)
go

/*==============================================================*/
/* Table: TAACCESSSYSTEM                                        */
/*==============================================================*/
create table TAACCESSSYSTEM (
   ID                   varchar(36)          not null,
   SYSCODE              varchar(20)          not null,
   SYSNAME              varchar(100)         not null,
   SPELL                varchar(30)          null,
   PROTOCOL             varchar(10)          null,
   DOMAIN               varchar(100)         null,
   PORT                 varchar(5)           null,
   CONTEXTPATH          varchar(30)          null,
   PORTALSYSTEM         varchar(1)           null,
   MENUSERVICE          varchar(200)         null,
   PROVIDER             varchar(10)          null,
   EFFECTIVE            varchar(1)           null,
   REGTIME              datetime             null,
   MODIFYTIME           datetime             null,
   BACKGROUNDADDRESS    varchar(100)         null,
   constraint PK_TAACCESSSYSTEM primary key nonclustered (ID)
)
go

/*==============================================================*/
/* Table: TAAREA                                                */
/*==============================================================*/
create table TAAREA (
   AREAID               varchar(36)          not null,
   PARENTID             varchar(36)          not null,
   AREANAME             varchar(100)         not null,
   AREACODE             varchar(20)          not null,
   IDPATH               varchar(300)         not null,
   NAMEPATH             varchar(300)         not null,
   AREALEVEL            numeric(2,0)         null,
   SPELL                varchar(100)         null,
   CREATEUSER           varchar(36)          not null,
   AREAORDER            numeric(20,0)        null,
   EFFECTIVE            varchar(1)           not null,
   CREATETIME           datetime             not null,
   MODIFYTIME           datetime             not null,
   DESTORY              varchar(1)           null,
   constraint PK_TAAREA primary key nonclustered (AREAID)
)
go

/*==============================================================*/
/* Table: TAAVATAR                                              */
/*==============================================================*/
create table TAAVATAR (
   USERID               varchar(36)          not null,
   AVATAR               varbinary(max)            null,
   UPDATETIME           datetime             not null,
   constraint PK_TAAVATAR primary key nonclustered (USERID, UPDATETIME)
)
go

/*==============================================================*/
/* Table: TACONFIG                                              */
/*==============================================================*/
create table TACONFIG (
   CONFIGID             varchar(36)          not null,
   CONFIGKEY            varchar(30)          not null,
   CONFIGVALUE          varchar(1000)        not null,
   CONFIGNAME           varchar(150)         null,
   CONFIGDESC           varchar(1000)        null,
   constraint PK_TACONFIG primary key nonclustered (CONFIGID)
)
go

/*==============================================================*/
/* Table: TACUSTOMORG                                           */
/*==============================================================*/
create table TACUSTOMORG (
   CUSTOMORGID          varchar(36)          not null,
   PARENTID             varchar(36)          null,
   CUSTOMCODE           varchar(100)         null,
   CUSTOMORGTYPENAMEID  varchar(36)          not null,
   EFFECTIVE            varchar(1)           not null default '1',
   ORDERNO              numeric(10,0)        null,
   CUSTOMORGNAME        varchar(128)         null,
   CUSTOMORGPATHID      varchar(128)         null,
   CUSTOMORGPATH        varchar(256)         null,
   CREATEUSER           varchar(20)          null,
   CREATETIME           datetime             not null default GETDATE(),
   UPDATETIME           datetime             not null default GETDATE(),
   DESTORY              varchar(20)          not null default '0',
   SPELL                varchar(20)          null,
   ORGMANAGER           varchar(36)          null,
   ORGCODE              varchar(18)          null,
   CONTACTS             varchar(36)          null,
   ADDRESS              varchar(450)         null,
   TEL                  varchar(20)          null,
   FIELD01              varchar(1000)        null,
   FIELD02              varchar(1000)        null,
   FIELD03              varchar(1000)        null,
   FIELD04              varchar(1000)        null,
   FIELD05              varchar(1000)        null,
   FIELD06              varchar(1000)        null,
   FIELD07              varchar(1000)        null,
   FIELD08              varchar(1000)        null,
   FIELD09              varchar(1000)        null,
   FIELD10              varchar(1000)        null,
   constraint PK_TACUSTOMORG primary key nonclustered (CUSTOMORGID)
)
go

/*==============================================================*/
/* Table: TACUSTOMORGTYPENAME                                   */
/*==============================================================*/
create table TACUSTOMORGTYPENAME (
   CUSTOMORGTYPENAMEID  varchar(32)          not null,
   CUSTOMORGTYPENAME    varchar(20)          null,
   EFFECTIVE            varchar(20)          not null default '1',
   CREATEUSER           varchar(20)          null,
   CREATETIME           datetime             not null default GETDATE(),
   UPDATETIME           datetime             not null default GETDATE(),
   DESTORY              varchar(20)          not null default '0',
   CUSTOMORGTYPENAMECODE varchar(20)          null,
   CUSTOMORGTYPENAMEDESC varchar(20)          null,
   constraint PK_TACUSTOMORGTYPENAME primary key nonclustered (CUSTOMORGTYPENAMEID)
)
go

/*==============================================================*/
/* Table: TACUSTOMORGUSER                                       */
/*==============================================================*/
create table TACUSTOMORGUSER (
   CUSTOMORGID          varchar(36)          not null,
   USERID               varchar(36)          not null,
   constraint PK_TACUSTOMORGUSER primary key nonclustered (CUSTOMORGID, USERID)
)
go

/*==============================================================*/
/* Table: TACUSTOMRESOURCE                                      */
/*==============================================================*/
create table TACUSTOMRESOURCE (
   CUSTOMRESOURCEID     varchar(36)          not null,
   RESOURCENAME         varchar(450)         not null,
   PARENTID             varchar(32)          not null,
   CODE                 varchar(100)         null,
   RESOURCECONTENT      varchar(1024)        null,
   RESOURCECATEGORY     varchar(32)          null,
   EFFECTIVE            varchar(1)           null,
   ADDTIME              datetime             null,
   MODIFYTIME           datetime             null,
   SYSTEM               varchar(36)          null,
   constraint PK_TACUSTOMRESOURCE primary key nonclustered (CUSTOMRESOURCEID)
)
go

/*==============================================================*/
/* Table: TADICT                                                */
/*==============================================================*/
create table TADICT (
   NAME                 varchar(50)          not null,
   TYPE                 varchar(50)          not null,
   LABEL                varchar(50)          not null,
   VALUE                varchar(6)           not null,
   PARENTVALUE          varchar(6)           null,
   SORT                 numeric(4,0)         not null,
   AUTHORITY            varchar(20)          not null,
   CSSCLASS             varchar(20)          null,
   CSSSTYLE             varchar(128)         null,
   REMARKS              varchar(256)         null,
   CREATEDATE           datetime             not null,
   CREATEUSER           varchar(36)          not null,
   VERSION              varchar(10)          not null,
   STATUS               varchar(2)           not null,
   FIELD01              varchar(10)          null,
   FIELD02              varchar(10)          null,
   FIELD03              varchar(10)          null,
   FIELD04              varchar(10)          null,
   FIELD05              varchar(10)          null,
   SYSTEM               varchar(2)           not null,
   NEWTYPE              varchar(2)           not null,
   constraint PK_TADICT primary key nonclustered (TYPE, VALUE)
)
go

/*==============================================================*/
/* Table: TAEXTENDSETTING                                       */
/*==============================================================*/
create table TAEXTENDSETTING (
   FIELDID              varchar(10)          not null,
   TYPE                 varchar(2)           not null,
   EFFECTIVE            varchar(1)           null,
   HIDE                 varchar(1)           null,
   DISPLAYTEXT          varchar(150)         null,
   TITEXT               varchar(1500)        null,
   ORDERNO              numeric(10,0)        null,
   REQUIRED             varchar(1)           null,
   UNCHANGEABLE         varchar(1)           null,
   FORMTYPE             varchar(20)          null,
   CONTENTSIZE          numeric(16,0)        null,
   VALIDREG             varchar(1000)        null,
   CONNECTAA10          varchar(30)          null,
   PROTECTPRIVACY       varchar(1)           null,
   constraint PK_TAEXTENDSETTING primary key nonclustered (FIELDID, TYPE)
)
go

/*==============================================================*/
/* Table: TALOGINHISTORYLOG                                     */
/*==============================================================*/
create table TALOGINHISTORYLOG (
   LOGID                varchar(36)          not null,
   USERID               varchar(36)          not null,
   LOGINTIME            datetime             not null,
   LOGOUTTIME           datetime             not null,
   CLIENTIP             varchar(200)         not null,
   SESSIONID            varchar(200)         not null,
   SERVERIP             varchar(200)         null,
   SYSPATH              varchar(50)          null,
   CLIENTSYSTEM         varchar(50)          null,
   CLIENTBROWSER        varchar(50)          null,
   CLIENTSCREENSIZE     varchar(50)          null,
   constraint PK_TALOGINHISTORYLOG primary key nonclustered (LOGID)
)
go

/*==============================================================*/
/* Table: TALOGINSTATLOG                                        */
/*==============================================================*/
create table TALOGINSTATLOG (
   STATDATE             varchar(20)          not null,
   POINTINTIME          varchar(20)          not null,
   LOGINNUM             numeric(15,0)        null,
   constraint PK_TALOGINSTATLOG primary key nonclustered (STATDATE, POINTINTIME)
)
go

/*==============================================================*/
/* Table: TAOBJECTTAGS                                          */
/*==============================================================*/
create table TAOBJECTTAGS (
   OBJECTID             varchar(36)          not null,
   OBJECTTYPE           varchar(3)           not null,
   TAGID                varchar(36)          not null,
   constraint PK_TAOBJECTTAGS primary key nonclustered (OBJECTID, OBJECTTYPE, TAGID)
)
go

/*==============================================================*/
/* Table: TAONLINELOG                                           */
/*==============================================================*/
create table TAONLINELOG (
   LOGID                varchar(36)          not null,
   USERID               varchar(36)          not null,
   LOGINTIME            datetime             not null,
   CURRESOURCE          varchar(1000)        null,
   CLIENTIP             varchar(200)         not null,
   SESSIONID            varchar(200)         not null,
   SYSPATH              varchar(50)          null,
   SERVERIP             varchar(200)         null,
   CLIENTSYSTEM         varchar(50)          null,
   CLIENTBROWSER        varchar(50)          null,
   CLIENTSCREENSIZE     varchar(50)          null,
   constraint PK_TAONLINELOG primary key nonclustered (LOGID)
)
go

/*==============================================================*/
/* Table: TAONLINESTATLOG                                       */
/*==============================================================*/
create table TAONLINESTATLOG (
   STATDATE             varchar(20)          not null,
   POINTINTIME          varchar(20)          not null,
   LOGINNUM             numeric(15,0)        null,
   constraint PK_TAONLINESTATLOG primary key nonclustered (STATDATE, POINTINTIME)
)
go

/*==============================================================*/
/* Table: TAORG                                                 */
/*==============================================================*/
create table TAORG (
   ORGID                varchar(36)          not null,
   ORGNAME              varchar(300)         not null,
   SPELL                varchar(100)         not null,
   PARENTID             varchar(36)          not null,
   IDPATH               varchar(1024)        not null,
   NAMEPATH             varchar(1024)        not null,
   CUSTOMNO             varchar(30)          null,
   ORDERNO              numeric(10,0)        not null,
   ORGLEVEL             numeric(16,0)        null,
   AREA                 varchar(36)          null,
   EFFECTIVE            varchar(1)           null,
   ORGTYPE              varchar(2)           not null,
   CREATEUSER           varchar(36)          not null,
   CREATETIME           datetime             null,
   MODIFYTIME           datetime             null,
   ORGMANAGER           varchar(36)          null,
   ORGCODE              varchar(18)          null,
   CONTACTS             varchar(36)          null,
   ADDRESS              varchar(450)         null,
   TEL                  varchar(20)          null,
   DESTORY              varchar(1)           null,
   FIELD01              varchar(1000)        null,
   FIELD02              varchar(1000)        null,
   FIELD03              varchar(1000)        null,
   FIELD04              varchar(1000)        null,
   FIELD05              varchar(1000)        null,
   FIELD06              varchar(1000)        null,
   FIELD07              varchar(1000)        null,
   FIELD08              varchar(1000)        null,
   FIELD09              varchar(1000)        null,
   FIELD10              varchar(1000)        null,
   constraint PK_TAORG primary key nonclustered (ORGID)
)
go

/*==============================================================*/
/* Table: TAORGCONSTRAINT                                       */
/*==============================================================*/
create table TAORGCONSTRAINT (
   CATEGORYID           varchar(2)           not null,
   ALLOWCATEGORYID      varchar(2)           not null,
   constraint PK_TAORGCONSTRAINT primary key nonclustered (CATEGORYID, ALLOWCATEGORYID)
)
go

/*==============================================================*/
/* Table: TAORGMG                                               */
/*==============================================================*/
create table TAORGMG (
   ROLEID               varchar(36)          not null,
   ORGID                varchar(36)          not null,
   EFFECTTIME           datetime             null,
   constraint PK_TAORGMG primary key nonclustered (ROLEID, ORGID)
)
go

/*==============================================================*/
/* Table: TAORGOPLOG                                            */
/*==============================================================*/
create table TAORGOPLOG (
   LOGID                varchar(36)          not null,
   BATCHNO              varchar(36)          not null,
   OPTYPE               varchar(2)           not null,
   INFLUENCEBODYTYPE    varchar(2)           not null,
   INFLUENCEBODY        varchar(36)          not null,
   OPBODYTYPE           varchar(2)           null,
   OPSUBJECT            varchar(36)          null,
   CHANGECONTENT        varchar(2048)        null,
   OPTIME               datetime             not null,
   OPUSER               varchar(36)          not null,
   ISPERMISSION         varchar(1)           not null,
   constraint PK_TAORGOPLOG primary key nonclustered (LOGID)
)
go

/*==============================================================*/
/* Table: TARESOURCE                                            */
/*==============================================================*/
create table TARESOURCE (
   RESOURCEID           varchar(36)          not null,
   PRESOURCEID          varchar(36)          not null,
   NAME                 varchar(100)         not null,
   CODE                 varchar(30)          null,
   SYSCODE              varchar(20)          not null,
   URL                  varchar(100)         null,
   ORDERNO              numeric(10,0)        null,
   IDPATH               varchar(1024)        not null,
   NAMEPATH             varchar(1024)        not null,
   RESOURCELEVEL        varchar(2)           not null,
   ICON                 varchar(30)          null,
   ICONCOLOR            varchar(7)           null,
   SECURITYPOLICY       varchar(2)           not null,
   SECURITYLEVEL        numeric(4,0)         not null,
   RESOURCETYPE         varchar(2)           not null,
   EFFECTIVE            varchar(1)           not null,
   ISDISPLAY            varchar(1)           null,
   ISFILEDSCONTROL      varchar(1)           null,
   CREATEDATE           datetime             not null,
   CREATEUSER           varchar(36)          not null,
   UIAUTHORITYPOLICY    varchar(2)           null,
   FIELD01              varchar(10)          null,
   FIELD02              varchar(10)          null,
   FIELD03              varchar(10)          null,
   FIELD04              varchar(10)          null,
   FIELD05              varchar(10)          null,
   FIELD06              varchar(10)          null,
   FIELD07              varchar(10)          null,
   FIELD08              varchar(10)          null,
   FIELD09              varchar(10)          null,
   FIELD10              varchar(10)          null,
   WORKBENCH            varchar(100)         null,
   constraint PK_TARESOURCE primary key nonclustered (RESOURCEID)
)
go


/*==============================================================*/
/* Table: TARESOURCEURL     功能资源Rest Url 表                  */
/*==============================================================*/
create table TARESOURCEURL(
  URLID           varchar(36)  not null,
  RESOURCEID      varchar(36)  not null,
  URLNAME         varchar(40)  null,
  RESTURL         varchar(256) not null,
  AUTHORITYPOLICY varchar(1)   not null,
  constraint PK_TARESOURCEURL primary key nonclustered (URLID)
)
go

/*==============================================================*/
/* Table: TAROLEWORKBENCH     角色工作台模板表                    */
/*==============================================================*/
create table TAROLEWORKBENCH(
  ROLEID        varchar(36)    not null,
  WORKBENCHJSON varbinary(max) null,
  constraint PK_TAROLEWORKBENCH primary key nonclustered (ROLEID)
)
go

/*==============================================================*/
/* Table: TAUSERWORKBENCH     用户工作台模板表                    */
/*==============================================================*/
create table TAUSERWORKBENCH(
  USERID        varchar(36)    not null,
  WORKBENCHJSON varbinary(max) null,
  constraint PK_TAUSERWORKBENCH primary key nonclustered (USERID)
)
go

/*==============================================================*/
/* Table: TARESOURCECATEGORY                                    */
/*==============================================================*/
create table TARESOURCECATEGORY (
   CATEGORYID           varchar(36)          not null,
   CATEGORYNAME         varchar(300)         not null,
   EFFECTIVE            varchar(1)           null,
   CODE                 varchar(100)         null,
   CATEGORYCONTENT      varchar(1024)        null,
   constraint PK_TARESOURCECATEGORY primary key nonclustered (CATEGORYID)
)
go

/*==============================================================*/
/* Table: TARESOURCEUI                                          */
/*==============================================================*/
create table TARESOURCEUI (
   PAGEELEMENTID        varchar(36)          not null,
   RESOURCEID           varchar(36)          not null,
   ELENMENTNAME         varchar(20)          not null,
   ELEMENTID            varchar(30)          not null,
   CODE                 varchar(20)          null,
   AUTHORITYPOLICY      varchar(2)           not null,
   CREATEUSER           varchar(36)          not null,
   CREATEDATE           datetime             not null,
   FIELD01              varchar(10)          null,
   FIELD02              varchar(10)          null,
   FIELD03              varchar(10)          null,
   EFFECTIVE            varchar(2)           not null,
   constraint PK_TARESOURCEUI primary key nonclustered (PAGEELEMENTID)
)
go

/*==============================================================*/
/* Table: TAROLE                                                */
/*==============================================================*/
create table TAROLE (
   ROLEID               varchar(36)          not null,
   ROLENAME             varchar(150)         not null,
   ISADMIN              varchar(1)           null,
   ROLELEVEL            numeric(32,0)        null,
   ORGID                varchar(36)          not null,
   ROLETYPE             varchar(2)           not null,
   EFFECTIVE            varchar(1)           not null,
   EFFECTIVETIME        datetime             null,
   CREATEUSER           varchar(36)          null,
   CREATETIME           datetime             null,
   ROLEDESC             varchar(150)         null,
   ROLESIGN             varchar(2)           null,
   SUBORDINATE          varchar(1)           null,
   constraint PK_TAROLE primary key nonclustered (ROLEID)
)
go

/*==============================================================*/
/* Table: TAROLEAUTHORITY                                       */
/*==============================================================*/
create table TAROLEAUTHORITY (
   ROLEID               varchar(36)          not null,
   RESOURCEID           varchar(36)          not null,
   RESOURCETYPE         varchar(1)           not null,
   USEPERMISSION        varchar(1)           null,
   REPERMISSION         varchar(1)           null,
   REAUTHRITY           varchar(1)           null,
   CREATEUSER           varchar(36)          null,
   CREATETIME           datetime             null,
   EFFECTTIME           datetime             null,
   constraint PK_TAROLEAUTHORITY primary key nonclustered (ROLEID, RESOURCEID, RESOURCETYPE)
)
go

/*==============================================================*/
/* Table: TAROLEURLAUTHORITY       角色Rest Url 授权             */
/*==============================================================*/
create table TAROLEURLAUTHORITY(
  roleid varchar(36) NOT NULL,
	resourceid varchar(36) NOT NULL,
	urlid varchar(36) NOT NULL,
	constraint PK_TAROLEURLAUTHORITY primary key nonclustered (roleid, urlid)

)
/*==============================================================*/
/* Table: TAROLEUSER                                            */
/*==============================================================*/
create table TAROLEUSER (
   USERID               varchar(36)          not null,
   ROLEID               varchar(36)          not null,
   DEFAULTROLE          varchar(1)           null,
   CREATEUSER           varchar(36)          not null,
   CREATETIME           datetime             not null,
   constraint PK_TAROLEUSER primary key nonclustered (ROLEID, USERID)
)
go

/*==============================================================*/
/* Table: TASERVEREXCEPTIONLOG                                  */
/*==============================================================*/
create table TASERVEREXCEPTIONLOG (
   LOGID                varchar(50)          not null,
   IPADDRESS            varchar(100)         null,
   PORT                 varchar(10)          null,
   EXCEPTIONNAME        varchar(255)         null,
   CONTENT              varbinary(max)            null,
   CREATETIME           datetime             null,
   SYSPATH              varchar(50)          null,
   CLIENTIP             varchar(50)          null,
   URL                  varchar(100)         null,
   MENUID               varchar(50)          null,
   MENUNAME             varchar(30)          null,
   USERAGENT            varchar(200)         null,
   EXCEPTIONTYPE        varchar(2)           null,
   REQUESTPARAMETER     varbinary(max)            null,
   constraint PK_TASERVEREXCEPTIONLOG primary key nonclustered (LOGID)
)
go

/*==============================================================*/
/* Table: TATAG                                                 */
/*==============================================================*/
create table TATAG (
   TAGID                varchar(32)          not null,
   TAGNAME              varchar(20)          null,
   TAGGROUPID           varchar(20)          null,
   CREATETIME           datetime             not null default GETDATE(),
   EFFECTIVE            varchar(20)          not null default '1',
   CREATEUSER           varchar(20)          not null default 'developer',
   DESTORY              varchar(20)          not null default '0',
   UPDATETIME           datetime             not null default GETDATE(),
   constraint PK_TATAG primary key nonclustered (TAGID)
)
go

/*==============================================================*/
/* Table: TATAGGROUP                                            */
/*==============================================================*/
create table TATAGGROUP (
   TAGGROUPID           varchar(20)          not null default '001',
   TAGGROUPNAME         varchar(20)          not null default '用户',
   EFFECTIVE            varchar(20)          not null default '1',
   CREATEUSER           varchar(20)          not null default 'developer',
   CREATETIME           datetime             not null default GETDATE(),
   UPDATETIME           datetime             not null default GETDATE(),
   DESTORY              varchar(20)          not null default '0'
)
go

/*==============================================================*/
/* Table: TATAGSSTORE                                           */
/*==============================================================*/
create table TATAGSSTORE (
   TAGID                varchar(36)          not null,
   TAGNAME              varchar(100)         not null,
   TAGCODE              varchar(30)          not null,
   TAGCATEGORY          varchar(2)           not null,
   constraint PK_TATAGSSTORE primary key nonclustered (TAGID)
)
go

/*==============================================================*/
/* Table: TAUSER                                                */
/*==============================================================*/
create table TAUSER (
   USERID               varchar(36)          not null,
   LOGINID              varchar(30)          not null,
   PASSWORD             varchar(100)         not null,
   PASSWORDDEFAULTNUM   numeric(16,0)        null,
   PWDLASTMODIFYDATE    datetime             null,
   ISLOCK               varchar(1)           null,
   ORDERNO              numeric(10,0)        null,
   NAME                 varchar(450)         not null,
   SEX                  varchar(2)           null,
   IDCARDTYPE           varchar(2)           null,
   IDCARDNO             varchar(30)          null,
   MOBILE               varchar(20)          null,
   CREATEUSER           varchar(36)          null,
   CREATETIME           datetime             null,
   MODIFYTIME           datetime             null,
   DESTORY              varchar(1)           null,
   ACCOUNTSOURCE        varchar(2)           null,
   EFFECTIVE            varchar(1)           null,
   EFFECTIVETIME        datetime             null,
   JOBNUMBER            varchar(15)          null,
   STATE                varchar(10)          null,
   BIRTHPLACE           varchar(12)          null,
   ADDRESS              varchar(450)         null,
   ZIPCODE              varchar(10)          null,
   EMAIL                varchar(100)         null,
   PHONE                varchar(20)          null,
   EDUCATION            varchar(30)          null,
   GRADUATESCHOOL       varchar(150)         null,
   WORKPLACE            varchar(300)         null,
   FIELD01              varchar(1000)        null,
   FIELD02              varchar(1000)        null,
   FIELD03              varchar(1000)        null,
   FIELD04              varchar(1000)        null,
   FIELD05              varchar(1000)        null,
   FIELD06              varchar(1000)        null,
   FIELD07              varchar(1000)        null,
   FIELD08              varchar(1000)        null,
   FIELD09              varchar(1000)        null,
   FIELD10              varchar(1000)        null,
   constraint PK_TAUSER primary key nonclustered (USERID)
)
go

/*==============================================================*/
/* Table: TAUSERORG                                             */
/*==============================================================*/
create table TAUSERORG (
   USERID               varchar(32)          not null,
   ORGID                varchar(32)          not null,
   ISDIRECT             varchar(1)           not null,
   constraint PK_TAUSERORG primary key nonclustered (USERID, ORGID)
)
go

/*==============================================================*/
/* View: v_dict                                                 */
/*==============================================================*/
create view v_dict as
select x0.name ,x0.type ,x0.label ,x0.value ,x0.parentvalue ,x0.sort ,x0.authority ,x0.cssclass ,x0.cssstyle ,x0.remarks ,x0.createdate ,x0.createuser ,x0.version ,x0.status ,x0.field01 ,x0.field02 ,x0.field03 ,x0.field04 ,x0.field05 ,x0.system ,x0.newtype from tadict x0 ;
go


-- 初始化人员表
INSERT INTO tauser (userid,loginid,password,passworddefaultnum,pwdlastmodifydate,islock,orderno,name,sex,idcardtype,idcardno,mobile,createuser,createtime,modifytime,destory,accountsource,effective,effectivetime,jobnumber,state,birthplace,address,zipcode,email,phone,education,graduateschool,workplace,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'1','developer','$2a$10$ZI3ewI1LvnS8bvTDiZk5X.w9/u.LXy8vPTwQFl8SlDtMTYBQHnIEm',0,GETDATE(),'0',1,'超级管理员','1','0',null,'18011567700','1',GETDATE(),NULL,'0',NULL,'1',NULL,NULL,NULL,NULL,'成都市/锦江区/久远银海',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化组织表
INSERT INTO taorg (orgid,orgname,spell,parentid,idpath,namepath,customno,orderno,orglevel,area,effective,orgtype,createuser,createtime,modifytime,orgmanager,orgcode,contacts,address,tel,destory,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'fd811ab9c30440088df3e29ea784460a','久远银海','JYYH','0','fd811ab9c30440088df3e29ea784460a','久远银海','',0,0,NULL,'1','01','1',NULL,GETDATE(),'','','','','','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化码表
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','个人角色','02',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','公有角色','01',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','代理角色','03',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','用户名密码','01',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','指纹识别','02',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','人脸识别','03',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'认证方式类型','AUTHMODE','Key盘','04',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','OFF','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','FATAL','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ERROR','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','WARN','3',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','INFO','4',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','DEBUG','5',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','TRACE','6',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ALL','7',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','通用菜单','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','管理菜单','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','经办菜单','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','组织','01',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','人员','02',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','岗位','03',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','权限','07',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增组织','01',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑组织','02',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用组织','03',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增人员','04',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑人员','05',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用人员','06',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','密码重置','07',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位给人员','08',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','设置主岗位给人员','09',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增岗位','10',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑岗位','11',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用岗位','12',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位使用权限','13',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位使用权限','14',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用岗位','15',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用人员','16',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用组织','17',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除组织','18',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人使用权限','19',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人使用权限','20',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人授权权限','21',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人授权权限','22',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除管理员','23',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增管理员','24',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除岗位','25',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除人员','26',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','机构','01',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','部门','02',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','组','03',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','无','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','男','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','女','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','是','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','否','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','不显示','0',NULL,31,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见不可编辑','1','',21,'0','','',NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见可编辑','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','有效','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','居民身份证(户口簿)','0','',10,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','111','11','',10,'0','','',NULL,GETDATE(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','无效','0',NULL,0,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','无限制','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','一级','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','二级','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','三级','3',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','四级','4',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','无需登录可访问','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','登陆均可访问','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','授权可访问','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','继承当前菜单权限','0',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','独立授权','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','未同步','0','',0,'0','','',NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','已同步','2','',2,'0','','',NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','脏数据','1','',1,'0','','',NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','123','111','',20,'0','','',NULL,GETDATE(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','护照','1','',20,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1','1','',1,'0','','',NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-2','12','',1,'0','','',NULL,GETDATE(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1-1U','123','1',1,'0','','',NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','组织账户','2',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','个人账户','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','a','2',NULL,3,'0',NULL,NULL,NULL,GETDATE(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','军官证','2','',30,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','其他','3','',40,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'test1','TEST1','a','1','',10,'0',NULL,NULL,NULL,GETDATE(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','稽核角色','2',NULL,2,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','业务角色','1',NULL,1,'0',NULL,NULL,NULL,GETDATE(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');

-- 初始化功能菜单
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'40337bdecb19484ebeb39d6c21aaca26','0','银海软件','','portal','sysmg.html#/resourceManagement',0,'40337bdecb19484ebeb39d6c21aaca26','银海软件','0','','','1',1,'0','1','','', GETDATE(),'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'7459c1b525934151a1d309a304933644','40337bdecb19484ebeb39d6c21aaca26','资源管理','','portal','',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644','银海软件/门户菜单/资源管理','2','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'ec56a0a43b09429482632cb61f7c6908','7459c1b525934151a1d309a304933644','功能资源管理','','portal','sysmg.html#/resourceManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/ec56a0a43b09429482632cb61f7c6908','银海软件/门户菜单/资源管理/功能资源管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'6b8c6d0a93844985894713e396e7a99f','7459c1b525934151a1d309a304933644','接入系统管理','','portal','sysmg.html#/accessSystem',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/6b8c6d0a93844985894713e396e7a99f','银海软件/门户菜单/资源管理/接入系统管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'3df588fc565d4287b3cefcd00a39cd91','7459c1b525934151a1d309a304933644','标签管理','','portal','sysmg.html#/tagManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/3df588fc565d4287b3cefcd00a39cd91','银海软件/门户菜单/资源管理/标签管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'c2745b7cae7846acb9bcf8d0f4e836e8','7459c1b525934151a1d309a304933644','授权对象管理','','portal','sysmg.html#/customResource',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/c2745b7cae7846acb9bcf8d0f4e836e8','银海软件/门户菜单/资源管理/授权对象管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'78ad02fdb879406ebc5e7a4faf8f5905','40337bdecb19484ebeb39d6c21aaca26','资源权限管理','','portal','',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905','银海软件/门户菜单/资源权限管理','2','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'95bb9b749bf54e4692b0b1f14fd1b5ab','78ad02fdb879406ebc5e7a4faf8f5905','角色权限管理','','portal','authority.html#/roleAuthorityManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/95bb9b749bf54e4692b0b1f14fd1b5ab','银海软件/门户菜单/资源权限管理/角色权限管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'8aa86ed4c7f84183935a262db4a605d3','78ad02fdb879406ebc5e7a4faf8f5905','管理员权限管理','','portal','authority.html#/adminAuthority',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/8aa86ed4c7f84183935a262db4a605d3','银海软件/门户菜单/资源权限管理/管理员权限管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'1d4e283ad5584e02811f6b188d3592bc','40337bdecb19484ebeb39d6c21aaca26','系统管理','','portal','',41,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc','银海软件/门户菜单/系统管理','2','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'7c1dabd160974d8f90858c187cefa128','1d4e283ad5584e02811f6b188d3592bc','日志动态配置','','portal','logmg.html#/logConfig',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c1dabd160974d8f90858c187cefa128','银海软件/门户菜单/系统管理/日志动态配置','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'770b2f1e77eb4d9092dc89743b5792c9','1d4e283ad5584e02811f6b188d3592bc','登录日志统计','','portal','logmg.html#/loginLogAnalysis',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/770b2f1e77eb4d9092dc89743b5792c9','银海软件/门户菜单/系统管理/登录日志统计','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'7c2862a57e34459484bd701c19a8de27','1d4e283ad5584e02811f6b188d3592bc','系统异常日志','','portal','logmg.html#/systemExceptionLog',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c2862a57e34459484bd701c19a8de27','银海软件/门户菜单/系统管理/系统异常日志','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'877e407281dd48acb05a77fcb922bc73','78ad02fdb879406ebc5e7a4faf8f5905','权限代理','','portal','authority.html#/authorityAgent',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/877e407281dd48acb05a77fcb922bc73','银海软件/门户菜单/资源权限管理/权限代理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'5e67c7acef914c349d8aff076921f6b5','78ad02fdb879406ebc5e7a4faf8f5905','相似权限管理','','portal','authority.html#/similarAuthority',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/5e67c7acef914c349d8aff076921f6b5','银海软件/门户菜单/资源权限管理/相似权限管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'48afedddc8f04c668b3c1572c30a7745','40337bdecb19484ebeb39d6c21aaca26','组织人员管理','','portal','',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745','银海软件/门户菜单/组织人员管理','2','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'1e706f26bc144c1da12022359c238053','48afedddc8f04c668b3c1572c30a7745','组织机构管理','','portal','orguser.html#/orgManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/1e706f26bc144c1da12022359c238053','银海软件/门户菜单/组织人员管理/组织机构管理','3','','','2',1,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'daceeff8a97b46cb9573b93ba3a5a792','48afedddc8f04c668b3c1572c30a7745','人员管理','','portal','orguser.html#/userManagement',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/daceeff8a97b46cb9573b93ba3a5a792','银海软件/门户菜单/组织人员管理/人员管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'3dbde33722154503a7d22ac60f6a0e4e','48afedddc8f04c668b3c1572c30a7745','自定义组织管理','','portal','orguser.html#/customOrgManagement',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/3dbde33722154503a7d22ac60f6a0e4e','银海软件/门户菜单/组织人员管理/自定义组织管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'4b2eee0d7ded4e8094d4acf439fd3a1c','48afedddc8f04c668b3c1572c30a7745','行政区划管理','','portal','orguser.html#/areaManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/4b2eee0d7ded4e8094d4acf439fd3a1c','银海软件/门户菜单/组织人员管理/行政区划管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'bd9d0bba145c458e841aa9da0aeeb1d8','7459c1b525934151a1d309a304933644','用户可管理字段配置','','portal','sysmg.html#/userInfoManagement',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/bd9d0bba145c458e841aa9da0aeeb1d8','银海软件/门户菜单/资源管理/用户可管理字段配置','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'ffa74f43e853441dac0ee90c787cb2e6','1d4e283ad5584e02811f6b188d3592bc','字典管理','','portal','sysmg.html#/dictionaryManager',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/ffa74f43e853441dac0ee90c787cb2e6','银海软件/门户菜单/系统管理/字典管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'c88413826abd4a5b88612a6442e6db07','1d4e283ad5584e02811f6b188d3592bc','操作日志',NULL,'portal','logmg.html#/operationLog',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/c88413826abd4a5b88612a6442e6db07','银海软件/门户菜单/系统管理/操作日志','3',NULL,NULL,'2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'e4a8cc2bcd7d4e8ebdb3994f4caed572','7459c1b525934151a1d309a304933644','工作台模块管理','','ggyww','sysmg.html#/defaultTemplate',50,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/e4a8cc2bcd7d4e8ebdb3994f4caed572','银海软件/门户菜单/资源管理/工作台模块管理','3','','','2',0,'1','1',NULL,NULL, GETDATE(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');


-- 初始化角色
INSERT INTO tarole (roleid,rolename,isadmin,rolelevel,orgid,roletype,effective,effectivetime,createuser,createtime,roledesc,rolesign,subordinate) VALUES (
'1','超级管理员',NULL,NULL,'fd811ab9c30440088df3e29ea784460a','04','1',NULL,'1',GETDATE(),'超级管理员角色',NULL,NULL);

-- 初始化用户组织表
INSERT INTO tauserorg (userid,orgid,isdirect) VALUES (
'1','fd811ab9c30440088df3e29ea784460a','1');

--初始化接入系统
INSERT INTO taaccesssystem (id,syscode,sysname,spell,protocol,domain,port,contextpath,portalsystem,menuservice,provider,effective,regtime,modifytime,backgroundaddress) VALUES (
'252317feb5cf4d04924c4e6bea09be60','ggyww','公共业务系统','GGYWXT','http','localhost','8082','ggyww','1','111','11','1',GETDATE(),GETDATE(),'192.168.0.0:8081');
INSERT INTO taaccesssystem (id,syscode,sysname,spell,protocol,domain,port,contextpath,portalsystem,menuservice,provider,effective,regtime,modifytime,backgroundaddress) VALUES (
'e55b9b7046434d66bc11b94d553fada2','portal','门户系统','MHXT','http','localhost','8082','portal02','1','111','ss','1',GETDATE(),GETDATE(),'192.168.0.1:8083');

--初始化标签
INSERT INTO tataggroup (taggroupid,taggroupname,effective,createuser,createtime,updatetime,destory) VALUES (
'001','用户','1','developer',GETDATE(),GETDATE(),'0');
INSERT INTO tataggroup (taggroupid,taggroupname,effective,createuser,createtime,updatetime,destory) VALUES (
'002','组织','1','developer',GETDATE(),GETDATE(),'0');

--初始化 Resource Url 表
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('ec56a0a43b09429482632cb61f7c6908', 'org/sysmg/resourceManagementRestService/**', '所有', '0', '821a8ca012154ba2a4c451918c99cfc0');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('6b8c6d0a93844985894713e396e7a99f', 'org/sysmg/accessSystemManagementRestService/**', '所有', '0', 'e2934e28ea9f4ed1979ad59e611dde00');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('3df588fc565d4287b3cefcd00a39cd91', 'org/sysmg/tagManagementRestService/**', '所有', '0', '885e01d3494b44759c62b8b3568e6145');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('95bb9b749bf54e4692b0b1f14fd1b5ab', 'org/authority/roleAuthorityManagementRestService/**', '所有', '0', 'fc45635549b5487c87d0287490a79b5f');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('8aa86ed4c7f84183935a262db4a605d3', 'org/authority/adminAuthorityManagementRestService/**', '所有', '0', 'cbcdd31ffb6c40499bb1af744851a783');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('877e407281dd48acb05a77fcb922bc73', 'org/authority/authorityAgentRestService/**', '所有', '0', 'c4360fdef9ad46278a677508ee8b290a');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('5e67c7acef914c349d8aff076921f6b5', 'org/authority/similarAuthorityManagementRestService/**', '所有', '0', '2c3c1591444842e1aa1588a0aab998a8');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('1e706f26bc144c1da12022359c238053', 'org/orguser/orgManagementRestService/**', '所有', '0', 'aa60a746c0a24640a06dea49a4d42572');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('daceeff8a97b46cb9573b93ba3a5a792', 'org/orguser/userManagementRestService/**', '所有', '0', 'b32b96d0142d4fea8d5fd8a58011c0c2');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('3dbde33722154503a7d22ac60f6a0e4e', 'org/orguser/customOrgManagementRestService/**', '所有', '0', 'b091f4af3eba4c5c946af8dacaf951b9');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('4b2eee0d7ded4e8094d4acf439fd3a1c', 'org/orguser/areaManagementRestService/**', '所有', '0', '7859157a0cd94c89995b0dde9fc40bc6');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('bd9d0bba145c458e841aa9da0aeeb1d8', 'org/sysmg/manageableFieldsOfUserInfoManagementRestService/**', '所有', '0', '017ebd5a5ab24128ad1122dbc2bdad72');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('c2745b7cae7846acb9bcf8d0f4e836e8', 'org/sysmg/resourceCategoryManagementRestService/**', '所有', '0', '7f621426782342faa371319c677f066e');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('ffa74f43e853441dac0ee90c787cb2e6', 'dictmg/dictMgRestService/**', '所有', '0', 'b88be2871149451a918264fd1ff468a3');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('770b2f1e77eb4d9092dc89743b5792c9', 'logmg/loginLog/loginLogAnalysisRestService/**', '所有', '0', '12cf0f814c4d41aa8b36ebbe5c9f14cb');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('7c2862a57e34459484bd701c19a8de27', 'logmg/exceptionlog/serverExceptionLogRestService/**', '所有', '0', '0b1ccf818760461ca7bafb439f62b711');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('7c1dabd160974d8f90858c187cefa128', 'logmg/logconfig/logConfigRestService', '所有', '0', 'cc101f8405664990b7c5e489d4a2785e');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('c88413826abd4a5b88612a6442e6db07', 'org/sysmg/orgOpLogRestService/**', '所有', '0', '00840362514a445f8188415fbc52e056');


