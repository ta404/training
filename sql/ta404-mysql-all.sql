DROP VIEW
IF
	EXISTS V_DICT;
DROP TABLE
IF
	EXISTS tauserworkbench;
DROP TABLE
IF
	EXISTS taroleworkbench;
DROP TABLE
IF
	EXISTS taroleurlauthority;
DROP TABLE
IF
	EXISTS taresourceurl;
DROP TABLE
IF
	EXISTS TAACCESSLOG;
DROP TABLE
IF
	EXISTS TAACCESSSYSTEM;
DROP TABLE
IF
	EXISTS TAAREA;
DROP TABLE
IF
	EXISTS TAAVATAR;
DROP TABLE
IF
	EXISTS TACONFIG;
DROP TABLE
IF
	EXISTS TACUSTOMORG;
DROP TABLE
IF
	EXISTS TACUSTOMORGTYPENAME;
DROP TABLE
IF
	EXISTS TACUSTOMORGUSER;
DROP TABLE
IF
	EXISTS TACUSTOMRESOURCE;
DROP TABLE
IF
	EXISTS TADICT;
DROP TABLE
IF
	EXISTS TAEXTENDSETTING;
DROP TABLE
IF
	EXISTS TALOGINHISTORYLOG;
DROP TABLE
IF
	EXISTS TALOGINSTATLOG;
DROP TABLE
IF
	EXISTS TAOBJECTTAGS;
DROP TABLE
IF
	EXISTS TAONLINELOG;
DROP TABLE
IF
	EXISTS TAONLINESTATLOG;
DROP TABLE
IF
	EXISTS TAORG;
DROP TABLE
IF
	EXISTS TAORGCONSTRAINT;
DROP TABLE
IF
	EXISTS TAORGMG;
DROP TABLE
IF
	EXISTS TAORGOPLOG;
DROP TABLE
IF
	EXISTS TARESOURCE;
DROP TABLE
IF
	EXISTS TARESOURCECATEGORY;
DROP TABLE
IF
	EXISTS TARESOURCEUI;
DROP TABLE
IF
	EXISTS TAROLE;
DROP TABLE
IF
	EXISTS TAROLEAUTHORITY;
DROP TABLE
IF
	EXISTS TAROLEUSER;
DROP TABLE
IF
	EXISTS TASERVEREXCEPTIONLOG;
DROP TABLE
IF
	EXISTS TATAG;
DROP TABLE
IF
	EXISTS TATAGGROUP;
DROP TABLE
IF
	EXISTS TATAGSSTORE;
DROP TABLE
IF
	EXISTS TAUSER;
DROP TABLE
IF
	EXISTS TAUSERORG;
	
CREATE TABLE  taaccesslog (
 logid VARCHAR(36) NOT NULL ,
 userid VARCHAR(36) NOT NULL,
 roleid VARCHAR(36) NOT NULL,
 menuid VARCHAR(36) NOT NULL,
 ispermission CHAR(1) NOT NULL,
 accesstime DATE NOT NULL,
 url VARCHAR(1024),
 sysflag VARCHAR(50),
 field01 VARCHAR(10),
 field02 VARCHAR(10),
 field03 VARCHAR(10),
 field04 VARCHAR(10),
 field05 VARCHAR(10),
 constraint PK_TAACCESSLOG primary key (logid)
);
CREATE TABLE  taaccesssystem (
 id VARCHAR(36) NOT NULL,
 syscode VARCHAR(20) NOT NULL,
 sysname VARCHAR(100) NOT NULL,
 spell VARCHAR(30),
 protocol VARCHAR(10),
 domain VARCHAR(100),
 port VARCHAR(5),
 contextpath VARCHAR(30),
 portalsystem VARCHAR(1),
 menuservice VARCHAR(200),
 provider VARCHAR(10),
 effective VARCHAR(1),
 regtime DATE ,
 modifytime DATE ,
 backgroundaddress VARCHAR(100),
 constraint PK_taaccesssystem primary key (id)
);

CREATE TABLE  taarea (
 areaid VARCHAR(36) NOT NULL,
 parentid VARCHAR(36) NOT NULL,
 areaname VARCHAR(100) NOT NULL,
 areacode VARCHAR(20) NOT NULL,
 idpath VARCHAR(300) NOT NULL,
 namepath VARCHAR(300) NOT NULL,
 arealevel DECIMAL(2,0),
 spell VARCHAR(100),
 createuser VARCHAR(36) NOT NULL,
 areaorder DECIMAL(20,0),
 effective VARCHAR(1) NOT NULL,
 createtime DATE  NOT NULL,
 modifytime DATE  NOT NULL,
 destory VARCHAR(1),
 constraint PK_taarea primary key (areaid)
);

CREATE TABLE  taavatar (
 userid VARCHAR(36) NOT NULL,
 avatar BLOB,
 updatetime DATE  NOT NULL,
 constraint PK_taavatar primary key (userid,updatetime)
);


CREATE TABLE  taconfig (
 configid VARCHAR(36) NOT NULL,
 configkey VARCHAR(30) NOT NULL,
 configvalue VARCHAR(1000) NOT NULL,
 configname VARCHAR(150),
 configdesc VARCHAR(1000),
 constraint PK_taconfig primary key (configid)
);

CREATE TABLE  tacustomorg (
 customorgid VARCHAR(36) NOT NULL,
 parentid VARCHAR(36),
 customcode VARCHAR(100),
 customorgtypenameid VARCHAR(36) NOT NULL,
 effective VARCHAR(1) DEFAULT '1' NOT NULL,
 orderno DECIMAL(10,0),
 customorgname VARCHAR(128),
 customorgpathid VARCHAR(128),
 customorgpath VARCHAR(256),
 createuser VARCHAR(20),
 createtime DATE   NOT NULL,
 updatetime DATE   NOT NULL,
 destory VARCHAR(20) DEFAULT '0' NOT NULL,
 spell VARCHAR(20),
 orgmanager VARCHAR(36),
 orgcode VARCHAR(18),
 contacts VARCHAR(36),
 address VARCHAR(450),
 tel VARCHAR(20),
 field01 VARCHAR(1000),
 field02 VARCHAR(1000),
 field03 VARCHAR(1000),
 field04 VARCHAR(1000),
 field05 VARCHAR(1000),
 field06 VARCHAR(1000),
 field07 VARCHAR(1000),
 field08 VARCHAR(1000),
 field09 VARCHAR(1000),
 field10 VARCHAR(1000),
 constraint PK_tacustomorg primary key (customorgid)
);

CREATE TABLE  tacustomorgtypename (
 customorgtypenameid VARCHAR(32),
 customorgtypename VARCHAR(20),
 effective VARCHAR(20) DEFAULT '1' NOT NULL ,
 createuser VARCHAR(20),
 createtime DATE   NOT NULL ,
 updatetime DATE    NOT NULL ,
 destory VARCHAR(20) DEFAULT '0' NOT NULL ,
 customorgtypenamecode VARCHAR(20),
 customorgtypenamedesc VARCHAR(20),
 constraint PK_tacustomorgtypename primary key (customorgtypenameid)
);

CREATE TABLE  tacustomorguser (
 customorgid VARCHAR(36) NOT NULL,
 userid VARCHAR(36) NOT NULL,
 constraint PK_tacustomorguser primary key (customorgid,userid)
);

CREATE TABLE  tacustomresource (
 customresourceid VARCHAR(36) NOT NULL,
 resourcename VARCHAR(450) NOT NULL,
 parentid VARCHAR(32) NOT NULL,
 code VARCHAR(100),
 resourcecontent VARCHAR(1024),
 resourcecategory VARCHAR(32),
 effective VARCHAR(1),
 addtime DATE ,
 modifytime DATE ,
 system VARCHAR(36),
 constraint PK_tacustomresource primary key (customresourceid)
);

CREATE TABLE  tadict (
 name VARCHAR(50) NOT NULL,
 type VARCHAR(50) NOT NULL,
 label VARCHAR(50) NOT NULL,
 value VARCHAR(6) NOT NULL,
 parentvalue VARCHAR(6),
 sort DECIMAL(4,0) NOT NULL,
 authority VARCHAR(20) NOT NULL,
 cssclass VARCHAR(20),
 cssstyle VARCHAR(128),
 remarks VARCHAR(256),
 createdate DATE  NOT NULL,
 createuser VARCHAR(36) NOT NULL,
 version VARCHAR(10) NOT NULL,
 status VARCHAR(2) NOT NULL,
 field01 VARCHAR(10),
 field02 VARCHAR(10),
 field03 VARCHAR(10),
 field04 VARCHAR(10),
 field05 VARCHAR(10),
 system VARCHAR(2) NOT NULL,
 newtype VARCHAR(2) NOT NULL,
 constraint PK_tadict primary key (type,value)
);

CREATE TABLE  taextendsetting (
 fieldid VARCHAR(10) NOT NULL,
 type VARCHAR(2) NOT NULL,
 effective VARCHAR(1),
 hide VARCHAR(1),
 displaytext VARCHAR(150),
 titext VARCHAR(1500),
 orderno DECIMAL(10,0),
 required VARCHAR(1),
 unchangeable VARCHAR(1),
 formtype VARCHAR(20),
 contentsize DECIMAL(16,0),
 validreg VARCHAR(1000),
 connectaa10 VARCHAR(30),
 protectprivacy VARCHAR(1),
 constraint PK_taextendsetting primary key (fieldid,type)
);

CREATE TABLE  taloginhistorylog (
 logid VARCHAR(36) NOT NULL,
 userid VARCHAR(36) NOT NULL,
 logintime DATE  NOT NULL,
 logouttime DATE  NOT NULL,
 clientip VARCHAR(200) NOT NULL,
 sessionid VARCHAR(200) NOT NULL,
 serverip VARCHAR(200),
 syspath VARCHAR(50),
 clientsystem VARCHAR(50),
 clientbrowser VARCHAR(50),
 clientscreensize VARCHAR(50),
 constraint PK_taloginhistorylog primary key (logid)
);

CREATE TABLE  taloginstatlog (
 statdate VARCHAR(20) NOT NULL,
 pointintime VARCHAR(20) NOT NULL,
 loginnum DECIMAL(15,0),
 constraint PK_taloginstatlog primary key (statdate,pointintime)
);

CREATE TABLE  taobjecttags (
 objectid VARCHAR(36) NOT NULL,
 objecttype VARCHAR(3) NOT NULL,
 tagid VARCHAR(36) NOT NULL,
 constraint PK_taobjecttags primary key (objectid,objecttype,tagid)
);

CREATE TABLE  taonlinelog (
 logid VARCHAR(36) NOT NULL,
 userid VARCHAR(36) NOT NULL,
 logintime DATE  NOT NULL,
 curresource VARCHAR(1000),
 clientip VARCHAR(200) NOT NULL,
 sessionid VARCHAR(200) NOT NULL,
 syspath VARCHAR(50),
 serverip VARCHAR(200),
 clientsystem VARCHAR(50),
 clientbrowser VARCHAR(50),
 clientscreensize VARCHAR(50),
 constraint PK_taonlinelog primary key (logid)
);

CREATE TABLE  taonlinestatlog (
 statdate VARCHAR(20) NOT NULL,
 pointintime VARCHAR(20) NOT NULL,
 loginnum DECIMAL(15,0),
 constraint PK_taonlinestatlog primary key (statdate,pointintime)
);

CREATE TABLE  taorg (
 orgid VARCHAR(36) NOT NULL,
 orgname VARCHAR(300) NOT NULL,
 spell VARCHAR(100) NOT NULL,
 parentid VARCHAR(36) NOT NULL,
 idpath VARCHAR(1024) NOT NULL,
 namepath VARCHAR(1024) NOT NULL,
 customno VARCHAR(30),
 orderno DECIMAL(10,0) NOT NULL,
 orglevel DECIMAL(16,0),
 area VARCHAR(36),
 effective VARCHAR(1),
 orgtype VARCHAR(2) NOT NULL,
 createuser VARCHAR(36) NOT NULL,
 createtime DATE ,
 modifytime DATE ,
 orgmanager VARCHAR(36),
 orgcode VARCHAR(18),
 contacts VARCHAR(36),
 address VARCHAR(450),
 tel VARCHAR(20),
 destory VARCHAR(1),
 field01 VARCHAR(1000),
 field02 VARCHAR(1000),
 field03 VARCHAR(1000),
 field04 VARCHAR(1000),
 field05 VARCHAR(1000),
 field06 VARCHAR(1000),
 field07 VARCHAR(1000),
 field08 VARCHAR(1000),
 field09 VARCHAR(1000),
 field10 VARCHAR(1000),
 constraint PK_taorg primary key (orgid)
);

CREATE TABLE  taorgconstraint (
 categoryid VARCHAR(2) NOT NULL,
 allowcategoryid VARCHAR(2) NOT NULL,
 constraint PK_taorgconstraint primary key (categoryid,allowcategoryid)
);

CREATE TABLE  taorgmg (
 roleid VARCHAR(36) NOT NULL,
 orgid VARCHAR(36) NOT NULL,
 effecttime DATE ,
 constraint PK_taorgmg primary key (roleid,orgid)
);

CREATE TABLE  taorgoplog (
 logid VARCHAR(36) NOT NULL,
 batchno VARCHAR(36) NOT NULL,
 optype VARCHAR(2) NOT NULL,
 influencebodytype VARCHAR(2) NOT NULL,
 influencebody VARCHAR(36) NOT NULL,
 opbodytype VARCHAR(2),
 opsubject VARCHAR(36),
 changecontent VARCHAR(2048),
 optime DATE  NOT NULL,
 opuser VARCHAR(36) NOT NULL,
 ispermission VARCHAR(1) NOT NULL,
 constraint PK_taorgoplog primary key (logid)
);

CREATE TABLE  taresource (
 resourceid VARCHAR(36) NOT NULL,
 presourceid VARCHAR(36) NOT NULL,
 name VARCHAR(100) NOT NULL,
 code VARCHAR(30),
 syscode VARCHAR(20) NOT NULL,
 url VARCHAR(100),
 orderno DECIMAL(10,0),
 idpath VARCHAR(1024) NOT NULL,
 namepath VARCHAR(1024) NOT NULL,
 resourcelevel VARCHAR(2) NOT NULL,
 icon VARCHAR(30),
 iconcolor VARCHAR(7),
 securitypolicy VARCHAR(2) NOT NULL,
 securitylevel DECIMAL(4,0) NOT NULL,
 resourcetype VARCHAR(2) NOT NULL,
 effective VARCHAR(1) NOT NULL,
 isdisplay VARCHAR(1),
 isfiledscontrol VARCHAR(1),
 createdate DATE  NOT NULL,
 createuser VARCHAR(36) NOT NULL,
 uiauthoritypolicy VARCHAR(2),
 field01 VARCHAR(10),
 field02 VARCHAR(10),
 field03 VARCHAR(10),
 field04 VARCHAR(10),
 field05 VARCHAR(10),
 field06 VARCHAR(10),
 field07 VARCHAR(10),
 field08 VARCHAR(10),
 field09 VARCHAR(10),
 field10 VARCHAR(10),
 workbench VARCHAR(1),
 constraint PK_taresource primary key (resourceid)
);

CREATE TABLE  taresourcecategory (
 categoryid VARCHAR(36) NOT NULL,
 categoryname VARCHAR(300) NOT NULL,
 effective VARCHAR(1),
 code VARCHAR(100),
 categorycontent VARCHAR(1024),
 constraint PK_taresourcecategory primary key (categoryid)
);

CREATE TABLE  taresourceui (
 pageelementid VARCHAR(36) NOT NULL,
 resourceid VARCHAR(36) NOT NULL,
 elenmentname VARCHAR(20) NOT NULL,
 elementid VARCHAR(30) NOT NULL,
 code VARCHAR(20),
 authoritypolicy VARCHAR(2) NOT NULL,
 createuser VARCHAR(36) NOT NULL,
 createdate DATE  NOT NULL,
 field01 VARCHAR(10),
 field02 VARCHAR(10),
 field03 VARCHAR(10),
 effective VARCHAR(2) NOT NULL,
 constraint PK_taresourceui primary key (pageelementid)
);

CREATE TABLE  tarole (
 roleid VARCHAR(36) NOT NULL,
 rolename VARCHAR(150) NOT NULL,
 isadmin VARCHAR(1),
 rolelevel DECIMAL(32,0),
 orgid VARCHAR(36) NOT NULL,
 roletype VARCHAR(2) NOT NULL,
 effective VARCHAR(1) NOT NULL,
 effectivetime DATE ,
 createuser VARCHAR(36),
 createtime DATE ,
 roledesc VARCHAR(150),
 rolesign VARCHAR(2),
 subordinate VARCHAR(1),
 constraint PK_tarole primary key (roleid)
);

CREATE TABLE  taroleauthority (
 roleid VARCHAR(36) NOT NULL,
 resourceid VARCHAR(36) NOT NULL,
 resourcetype VARCHAR(1) NOT NULL,
 usepermission VARCHAR(1),
 repermission VARCHAR(1),
 reauthrity VARCHAR(1),
 createuser VARCHAR(36),
 createtime DATE ,
 effecttime DATE ,
 constraint PK_taroleauthority primary key (roleid,resourceid,resourcetype)
);

CREATE TABLE  taroleuser (
 userid VARCHAR(36) NOT NULL,
 roleid VARCHAR(36) NOT NULL,
 defaultrole VARCHAR(1),
 createuser VARCHAR(36) NOT NULL,
 createtime DATE  NOT NULL,
 constraint PK_taroleuser primary key (roleid,userid)
);

CREATE TABLE  taserverexceptionlog (
 logid VARCHAR(50) NOT NULL,
 ipaddress VARCHAR(100),
 port VARCHAR(10),
 exceptionname VARCHAR(255),
 content BLOB,
 createtime DATE ,
 syspath VARCHAR(50),
 clientip VARCHAR(50),
 url VARCHAR(100),
 menuid VARCHAR(50),
 menuname VARCHAR(30),
 useragent VARCHAR(200),
 exceptiontype VARCHAR(2),
 requestparameter BLOB,
 constraint PK_taserverexceptionlog primary key (logid)
);

CREATE TABLE  tatag (
 tagid VARCHAR(32) NOT NULL,
 tagname VARCHAR(20),
 taggroupid VARCHAR(20),
 createtime DATE   NOT NULL ,
 effective VARCHAR(20)  DEFAULT '1' NOT NULL ,
 createuser VARCHAR(20) DEFAULT 'developer' NOT NULL ,
 destory VARCHAR(20) DEFAULT '0' NOT NULL ,
 updatetime DATE   NOT NULL,
 constraint PK_tatag primary key (tagid)
);

CREATE TABLE  tataggroup (
 taggroupid VARCHAR(20) DEFAULT '001' NOT NULL ,
 taggroupname VARCHAR(20) DEFAULT '用户' NOT NULL ,
 effective VARCHAR(20) DEFAULT '1' NOT NULL ,
 createuser VARCHAR(20) DEFAULT 'developer' NOT NULL ,
 createtime DATE   NOT NULL ,
 updatetime DATE   NOT NULL ,
 destory VARCHAR(20) DEFAULT '0' NOT NULL
);

CREATE TABLE  tatagsstore (
 tagid VARCHAR(36) NOT NULL,
 tagname VARCHAR(100) NOT NULL,
 tagcode VARCHAR(30) NOT NULL,
 tagcategory VARCHAR(2) NOT NULL,
 constraint PK_tatagsstore primary key (tagid)
);

CREATE TABLE  tauser (
 userid VARCHAR(36) NOT NULL,
 loginid VARCHAR(30) NOT NULL,
 password VARCHAR(100) NOT NULL,
 passworddefaultnum DECIMAL(16,0),
 pwdlastmodifydate DATE ,
 islock VARCHAR(1),
 orderno DECIMAL(10,0),
 name VARCHAR(450) NOT NULL,
 sex VARCHAR(2),
 idcardtype VARCHAR(2),
 idcardno VARCHAR(30),
 mobile VARCHAR(20),
 createuser VARCHAR(36),
 createtime DATE ,
 modifytime DATE ,
 destory VARCHAR(1),
 accountsource VARCHAR(2),
 effective VARCHAR(1),
 effectivetime DATE ,
 jobnumber VARCHAR(15),
 state VARCHAR(10),
 birthplace VARCHAR(12),
 address VARCHAR(450),
 zipcode VARCHAR(10),
 email VARCHAR(100),
 phone VARCHAR(20),
 education VARCHAR(30),
 graduateschool VARCHAR(150),
 workplace VARCHAR(300),
 field01 VARCHAR(1000),
 field02 VARCHAR(1000),
 field03 VARCHAR(1000),
 field04 VARCHAR(1000),
 field05 VARCHAR(1000),
 field06 VARCHAR(1000),
 field07 VARCHAR(1000),
 field08 VARCHAR(1000),
 field09 VARCHAR(1000),
 field10 VARCHAR(1000),
 constraint PK_tauser primary key (userid)
);

CREATE TABLE  tauserorg (
 userid VARCHAR(32) NOT NULL,
 orgid VARCHAR(32) NOT NULL,
 isdirect VARCHAR(1) NOT NULL,
 constraint PK_tauserorg primary key (userid,orgid)
);


CREATE TABLE  taresourceurl (
 resourceid VARCHAR(36) NOT NULL,
 resturl VARCHAR(256) NOT NULL,
 urlname VARCHAR(40) ,
  authoritypolicy VARCHAR(1) ,
  urlid VARCHAR(36) ,
 constraint PK_taresourceurl primary key (urlid)
);

CREATE TABLE  taroleurlauthority (
 roleid VARCHAR(36) NOT NULL,
 resourceid VARCHAR(36) NOT NULL,
 urlid VARCHAR(36)  NOT NULL,
 constraint PK_taroleurlauthority primary key (roleid,urlid)
);
CREATE TABLE  taroleworkbench (
 roleid VARCHAR(36) NOT NULL,
 workbenchjson BLOB  ,
 constraint PK_taroleworkbench primary key (roleid)
);

CREATE TABLE  tauserworkbench (
  userid VARCHAR(36) NOT NULL,
 workbenchjson BLOB  ,
 constraint PK_tauserworkbench primary key (userid)

);

create OR REPLACE view v_dict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) as select x0.name ,x0.type ,x0.label ,x0.value ,x0.parentvalue ,x0.sort ,x0.authority ,x0.cssclass ,x0.cssstyle ,x0.remarks ,x0.createdate ,x0.createuser ,x0.version ,x0.status ,x0.field01 ,x0.field02 ,x0.field03 ,x0.field04 ,x0.field05 ,x0.system ,x0.newtype from tadict x0 ;


-- 初始化人员表
INSERT INTO tauser (userid,loginid,password,passworddefaultnum,pwdlastmodifydate,islock,orderno,name,sex,idcardtype,idcardno,mobile,createuser,createtime,modifytime,destory,accountsource,effective,effectivetime,jobnumber,state,birthplace,address,zipcode,email,phone,education,graduateschool,workplace,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'1','developer','$2a$10$ZI3ewI1LvnS8bvTDiZk5X.w9/u.LXy8vPTwQFl8SlDtMTYBQHnIEm',0, sysdate(),'0',1,'超级管理员','1','0',null,'18011567700','1', sysdate(),NULL,'0',NULL,'1',NULL,NULL,NULL,NULL,'成都市/锦江区/久远银海',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化组织表
INSERT INTO taorg (orgid,orgname,spell,parentid,idpath,namepath,customno,orderno,orglevel,area,effective,orgtype,createuser,createtime,modifytime,orgmanager,orgcode,contacts,address,tel,destory,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'fd811ab9c30440088df3e29ea784460a','久远银海','JYYH','0','fd811ab9c30440088df3e29ea784460a','久远银海','',0,0,NULL,'1','01','1',NULL, sysdate(),'','','','','','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化码表
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','个人角色','02',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','公有角色','01',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','代理角色','03',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','用户名密码','01',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','指纹识别','02',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','人脸识别','03',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'认证方式类型','AUTHMODE','Key盘','04',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','OFF','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','FATAL','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ERROR','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','WARN','3',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','INFO','4',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','DEBUG','5',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','TRACE','6',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ALL','7',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','通用菜单','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','管理菜单','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','经办菜单','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','组织','01',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','人员','02',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','岗位','03',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','权限','07',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增组织','01',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑组织','02',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用组织','03',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增人员','04',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑人员','05',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用人员','06',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','密码重置','07',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位给人员','08',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','设置主岗位给人员','09',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增岗位','10',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑岗位','11',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用岗位','12',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位使用权限','13',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位使用权限','14',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用岗位','15',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用人员','16',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用组织','17',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除组织','18',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人使用权限','19',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人使用权限','20',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人授权权限','21',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人授权权限','22',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除管理员','23',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增管理员','24',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除岗位','25',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除人员','26',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','机构','01',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','部门','02',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','组','03',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','无','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','男','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','女','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','是','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','否','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','不显示','0',NULL,31,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见不可编辑','1','',21,'0','','',NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见可编辑','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','有效','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','居民身份证(户口簿)','0','',10,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','111','11','',10,'0','','',NULL, sysdate(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','无效','0',NULL,0,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','无限制','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','一级','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','二级','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','三级','3',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','四级','4',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','无需登录可访问','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','登陆均可访问','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','授权可访问','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','继承当前菜单权限','0',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','独立授权','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','未同步','0','',0,'0','','',NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','已同步','2','',2,'0','','',NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','脏数据','1','',1,'0','','',NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','123','111','',20,'0','','',NULL, sysdate(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','护照','1','',20,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1','1','',1,'0','','',NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-2','12','',1,'0','','',NULL, sysdate(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1-1U','123','1',1,'0','','',NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','组织账户','2',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','个人账户','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','a','2',NULL,3,'0',NULL,NULL,NULL, sysdate(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','军官证','2','',30,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','其他','3','',40,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'test1','TEST1','a','1','',10,'0',NULL,NULL,NULL, sysdate(),'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','稽核角色','2',NULL,2,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','业务角色','1',NULL,1,'0',NULL,NULL,NULL, sysdate(),'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');

-- 初始化功能菜单
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'40337bdecb19484ebeb39d6c21aaca26','0','银海软件','','portal','sysmg.html#/resourceManagement',0,'40337bdecb19484ebeb39d6c21aaca26','银海软件','0','','','1',1,'0','1','','',sysdate(),'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'7459c1b525934151a1d309a304933644','40337bdecb19484ebeb39d6c21aaca26','资源管理','','portal','',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644','银海软件/门户菜单/资源管理','2','','','2',0,'1','1',NULL,NULL,sysdate(),'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'ec56a0a43b09429482632cb61f7c6908','7459c1b525934151a1d309a304933644','功能资源管理','','portal','sysmg.html#/resourceManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/ec56a0a43b09429482632cb61f7c6908','银海软件/门户菜单/资源管理/功能资源管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'6b8c6d0a93844985894713e396e7a99f','7459c1b525934151a1d309a304933644','接入系统管理','','portal','sysmg.html#/accessSystem',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/6b8c6d0a93844985894713e396e7a99f','银海软件/门户菜单/资源管理/接入系统管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'3df588fc565d4287b3cefcd00a39cd91','7459c1b525934151a1d309a304933644','标签管理','','portal','sysmg.html#/tagManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/3df588fc565d4287b3cefcd00a39cd91','银海软件/门户菜单/资源管理/标签管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'c2745b7cae7846acb9bcf8d0f4e836e8','7459c1b525934151a1d309a304933644','授权对象管理','','portal','sysmg.html#/customResource',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/c2745b7cae7846acb9bcf8d0f4e836e8','银海软件/门户菜单/资源管理/授权对象管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'78ad02fdb879406ebc5e7a4faf8f5905','40337bdecb19484ebeb39d6c21aaca26','资源权限管理','','portal','',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905','银海软件/门户菜单/资源权限管理','2','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'95bb9b749bf54e4692b0b1f14fd1b5ab','78ad02fdb879406ebc5e7a4faf8f5905','角色权限管理','','portal','authority.html#/roleAuthorityManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/95bb9b749bf54e4692b0b1f14fd1b5ab','银海软件/门户菜单/资源权限管理/角色权限管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'8aa86ed4c7f84183935a262db4a605d3','78ad02fdb879406ebc5e7a4faf8f5905','管理员权限管理','','portal','authority.html#/adminAuthority',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/8aa86ed4c7f84183935a262db4a605d3','银海软件/门户菜单/资源权限管理/管理员权限管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'1d4e283ad5584e02811f6b188d3592bc','40337bdecb19484ebeb39d6c21aaca26','系统管理','','portal','',41,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc','银海软件/门户菜单/系统管理','2','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'7c1dabd160974d8f90858c187cefa128','1d4e283ad5584e02811f6b188d3592bc','日志动态配置','','portal','logmg.html#/logConfig',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c1dabd160974d8f90858c187cefa128','银海软件/门户菜单/系统管理/日志动态配置','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'770b2f1e77eb4d9092dc89743b5792c9','1d4e283ad5584e02811f6b188d3592bc','登录日志统计','','portal','logmg.html#/loginLogAnalysis',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/770b2f1e77eb4d9092dc89743b5792c9','银海软件/门户菜单/系统管理/登录日志统计','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'7c2862a57e34459484bd701c19a8de27','1d4e283ad5584e02811f6b188d3592bc','系统异常日志','','portal','logmg.html#/systemExceptionLog',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c2862a57e34459484bd701c19a8de27','银海软件/门户菜单/系统管理/系统异常日志','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'877e407281dd48acb05a77fcb922bc73','78ad02fdb879406ebc5e7a4faf8f5905','权限代理','','portal','authority.html#/authorityAgent',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/877e407281dd48acb05a77fcb922bc73','银海软件/门户菜单/资源权限管理/权限代理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'5e67c7acef914c349d8aff076921f6b5','78ad02fdb879406ebc5e7a4faf8f5905','相似权限管理','','portal','authority.html#/similarAuthority',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/5e67c7acef914c349d8aff076921f6b5','银海软件/门户菜单/资源权限管理/相似权限管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'48afedddc8f04c668b3c1572c30a7745','40337bdecb19484ebeb39d6c21aaca26','组织人员管理','','portal','',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745','银海软件/门户菜单/组织人员管理','2','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'1e706f26bc144c1da12022359c238053','48afedddc8f04c668b3c1572c30a7745','组织机构管理','','portal','orguser.html#/orgManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/1e706f26bc144c1da12022359c238053','银海软件/门户菜单/组织人员管理/组织机构管理','3','','','2',1,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'daceeff8a97b46cb9573b93ba3a5a792','48afedddc8f04c668b3c1572c30a7745','人员管理','','portal','orguser.html#/userManagement',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/daceeff8a97b46cb9573b93ba3a5a792','银海软件/门户菜单/组织人员管理/人员管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'3dbde33722154503a7d22ac60f6a0e4e','48afedddc8f04c668b3c1572c30a7745','自定义组织管理','','portal','orguser.html#/customOrgManagement',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/3dbde33722154503a7d22ac60f6a0e4e','银海软件/门户菜单/组织人员管理/自定义组织管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'4b2eee0d7ded4e8094d4acf439fd3a1c','48afedddc8f04c668b3c1572c30a7745','行政区划管理','','portal','orguser.html#/areaManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/4b2eee0d7ded4e8094d4acf439fd3a1c','银海软件/门户菜单/组织人员管理/行政区划管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'bd9d0bba145c458e841aa9da0aeeb1d8','7459c1b525934151a1d309a304933644','用户可管理字段配置','','portal','sysmg.html#/userInfoManagement',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/bd9d0bba145c458e841aa9da0aeeb1d8','银海软件/门户菜单/资源管理/用户可管理字段配置','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'ffa74f43e853441dac0ee90c787cb2e6','1d4e283ad5584e02811f6b188d3592bc','字典管理','','portal','sysmg.html#/dictionaryManager',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/ffa74f43e853441dac0ee90c787cb2e6','银海软件/门户菜单/系统管理/字典管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'c88413826abd4a5b88612a6442e6db07','1d4e283ad5584e02811f6b188d3592bc','操作日志',NULL,'portal','logmg.html#/operationLog',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/c88413826abd4a5b88612a6442e6db07','银海软件/门户菜单/系统管理/操作日志','3',NULL,NULL,'2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,workbench) VALUES (
'e4a8cc2bcd7d4e8ebdb3994f4caed572','7459c1b525934151a1d309a304933644','工作台模块管理','','ggyww','sysmg.html#/defaultTemplate',50,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/e4a8cc2bcd7d4e8ebdb3994f4caed572','银海软件/门户菜单/资源管理/工作台模块管理','3','','','2',0,'1','1',NULL,NULL,sysdate(),'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1');



-- 初始化角色
INSERT INTO tarole (roleid,rolename,isadmin,rolelevel,orgid,roletype,effective,effectivetime,createuser,createtime,roledesc,rolesign,subordinate) VALUES (
'1','超级管理员',NULL,NULL,'fd811ab9c30440088df3e29ea784460a','04','1',NULL,'1', sysdate(),'超级管理员角色',NULL,NULL);

-- 初始化用户组织表
INSERT INTO tauserorg (userid,orgid,isdirect) VALUES (
'1','fd811ab9c30440088df3e29ea784460a','1');

-- 初始化接入系统
INSERT INTO taaccesssystem (id,syscode,sysname,spell,protocol,domain,port,contextpath,portalsystem,menuservice,provider,effective,regtime,modifytime,backgroundaddress) VALUES (
'252317feb5cf4d04924c4e6bea09be60','ggyww','公共业务系统','GGYWXT','http','localhost','8082','ggyww','1','111','11','1', sysdate(), sysdate(),'192.168.0.0:8081');
INSERT INTO taaccesssystem (id,syscode,sysname,spell,protocol,domain,port,contextpath,portalsystem,menuservice,provider,effective,regtime,modifytime,backgroundaddress) VALUES (
'e55b9b7046434d66bc11b94d553fada2','portal','门户系统','MHXT','http','localhost','8082','portal02','1','111','ss','1', sysdate(), sysdate(),'192.168.0.1:8083');

-- 初始化标签
INSERT INTO tataggroup (taggroupid,taggroupname,effective,createuser,createtime,updatetime,destory) VALUES (
'001','用户','1','developer', sysdate(), sysdate(),'0');
INSERT INTO tataggroup (taggroupid,taggroupname,effective,createuser,createtime,updatetime,destory) VALUES (
'002','组织','1','developer', sysdate(), sysdate(),'0');

-- 初始化菜单路径表taresourceurl
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('ec56a0a43b09429482632cb61f7c6908', 'org/sysmg/resourceManagementRestService/**', '所有', '0', '821a8ca012154ba2a4c451918c99cfc0');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('6b8c6d0a93844985894713e396e7a99f', 'org/sysmg/accessSystemManagementRestService/**', '所有', '0', 'e2934e28ea9f4ed1979ad59e611dde00');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('3df588fc565d4287b3cefcd00a39cd91', 'org/sysmg/tagManagementRestService/**', '所有', '0', '885e01d3494b44759c62b8b3568e6145');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('95bb9b749bf54e4692b0b1f14fd1b5ab', 'org/authority/roleAuthorityManagementRestService/**', '所有', '0', 'fc45635549b5487c87d0287490a79b5f');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('8aa86ed4c7f84183935a262db4a605d3', 'org/authority/adminAuthorityManagementRestService/**', '所有', '0', 'cbcdd31ffb6c40499bb1af744851a783');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('877e407281dd48acb05a77fcb922bc73', 'org/authority/authorityAgentRestService/**', '所有', '0', 'c4360fdef9ad46278a677508ee8b290a');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('5e67c7acef914c349d8aff076921f6b5', 'org/authority/similarAuthorityManagementRestService/**', '所有', '0', '2c3c1591444842e1aa1588a0aab998a8');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('1e706f26bc144c1da12022359c238053', 'org/orguser/orgManagementRestService/**', '所有', '0', 'aa60a746c0a24640a06dea49a4d42572');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('daceeff8a97b46cb9573b93ba3a5a792', 'org/orguser/userManagementRestService/**', '所有', '0', 'b32b96d0142d4fea8d5fd8a58011c0c2');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('3dbde33722154503a7d22ac60f6a0e4e', 'org/orguser/customOrgManagementRestService/**', '所有', '0', 'b091f4af3eba4c5c946af8dacaf951b9');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('4b2eee0d7ded4e8094d4acf439fd3a1c', 'org/orguser/areaManagementRestService/**', '所有', '0', '7859157a0cd94c89995b0dde9fc40bc6');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('bd9d0bba145c458e841aa9da0aeeb1d8', 'org/sysmg/manageableFieldsOfUserInfoManagementRestService/**', '所有', '0', '017ebd5a5ab24128ad1122dbc2bdad72');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('c2745b7cae7846acb9bcf8d0f4e836e8', 'org/sysmg/resourceCategoryManagementRestService/**', '所有', '0', '7f621426782342faa371319c677f066e');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('ffa74f43e853441dac0ee90c787cb2e6', 'dictmg/dictMgRestService/**', '所有', '0', 'b88be2871149451a918264fd1ff468a3');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('770b2f1e77eb4d9092dc89743b5792c9', 'logmg/loginLog/loginLogAnalysisRestService/**', '所有', '0', '12cf0f814c4d41aa8b36ebbe5c9f14cb');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('7c2862a57e34459484bd701c19a8de27', 'logmg/exceptionlog/serverExceptionLogRestService/**', '所有', '0', '0b1ccf818760461ca7bafb439f62b711');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('7c1dabd160974d8f90858c187cefa128', 'logmg/logconfig/logConfigRestService', '所有', '0', 'cc101f8405664990b7c5e489d4a2785e');
INSERT INTO taresourceurl
(resourceid, resturl, urlname, authoritypolicy, urlid)
VALUES('c88413826abd4a5b88612a6442e6db07', 'org/sysmg/orgOpLogRestService/**', '所有', '0', '00840362514a445f8188415fbc52e056');
