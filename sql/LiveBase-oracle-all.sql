﻿drop view V_DICT;

drop table TAACCESSLOG cascade constraints;

drop table TAACCESSSYSTEM cascade constraints;

drop table TAAREA cascade constraints;

drop table TAAVATAR cascade constraints;

drop table TACONFIG cascade constraints;

drop table TACUSTOMORG cascade constraints;

drop table TACUSTOMORGTYPENAME cascade constraints;

drop table TACUSTOMORGUSER cascade constraints;

drop table TACUSTOMRESOURCE cascade constraints;

drop table TADICT cascade constraints;

drop table TAEXTENDSETTING cascade constraints;

drop table TALOGINHISTORYLOG cascade constraints;

drop table TALOGINSTATLOG cascade constraints;

drop table TAOBJECTTAGS cascade constraints;

drop table TAONLINELOG cascade constraints;

drop table TAONLINESTATLOG cascade constraints;

drop table TAORG cascade constraints;

drop table TAORGCONSTRAINT cascade constraints;

drop table TAORGMG cascade constraints;

drop table TAORGOPLOG cascade constraints;

drop table TARESOURCE cascade constraints;

drop table TARESOURCECATEGORY cascade constraints;

drop table TARESOURCEUI cascade constraints;

drop table TAROLE cascade constraints;

drop table TAROLEAUTHORITY cascade constraints;

drop table TAROLEUSER cascade constraints;

drop table TASERVEREXCEPTIONLOG cascade constraints;

drop table TATAG cascade constraints;

drop table TATAGGROUP cascade constraints;

drop table TATAGSSTORE cascade constraints;

drop table TAUSER cascade constraints;

drop table TAUSERORG cascade constraints;



CREATE TABLE  taaccesslog (
	logid VARCHAR2(36) NOT NULL ,
	userid VARCHAR2(36) NOT NULL,
	roleid VARCHAR2(36) NOT NULL,
	menuid VARCHAR2(36) NOT NULL,
	ispermission CHAR(1) NOT NULL,
	accesstime DATE NOT NULL,
	url VARCHAR2(1024),
	sysflag VARCHAR2(50),
	field01 VARCHAR2(10),
	field02 VARCHAR2(10),
	field03 VARCHAR2(10),
	field04 VARCHAR2(10),
	field05 VARCHAR2(10),
	constraint PK_TAACCESSLOG primary key (logid)
);
CREATE TABLE  taaccesssystem (
	id VARCHAR2(36) NOT NULL,
	syscode VARCHAR2(20) NOT NULL,
	sysname VARCHAR2(100) NOT NULL,
	spell VARCHAR2(30),
	protocol VARCHAR2(10),
	domain VARCHAR2(100),
	port VARCHAR2(5),
	contextpath VARCHAR2(30),
	portalsystem VARCHAR2(1),
	menuservice VARCHAR2(200),
	provider VARCHAR2(10),
	effective VARCHAR2(1),
	regtime DATE ,
	modifytime DATE ,
	backgroundaddress VARCHAR2(100),
	constraint PK_taaccesssystem primary key (id)
);

CREATE TABLE  taarea (
	areaid VARCHAR2(36) NOT NULL,
	parentid VARCHAR2(36) NOT NULL,
	areaname VARCHAR2(100) NOT NULL,
	areacode VARCHAR2(20) NOT NULL,
	idpath VARCHAR2(300) NOT NULL,
	namepath VARCHAR2(300) NOT NULL,
	arealevel DECIMAL(2,0),
	spell VARCHAR2(100),
	createuser VARCHAR2(36) NOT NULL,
	areaorder DECIMAL(20,0),
	effective VARCHAR2(1) NOT NULL,
	createtime DATE  NOT NULL,
	modifytime DATE  NOT NULL,
	destory VARCHAR2(1),
	constraint PK_taarea primary key (areaid)
);

CREATE TABLE  taavatar (
	userid VARCHAR2(36) NOT NULL,
	avatar BLOB,
	updatetime DATE  NOT NULL,
	constraint PK_taavatar primary key (userid,updatetime)
);


CREATE TABLE  taconfig (
	configid VARCHAR2(36) NOT NULL,
	configkey VARCHAR2(30) NOT NULL,
	configvalue VARCHAR2(1000) NOT NULL,
	configname VARCHAR2(150),
	configdesc VARCHAR2(1000),
	constraint PK_taconfig primary key (configid)
);

CREATE TABLE  tacustomorg (
	customorgid VARCHAR2(36) NOT NULL,
	parentid VARCHAR2(36),
	customcode VARCHAR2(100),
	customorgtypenameid VARCHAR2(36) NOT NULL,
	effective VARCHAR2(1) DEFAULT '1' NOT NULL,
	orderno DECIMAL(10,0),
	customorgname VARCHAR2(128),
	customorgpathid VARCHAR2(128),
	customorgpath VARCHAR2(256),
	createuser VARCHAR2(20),
	createtime DATE DEFAULT sysdate NOT NULL,
	updatetime DATE DEFAULT sysdate NOT NULL,
	destory VARCHAR2(20) DEFAULT '0' NOT NULL,
	spell VARCHAR2(20),
	orgmanager VARCHAR2(36),
	orgcode VARCHAR2(18),
	contacts VARCHAR2(36),
	address VARCHAR2(450),
	tel VARCHAR2(20),
	field01 VARCHAR2(1000),
	field02 VARCHAR2(1000),
	field03 VARCHAR2(1000),
	field04 VARCHAR2(1000),
	field05 VARCHAR2(1000),
	field06 VARCHAR2(1000),
	field07 VARCHAR2(1000),
	field08 VARCHAR2(1000),
	field09 VARCHAR2(1000),
	field10 VARCHAR2(1000),
	constraint PK_tacustomorg primary key (customorgid)
);

CREATE TABLE  tacustomorgtypename (
	customorgtypenameid VARCHAR2(32),
	customorgtypename VARCHAR2(20),
	effective VARCHAR2(20) DEFAULT '1' NOT NULL ,
	createuser VARCHAR2(20),
	createtime DATE DEFAULT sysdate NOT NULL ,
	updatetime DATE  DEFAULT sysdate NOT NULL ,
	destory VARCHAR2(20) DEFAULT '0' NOT NULL ,
	customorgtypenamecode VARCHAR2(20),
	customorgtypenamedesc VARCHAR2(20),
	constraint PK_tacustomorgtypename primary key (customorgtypenameid)
);

CREATE TABLE  tacustomorguser (
	customorgid VARCHAR2(36) NOT NULL,
	userid VARCHAR2(36) NOT NULL,
	constraint PK_tacustomorguser primary key (customorgid,userid)
);

CREATE TABLE  tacustomresource (
	customresourceid VARCHAR2(36) NOT NULL,
	resourcename VARCHAR2(450) NOT NULL,
	parentid VARCHAR2(32) NOT NULL,
	code VARCHAR2(100),
	resourcecontent VARCHAR2(1024),
	resourcecategory VARCHAR2(32),
	effective VARCHAR2(1),
	addtime DATE ,
	modifytime DATE ,
	system VARCHAR2(36),
	constraint PK_tacustomresource primary key (customresourceid)
);

CREATE TABLE  tadict (
	name VARCHAR2(50) NOT NULL,
	type VARCHAR2(50) NOT NULL,
	label VARCHAR2(50) NOT NULL,
	value VARCHAR2(6) NOT NULL,
	parentvalue VARCHAR2(6),
	sort DECIMAL(4,0) NOT NULL,
	authority VARCHAR2(20) NOT NULL,
	cssclass VARCHAR2(20),
	cssstyle VARCHAR2(128),
	remarks VARCHAR2(256),
	createdate DATE  NOT NULL,
	createuser VARCHAR2(36) NOT NULL,
	version VARCHAR2(10) NOT NULL,
	status VARCHAR2(2) NOT NULL,
	field01 VARCHAR2(10),
	field02 VARCHAR2(10),
	field03 VARCHAR2(10),
	field04 VARCHAR2(10),
	field05 VARCHAR2(10),
	system VARCHAR2(2) NOT NULL,
	newtype VARCHAR2(2) NOT NULL,
	constraint PK_tadict primary key (type,value)
);

CREATE TABLE  taextendsetting (
	fieldid VARCHAR2(10) NOT NULL,
	type VARCHAR2(2) NOT NULL,
	effective VARCHAR2(1),
	hide VARCHAR2(1),
	displaytext VARCHAR2(150),
	titext VARCHAR2(1500),
	orderno DECIMAL(10,0),
	required VARCHAR2(1),
	unchangeable VARCHAR2(1),
	formtype VARCHAR2(20),
	contentsize DECIMAL(16,0),
	validreg VARCHAR2(1000),
	connectaa10 VARCHAR2(30),
	protectprivacy VARCHAR2(1),
	constraint PK_taextendsetting primary key (fieldid,type)
);

CREATE TABLE  taloginhistorylog (
	logid VARCHAR2(36) NOT NULL,
	userid VARCHAR2(36) NOT NULL,
	logintime DATE  NOT NULL,
	logouttime DATE  NOT NULL,
	clientip VARCHAR2(200) NOT NULL,
	sessionid VARCHAR2(200) NOT NULL,
	serverip VARCHAR2(200),
	syspath VARCHAR2(50),
	clientsystem VARCHAR2(50),
	clientbrowser VARCHAR2(50),
	clientscreensize VARCHAR2(50),
	constraint PK_taloginhistorylog primary key (logid)
);

CREATE TABLE  taloginstatlog (
	statdate VARCHAR2(20) NOT NULL,
	pointintime VARCHAR2(20) NOT NULL,
	loginnum DECIMAL(15,0),
	constraint PK_taloginstatlog primary key (statdate,pointintime)
);

CREATE TABLE  taobjecttags (
	objectid VARCHAR2(36) NOT NULL,
	objecttype VARCHAR2(3) NOT NULL,
	tagid VARCHAR2(36) NOT NULL,
	constraint PK_taobjecttags primary key (objectid,objecttype,tagid)
);

CREATE TABLE  taonlinelog (
	logid VARCHAR2(36) NOT NULL,
	userid VARCHAR2(36) NOT NULL,
	logintime DATE  NOT NULL,
	curresource VARCHAR2(1000),
	clientip VARCHAR2(200) NOT NULL,
	sessionid VARCHAR2(200) NOT NULL,
	syspath VARCHAR2(50),
	serverip VARCHAR2(200),
	clientsystem VARCHAR2(50),
	clientbrowser VARCHAR2(50),
	clientscreensize VARCHAR2(50),
	constraint PK_taonlinelog primary key (logid)
);

CREATE TABLE  taonlinestatlog (
	statdate VARCHAR2(20) NOT NULL,
	pointintime VARCHAR2(20) NOT NULL,
	loginnum DECIMAL(15,0),
	constraint PK_taonlinestatlog primary key (statdate,pointintime)
);

CREATE TABLE  taorg (
	orgid VARCHAR2(36) NOT NULL,
	orgname VARCHAR2(300) NOT NULL,
	spell VARCHAR2(100) NOT NULL,
	parentid VARCHAR2(36) NOT NULL,
	idpath VARCHAR2(1024) NOT NULL,
	namepath VARCHAR2(1024) NOT NULL,
	customno VARCHAR2(30),
	orderno DECIMAL(10,0) NOT NULL,
	orglevel DECIMAL(16,0),
	area VARCHAR2(36),
	effective VARCHAR2(1),
	orgtype VARCHAR2(2) NOT NULL,
	createuser VARCHAR2(36) NOT NULL,
	createtime DATE ,
	modifytime DATE ,
	orgmanager VARCHAR2(36),
	orgcode VARCHAR2(18),
	contacts VARCHAR2(36),
	address VARCHAR2(450),
	tel VARCHAR2(20),
	destory VARCHAR2(1),
	field01 VARCHAR2(1000),
	field02 VARCHAR2(1000),
	field03 VARCHAR2(1000),
	field04 VARCHAR2(1000),
	field05 VARCHAR2(1000),
	field06 VARCHAR2(1000),
	field07 VARCHAR2(1000),
	field08 VARCHAR2(1000),
	field09 VARCHAR2(1000),
	field10 VARCHAR2(1000),
	constraint PK_taorg primary key (orgid)
);

CREATE TABLE  taorgconstraint (
	categoryid VARCHAR2(2) NOT NULL,
	allowcategoryid VARCHAR2(2) NOT NULL,
	constraint PK_taorgconstraint primary key (categoryid,allowcategoryid)
);

CREATE TABLE  taorgmg (
	roleid VARCHAR2(36) NOT NULL,
	orgid VARCHAR2(36) NOT NULL,
	effecttime DATE ,
	constraint PK_taorgmg primary key (roleid,orgid)
);

CREATE TABLE  taorgoplog (
	logid VARCHAR2(36) NOT NULL,
	batchno VARCHAR2(36) NOT NULL,
	optype VARCHAR2(2) NOT NULL,
	influencebodytype VARCHAR2(2) NOT NULL,
	influencebody VARCHAR2(36) NOT NULL,
	opbodytype VARCHAR2(2),
	opsubject VARCHAR2(36),
	changecontent VARCHAR2(2048),
	optime DATE  NOT NULL,
	opuser VARCHAR2(36) NOT NULL,
	oprole VARCHAR2(36) NOT NULL,
	ispermission VARCHAR2(1) NOT NULL,
	constraint PK_taorgoplog primary key (logid)
);

CREATE TABLE  taresource (
	resourceid VARCHAR2(36) NOT NULL,
	presourceid VARCHAR2(36) NOT NULL,
	name VARCHAR2(100) NOT NULL,
	code VARCHAR2(30),
	syscode VARCHAR2(20) NOT NULL,
	url VARCHAR2(100),
	orderno DECIMAL(10,0),
	idpath VARCHAR2(1024) NOT NULL,
	namepath VARCHAR2(1024) NOT NULL,
	resourcelevel VARCHAR2(2) NOT NULL,
	icon VARCHAR2(30),
	iconcolor VARCHAR2(7),
	securitypolicy VARCHAR2(2) NOT NULL,
	securitylevel DECIMAL(4,0) NOT NULL,
	resourcetype VARCHAR2(2) NOT NULL,
	effective VARCHAR2(1) NOT NULL,
	isdisplay VARCHAR2(1),
	isfiledscontrol VARCHAR2(1),
	createdate DATE  NOT NULL,
	createuser VARCHAR2(36) NOT NULL,
	uiauthoritypolicy VARCHAR2(2),
	field01 VARCHAR2(10),
	field02 VARCHAR2(10),
	field03 VARCHAR2(10),
	field04 VARCHAR2(10),
	field05 VARCHAR2(10),
	field06 VARCHAR2(10),
	field07 VARCHAR2(10),
	field08 VARCHAR2(10),
	field09 VARCHAR2(10),
	field10 VARCHAR2(10),
	resturl VARCHAR2(100),
	constraint PK_taresource primary key (resourceid)
);

CREATE TABLE  taresourcecategory (
	categoryid VARCHAR2(36) NOT NULL,
	categoryname VARCHAR2(300) NOT NULL,
	effective VARCHAR2(1),
	code VARCHAR2(100),
	categorycontent VARCHAR2(1024),
	constraint PK_taresourcecategory primary key (categoryid)
);

CREATE TABLE  taresourceui (
	pageelementid VARCHAR2(36) NOT NULL,
	resourceid VARCHAR2(36) NOT NULL,
	elenmentname VARCHAR2(20) NOT NULL,
	elementid VARCHAR2(30) NOT NULL,
	code VARCHAR2(20),
	authoritypolicy VARCHAR2(2) NOT NULL,
	createuser VARCHAR2(36) NOT NULL,
	createdate DATE  NOT NULL,
	field01 VARCHAR2(10),
	field02 VARCHAR2(10),
	field03 VARCHAR2(10),
	effective VARCHAR2(2) NOT NULL,
	constraint PK_taresourceui primary key (pageelementid)
);

CREATE TABLE  tarole (
	roleid VARCHAR2(36) NOT NULL,
	rolename VARCHAR2(150) NOT NULL,
	isadmin VARCHAR2(1),
	rolelevel DECIMAL(32,0),
	orgid VARCHAR2(36) NOT NULL,
	roletype VARCHAR2(2) NOT NULL,
	effective VARCHAR2(1) NOT NULL,
	effectivetime DATE ,
	createuser VARCHAR2(36),
	createtime DATE ,
	roledesc VARCHAR2(150),
	rolesign VARCHAR2(2),
	subordinate VARCHAR2(1),
	constraint PK_tarole primary key (roleid)
);

CREATE TABLE  taroleauthority (
	roleid VARCHAR2(36) NOT NULL,
	resourceid VARCHAR2(36) NOT NULL,
	resourcetype VARCHAR2(1) NOT NULL,
	usepermission VARCHAR2(1),
	repermission VARCHAR2(1),
	reauthrity VARCHAR2(1),
	createuser VARCHAR2(36),
	createtime DATE ,
	effecttime DATE ,
	constraint PK_taroleauthority primary key (roleid,resourceid,resourcetype)
);

CREATE TABLE  taroleuser (
	userid VARCHAR2(36) NOT NULL,
	roleid VARCHAR2(36) NOT NULL,
	defaultrole VARCHAR2(1),
	createuser VARCHAR2(36) NOT NULL,
	createtime DATE  NOT NULL,
	constraint PK_taroleuser primary key (roleid,userid)
);

CREATE TABLE  taserverexceptionlog (
	logid VARCHAR2(50) NOT NULL,
	ipaddress VARCHAR2(100),
	port VARCHAR2(10),
	exceptionname VARCHAR2(255),
	content BLOB,
	createtime DATE ,
	syspath VARCHAR2(50),
	clientip VARCHAR2(50),
	url VARCHAR2(100),
	menuid VARCHAR2(50),
	menuname VARCHAR2(30),
	useragent VARCHAR2(200),
	exceptiontype VARCHAR2(2),
	requestparameter BLOB,
	constraint PK_taserverexceptionlog primary key (logid)
);

CREATE TABLE  tatag (
	tagid VARCHAR2(32) NOT NULL,
	tagname VARCHAR2(20),
	taggroupid VARCHAR2(20),
	createtime DATE DEFAULT sysdate NOT NULL ,
	effective VARCHAR2(20)  DEFAULT '1' NOT NULL ,
	createuser VARCHAR2(20) DEFAULT 'developer' NOT NULL ,
	destory VARCHAR2(20) DEFAULT '0' NOT NULL ,
	updatetime DATE DEFAULT sysdate NOT NULL,
	constraint PK_tatag primary key (tagid)
);

CREATE TABLE  tataggroup (
	taggroupid VARCHAR2(20) DEFAULT '001' NOT NULL ,
	taggroupname VARCHAR2(20) DEFAULT '用户' NOT NULL ,
	effective VARCHAR2(20) DEFAULT '1' NOT NULL ,
	createuser VARCHAR2(20) DEFAULT 'developer' NOT NULL ,
	createtime DATE DEFAULT sysdate NOT NULL ,
	updatetime DATE DEFAULT sysdate NOT NULL ,
	destory VARCHAR2(20) DEFAULT '0' NOT NULL
);

CREATE TABLE  tatagsstore (
	tagid VARCHAR2(36) NOT NULL,
	tagname VARCHAR2(100) NOT NULL,
	tagcode VARCHAR2(30) NOT NULL,
	tagcategory VARCHAR2(2) NOT NULL,
	constraint PK_tatagsstore primary key (tagid)
);

CREATE TABLE  tauser (
	userid VARCHAR2(36) NOT NULL,
	loginid VARCHAR2(30) NOT NULL,
	password VARCHAR2(100) NOT NULL,
	passworddefaultnum DECIMAL(16,0),
	pwdlastmodifydate DATE ,
	islock VARCHAR2(1),
	orderno DECIMAL(10,0),
	name VARCHAR2(450) NOT NULL,
	sex VARCHAR2(2),
	idcardtype VARCHAR2(2),
	idcardno VARCHAR2(30),
	mobile VARCHAR2(20),
	createuser VARCHAR2(36),
	createtime DATE ,
	modifytime DATE ,
	destory VARCHAR2(1),
	accountsource VARCHAR2(2),
	effective VARCHAR2(1),
	effectivetime DATE ,
	jobnumber VARCHAR2(15),
	state VARCHAR2(10),
	birthplace VARCHAR2(12),
	address VARCHAR2(450),
	zipcode VARCHAR2(10),
	email VARCHAR2(100),
	phone VARCHAR2(20),
	education VARCHAR2(30),
	graduateschool VARCHAR2(150),
	workplace VARCHAR2(300),
	field01 VARCHAR2(1000),
	field02 VARCHAR2(1000),
	field03 VARCHAR2(1000),
	field04 VARCHAR2(1000),
	field05 VARCHAR2(1000),
	field06 VARCHAR2(1000),
	field07 VARCHAR2(1000),
	field08 VARCHAR2(1000),
	field09 VARCHAR2(1000),
	field10 VARCHAR2(1000),
	constraint PK_tauser primary key (userid)
);

CREATE TABLE  tauserorg (
	userid VARCHAR2(32) NOT NULL,
	orgid VARCHAR2(32) NOT NULL,
	isdirect VARCHAR2(1) NOT NULL,
	constraint PK_tauserorg primary key (userid,orgid)
);

create OR REPLACE view v_dict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) as select x0.name ,x0.type ,x0.label ,x0.value ,x0.parentvalue ,x0.sort ,x0.authority ,x0.cssclass ,x0.cssstyle ,x0.remarks ,x0.createdate ,x0.createuser ,x0.version ,x0.status ,x0.field01 ,x0.field02 ,x0.field03 ,x0.field04 ,x0.field05 ,x0.system ,x0.newtype from tadict x0 ;


-- 初始化人员表
INSERT INTO tauser (userid,loginid,password,passworddefaultnum,pwdlastmodifydate,islock,orderno,name,sex,idcardtype,idcardno,mobile,createuser,createtime,modifytime,destory,accountsource,effective,effectivetime,jobnumber,state,birthplace,address,zipcode,email,phone,education,graduateschool,workplace,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'1','developer','$2a$10$ZI3ewI1LvnS8bvTDiZk5X.w9/u.LXy8vPTwQFl8SlDtMTYBQHnIEm',0,sysdate,'0',1,'超级管理员','1','0',null,'18011567700','1',sysdate,NULL,'0',NULL,'1',NULL,NULL,NULL,NULL,'成都市/锦江区/久远银海',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化组织表
INSERT INTO taorg (orgid,orgname,spell,parentid,idpath,namepath,customno,orderno,orglevel,area,effective,orgtype,createuser,createtime,modifytime,orgmanager,orgcode,contacts,address,tel,destory,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10) VALUES (
'fd811ab9c30440088df3e29ea784460a','久远银海','JYYH','0','fd811ab9c30440088df3e29ea784460a','久远银海','',0,0,'67979672000d4c6cbecf422f233213df','1','01','1',NULL,sysdate,'','','','','','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化码表
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','个人角色','02',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','公有角色','01',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色类型','ROLETYPE','代理角色','03',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','用户名密码','01',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','指纹识别','02',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'权限认证方式','AUTHMODE','人脸识别','03',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'认证方式类型','AUTHMODE','Key盘','04',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','OFF','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','FATAL','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ERROR','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','WARN','3',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','INFO','4',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','DEBUG','5',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','TRACE','6',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'日志等级','LOGLEVEL','ALL','7',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','通用菜单','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','管理菜单','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'资源类型','RESOURCETYPE','经办菜单','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','组织','01',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','人员','02',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','岗位','03',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作对象类型','OPOBJTYPE','权限','07',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增组织','01',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑组织','02',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用组织','03',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增人员','04',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑人员','05',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用人员','06',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','密码重置','07',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位给人员','08',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','设置主岗位给人员','09',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增岗位','10',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','编辑岗位','11',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','禁用岗位','12',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位使用权限','13',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位使用权限','14',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用岗位','15',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用人员','16',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','启用组织','17',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除组织','18',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人使用权限','19',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人使用权限','20',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','赋予岗位授权别人授权权限','21',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','回收岗位授权别人授权权限','22',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除管理员','23',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','新增管理员','24',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除岗位','25',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'操作类型','OPTYPE','删除人员','26',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','机构','01',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','部门','02',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'组织类型','ORGTYPE','组','03',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','无','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','男','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'性别','SEX','女','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','是','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'是否','YESORNO','否','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','不显示','0',NULL,31,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见不可编辑','1','',21,'0','','',NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'元素在界面中的授权策略','ELEMENTAUTHORITYPOLICY','数据可见可编辑','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','有效','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','居民身份证(户口簿)','0','',10,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','111','11','',10,'0','','',NULL,sysdate,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','无效','0',NULL,0,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','无限制','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','一级','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','二级','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','三级','3',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全认证级别','SECURIYTLEVEL','四级','4',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','无需登录可访问','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','登陆均可访问','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'安全策略','SECURITYPOLICY','授权可访问','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','继承当前菜单权限','0',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'界面元素授权策略','UIAUTHORITYPOLICY','独立授权','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','未同步','0','',0,'0','','',NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','已同步','2','',2,'0','','',NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'字典数据类型','DICTDATATYPE','脏数据','1','',1,'0','','',NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试','Minus','123','111','',20,'0','','',NULL,sysdate,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','护照','1','',20,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1','1','',1,'0','','',NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-2','12','',1,'0','','',NULL,sysdate,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'测试1','MINUS','测试1-1-1U','123','1',1,'0','','',NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','组织账户','2',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'账户类型','ACCOUNTTYPE','个人账户','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'有效标识','EFFECTIVE','a','2',NULL,3,'0',NULL,NULL,NULL,sysdate,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','军官证','2','',30,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'证件类型','IDCARDTYPE','其他','3','',40,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'test1','TEST1','a','1','',10,'0',NULL,NULL,NULL,sysdate,'1','0','0',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','稽核角色','2',NULL,2,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');
INSERT INTO tadict (name,type,label,value,parentvalue,sort,authority,cssclass,cssstyle,remarks,createdate,createuser,version,status,field01,field02,field03,field04,field05,system,newtype) VALUES (
'角色标识','ROLESIGN','业务角色','1',NULL,1,'0',NULL,NULL,NULL,sysdate,'1','0','1',NULL,NULL,NULL,NULL,NULL,'1','0');

-- 初始化功能菜单
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'40337bdecb19484ebeb39d6c21aaca26','0','银海软件','','portal','',0,'40337bdecb19484ebeb39d6c21aaca26','银海软件','0','','','1',1,'0','1','','',sysdate,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'0415d44401b24605a21b589b6aaa349e','40337bdecb19484ebeb39d6c21aaca26','门户菜单','','portal','',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e','银海软件/门户菜单','1','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'7459c1b525934151a1d309a304933644','0415d44401b24605a21b589b6aaa349e','资源管理','','portal','',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644','银海软件/门户菜单/资源管理','2','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'ec56a0a43b09429482632cb61f7c6908','7459c1b525934151a1d309a304933644','功能菜单管理','','portal','sysmg.html#/resourceManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/ec56a0a43b09429482632cb61f7c6908','银海软件/门户菜单/资源管理/功能菜单管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/resourceManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'6b8c6d0a93844985894713e396e7a99f','7459c1b525934151a1d309a304933644','接入系统管理','','portal','sysmg.html#/accessSystem',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/6b8c6d0a93844985894713e396e7a99f','银海软件/门户菜单/资源管理/接入系统管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/accessSystemManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'3df588fc565d4287b3cefcd00a39cd91','7459c1b525934151a1d309a304933644','标签管理','','portal','sysmg.html#/tagManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/3df588fc565d4287b3cefcd00a39cd91','银海软件/门户菜单/资源管理/标签管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/tagManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'c2745b7cae7846acb9bcf8d0f4e836e8','7459c1b525934151a1d309a304933644','授权对象管理','','portal','sysmg.html#/customResource',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/c2745b7cae7846acb9bcf8d0f4e836e8','银海软件/门户菜单/资源管理/授权对象管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'78ad02fdb879406ebc5e7a4faf8f5905','0415d44401b24605a21b589b6aaa349e','资源权限管理','','portal','',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905','银海软件/门户菜单/资源权限管理','2','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'95bb9b749bf54e4692b0b1f14fd1b5ab','78ad02fdb879406ebc5e7a4faf8f5905','角色权限管理','','portal','authority.html#/roleAuthorityManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/95bb9b749bf54e4692b0b1f14fd1b5ab','银海软件/门户菜单/资源权限管理/角色权限管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/roleAuthorityManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'8aa86ed4c7f84183935a262db4a605d3','78ad02fdb879406ebc5e7a4faf8f5905','管理员权限管理','','portal','authority.html#/adminAuthority',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/8aa86ed4c7f84183935a262db4a605d3','银海软件/门户菜单/资源权限管理/管理员权限管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/adminAuthorityManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'1d4e283ad5584e02811f6b188d3592bc','0415d44401b24605a21b589b6aaa349e','系统管理','','portal','',41,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc','银海软件/门户菜单/系统管理','2','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'7c1dabd160974d8f90858c187cefa128','1d4e283ad5584e02811f6b188d3592bc','日志动态配置','','portal','logmg.html#/logConfig',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c1dabd160974d8f90858c187cefa128','银海软件/门户菜单/系统管理/日志动态配置','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'770b2f1e77eb4d9092dc89743b5792c9','1d4e283ad5584e02811f6b188d3592bc','登录日志统计','','portal','logmg.html#/loginLogAnalysis',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/770b2f1e77eb4d9092dc89743b5792c9','银海软件/门户菜单/系统管理/登录日志统计','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'7c2862a57e34459484bd701c19a8de27','1d4e283ad5584e02811f6b188d3592bc','系统异常日志','','portal','logmg.html#/systemExceptionLog',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/7c2862a57e34459484bd701c19a8de27','银海软件/门户菜单/系统管理/系统异常日志','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'877e407281dd48acb05a77fcb922bc73','78ad02fdb879406ebc5e7a4faf8f5905','权限代理','','portal','authority.html#/authorityAgent',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/877e407281dd48acb05a77fcb922bc73','银海软件/门户菜单/资源权限管理/权限代理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/authorityAgentRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'5e67c7acef914c349d8aff076921f6b5','78ad02fdb879406ebc5e7a4faf8f5905','相似权限管理','','portal','authority.html#/similarAuthority',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/78ad02fdb879406ebc5e7a4faf8f5905/5e67c7acef914c349d8aff076921f6b5','银海软件/门户菜单/资源权限管理/相似权限管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/authority/similarAuthorityManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'48afedddc8f04c668b3c1572c30a7745','0415d44401b24605a21b589b6aaa349e','组织人员管理','','portal','',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745','银海软件/门户菜单/组织人员管理','2','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'1e706f26bc144c1da12022359c238053','48afedddc8f04c668b3c1572c30a7745','组织机构管理','','portal','orguser.html#/orgManagement',0,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/1e706f26bc144c1da12022359c238053','银海软件/门户菜单/组织人员管理/组织机构管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/orgManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'daceeff8a97b46cb9573b93ba3a5a792','48afedddc8f04c668b3c1572c30a7745','人员管理','','portal','orguser.html#/userManagement',10,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/daceeff8a97b46cb9573b93ba3a5a792','银海软件/门户菜单/组织人员管理/人员管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/userManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'3dbde33722154503a7d22ac60f6a0e4e','48afedddc8f04c668b3c1572c30a7745','自定义组织管理','','portal','orguser.html#/customOrgManagement',20,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/3dbde33722154503a7d22ac60f6a0e4e','银海软件/门户菜单/组织人员管理/自定义组织管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/customOrgManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'4b2eee0d7ded4e8094d4acf439fd3a1c','48afedddc8f04c668b3c1572c30a7745','行政区划管理','','portal','orguser.html#/areaManagement',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/48afedddc8f04c668b3c1572c30a7745/4b2eee0d7ded4e8094d4acf439fd3a1c','银海软件/门户菜单/组织人员管理/行政区划管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/orguser/areaManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'bd9d0bba145c458e841aa9da0aeeb1d8','7459c1b525934151a1d309a304933644','用户可管理字段配置','','portal','sysmg.html#/userInfoManagement',40,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/7459c1b525934151a1d309a304933644/bd9d0bba145c458e841aa9da0aeeb1d8','银海软件/门户菜单/资源管理/用户可管理字段配置','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'org/sysmg/manageableFieldsOfUserInfoManagementRestService');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'd0afd42a20a441c5867c93eba60cfdd6','40337bdecb19484ebeb39d6c21aaca26','公共业务菜单','','ggyww','',10,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6','银海软件/公共业务菜单','1','','','2',0,'2','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'ffa74f43e853441dac0ee90c787cb2e6','1d4e283ad5584e02811f6b188d3592bc','字典管理','','portal','sysmg.html#/dictionaryManager',30,'40337bdecb19484ebeb39d6c21aaca26/0415d44401b24605a21b589b6aaa349e/1d4e283ad5584e02811f6b188d3592bc/ffa74f43e853441dac0ee90c787cb2e6','银海软件/门户菜单/系统管理/字典管理','3','','','2',0,'1','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'58129adf266c4b67bb99e2abb862ddf1','d0afd42a20a441c5867c93eba60cfdd6','单位业务','','ggyww','',0,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/58129adf266c4b67bb99e2abb862ddf1','银海软件/公共业务菜单/单位业务','2','','','2',0,'2','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'395eb1c2a6d84c7c933b9e71c9a3a494','58129adf266c4b67bb99e2abb862ddf1','单位新参保','','ggyww','',0,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/58129adf266c4b67bb99e2abb862ddf1/395eb1c2a6d84c7c933b9e71c9a3a494','银海软件/公共业务菜单/单位业务/单位新参保','3','','','2',0,'2','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'77145930ff8f478c803531b2e06f7750','d0afd42a20a441c5867c93eba60cfdd6','个人业务','','ggyww','',10,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/77145930ff8f478c803531b2e06f7750','银海软件/公共业务菜单/个人业务','2','','','2',0,'2','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'98f50db0ef6d4707844e692d4e5c14b2','77145930ff8f478c803531b2e06f7750','个人新参保','','ggyww','',0,'40337bdecb19484ebeb39d6c21aaca26/d0afd42a20a441c5867c93eba60cfdd6/77145930ff8f478c803531b2e06f7750/98f50db0ef6d4707844e692d4e5c14b2','银海软件/公共业务菜单/个人业务/个人新参保','3','','','2',0,'2','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'fef08bdf9b4a4c3c9dae02c66e5ed009','40337bdecb19484ebeb39d6c21aaca26','demo菜单','','ggyww','',20,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009','银海软件/demo菜单','1','','','2',0,'0','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'');
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'85c224dd0c8b4ec0a1199dbb195765ba','fef08bdf9b4a4c3c9dae02c66e5ed009','模块1',NULL,'ggyww',NULL,0,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009/85c224dd0c8b4ec0a1199dbb195765ba','银海软件/demo菜单/模块1','2',NULL,NULL,'2',0,'0','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'65d3fe6dcba7423688c715aa3a2530e9','85c224dd0c8b4ec0a1199dbb195765ba','part1',NULL,'ggyww','projectModuleOne.html#/modulePart1',0,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009/85c224dd0c8b4ec0a1199dbb195765ba/65d3fe6dcba7423688c715aa3a2530e9','银海软件/demo菜单/模块1/part1','3',NULL,NULL,'2',0,'0','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
INSERT INTO taresource (resourceid,presourceid,name,code,syscode,url,orderno,idpath,namepath,resourcelevel,icon,iconcolor,securitypolicy,securitylevel,resourcetype,effective,isdisplay,isfiledscontrol,createdate,createuser,uiauthoritypolicy,field01,field02,field03,field04,field05,field06,field07,field08,field09,field10,resturl) VALUES (
'b98b1678c10f46c193e79a028c35b7a9','85c224dd0c8b4ec0a1199dbb195765ba','part2',NULL,'ggyww','projectModuleOne.html#/modulePart2',10,'40337bdecb19484ebeb39d6c21aaca26/fef08bdf9b4a4c3c9dae02c66e5ed009/85c224dd0c8b4ec0a1199dbb195765ba/b98b1678c10f46c193e79a028c35b7a9','银海软件/demo菜单/模块1/part2','3',NULL,NULL,'2',0,'0','1',NULL,NULL,sysdate,'1','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

-- 初始化角色
INSERT INTO tarole (roleid,rolename,isadmin,rolelevel,orgid,roletype,effective,effectivetime,createuser,createtime,roledesc,rolesign,subordinate) VALUES (
'1','超级管理员',NULL,NULL,'fd811ab9c30440088df3e29ea784460a','04','1',NULL,'1',sysdate,'超级管理员角色',NULL,NULL);

-- 初始化用户组织表
INSERT INTO tauserorg (userid,orgid,isdirect) VALUES (
'1','fd811ab9c30440088df3e29ea784460a','1');

commit ;


