# 后台培训前介绍

## 前后端分离

前后端分离后，通常前端通过AJAX技术异步请求后端资源，后端通过JSON返回响应数据交由前端处理数据逻辑。

- 前后端分离，使得前后端能够各司其职，后端更注重于服务的提供，而前端更注重服务的使用
- 前端通过JS可以做非常多的数据处理工作，所以一定程度上也能够降低服务器的压力
- 后端的处理异常也不用直接反映到前端，通常分离可将异常处理变得更友好，比如以炫丽的页面效果展示错误消息。
- 减少耦合,降低开发难度

## springboot介绍

Spring Boot是由Pivotal团队提供的全新框架，其设计目的是用来简化新Spring应用的初始搭建以及开发过程。
该框架使用了特定的方式来进行配置，从而使开发人员不再需要定义样板化的配置。

特点:
1. 创建独立的Spring应用程序
2. 嵌入的Tomcat，无需部署WAR文件
3. 简化Maven配置
4. 自动配置Spring
5. 提供生产就绪型功能，如指标，健康检查和外部配置
6. 绝对没有代码生成并且对XML也没有配置要求

## Mybatis介绍

MyBatis是支持普通 SQL查询，存储过程和高级映射的优秀持久层框架。MyBatis 消除了几乎所有的JDBC代码和参数的手工设置以及结果集的检索。
MyBatis 使用简单的 XML或注解用于配置和原始映射，将接口和 Java 的POJOs（Plain Ordinary Java Objects，普通的 Java对象）映射成数据库中的记录。

1. 与JDBC相比，减少了50%以上的代码量。
2. MyBatis是最简单的持久化框架，小巧并且简单易学。
3. MyBatis相当灵活，不会对应用程序或者数据库的现有设计强加任何影响，SQL写在XML里，从程序代码中彻底分离，降低耦合度，便于统一管理和优化，并可重用。
4. 提供XML标签，支持编写动态SQL语句。
5. 提供映射标签，支持对象与数据库的ORM字段关系映射。

