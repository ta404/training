# 问题总结

### 问题一 zk在框架中的作用

zookeeper在框架中有两处使用

- 分布式定时任务

    zk与elasticjob配套使用,实现分布式定时任务功能.具体配置在application-elasticjob.yaml中.

	- 如果不想启用zk,需要把application-elasticjob.yaml中的elasticjob.enable设置为false

- 注册中心

    注册中心提供集群的发现和注册功能等.与ehcache配合使用,实现了ehcache的缓存同步功能.
    具体配置在application.yml中的ta404.modules.registry-center中

    - 如果不想启用zk,需要
        - 把application.yml中的ta404.modules.cache.register设置为false.
        - 把 ta404.modules.registry-center.enable设置为false
        - 把 ta404.modules.cluster.enable设置为false
    
    - 由于cachemg模块是基于zk的缓存管理功能,需要去pom.xml中将ta404-component-cachemg包给注释掉

### 问题二 框架提供哪些功能?

- 详见 [Livebase开发平台.mmap](./Livebase开发平台.mmap)

### 问题三 前后端部署文档?

- 打包

    - 前台

        使用npm run build打包,会将前台整个打包成静态资源html和css,生成在dist目录下

    - 后台

        后台使用maven打包工具,直接package打包(忽略测试),在输出目录下生成对应jar包

        如果需要部署在weblogic/tomcat容器下,需要在maven中排除springboot自带的tomcat容器,然后再打包

- 发布

    - 分离部署

        分别部署前端的静态页面和后端的jar包

    - 整体部署

        把前台打包好的整个dist目录下的内容,复制到后端web目录下的template目录,然后由后端统一打包发布


### 问题四 配置文件说明

- [配置文件说明](./config.md)

### 问题五 项目搭建问题

框架推荐使用启动模块+子模块的方式,但是不强制规定,可以只有一个启动模块,然后用包路径分块.请根据各个项目功能业务的需求进行选择.

### 问题六 session失效问题

当重启项目之后,session会失效跳转到登录页面,如果在登录页面报session失效,只需要刷新一次再进行访问即可

### 问题七 项目更新说明

- 前端更新

前端更新为覆盖更新,除了src目录下自定义的模块,其他配置都会被覆盖

- 后端更新

主要是maven更新依赖包,启动模块下的配置文件如果有更新,请把ta404-project-default包下的配置文件复制出来修改

### 问题八 数据库问题

框架自身提供了Oralce,Gbase,Mysql,SqlServer的数据库脚本

其中
- Gbase 8S
- oracle 11g 12c
- sqlserver 14
- mysql 5.7.23

### 问题九 配置中心

框架计划提供一个额外的统一配置管理功能,允许动态修改框架中的配置属性,暂时还没有
