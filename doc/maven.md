# MAVEN 远程仓库配置
- 新建一个xml，如setting-liveBase.xml 复制下面内容粘贴保存
- 在idea中【maven配置中】选择改文件作为maven的配置文件
```
<?xml version="1.0" encoding="UTF-8"?>

<settings xmlns="http://maven.apache.org/SETTINGS/1.0.0"
          xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
          xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.0.0 http://maven.apache.org/xsd/settings-1.0.0.xsd">

  <localRepository>自己的仓库地址（如：d:/.m2)</localRepository>
  <pluginGroups>
  </pluginGroups>
  <proxies>
  </proxies>
  <servers>
     <server>
    <id>discussGroup</id>
    <username>discuss</username>
    <password>123456</password>
    </server>
  </servers>
  <mirrors>
   <mirror>
      <id>discussGroup</id>
      <mirrorOf>*</mirrorOf>
      <url>http://118.112.188.108:9289/nexus/content/groups/discussGroup/</url>
    </mirror>
  </mirrors>
  <profiles>
  </profiles>
</settings>
```