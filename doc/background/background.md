# livebase 后端培训

## 环境依赖

1. maven 3.5.0+
1. JDK 1.8
1. IDEA 2018
1. 银海idea插件
1. Oralce 11g/Gbase8t

## livebase项目搭建

#### 一.新建父项目

1、依次点击：File->New->Project

2、左侧面板选择maven（不要选择Create from archetype选项），如下图，点击Next即可。

![create.png](img/create.png)

3、依次补全如下信息，点击Next,直到完成(注意不能建立在包含中文的目录下)

#### 二.新建框架模块

在父项目下,选择新建模块,Spring Initializr,选中Custom输入如下地址：http://192.168.17.18:8080/cloud/ 或者 http://118.112.188.108:9289/cloud/  然后点next

![newProject.jpg](img/newModule.jpg)

![newProject.png](img/newProject.png)

出现如下界面，Group，Artifact改成项目需要的，其他参数根据自己需要修改，填写完点next

![projectMeta.png](img/projectMeta.png)

#### 三.功能模块选择

1. 核心模块全选
1. 组织机构全选
1. 持久化框架选mybatis
1. 缓存类型选ehcache
1. 服务注册选zookeeper
1. 集群同步选cluster
1. 验证码选中
1. 分布式事务不选
1. 字典选中

最终结果如下:

![result1.png](img/result1.png)

![result2.png](img/result2.png)

#### 四.完成创建

填写项目实际目录,然后完成创建

![finish.png](img/finish.png)

#### 五.Maven更新

 需要将maven配置成提供的[settings.xml](source/settings.xml)里的路径,然后再maven更新

#### 六.创建独立子模块

1. 在父模块上右键如下图操作，创建一个模块，该模块即为子模块。

![child.png](img/child.png)

2. 在新建的子模块的pom.xml中添加框架核心依赖

````
<dependency>
    <groupId>com.yinhai.ta404</groupId>
    <artifactId>ta404-core</artifactId>
    <version>5.0.1-RELEASE</version>
 </dependency>
````

3. 在框架的启动模块中添加独立模块的依赖

## 工程配置说明

#### 一.配置文件加载顺序

bootstrap.yml > application.yml > application-*.yaml
如果存在相同的属性,先加载的生效

application-datasource.yaml中添加了mybatis的扫描,会加载mapper.xml文件

#### 二.详细配置说明

1.bootstrap.yml

- Server.port为项目启动端口号
- Server.servlet.session.timeout为session失效时间

2.application.yml

- ta404.log日志输出配置
- spring.jackson中设置为日期格式
- Ta404.profiles.active 额外加载的配置文件,配置x时,会去自动加载application-x.yaml文件
- Ta404.validation.customRegex自定义验证规则
- Ta404.modules.security安全相关配置
- Ta404.modules.security .permitUrls 无需登录即可访问地址
- Ta404.modules.security .loginPermitUrls 登陆之后所有角色都可访问地址

3.application-datasource.yml

- Ta404.database.transaction.packageStartCheck事务扫描目录
- Ta404.database.mybatis.page-dialect分页组件方言
- Ta404.database.mybatis.datasourceList启用数据源
- Ta404.database.mybatis.ta404ds框架数据源相关配置
- Ta404.database.mybatis.ta404ds.mapper框架mapper接口扫描路径
- Ta404.database.mybatis.ta404ds. type-aliases-package实体类扫描路径
- Ta404.database.mybatis.ta404ds. mapper-locations框架mapper.xml扫描路径

## 代码分层介绍

![part.png](img/part.png)

- Entity:实体,和数据库表一一对应
- Mapper:mybatis接口类和mapper.xml一一对应          对应dao层
- Rest:供前台调用的服务接口                         对应restservice
- Service:业务逻辑,分为读/写                        对应service层
- Vo:实体类,用于数据交互,可以包含多表字段

调用顺序为,前端调用Rest接口,Rest中注入Service实例,且调用Service中的方法,在Service中注入mapper实例,并调用mapper方法

1. restservice层
主要用于处理与前台的数据交互,提供访问接口

1. service层
主要用于处理业务逻辑,其中bpo是复合业务逻辑,blo是原子业务逻辑,且需要控制事务

1. dao层
主要处理数据库操作,java类中只有接口,实现直接在对应xml中,按照规范编写sql

## 具体编写一个完整业务

1.准备好数据库,jdk和idea
2.安装银海idea插件

![plugin.jpg](img/plugin.jpg)

3.配置数据库连接

   - a.常用数据库直接通过idea下载驱动,GBase数据库需要选择DriverAndDataSource,自定义选择驱动

![database.jpg](img/database.jpg)

   - b.填写数据库连接地址,用户,密码,选择驱动,点击测试通过之后保存

![driver.jpg](img/driver.jpg)

4.选中一张表,右键代码创建工具,代码生成

![newCode.jpg](img/newCode.jpg)

注意选择包路径对应生成的类里面的包路径,还有具体生成的路径.

可以直接生成一套完整的后台功能代码,注意xml和java代码的包路径必须要一致.

5.在application-datasource.yaml中配置对应mybatis扫描路径

## 后台常用功能介绍

#### 配置读取

- a.@Value标注在字段上

````
 @Value("${ta404.modules.security.passwordUsefulLife}")
 String passwordUsefulLife;
````

- b.@ConfigurationProperties标注在配置类上

````
@Component
@ConfigurationProperties(prefix="ta404.modules.security")
public class WebSecurityConfiger {
````

#### 参数校验

1. 框架提供了@Validated和@V注解用于校验参数,具体如下:
- @Validated标注在实体类上或者后台传参的实现类上
- @V标注在具体的字段或者参数上,且需要传入一个对象,里面有校验的具体规则

2. 验证失败时,会自动抛出AppException,返回到ResultBean的异常中

3. 参考示例代码[ValidateTestService.java](source/ValidateTestService.java)

````
1.@Validated写法
@Validated
public class ValidateTestService extends BaseRestService

public String query1(@RequestBody ,@Validated UserVo vo)

2.@V写法
public void query2(@V({"notnull", "email", "min=10", "max=20"}) String email)

@V({"notnull", "email", "min=10", "max=20"})
private String user;

@V({"regex=^[0-9]+$"})
private String password;
````

4. 可以在application.yml中ta404.validation.customeRegex下配置自定义规则,如下图

![rule.png](img/rule.png)

#### 分页查询

框架提供了一个分页的便捷写法,集成在Ta404SupportMapper中,只要Mapper继承这个类即可调用对应的分页方法.

````
public interface UserManagementReadMapper extends Ta404SupportMapper
````

具体分页方法如下:

````
userManagementReadMapper.beginPager(pageParam);
Page<TaUserVo> resultPage = userManagementReadMapper.endPager(queryUserByCondition(condition));
````

- beginPager需要传入PageParam对象,该对象包含pageNumber和pageSize两个参数,分别代表当前页数,和每页多少条数据.
- endPager需要传入一个查询接口,该接口需要传入一个完整mapper查询,且这个查询返回的是List对象.
- Page中包含当前页号,每页数量,当前页数量,总记录数,总页数,当前查询结果集

#### 工具类

- WebUtil框架提供获取当前服务端信息,如当前登录人员信息等
- DateUtils是jdk1.8的日期工具类
- JSonFactory是json处理工具类
- ValidateUtil是校验工具类,可以对引入类型判空校验等
- ServiceLocator可以获取springbean对象
- ObjectConversionUtil对象转换(效率不高,不推荐循环使用)

#### 事务控制

框架提供了@TaTransactional用于控制事务,当有readOnly = true时表示为只读事务.该注解标注在service层上,以提高效率.
事务允许嵌套,但是不允许在只读事务里面包含非只读的事务.

````
@Service
@TaTransactional(readOnly = true)
public class UserManagementReadServiceImpl extends OrgBaseService implements UserManagementReadService{
````

````
@Service
@TaTransactional
public class UserManagementWriteServiceImpl extends OrgBaseService implements UserManagementWriteService {
	@Resource
	private UserOrgWriteService userOrgWriteService;
````

