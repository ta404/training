package com.yinhai.ta404.core.validate;


import com.yinhai.ta404.core.restservice.BaseRestService;
import com.yinhai.ta404.core.restservice.annotation.RestService;
import com.yinhai.ta404.core.validate.annotation.V;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@RestService
@Validated
public class ValidateTestService extends BaseRestService {

    @RequestMapping("/test/core/validate1")
    @ResponseBody
    public String query1(@RequestBody @Validated UserVo vo) {
        return vo.getPassword();
    }

    @RequestMapping("/test/core/validate2")
    public void query2(@V({"notnull", "email", "min=10", "max=20"}) String email) {
        setData("email", email);
    }

}

class UserVo{
    @V({"notnull", "email", "min=10", "max=20"})
    // @Email
    private String user;
    // @Email
    @V({"regex=^[0-9]+$"})
    private String password;

    public String getUser() {
        return user;
    }

    public void setUser(final String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserVo{" +
                "user='" + user + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
