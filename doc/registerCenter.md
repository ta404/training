
# 注册中心

## 注册中心使用说明

注册中心提供zookeeper,redis(还在完善中),数据库(还在完善中)三种实现方式，
将分布式信息，如节点信息，缓存信息进行统一管理，提供注册，获取，删除，监听变化的接口

## 配置说明
1. 在application.yml文件中进行配置
2. 具体配置说明：
```
ta404: 
  registry-center:  #分布式注册中心配置
        enable: true  #是否启用注册中心
        default: zookeeper #配置启用以下那种注册中心
        zookeeper:
          url: 192.168.17.18:2181 # 配置zk地址
        redis:
          url: 192.168.29.219:6379 # 配置redis地址
```
## 接口说明
```
    /**
     * 初始化分布式服务
     * @param url 分布式服务地址(如zk地址，redis地址，数据库地址)
     */
    void initClusterClient(String url);
```
```
    /**
     * 注册节点
     * @param clusterKey 目标地址
     * @param clusterValue 要注册的节点
     */
    void setClusterNode(String clusterKey, String clusterValue);
```
```
    /**
     * 注册信息(存储在节点上)
     * @param clusterKey 目标节点
     * @param clusterValue 存储在节点上的信息
     */
    void setClusterInfo(String clusterKey, String clusterValue);
```
```
    **
    * 获取目标节点的信息
    * @param clusterKey 目标节点
    * @return 返回节点信息
    */
    String getClusterInfo(String clusterKey);
```
```
    /**
     * 删除节点
     * @param clusterKey 删除的目标节点
     */
    void deleteClusterInfo(String clusterKey);
```
```
    /**
     * 获取节点列表
     * @param clusterPath 目标地址
     * @return
     */
    List<String> getClusterNodeList(String clusterPath);
```
```
    /**
     * 节点监听回调
     * @param target 目标节点
     * @param listener 回调方法
     * @return
     */
    Object addTargetListener(String target, IClusterEventListener listener);
        回调中重写三个方法：
          //子节点更新时触发  
          @Override
          public void targetUpdate(String clusterKey) {
              setTCPInitialHosts(getInitialHosts());
          }
          //新增子节点时触发
          @Override
          public void targetAdd(String clusterKey) {
              setTCPInitialHosts(getInitialHosts());
          }
          //删除子节点时触发
          @Override
          public void targetRemove(String clusterKey) {
              setTCPInitialHosts(getInitialHosts());
          }
```
```
    /**
     * 获取分布式计数器
     * @param path 目标地址（在该目标地址下获取分布式计数器）
     * @return
     */
    long getClusterCounter(String path);
```
```
     /**
     * 销毁方法,关闭客户端与注册中心连接
     */
    void destory();
```
### 拓展方法
1. 编写模块，实现以上接口，并编写配置类（参考TaRegistryZookeeperConfiguration.java)
2. 在启动项目maven pom文件中引入拓展的模块，以我引入zookeeper实现为例：
```
        <dependency>
            <groupId>com.yinhai.ta404</groupId>
            <version>5.0.1-RELEASE</version>
            <artifactId>ta404-module-registry-zookeeper</artifactId>
        </dependency>
```