# 数据源配置说明
## 总体说明
1. 在application.yml文件中引入application-datasource.yaml配置文件
2. 前缀
````
ta404: 
    database: 
        mybatis: #mybatis前缀
        ibatis: #ibatis前缀
        hibernate: #hibernate前缀
````
## 命名规则
### 1.service包路径命名规则
1.1 所有service包路径均应在对应包路径中包含“service”，例如：com.yinhai.ta404.module.appcode.**service**....
1.2 service包路径必须包含数据源名称，框架默认会根据该数据源名称去自动分配事务管理器，例如数据源叫“ds1”，则包路径为：com.yinhai.ta404.test.**ds1**.service...
### 2.数据源命名规则
数据源的beanName均以每种数据源下的ta404.database.mybatis/ibatis/hibernate.datasourceList列表中的字符串为名称
### 3.事务管理器命名规则
以数据源的beanName开头+TransactionManager，例如“ta404dsTransactionManager”
### 4.其他
均以数据源的beanName开头，并加上对应的后缀
## 框架默认数据源
1.在pom文件中引入：
```
<dependency>
    <groupId>com.yinhai</groupId>
    <artifactId>ta404-module-mybatis</artifactId>
    <version>5.0.0.SNAPSHOT</version>
</dependency>
```
2.在application-datasource.yaml配置文件中引入mybatis数据源相关信息:
```
ta404:
  database:
    mybatis:
      datasourceList: ta404ds #框架默认数据源名称，不要更改！
      ta404ds:
        jdbcUrl: jdbc:oracle:thin:@//192.168.17.135:1521/pdb1
        username: ta410
        password: ta410
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        mappers: 
            - com.yinhai.datasource.ta404ds.mapper
        type-aliases-package: 
            - com.yinhai.datasource.entity
        mapper-locations: 
            - classpath*:mapper/ta404dsMapper/*Mapper.xml
```
## mybatis
1.如果不想使用框架默认数据源，则在默认数据源的基础上（也是多数据源的配置方式），在路径ta404.database.mybatis.datasourceList中添加数据源名称，以“,”隔开，例如
```
ta404:
  database:
    mybatis:
      datasourceList: ta404ds,myds #框架默认数据源，不要更改！
      ta404ds:
        jdbcUrl: jdbc:oracle:thin:@//192.168.17.135:1521/pdb1
        username: ta410
        password: ta410
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        mappers:
          - com.yinhai.datasource.ta404ds.mapper
          - com.yinhai.ta404.core.transaction.log.mapper
        type-aliases-package:
          - com.yinhai.datasource.entity
          - com.yinhai.ta404.core.transaction.log.entity
        mapper-locations:
          - classpath*:mapper/ta404dsMapper/*Mapper.xml
      myds:
          jdbcUrl: jdbc:oracle:thin:@//localhost:1521/XE
          username: ta404
          password: ta404
          driverClassName: oracle.jdbc.driver.OracleDriver
          minimumIdle: 5
          mappers:
            - com.yinhai.datasource.ds2.mapper
          type-aliases-package:
            - com.yinhai.datasource.ds2.entity
          mapper-locations:
            - classpath*:mapper/ds2Mapper/*Mapper.xml
```
2.属性扩展，transaction用于扩展事务管理器，sqlSessionFactory用于扩展SqlSessionFactory属性
```
ta404:
  database:
    mybatis:
      datasourceList: ta404ds #框架默认数据源，不要更改！
      ta404ds:
        jdbcUrl: jdbc:oracle:thin:@//192.168.17.135:1521/pdb1
        username: ta410
        password: ta410
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        mappers:
          - com.yinhai.datasource.ta404ds.mapper
          - com.yinhai.ta404.core.transaction.log.mapper
        type-aliases-package:
          - com.yinhai.datasource.entity
          - com.yinhai.ta404.core.transaction.log.entity
        mapper-locations:
          - classpath*:mapper/ta404dsMapper/*Mapper.xml
#        transaction: #用于扩展事务管理器属性
#        sqlsessionfactory: #用于扩展sqlSessionFactory属性
```
## ibatis
1.在pom文件中引入：
```
<dependency>
    <groupId>com.yinhai.ta404</groupId>
    <artifactId>ta404-module-ibatis</artifactId>
    <version>5.0.1-RELEASE</version>
</dependency>
<dependency>
    <groupId>com.yinhai</groupId>
    <artifactId>ibatis</artifactId>
    <version>2.0</version>
    <scope>provided</scope>
</dependency>
```
2.在application-datasource.yaml配置文件中加入ibatis配置：
```
ta404:
  database:
    mybatis:
      datasourceList: ta404ds #框架默认数据源，不要更改！
      ta404ds:
        jdbcUrl: jdbc:oracle:thin:@//192.168.17.135:1521/pdb1
        username: ta410
        password: ta410
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        mappers:
          - com.yinhai.datasource.ta404ds.mapper
          - com.yinhai.ta404.core.transaction.log.mapper
        type-aliases-package:
          - com.yinhai.datasource.entity
          - com.yinhai.ta404.core.transaction.log.entity
        mapper-locations:
          - classpath*:mapper/ta404dsMapper/*Mapper.xml
    ibatis:
      datasourceList: ds2
      ds2:
        jdbcUrl: jdbc:oracle:thin:@//localhost:1521/XE
        username: ta404
        password: ta404
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        config-locations:
          - classpath:/ibatis/SqlmapConfig-test.xml
```
3.属性扩展，transaction用于扩展事务管理器，sqlMapClient用于扩展sqlMapClient属性
```
ta404:
  database:
    ibatis:
      datasourceList: ds2
      ds2:
        jdbcUrl: jdbc:oracle:thin:@//localhost:1521/XE
        username: ta404
        password: ta404
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        config-locations:
          - classpath:/ibatis/SqlmapConfig-test.xml
#        transaction: #用于扩展事务管理器属性
#        sqlmapclient: #用于扩展sqlMapClient属性
```
## hibernate
1.pom文件中引入：
```
<dependency>
    <groupId>com.yinhai.ta404</groupId>
    <artifactId>ta404-module-hibernate</artifactId>
    <version>5.0.1-RELEASE</version>
</dependency>
```
2.在application-datasource.yaml配置文件中加入hibernate配置：
```
ta404:
  database:
    mybatis:
      datasourceList: ta404ds #框架默认数据源，不要更改！
      ta404ds:
        jdbcUrl: jdbc:oracle:thin:@//192.168.17.135:1521/pdb1
        username: ta410
        password: ta410
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        mappers:
          - com.yinhai.datasource.ta404ds.mapper
          - com.yinhai.ta404.core.transaction.log.mapper
        type-aliases-package:
          - com.yinhai.datasource.entity
          - com.yinhai.ta404.core.transaction.log.entity
        mapper-locations:
          - classpath*:mapper/ta404dsMapper/*Mapper.xml
    hibernate:
      datasourceList: ds3
      ds3:
      jdbcUrl: jdbc:oracle:thin:@//localhost:1521/XE
      username: ta3new
      password: ta3new
      driverClassName: oracle.jdbc.driver.OracleDriver
      minimumIdle: 5
      packages-to-scan:
        - com.yinhai.module.hibernate.entity #实体类
      mapping-resources:
        - Org.hbm.xml #hbm文件
      hibernateProperties:
        dialect: org.hibernate.dialect.OracleDialect
        show_sql: true
```
3.属性扩展，transaction用于扩展事务管理器，sessionFactory用于扩展sessionFactory属性
```
ta404:
  database:
    hibernate:
      datasourceList: ds3
      ds3:
      jdbcUrl: jdbc:oracle:thin:@//localhost:1521/XE
      username: ta3new
      password: ta3new
      driverClassName: oracle.jdbc.driver.OracleDriver
      minimumIdle: 5
      packages-to-scan:
        - com.yinhai.module.hibernate.entity #实体类
      mapping-resources:
        - Org.hbm.xml #hbm文件
      hibernateProperties:
        dialect: org.hibernate.dialect.OracleDialect
        show_sql: true
#      transaction: #用于扩展事务管理器属性
#      sessionfactory: #用于扩展sessionFactory属性
```