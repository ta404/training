# 配置说明

- [后端配置说明](#后端配置说明)
    - bootstrap.yml
    - application.yml
    - application-datasource.yaml
    - application-elasticjob.yaml
- [前端配置说明](#前台配置)
    - package.json
    - faceConfig.js
    - 前端接口配置

# 后端配置说明

### bootstrap.yml

```
spring:
  application:
    name: ta404             #项目名称
  banner:
    location: banner.txt    #启动抬头(佛祖)
  output:
    ansi:
      enabled: always       #编码输出
server:
  port: 8081                #后台端口号
  servlet:
    session:
      timeout: 7200s        #session失效时间
    context-path: /git      #web上下文
```

### application.yml

```
transaction-log: true           # 事务日志是否开启
ta404:
  profiles:
    active: datasource,org,elasticjob           #加载的额外配置文件
  application:
    name: ta404                                 #应用名
    version: 5.0.0-SNAPSHOT                     #应用版本号
  validation:
    customeRegex:                               #自定义校验规则
      email:
        regex: ^[a-z0-9]+@[a-z0-9]+\.[a-z0-9]+$
        message: 邮箱格式不正确
      cellphone:
        regex: ^[0-9]{11}$
        message: 电话号码格式不正确
      emailOrEmpty:
        regex: ^[a-z0-9]+@[a-z0-9]+\.[a-z0-9]+$|\s*
        message: 邮箱格式不正确
      cellphoneOrEmpty:
        regex: ^[0-9]{11}$|\s*
        message: 电话号码格式不正确
  log:
    output-type: console                        #日志输出类型
    root-logger-level: INFO                     #日志输出级别
    package-level:                              #对应模块日志输出级别
      com.yinhai: DEBUG
      org.springframework: INFO
      org.springframework.boot: INFO
      org.hibernate.SQL: DEBUG
      org.apache.tomcat: TRACE
      org.apache.catalina: TRACE
      org.eclipse.jetty: TRACE
      com.codingapi: DEBUG
    appender-type:                              #日志输出类型定义
      console:
        appender-loader-class: com.yinhai.ta404.core.log.logback.config.ConsoleAppenderLoader   #console输出类
        format: "%clr(%d{HH:mm:ss.SSS}){faint} %clr([%thread]){faint} %clr(%user){cyan} %clr(%-5level) %clr(%logger{100}){cyan} %clr(:){faint} %clr(%msg%n){faint}"     #输出格式
        use-all-package-level: false            #是否启用package-level
        monitor-packages:
          - com.yinhai
          - org.springframework
          - org.springframework.boot
          - com.codingapi
      file:
        appender-loader-class: com.yinhai.ta404.core.log.logback.config.RollingFileAppenderLoader
        file-name-pattern: F:/test/file_%d{yyyy-MM-dd}_%i.log
        format: "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} : %msg%n"
        max-history: 30
        max-file-Size: 10mb
        clear-size: 10gb
        use-all-package-level: false
        monitor-packages:
          - com.yinhai
      kafka:
        appender-loader-class: com.yinhai.ta404.core.log.logback.config.KafkaAppenderLoader
        format: "%msg%n"
        topic: ta404                                                #kafka主题
        producer-config: bootstrap.servers=192.168.128.135:9092     #生产者配置
        async: false                                                #是否异步
        use-all-package-level: false                                #是否启用package-level
        monitor-packages:
          - com.yinhai
  modules:
    captcha:
      userCheckCode: true # 是否启用验证码
      checkCodeType: simple # 验证码类型(simple/slide)
      passwordValidationErrorNumber: 2 # 密码错误开启验证码次数
      define:
        simple:
          numberCheckCodeLevel: 1 #验证码为simple时,验证码复杂度
        slide:
          photoPath:              #滑动验证码图片路径
    security:               #单机无单点登录配置
      isOpenSecuirtyManagemer: true # 是否使用springsecurity管理
      passwordRSA: true # 是否启用密码rsa加密
      dbEncoderType: BCrypt # 数据库密码加密类型(BCrypt,MD5,Sm2,Sm3,Sm4)
      isOpenJsonFilter: true # 是否开启application/json处理filter
      passwordFirstlogin: true # 第一次登录是否修改密码
      maximumSessions: -1 # 同一个账户最多在几个地方登录
      passwordMaxFaultNumber: 5 # 密码错误锁账户次数
      passwordUsefulLife: 30  # 密码过期时间
      decisionVoters: # 投票配置 只要有一个Voter可以通过权限要求，就可以访问
        - org.springframework.security.access.vote.RoleVoter
        - org.springframework.security.access.vote.AuthenticatedVoter
      loginProcessingUrl: /login # 登录请求地址
      logoutUrl: /logout #退出请求地址
      permitUrls:           #不需要登录都有权限访问的地址
        - /error
        - /loginRestService/getConfig
        - /loginRestService/changePassword
        - /loginRestService/checkUser
        - /CaptchaImg**
        - /publish
        - /template/**
      loginPermitUrls:      # 登陆之后任意角色都能访问
        - /indexRestService/getCurUserAccount
        - /codetable/getCode
        - /menu/menuAction/queryRootChildrenMenus
        - /indexRestService/defaultOpen
        - /indexRestService/changePassword
        - /tauserworkbench/**
        - /taroleworkbench/**
    cas:                    # 单点登录配置
      #单点登录服务器登录页地址
      casServerLoginUrl: http://114.116.159.110:31010/casserver/login
      #单点服务器地址
      casServerUrlPrefix: http://192.168.0.55:31010/casserver
      #接入单点应用地址
      serverName: http://114.116.159.110:30010/git/j_spring_cas_security_check
      #单点服务器退出地址
      ssoLogoutUrl: http://114.116.159.110:31010/casserver/logout
      #退出后前端默认跳转地址，优先级比frontUrl高
      ssoLogoutTargetUrl:
      #本地系统id，默认留空
      localSystemId:
      #是否启用本地用户转换，默认为false
      isTransSSOLoginIdToLocalLoginId: false
      #是否启用单点登录，启用单点同时需关闭普通通过spring security登录的登录方式 也即修改isOpenSecuirtyManagemer属性为false
      isOpenSso: false
      #退出请求地址
      logoutFilterProcessUrl: /logout
      #固定写法，spring security集成cas单点登录地址
      filterProcessUrl: /j_spring_cas_security_check
      #访问权限投票配置 只要有一个Voter可以通过权限要求，就可以访问
      decisionVoters:
        - org.springframework.security.access.vote.RoleVoter
        - org.springframework.security.access.vote.AuthenticatedVoter
      # 无需登录即可访问
      permitUrls:
        - /error
        - /loginRestService/getConfig
        - /loginRestService/changePassword
        - /loginRestService/checkUser
        - /CaptchaImg**
        - /publish
        - /template/**
      # 登陆之后任意角色都能访问
      loginPermitUrls:
        - /indexRestService/getCurUserAccount
        - /codetable/getCode
        - /menu/menuAction/queryRootChildrenMenus
        - /indexRestService/defaultOpen
        - /indexRestService/changePassword
        - /tauserworkbench/**
        - /taroleworkbench/**
    cluster:
      name: ta404_Cluster   #集群名称
      enable: true          #是否开启集群
      default: jgroup-tcp #可配置为jgroup-udp(广播形式),jgroup-tcp(tcp+注册中心)，http
      jgroup-udp:
      jgroup-tcp:
      http:
        address: 192.168.29.37:8081   # 在mode为http时可配置，配置为每个server的ip加端口号,每个server配置由逗号分隔
    registry-center:  #分布式注册中心配置
      enable: false         #是否开启注册中心
      default: zookeeper    #配置启用以下那种注册中心
      zookeeper:
        url: 192.168.17.18:2181         # 配置zk地址
      redis:
        url: 192.168.29.219:6379        # 配置redis地址
    distribute:
      startLcn: true                    #是否开启分布式事务
      txManagerUrl: http://127.0.0.1:8899/tx/manager/ #lcn manager地址
    cache:
      mode: ehcache                     #使用的缓存类型
      register: false                   #是否开启注册中心
      ehcache:
        configLocation: classpath:cache/ehcache.xml     #ehcache的配置文件位置
      redis:
        jedis:
          pool:
            #连接池的最大数据库连接数，默认 8
            maxTotal: 100
            #最大空闲连接数，默认 8
            maxIdle: 20
            #最小空闲连接数，默认 0
            minIdle: 10
            #连接耗尽时是否阻塞, false报异常,ture阻塞直到超时, 默认true
            blockWhenExhausted: true
            #最大建立连接等待时间
            maxWaitMillis: 3000
            #是否在从池中取出连接实例前进行检验,如果检验失败,则从池中去除连接并尝试取出另一个
            testOnBorrow: false
            #是否在return连接实例给pool前进行检验
            testOnReturn: false
            #在空闲时检查有效性, 校验失败会从pool中drop掉 默认false（这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义）
            testWhileIdle: true
            #逐出连接的最小空闲时间 默认1800000毫秒(30分钟) ；（这一项只有在timeBetweenEvictionRunsMillis大于0时才有意义）
            minEvictableIdleTimeMillis: 60000
            #逐出扫描的时间间隔(毫秒) 如果为负数,则不运行逐出线程, 默认-1
            timeBetweenEvictionRunsMillis: 30000
            #每次逐出检查时 逐出的最大数目 如果为负数就是 : 1/abs(n), 默认3
            numTestsPerEvictionRun: -1
        cluster:                # 缓存redis集群地址配置
          max-redirects: 5
          cluster-nodes[0]: 192.168.17.135:6379
          cluster-nodes[1]: 192.168.17.135:6380
          cluster-nodes[2]: 192.168.17.135:6381
          cluster-nodes[3]: 192.168.17.135:6382
          cluster-nodes[4]: 192.168.17.135:6383
          cluster-nodes[5]: 192.168.17.135:6384
      coherence:

    dict:
      openCache: true           #是否开启字典缓存

# 统一设置日期格式
spring:
  jackson:
    date-format: yyyy-MM-dd HH:mm:ss
    time-zone: GMT+8
```

### application-datasource.yaml

```
ta404:
  database:
    transaction:
      log: true         #是否开启事务日志
      packageStartCheck: com.yinhai     #启动事务类检测，只检测以下包路径开头的类
    mybatis:
      datasourceList: ta404ds #ta404ds为框架默认数据源，不要更改！
      ta404ds:                #框架数据源
        page-dialect: informix #分页方言oracle,mysql,mariadb,sqlite,hsqldb,postgresql,db2,sqlserver,informix,h2,sqlserver2012
        #数据库连接
        jdbcUrl: jdbc:informix-sqli://192.168.17.18:9088/ta3:INFORMIXSERVER=gbaseserver;NEWCODESET=UTF8,zh_cn.UTF8,57372;CLIENT_LOCALE=zh_cn.utf8;DB_LOCALE=zh_cn.utf8
        username: informix      # 用户名
        password: ta3           # 密码
        driverClassName: com.informix.jdbc.IfxDriver    #驱动类
        minimumIdle: 5          #数据库连接池最小连接数
        mappers:                #mybatis mapper自动扫描位置
          - com.yinhai.ta404.module.mybatis.mapper
          - com.yinhai.ta404.module.**.mapper.read
          - com.yinhai.ta404.module.**.mapper.write
          - com.yinhai.ta404.component.**.mapper.read
          - com.yinhai.ta404.component.**.mapper.write
        type-aliases-package:   #mybatis 实体类自动扫描位置
          - com.yinhai.ta404.component.**.entity
        mapper-locations:       #mybatis xml自动扫描位置
          - classpath*:mapper/read/*.xml
          - classpath*:mapper/write/*.xml
        transaction: #用于扩展事务管理器属性
          enforceReadOnly: true #表示DataSourceTrasactionManager支持只读事务，某些数据库(sqlserver)不支持此属性，需要关闭
      #        sqlsessionfactory: #用于扩展sqlSessionFactory属性
      ds2:
        page-dialect: oracle
        jdbcUrl: jdbc:oracle:thin:@//192.168.17.135:1521/pdb1
        username: ta401
        password: ta401
        driverClassName: oracle.jdbc.driver.OracleDriver
        minimumIdle: 5
        mappers:
          - com.yinhai.datasource.ds2.mapper
        type-aliases-package:
          - com.yinhai.datasource.ds2.entity
        mapper-locations:
          - classpath*:mapper/ds2Mapper/*Mapper.xml
        transaction: #用于扩展事务管理器属性
          enforceReadOnly: true #表示DataSourceTrasactionManager支持只读事务
```

### application-elasticjob.yaml

```
elasticjob:
  enable: false         #是否启用elasticjob
  zk:
    serverLists: 192.168.17.18:2181     #zk服务器地址
    namespace: ta404-job                #命名空间
  job:
    statLogJob:                         #具体job配置
      cron: 0 */1 * * * ?               #cron表达式
      shardingTotalCount: 1             #分片总数
      shardingItemParameters: 0=A       #分片参数
      disabled: true                    #是否开启job
      overwrite: true                   #是否重写
      description: 登录人数统计          #job描述
```

# 前台配置

### package.json

```
{
    "name": "demo",         //项目名称
    "version": "1.0.0",     //项目版本
    "description": "demo",  //项目描述
    "author": "demo",       //作者信息
    "private": true,        //是否私有
    "scripts": {            //启动脚本,以下都可以用 npm run *** 启动
        // basePath填写实际后台地址,前台直接请求后台,不经过代理
        "dev": "webpack-dev-server --mode development --inline --progress --config build/webpack.dev.conf.js ",
        // 前端打包命令
        "build": "node build/build.js",
        // 前端框架组件更新
        "update-ui": "npm uninstall @yh/ta404-ui --save && npm install @yh/ta404-ui --save",
        // 代码规范检查
        "lint": "eslint --ext .js,.vue src",
        // basePath填写后台web上下文,前台请求被代理到mock
        "dev-mock": "concurrently \"cross-env RUN_ENV=\"mock\" npm run dev\" \"node net.js\"",
        // basePath填写后台web上下文,前台请求被代理到后台
        "dev-server": "cross-env RUN_ENV=\"server\" npm run dev"
    },
    "dependencies": {        //依赖打包好的组件
        "@babel/polyfill": "^7.2.5",
        ...
    },
    "devDependencies": {    //依赖开发工具
        "@babel/core": "^7.0.0",
        ...
    },
    "engines": {            //引擎版本要求
        "node": ">= 6.0.0",
        "npm": ">= 3.0.0"
    },
    "browserslist": [       //浏览器版本要求
        "> 1%",
        "last 2 versions",
        "not ie <= 8"
    ]
}
```

### faceConfig.js

```
const faceConfig = () => {
  return {
    // 基础路径,打包发布的时候修改为后端发布服务地址
    'basePath': '/git',
    // 开发模式mockServer服务地址
    'mockServer': 'http://192.168.17.18:10000/mock/5b68f9b10b81e375f173f0ed',
    // 开发模式后端服务地址
    'devServer': 'http://localhost:8081/git',
    // 项目文件打包寻址地址
    'docModule': ['corePage/*', 'demoPage/*'],
    // 核心模块依赖
    'coreModules': [
      'orgModules/authority',
      'orgModules/orguser',
      'systemModules/sysmg',
      'systemModules/logmg',
      'editorModules/formDesign',
      'editorModules/formPreview',
      'editorModules/onlineEditor',
      'editorModules/staticResourceMg'
    ],
    // 额外模块依赖
    'projectModules': [
      'demoModules/projectModuleOne',
      'demoModules/projectModuleTwo',
      'demoModules/test'
    ],
    // 首页数据配置
    'indexPageConfig': {
      //menuType 菜单样式 topLeft,left,top
      menuType:'topLeft',
      //layout 布局  top,left
      layout:'left',
      // 顶部头的高度
      headerHeight: '64px',
      // 左侧菜单的宽度
      leftWidth: '230px',
      // logo框框的宽度
      logoWidth: '230px',
      // 用户信息框框的宽度
      userPaneWidth: '220px',
      // 第二条的那个tab页的高度
      tabHeight: '39px',
      // 一级菜单的显示方式(horizon水平,dropdown下拉)
      menuOneStyle: 'horizon',
      // 二级菜单显示方式(sliding侧滑,dropdown手风琴)
      menuTwoStyle: 'dropdown'
    }
  }
}
```

### 前端接口配置

在config/index.js中module.exports下,dev.port设置

