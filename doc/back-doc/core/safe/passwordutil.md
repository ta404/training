# 加密解密工具类使用说明

<!-- TOC -->

- [加密解密工具类使用说明](#加密解密工具类使用说明)
    - [MD5使用](#md5使用)
    - [RSA使用](#rsa使用)
    - [国密SM2使用](#国密sm2使用)
    - [国密SM3使用](#国密sm3使用)
    - [国密SM4使用](#国密sm4使用)

<!-- /TOC -->

## MD5使用

md5并不是加密使用,只是对文件进行信息摘要提取,md5工具类提供了两个静态方法进行使用,详情如下:

![md5.png](img/md5.png)

相关说明:

- MD5方法,对输入的内容进行md5摘要提取

    实例demo:
    
    ```
     String pass = MD5Util.hash("123");
     System.out.println("md5="+pass);
    ``` 

- verify方法,进行MD5验证,第一个参数是需要验证的内容,第二个是md5的字符串

    实例demo:
    
    ```
     boolean success = MD5Util.verify("123",pass);
     System.out.println("verify="+success);
    ```

## RSA使用

rsa是非对称加密,一般用作简单信息的非对称,例如网络通信协商使用哪种对称加密等等,由于其复杂度很高,所以产生也需要消耗部分时间,提供的方法如下:

![rsa.png](img/rsa.png)

相关说明:

- `loadKeyFromFile`,从当前类路径加载文件

    实例demo:
    
    ```
     //从类路径加载文件
     String pubKeyStr = TARSAUtil.loadKeyFromFile("publicKey.pem",true);
    ```

- `getKeyPair`获取密钥对,并调用`getPublicKey`和`getPrivateKey`获取base64编码后的公钥和私钥字符串,可以调用文件的`main`方法进行保存(默认公钥文件保存在`/tmp/rsa.pub.pem`,私钥保存在`/tmp/rsa.pri.pem`)或者新建相应的文件进行存储

    实例demo:
    
    ```
    KeyPair keyPair = TARSAUtil.getKeyPair();
    String publicKeyStr = TARSAUtil.getPublicKey(keyPair);
    String privateKeyStr = TARSAUtil.getPrivateKey(keyPair);
    System.out.println("RSA公钥Base64编码:" + publicKeyStr);
    System.out.println("RSA私钥Base64编码:" + privateKeyStr);
    ```

- `string2PublicKey`和`string2PrivateKey`,将ba64编码的公钥字符串和私钥字符串转成对应的`PublicKey`对象和`PrivateKey`对象,

    实例demo:
    
    ```
    //将Base64编码后的公钥转换成PublicKey对象
    PublicKey publicKey = TARSAUtil.string2PublicKey(publicKeyStr);
    //将Base64编码后的私钥转换成PrivateKey对象
    PrivateKey privateKey = TARSAUtil.string2PrivateKey(privateKeyStr);
    ```

- 公钥加密与私钥解密,使用`encrypt`方法进行公钥加密和`decrypt`进行私钥解密,进行相应的操作即可

    实例demo:
    
    ```
    //用公钥加密
    String publicEncrypt = TARSAUtil.encrypt(message, publicKey);
    //用私钥解密
    String privateDecrypt = TARSAUtil.decrypt(publicEncrypt, privateKey);
    ```

## 国密SM2使用
sm2也是非对称算法,但是其安全性要比rsa高一些,提供的方法有如下:

![sm2.png](img/sm2.png)

相关说明:

- `generateKeyPair`生成密钥对,可以调用`convertEcPubKeyToPem`和`convertEcPriKeyToPem`将密钥对保存到文件中进行存储,方便后续使用,也可以调用`main`方法进行相应的存储(默认的公钥存储路径`/tmp/sm2.pub.pem`,私钥存储路径`/tmp/sm2.pri.pem`)

    实例demo:
    
    ```
    AsymmetricCipherKeyPair keyPair = Sm2Util.generateKeyPair();
    ECPrivateKeyParameters priKey = (ECPrivateKeyParameters) keyPair.getPrivate();
    ECPublicKeyParameters pubKey = (ECPublicKeyParameters) keyPair.getPublic();
    String pubPath = "/tmp/sm2.pub.pem";
    String priPath = "/tmp/sm2.pri.pem";
    convertEcPubKeyToPem(pubKey,pubPath);
    System.out.println("sm2公钥保存成功,保存路径="+pubPath);
    convertEcPriKeyToPem(keyPair,priPath);
    System.out.println("sm2私钥保存成功,保存路径="+priPath);
    ```

- 从文件中读取公钥`pem2PublicKey`和私钥`pem2PrivateKey`并转换为`ECPublicKeyParameters`对象和`ECPrivateKeyParameters`对象

    实例demo:
    
    ```
    //将公钥pem转换成ECPublicKeyParameters
    ECPublicKeyParameters smPub=Sm2Util.pem2PublicKey("/tmp/sm2.pub.pem");
    //将私钥pem转换成ECPrivateKeyParameters
    ECPrivateKeyParameters smPri = Sm2Util.pem2PrivateKey("/tmp/sm2.pri.pem");
    ```

- 公钥加密`encrypt`和私钥解密`decrypt`

    实例demo:
    
    ```
    //公钥加密
    String encryptedData = Sm2Util.encrypt(pubKey, "123");
    //私钥解密
    String decryptedData = Sm2Util.decrypt(priKey, encryptedData);
    ```

- 私钥加签`sign`和公钥验签`verify`

    实例demo:
    
    ```
    //私钥签章
    String sign = Sm2Util.sign(priKey, "123");
    //公钥验签
    boolean flag = Sm2Util.verify(pubKey, "123", sign);
    ```

## 国密SM3使用
sm3是对内容进行消息摘要提取,其作用和md5类似,只是比md5更加复杂一些,提供的使用方法如下:

![sm3.png](img/sm3.png)

相关说明:

- `hash`,是对传入的文件内容进行摘要,并生成唯一的hash码

    实例demo:
    
    ```
    String hash = Sm3Util.hash("123");
    ```

- `verify`,进行hash校验,成功返回`true`,失败返回`false`

    实例demo:
    
    ```
    boolean flag = Sm3Util.verify("123", hash);
    ```

## 国密SM4使用
sm4采用的是对称加密算法,提供的方法如下:

![sm4.png](img/sm4.png)

相关说明:

- `generateKey`生成对称密钥,可以使用`main`方法调用进行存储(默认保存路径`/tmp/sm4.key.pem`)

    实例demo:
    
    ```
    String key = Sm4Util.generateKey();
    ```
- `getKey`获取对称密钥,默认从当前类路径进行加载,如果加载不成功,则从文件系统中加载文件

   实例demo:
   
   ```
   String key = Sm4Util.getKey("/tmp/sm4.key.pem");

   ```

- 对称加密`encrypt`和解密`decrypt`

    实例demo:

    ```
    //对称加密
    String cipherText = Sm4Util.encrypt(key, "123");
    //对称解密
    String decryptedData = Sm4Util.decrypt(key, cipherText);
    ```



