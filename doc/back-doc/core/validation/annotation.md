#  注解验证配置

##    @V 注解

@V注解可用于注解方法的参数或vo的属性，配置此注解的String对象可以在Spring注入具体的前端输入值之前对这个值进行验证，是否满足@V注解指定的规则。

##    注解用法

### 示例:
```java
 @V({"telePhone","notnull","min=11","max=11","regex=^[aABb]*$"})
```
形如此示例，@V注解接收一个String数组值，且可以在其中任意位置填充满足以下约定的值：

1. notnull/nullable：notnull验证注解的String对象是否为null、长度是否为0、是否为空白字符串。
1. telePhone：在yaml配置文件通过配置（稍后讲述）的自定义正则表达式。
1. min=?/max=?：指定注解值的最小/大长度，值为int值。
1. regex=^[aABb]*$：指定自定义的正则表达式。

注：

1. notnull和nullable同时设置时仅有notnull会生效。
1. 以上规则除第二项在配置文件设置（示例中的telePhone）的正则表达式名称（下一个示例将会说明）及第四项自定义的正则表达式外均不区分大小写。
1. 以上规则顺序对检查结果没有影响。若未传入任何规则，则将使用<code>{"nullable","regex=^.*$","min=0","max=Integer.MAX_VALUE"}</code>进行验证。
1. 传入多个min/max时，只有最后一个传入的范围（最后一个有效的min和max值）才会生效。
1. 可以在一个参数或属性上使用多个不同的@V注解。
1. 在需要提前验证数据合法的服务接口类上注解使用@org.springframework.validation.annotation.Validated，否则@V注解不会生效。

### 示例
```java
@RestService
@Validated
public class ValidateTestService extends BaseRestService {

    // demo1
    @RequestMapping("/test/core/validate1")
    @ResponseBody
    public String demo1(@RequestBody @Validated UserVo vo) {
        return vo.getPassword();
    }

    // demo2
    @RequestMapping("/test/core/validate2")
    public void demo2(@V({"notnull", "email", "min=10", "max=20"}) String email) {
        setData("email", email);
    }

}
// vo类
public class UserVo{
    @V({"notnull", "email", "min=10", "max=20"})
    private String user;
    @V({"regex=^[0-9]+$"})
    private String password;
    //getter/setter
}
```

##    应用yml自定义配置

### 示例：
``` yaml
ta404:
  validation:
    customeRegex:
      email:
        regex: ^[a-z0-9]+@[a-z0-9]+\.[a-z0-9]+$
        message: 邮箱格式不正确
      cellphone:
        regex: ^[0-9]{11}$
        message: 电话号码格式不正确
      demo: // 这条规则的名称，唯一，用于在@V注解中进行解析，大小写敏感
        regex: //正则表达式
        message: //验证不通过时返回的消息
```

形如上述代码为一个@V注解配置的示例代码，可根据customeRegex下的唯一key在注解验证时获取到自定义的正则表达式及验证不通过应该返回的消息。
