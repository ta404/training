

# 缓存使用说明

框架默认提供三种可选的缓存方案，分别是ehcache(默认)、redis、coherence，
它们都能提供全面和稳定的缓存支持，项目可根据实际情况，选择合适的缓存方案。

## 缓存策略的切换
配置 ```application.yaml``` 的 ta404.modules.cache.mode
* ehcache(默认)
```
    <dependency>
        <groupId>com.yinhai.ta404</groupId>
        <artifactId>ta404-module-cache-ehcache</artifactId>
        <version>5.0.1-RELEASE</version>
    </dependency>
```
相关配置文件 ```cache/ehcache.xml```，每类 cache 都需要一个对应 cache 配置, eq:
```
    <cache alias="menuReportCache">
        <key-type>java.lang.String</key-type>
        <value-type>java.util.Collection</value-type>
        <resources>
            <heap>200000</heap>
            <offheap unit="MB">100</offheap>
        </resources>
    </cache>
```
* redis
```
        <dependency>
            <groupId>com.yinhai.ta404</groupId>
            <artifactId>ta404-module-cache-redis</artifactId>
            <version>5.0.1-RELEASE</version>
        </dependency>
```
相关配置 : redis 配置项都在 ```application.yaml``` 中， redis目前按集群模式实现的, 需要在 ``` ta404.modules.cache.redis.cluster ``` 下配置redis节点, eq:
```
        cluster:
          max-redirects: 5
          cluster-nodes[0]: 192.168.17.135:6379
          cluster-nodes[1]: 192.168.17.135:6380
          cluster-nodes[2]: 192.168.17.135:6381
          cluster-nodes[3]: 192.168.17.135:6382
          cluster-nodes[4]: 192.168.17.135:6383
          cluster-nodes[5]: 192.168.17.135:6384
```
* coherence
```
        <dependency>
            <groupId>com.yinhai.ta404</groupId>
            <artifactId>ta404-module-cache-coherence</artifactId>
            <version>5.0.1-RELEASE</version>
        </dependency>
```
相关配置文件 ```tangosol-coherence-override.xml```、```cache/cache-config-coherence.xml```, 每类缓存也需要配置声明
```
       <cache-mapping>
           <cache-name>menuReportCache</cache-name>
           <scheme-name>distributed</scheme-name>
           <key-type>String</key-type>
           <value-type>String</value-type>
       </cache-mapping>
```
更多 coherence 配置,请参考 [coherence 官网](https://www.oracle.com/technetwork/middleware/coherence/documentation/index.html)

## 开发人员使用

框架缓存实现完全兼容 [spring-cache 标准](https://docs.spring.io/spring/docs/5.1.3.RELEASE/spring-framework-reference/integration.html#cache)
* 注解
 eq:
```
    @Async
    @CachePut(value = OrgConstant.TA_ORG_CACHE, key = "#orgId", condition = "#orgId != null")
    @Override
    public TaOrgVo addOrgCache(String orgId) {
        return orgBaseReadMapper.queryOrgInfoByOrgId(orgId, OrgConstant.NO);
    }
```
* 注入
```
    @Resource(name="taCacheManager")
    ITaCacheManager taCacheManager;
```
可获取 cache, 实现自 ```org.springframework.cache.Cache``` 接口, 一般推荐转换类型为 com.yinhai.ta404.core.cache.ITaCache 使用, 有更多的 API 选择

## 缓存集群同步

ehcache 缓存策略需要额外的集群同步配置, ```ta404.modules.cluster.enable : true``` 启用集群通信, 
* udp 
无需额外的配置
* http
需要配置 ```ta404.modules.cluster.http.address ``` , http 通信地址
* tcp(推荐)
需要配置 ```ta404.modules.registry-center.enable : true``` 启用注册中心

通过 TaCacheManager 获取的 cache 来自框架的自己实现 TaCache, 开启配置后缓存操作会自动进行缓存同步

redis、coherence 有自己的集群同步策略，大家可以自己了解。

## 缓存管理功能
缓存管理功能可实时查看集群中指定节点的缓存同步状况, 模块依赖注册中心, 需要配置 ```ta404.modules.registry-center.enable : true``` 启用注册中心 和 ```ta404.modules.cache.register : true``` 启用缓存同步跟踪注册