
# 分布式定时任务说明

## 使用说明

 1. 依赖模块
```
 <dependency>
    <groupId>com.yinhai.ta404</groupId>
    <artifactId>ta404-module-elasticjob</artifactId>
    <version>5.0.1-RELEASE</version>
 </dependency>
```
2. 说明        
新建自己的定时任务类，实现SimpleJob、DataflowJob、ScriptJob接口，如下：
```
@ElasticJobConf(name = "mySimpleJob")
public class MySimpleJob implements SimpleJob {

	@Override
	public void execute(ShardingContext shardingContext) {
		// 业务代码
	}
	
}
```
两种使用方式:
1). 注解方式
所有job属性都在@ElasticJobConf注解上配置，zookeeper属性需要在配置文件中配置，例如：
```
@ElasticJobConf(name = "mySimpleJob", cron = "0/10 * * * * ?", shardingItemParameters = "0=0,1=1", description = "简单任务")    (其他属性参考配置说明)
public class MySimpleJob implements SimpleJob {

	@Override
	public void execute(ShardingContext shardingContext) {
		// 业务代码
	}
	
}

配置文件中：
elasticjob:
  enable: true    启动项目是否注册zookeeper (true：注册，false：不注册)
  zk:
    serverLists: 192.168.17.18:2181    zookeeper集群地址
    namespace: ta404-job    zookeepr定时任务命名空间
    ...... (其他属性参考配置说明)
```
2). 配置文件方式：
需要加上@ElasticJobConf注解，并且必须加上name属性(name = "mySimpleJob")，其它属性均在application-elasticjob.yaml文件中配置，例如
```
@ElasticJobConf(name = "mySimpleJob")
public class MySimpleJob implements SimpleJob {

	@Override
	public void execute(ShardingContext shardingContext) {
		// 业务代码
	}
	
}

配置文件中：
elasticjob:
  enable: true    启动项目是否注册zookeeper (true：注册，false：不注册)
  zk:
    serverLists: 192.168.17.18:2181    zookeeper集群地址
    namespace: ta404-job    zookeepr定时任务命名空间
    ...... (其他属性参考配置说明)
  job:
    mySimpleJob:    (自己实现的定时任务类，此名称必须和注解 @ElasticJobConf(name = "statLogJob")的属性name一致)
      cron: 0 */1 * * * ?
      shardingTotalCount: 1
      shardingItemParameters: 0=A
      disabled: false
      overwrite: true
      description: 登录人数统计
      ...... (其他属性参考配置说明)
```

## 配置说明
1. 在application-elasticjob.yaml文件中进行配置
2. 配置属性说明：
```
zookeeper属性配置：
enable    启动项目是否注册zookeeper (true：注册，false：不注册)
serverLists    zookeeper集群地址
namespace    zookeepr定时任务命名空间
baseSleepTimeMilliseconds     等待重试的间隔时间的初始值. 单位毫秒.
maxSleepTimeMilliseconds    等待重试的间隔时间的最大值. 单位毫秒.
maxRetries    最大重试次数
sessionTimeoutMilliseconds    会话超时时间. 单位毫秒.
connectionTimeoutMilliseconds    连接超时时间. 单位毫秒.
digest    连接Zookeeper的权限令牌. 缺省为不需要权限验证.

job属性配置：
disabled    作业是否禁止启动,可用于部署作业时，先禁止启动，部署结束后统一启动
overwrite    本地配置是否可覆盖注册中心配置，如果可覆盖，每次启动作业都以本地配置为准
cron    cron表达式，用于控制作业触发时间
description   作业描述信息
shardingTotalCount    作业分片总数
shardingItemParameters    分片序列号和参数用等号分隔，多个键值对用逗号分隔
jobParameter    作业自定义参数，可通过传递该参数为作业调度的业务方法传参，用于实现带参数的作业
failover    是否开启任务执行失效转移，开启表示如果作业在一次任务执行中途宕机，允许将该次未完成的任务在另一作业节点上补偿执行
misfire    是否开启错过任务重新执行
streamingProcess    是否流式处理数据
scriptCommandLine    脚本型作业执行命令行
monitorExecution    监控作业运行时状态
monitorPort    作业监控端口
maxTimeDiffSeconds    允许的本机与注册中心的时间误差秒数
jobShardingStrategyClass    作业分片策略实现类全路径,默认使用平均分配策略
reconcileIntervalMinutes    修复作业服务器不一致状态服务调度间隔时间，配置为小于1的任意值表示不执行修复,单位：分钟
listener    前置后置任务监听实现类，需实现ElasticJobListener接口
distributedListener    前置后置任务分布式监听实现类，需继承AbstractDistributeOnceElasticJobListener类
startedTimeoutMilliseconds    最后一个作业执行前的执行方法的超时时间,单位：毫秒
completedTimeoutMilliseconds    最后一个作业执行后的执行方法的超时时间,单位：毫秒
jobExceptionHandler    自定义异常处理类
executorServiceHandler    自定义业务处理线程池
```