# 文件类型判断使用说明  

##常见文件头信息 initFileType()   
   例如jpg的头为FFD8FF  
    ```
     FILE_TYPE_MAP.put("jpg", "FFD8FF");
    ```
## 获取图片文件实际类型 getImageFileType(File file)
 获取图片文件实际类型,若不是图片则返回null
 ```
 String type=getImageFileType(file);
 ```
## 获取文件类型 getFileTypeByFile(File file)  
 获取文件类型,包括图片,若格式不是已配置的(已配置的指在Map FILE_TYPE_MAP 中配置的),则返回null
 ```
 String type=getFileTypeByFile(file);
 ```
## 获取文件类型 getFileTypeByStream(byte[] bytes)  
根据byte[] bytes 获取文件类型
 ```
 String type=getFileTypeByFile(file);
 ```
## 判断文件是否为Image isImage(File file)

 ```
 String type=getFileTypeByFile(file);
 ```