#汉语转拼音工具类
 直接上例子
```
  String str= " ,./666Hello大家好，。、,./ " //这是我需要转换的一段汉语掺杂各种标点符号   
  String getFirstLetter = PinyinUtil.getFirstLetter(str); //返回值 ""
  String getFirstLetters = PinyinUtil.getFirstLetters(str, HanyuPinyinCaseType.LOWERCASE);//返回值 ,./666Hellodjh，。、,./
  String getFirstLettersLower = PinyinUtil.getFirstLettersLower(str);//返回值 ,./666Hellodjh，。、,./
  String getFirstLettersUp = PinyinUtil.getFirstLettersUp(str);//返回值 ,./666HelloDJH，。、,./
  String toHanyuPinyin = PinyinUtil.toHanyuPinyin(str);//返回值 ,./666Hellodajiahao，。、,./
  String getPinyinString = PinyinUtil.getPinyinString(str);//返回值 666Hellodajiahao
```
 1. getFirstLetter() //方法返回值 为空 ,所以：如果输入的第一个字符不是汉字就不会有返回值  
 2. getFirstLetters(str, HanyuPinyinCaseType.LOWERCASE) //第二个参数说要大写还是小写，标点符号不处理
 3. getFirstLettersLower(str) //返回汉字首字母小写，英文、标点符号不处理
 4. getFirstLettersUp(str) // 返回汉字首字母大写，英文、标点符号不处理
 5. toHanyuPinyin(str) //返回小写的汉语全拼,英文、标点符号不处理
 6. getPinyinString(str) //返回小写的汉语全拼，标点符号删除