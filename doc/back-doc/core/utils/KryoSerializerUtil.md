# Kryo 序列化工具使用说明
 提供对象的序列化和反序列化  
 - serialize(Object obj) 方法对对想对象进进行序列化，返回一个byte 数组
 - deserialize(byte[] bits) 将Kryo序列化后的byte数组进行反序列化  
 **需要注意的是:Kryo工具序列化后的对象，只能通过Kryo工具进行反序列化** 