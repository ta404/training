#（日期）时间工具类使用说明
日期时间工具类的主要功能范围为正对JDk（>1.8）自带的时间类，其中包括：
~~~
* java.util.Date
* java.sql.Date
* java.sql.Time
* java.sql.Timestamp
* java.time.LocalDateTime
* java.time.LocalDate 
* java.time.LocalTiem
~~~

等总共七个时间类的工具类方法。本时间工具类的使用方式主要从六个方面提供工具，
其中包括六个主要功能以及两个辅助工具：
* 获取时间（主要工具，访问方式 ==> DateUtil.*）
* 时间类之间的相互转换（主要工具， 访问方式 ==> DateUtil.Convert.*）
* 时间格式化（主要工具）
* 时间计算（主要工具）
* 时间判断（主要工具）
* 时区换算（主要工具）
* 内置格式转换器（辅助工具）
* 内置时区辅助工具（辅助工具）  

#模块说明
模块说明中简单的说明了各个时间工具类下的各个子工具的使用示例，以及利用工具可做的扩展思路。
##时间获取
时间获取主要提供了获取当前时间、用户自定义事件以及获取一年（或一月）中特殊的日子。
获取时间的访问方式通过：
~~~
DateUtil.*
~~~
进行访问，示例说明如下：
~~~
//获取当前时间
DateUtil.getCurrentDate();

//自定义时间获取
DateUtil.getDate(2018,9,8,13,12,12); //时间为 2018年9月8日 13时12分12秒

//获取参数时间所在月的总天数
DateUtil.getDaysOfMonth(java.util.Date date);

/**
 * 获取特殊时间（此处举一个示例 ==> 获取参数时间所在月的第一天）。
 * 参数时间为 2018年9月8日 13时12分12秒，返回时间为 2018年9月1日 13时12分12秒
 * 如果想要获取时间为 2018年9月1日 0时0分0秒，参数传入java.sql.Date（改类继承java.util.Date）
 *
 */
DateUtil.getFirstDayOfMonth(java.util.Date date); 
~~~

##日期时间类之间的转换
本模块的工具方法，提供了7个时间类之间的相互转换工具。该模块访问方式通过：
~~~
DateUtil.Convert.*
~~~
进行访问，示例说明如下：
~~~
//java.time.LocalDateTime To java.util.Date
DateUtil.Convert.localDateTimeToDate(java.time.LocalDateTime localDateTime);

//java.time.LacalDateTime To java.sql.Timestamp
DateUtil.Convert.localDateTimeToSQLTimestamp(java.time.LocalDateTime localDateTime);

//java.time.LocalDate To java.sql.Date
DateUtil.Convert.locaDateToSQLDate(java.time.localDate localDate);

//java.time.LocalTime To java.sql.Time
DateUtil.Convert.localTimeToSQLTime(java.time.LocalTime localTime);

//java.util.Date To java.sql.Timestamp
DateUtil.Convert.dateToSQLTimestamp(java.util.Date date);

//java.util.Date To java.sql.Date 
DateUtil.Convert.dateToSQLDate(java.util.Date date);

java.util.Date To java.sql.Time
DateUtil.Convert.dateToTime(java.util.Date date);
~~~

## 时间格式化
时间格式化提供了7大时间类与字符串之间的相互格式化工具。该模块的工具访问方式通过：
~~~
DateUtil.Format.*
~~~
进行访问，另外，该模块的使用依赖DateUtil.TimeFormat模块提供的时间转换器。相关示例如下：
~~~
//时间字符串
String dateStr = "2018-9-10 12:00:00";

//当前时间
java.util.Date date = DateUtil.getCurrentDate();

//匹配字符串
String patternStr = "yyyy-MM-dd HH:mm:ss";

/**
 * 若将字符串转换为时间（时间转换为字符串）时未提供指定时间转换器，则通过默认的时间转换器进行转换。
 * 对应的默认转换器为：
 * yyyy-MM-dd HH:mm:ss
 *          java.util.Date
 *          java.sql.Timestamp
 *          java.time.LocalDateTime
 * yyyy-MM-dd
 *          java.sql.Date
 *          java.time.LocalDate
 * HH:mm:ss
 *          java.sql.Time
 *          java.time.LocalTime 
 */
 //将字符串转为时间，通过默认转换器
 DateUtil.Format.stringToDate(dateStr);
 
 //将字符串转为时间，通过选定指定时间转换器
 DateUtil.Format.stringToDate(dateStr, DateUtil.TimeFormat.YYYY_MM_DD_HH_MM_SS_LINE);
 
 //将时间根据自定义的时间转换器转换为字符串
 DateUtil.Format.dateToString(date,DateUtil.TimeFormat.getCustomPattern(patternStr));
 
 //其余时间转换类似以上举例，当时间转换为字符串时，若未提供指定时间转换器，则依然通过默认对应转换器。
~~~

##时间计算
时间计算模块主要提供了针对时间的加（减）法计算，计算维度包括了最小精确到秒的计算以及按周为单位的计算。访问方式通过：
~~~
DateUtil.Calculate.*
~~~
进行访问，简单示例如下：
~~~
//获取当前时间,(假设：2018-09-10 13:57:58)
java.util.Date date = DateUtil.getCurrentDate();

//计算3年2个月12天3秒后的时间
DateUtil.Calculate.datePlusCal(date,3,2,12,0,0,3); //返回一个时间: 2021-11-22 13:58:01

//计算17周后的时间
DateUtil.Calculate.datePlusWeekCal(date,17); //返回一个时间： 2019-01-07 13:57:58
~~~

##时间判断
时间判断主要提供时间之间的判断，判断角度包含了从年、月、天、小时、分钟、秒等等。
其访问方式通过：
~~~
 DateUtil.Judge.*
~~~
方式进行访问。其中并非所有的角度都提供全部日期时间类的时间判断，可借助其中已提供的时间判断工具结合时间类的转换工具来满足需求。
示例如下：
~~~
java.util.Date firstDate = DateUtil.getDate(2018, 9, 10, 12, 0, 0);
java.util.Date secondDate = DateUtil.getDate(2019, 10, 9, 12, 0, 0);
//从年的角度判断两个时间之间的大小，
DateUtil.Judge.intervalResultInYear(firstDate, secondDate); //返回结果: 1

//从月的角度比较两个时间之间的大小
DateUtil.Judge.intervalResultInMonth(firstDate, secondDate); //返回结果: 12

//从天的角度比较两个时间的大小
DateUtil.Judge.intervalResultInDay(firstDate, secondDate); //返回结果: 394

//从小时的角度比较两个时间的大小
DateUtil.Judge.intervalResultInHour(firstDate, secondDate); //返回结果: 9456

//从分钟的角度比较两个时间大小
DateUtil.Judge.intervalResultInMinute(firstDate, secondDate); //返回结果: 567360

//从毫秒比较两个时间的大小
DateUtil.Judge.intervalResultInMill(firstDate, secondDate); //返回结果: 34041600000

~~~

##时区换算
时区换算工具模块，提供了主要三类工具：
* 根据时区获取当前时间
* 根据提供当前默认时区的时间和目的时区，计算目的时区的对应时间
* 根据提供时间，时间所在的时区，以及目的时区，计算目的时区的时间  

其访问方式通过：
~~~
DateUtil.ZoneConvert.*
~~~
方式进行访问，该模块需要借助辅助工具 DateUtil.Zone.*.zoneId. 具体示例如下：
~~~
//根据时区获取时间
DateUtil.ZoneConvert.getDateByZone(DateUtil.Zone.ECT.getZoneId()); //获取时区为（ECT ==> "Europe/Paris"）的时间

java.util.Date date = DateUtil.getDate(2008, 5, 12, 14, 28, 0); //时间为"2008-5-12 14:28:0"
//根据时间以及目标时区获取时间
DateUtil.ZoneConvert.getDateByZone(date, DateUtil.Zone.ECT.getZoneId()); //当系统时间为"2008-5-12 14:28:0"时，在ECT时区时间为"2008-05-12 08:28:00"

//根据时间，时间所在时区，目标时区
DateUtil.ZoneConvert.getDateByZone(date, DateUtil.Zone.ECT.getZoneId(), DateUtil.Zone.CTT.getZoneId()); //当ECT时区的时间为"2008-5-12 14:28:0"时，CTT时区的时间为"2008-05-12 20:28:00"。

//自定义时区(例如自定义时区为：ECT ==> "Europe/Paris")，共有两种方式
DateUtil.ZoneConvert.getDateByZone(date, ZoneId.of("Europe/Paris"));
DateUtil.ZoneConvert.getDateByZone(date, TimeZone.getTimeZone("ECT").toZoneId(););
~~~