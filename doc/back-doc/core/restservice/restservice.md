# restService使用说明

<!-- TOC -->

- [restService使用说明](#restservice使用说明)
    - [@restService注解](#restservice注解)
    - [`ITAHandlerInterceptor`](#itahandlerinterceptor)
    - [自定义拦截器与配置](#自定义拦截器与配置)
    - [`BaseRestService`使用](#baserestservice使用)
    - [自定义应用拦截器](#自定义应用异常拦截器)

<!-- /TOC -->


##  @restService注解

restService注解可以理解为controller层,其实际也是controller的作用,只是不返回页面,只返回json数据

具体使用见如下:**该注解只能在类的顶部进行使用**,

- 如果想返回的数据是json格式

	```java
	@RestService
	public class BaseRestServiceTest{ 	
        @RequestMapping("/demo")
        public ResultBean demo(){
            return new ResultBean();
        }
	}
	```
	
- 如下是返回一个hello页面

	```java
	@RestService
	public class BaseRestServiceTest {
	
	    @RequestMapping("/")
	    public void index(HttpServletResponse response) throws IOException {
	        PrintWriter out = response.getWriter();
	        out.write("<html>");
	        out.write("<h1>Hello World,Welcome to Ta404!!!</h1>\n");
	        out.write("</html>");
	        out.flush();
	        out.close();
	    }
	 }
	```


- 当前类部分方法返回json,部分方法返回页面,此种方法不建议使用

	```java
	//注意这里不再是RestService
	@Controller 
	public class BaseRestServiceTest{
	    
	    //返回index界面,具体后缀根据springboot的配置文件进行读取
	    @RequestMapping("/")
	    public String index(){
	        return "index";
	    }
	    
	    //返回resultBean的json数据
	    @RequestMapping("/demo")
	    @ResponseBody
	    public ResultBean demo(){
	        return new ResultBean();
	    }
	}
	
	```
	
- 映射基础路径,只要进行相应的设置值即可,使用方法和`RequestMapping`一样

    ```java
    //该类指定,http请求映射的路径在/user下,里面的方法映射为/user/*,只能使用post方法进行请求,并且请求格式为json格式
    @RestService(value = "/user",method = RequestMethod.POST,produces = "application/json")
    public class BaseRestServiceTest{}

    ```
	
**Tip:建议方法名和映射的路径保持一致**

## `ITAHandlerInterceptor`
`ITAHandlerInterceptor`是所有拦截器的基础类,所有框架自定义拦截器都要实现该类,如果有相应的业务操作,可重写相应的方法进行逻辑书写,该类提供的可重写的方法有:

![ITAHandlerInterceptor.png](img/ITAHandlerInterceptor.png)

## 自定义拦截器与配置
使用步骤:

1. 实现`ITAHandlerInterceptor`

	```java
	public class DemoInterceptor implements ITAHandlerInterceptor {
	
	    @Override
	    public boolean preHandle(HttpServletRequest request,
	                             HttpServletResponse response, Object handler) throws Exception {
	       //TODO sth
	        return true;
	    }
	
	    @Override
	    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
			//TODO sth
	    }
	}
	```
2. 配置拦截规则

	在`Ta4InterceptorConfig`的`addInterceptors`方法中进行配置,具体的拦截方案可参考springboot的相关资料:
	[mvc-config-interceptors](https://docs.spring.io/spring/docs/5.0.8.RELEASE/spring-framework-reference/web.html#mvc-config-interceptors)

	```
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new DemoInterceptor()).addPathPatterns("/**");
    }
	```

## `BaseRestService`使用

`BaseRestService`实现了`IBaseRestService`接口,所有restService类必须继承`BaseRestService`,或自行创建一个`baseRestService`
eg:`userBaseRestService`继承`BaseRestService`,其他继承`userBaseRestService`来进行扩展,目前提供的方法有如下,具体详情参考`BaseRestService`:

![BaseRestService.png](img/BaseRestService.png)

实例demo:

```java
@RestService
public class DemoRestService extends BaseRestService{
    
    @RequestMapping("/")
    public ResultBean index(){
        setData("field1", "aaaa");
        setData("field2", "vvvv");
        setError("500","server internal error");
        setErrors(new String[]{"server inter err","there is a big err"});
        return result();
    }
}
```

## 自定义应用异常拦截器
应用拦截器可以对系统中抛出的异常进行捕获处理,并进行相应的记录同时返回给用户可以理解的信息,是整个应用中比较核心的部分

实例demo:

```java
/**
 * 应用拦截器,对抛出的异常进行拦截处理
 */
@ControllerAdvice
public class DemoRestServiceExceptionInterceptor extends BaseRestService {
    private static final Logger logger = LoggerFactory.getLogger(RestServiceExceptionInterceptor.class);
	
    @ExceptionHandler(value = OtherException.class)
    @ResponseBody
    public ResultBean exceptionHandler(HttpServletRequest request,
                                       HttpServletResponse response,
                                       HandlerMethod handlerMethod,
                                       OtherException otheeException,
                                       Model model) {
        //TODO sth
        return result();
    }

	
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResultBean exceptionHandler(HttpServletRequest request,
                                       HttpServletResponse response,
                                       HandlerMethod handlerMethod,
                                       Exception exception,
                                       Model model) {
      	//TODO sth
        return result();
    }
}
```




