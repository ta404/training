
# 集群通信

## 集群通信使用说明

集群通信，提供集群server之间发送消息以及接受到消息后处理消息的功能，如缓存信息的同步<br>
集群通信分别有三种实现方式：
1. jgroup-udp：通过jgroup广播形式，对网络环境有一定要求，需要在同一个局域网内发送
2. jgroup-tcp: 通过jgroup tcp形式，这种模式，需要依赖注册中心，将集群每个server的信息放入注册中心
3. http： 通过http形式发送，这种模式，要在配置文件中配置每个server的地址


## 配置说明
1. 在application.yml文件中进行配置
2. 具体配置说明：
```
ta404: 
  cluster:
     name: ta404_Cluster
     enable: true  # 是否启用集群通信
     default: jgroup-tcp #可配置为jgroup-udp(广播形式),jgroup-tcp(tcp+注册中心)，http
     jgroup-udp:
     jgroup-tcp:
     http:
       address: 192.168.29.37:8081   # 在mode为http时可配置，配置为每个server的ip加端口号,每个server配置由逗号分隔
```
## 接口说明
### 集群通信接口 IClusterMessage
```
    /**
     * 初始化连接
     */
    void connect();
```
```
    /**
     * 按对象发送消息
     * @param obj 消息对象
     */
    void send(T  obj);
```
```
    /**
     * 按字节数组发送消息
     * @param bytes 消息字节数组
     */
    void send(byte[] bytes);
```
```
    /**
     * 关闭通信
     */
    void close();
```
### 集群通信接收接口 IClusterReceiver
```
    /**
     * 序列化模式
     * @param command 接收消息后要处理的指令
     */
    void doCommand(IClusterCommand command);
```
```
    /**
     * json 模式
     * @param jsonStr 接收到消息后处理的json字符串
     */
    void doCommand(String jsonStr);
```
