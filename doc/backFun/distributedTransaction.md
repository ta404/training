# 分布式事务使用说明
框架提供的分布式事务功能  
* 功能介绍
分布式事务 用于控制微服务之间事务 以及 本地多数据源事务
* 使用方法
    1. 依赖模块
     ```
      <dependency>
                 <groupId>com.yinhai.ta404</groupId>
                 <artifactId>ta404-module-lcn-start</artifactId>
                 <version>5.0.0-SNAPSHOT</version>
             </dependency>
     ```
     2. 部署事务管理器 txmanager, txmanager 依赖 eureka，redis  
     解压文件 tx-manager-x.x.x.zip 直接修改配置文件application.properties 
     * eureka.client.service-url.defaultZone 为eureka地址
     * spring.redis.cluster.nodes 为redis地址  
     java -jar 运行 tx-manager-x.x.x.jar
     
     3. 修改项目application.yml 文件，属性为true ，地址为上一步txmanager部署地址
     ```
        distribute:
           startLcn: true
           txManagerUrl: http://127.0.0.1:8899/tx/manager/ 
     ```
     4. 启用分布式事务  
     service代码中使用@TaTransactional注解，并且设置属性useLcn = true
     ```
     @Service
     @TaTransactional(useLcn = true)
     public class UserServiceImpl implements UserService {
     ```
     调用的另一个事务的service 也同样需设置
     ```
     @Service
     @TaTransactional(useLcn = true)
     public class TestUserServiceImpl implements TestUserService {
     ```