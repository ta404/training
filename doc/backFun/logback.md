
# 日志配置说明

## 日志使用说明

框架默认提供logback作为日志输出框架
项目可根据实际情况，自定义日志输出方案。

## 配置说明
1. 在application.yml文件中进行配置
2. 具体配置说明：
```
ta404: 
  log:
    output-type: console,file,kafka   # 日志输出目的地，目前支持控制台、文件、kafka，如果需要增加别的输出，可以扩展
    package-level:    # 日志级别，按包名区分
      com.yinhai: DEBUG    
      org.springframework.boot: INFO
          org.springframework.boot: INFO
    appender-type:    #appender类型，默认提供三种appender（console、file、kafka）
      console:    # 控制台配置
        appender-loader-class: com.yinhai.ta404.core.log.logback.config.ConsoleAppenderLoader    # 控制台配置加载器类名
        format: "%clr(%d{HH:mm:ss.SSS}){faint} %clr(%-5level) %clr(%logger{36}){cyan} %clr(:){faint} %clr(%msg%n){faint}"    #日志输出格式
        use-all-package-level: false    # 是否使用package-level所配置的包和包的日志级别（true：使用，false：不使用）
        monitor-packages:    # 需要把日志输出到控制台的包（注意：在ta404.log.input.level里配置了的包，才能在这里配置）
          - com.yinhai
          - org.springframework.boot
      file:    # 文件配置
        appender-loader-class: com.yinhai.ta404.core.log.logback.config.RollingFileAppenderLoader    # 文件配置加载器类名
        fileNamePattern: F:/test/file_%d{yyyy-MM-dd}_%i.log    # 配置日志文件的路径和命名规则
        format: "%d{HH:mm:ss.SSS} [%thread] %-5level %logger{36} : %msg%n"    # 日志输出格式
        maxHistory: 30    # 文件最大保留天数
        maxFileSize: 1kb    # 单个文件最大容量(单位可以是kb,mb,gb)
        clearSize: 5kb    # 所有文件最大容量(单位可以是kb,mb,gb)
        use-all-package-level: false    # 是否使用package-level所配置的包和包的日志级别（true：使用，false：不使用）
        monitor-packages:    # 同上
          - com.yinhai
          - org.springframework.boot
      kafka:    # kafka配置
        appender-loader-class: com.yinhai.ta404.core.log.logback.config.KafkaAppenderLoader    # kafka配置加载器类名
        format: "%msg%n"    # 日志输出格式
        topic: ta404    # 日志输出到kafka的topic名称
        producerConfig: bootstrap.servers=192.168.128.135:9092    # kafka服务的地址
        async: false    # 是否是异步发送
        use-all-package-level: false    # 是否使用package-level所配置的包和包的日志级别（true：使用，false：不使用）
        monitor-packages:    # 同上
          - com.yinhai
```