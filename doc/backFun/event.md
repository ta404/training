# 事件使用说明   
事件发布提供两种方式  
事件监听提供一种方式
  
###  一. 注解式
-  事件发布   
 @Send("command-1") 表示发布了一条ID为 command-1 的事件  
 事件内容为 return内容 可接收事件内容为Object类型，即任何类型
```
    @Send("command-1")
    @RequestMapping("/event")
    public String test() {
        return "发布了一个事件"; //比如这个地方可以返回一个user对象
    }
```
- 事件监听  
1. 事件监听需要实现AbstractEventHandler接口  
2. @Consumer("command-1") 监听Id为command-1 的事件 此时是一个同步监听  
3. 若注解 @Consumer(value = "command-1", async = true) 表示异步监听
```
@Consumer("command-1")
@Component
public class ConsumerHandler1 implements AbstractEventHandler {
    @Override
    public void onEvent(IEventDisruptor eventDisruptor, long l, boolean b) {
        IEventMessage eventMessage = eventDisruptor.getEventMessage();
        Object eventSource = eventMessage.getEventSource(); // 此处为事件发布时return的值
        Assert.assertEquals("发布了一条命令",eventSource); //测试代码忽略

    }
}
```
### 二.代码式
- 事件发布
```
 @Resource
 private EventPublish eventPublish; //装配事件发布类

 public void saveUser() {
        User user = new User();
        user.setId("1111");
        eventPublish.publish(user, "command-1");//user 为事件内容，command-1为事件Id
 }
```
- 事件监听  
 只有注解式，同上