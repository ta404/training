# livebase 交互设计

由于现在前后台完全分离,所以后台不再控制前台页面跳转.现在推荐前台只发起post请求,后台只返回json数据.

## 交互流程

#### 一.dev-mock(前台-doclever-后台)

前台使用npm run dev-mock启动.

- 前台发起一个请求,请求被代理到doclever,如果接口不存在或者接口已完成,则跳转到实际后台地址进行访问
- 前台发起一个请求,请求被代理到doclever,如果接口是开发中,则直接返回

#### 二.dev(前台-后台)

修改faceConfig.js中的basePath为后台地址,前台使用npm run dev启动

- 前台发起一个请求,请求直接放问后台实际地址进行访问

## 请求地址配置说明

- 后台端口,在后端bootstrap.yml中的sever.port中定义
- 前台端口,在前端config目录下的index.js中的,module.exports下的port中定义
- mock地址,在前端faceConfig.js中的faceConfig下的mockServer中定义
- 实际后台地址,在前端faceConfig.js中的devServer中定义

## 前端交互

请求统一要求使用this.Base.submit,具体写法如下

````
       let submitParam = {
          url: "org/orguser/userManagementRestService!updateBatchUserDisabledByUserIds.do",
          data: {"userIds": userIds.join(",")}
        };
        let callBack = {
          successCallback: (data) => {
            this.userList = dataSource.filter((item) => {
              if (userIds.indexOf(item.userId) > -1) {
                item.effective = 0;
              }
              return item;
            });
            //提示
            this.$message.success('禁用操作成功');
            this.disableVisible = false;
            this.oneDisableVisible = false;
          }
        };
        this.Base.submit(null, submitParam, callBack);
````

this.Base.submit方法api如下

![formUtil.jpg](img/formUtil.jpg)

## 后端交互

主要是RestService的处理,需要继承BaseRestService.

1. 接收前台数据

````
//a.通过变量接收
public void deleteBatchUserByUserIds(String userIds)

//b.通过对象接收,如果对象中的字段名称匹配成功,则会直接将属性注入
public void queryBatchUserByOrgId(TaUserOrgKey taUserOrgKey, PageParam pageParam)

//c.从request中获取
String checkCode = (String) request.getParameter("checkCode");
````

2. 返回数据

返回数据推荐使用框架封装的ResultBean对象,在restservice中步骤如下:

````
//a.方法返回类型为void
public void getConfig(HttpServletRequest request)

//b.设置数据setData
setData("configMap",configMap);

//c.设置异常
setError("原始密码错误", "error");
setSuccess(false);
````

## doclever使用

#### 一.连接指定doclever

doclever地址:http://120.76.132.137:10000/mock/5c11188186a5543c05577f39

1.注册并登陆
2.选中或新建一个项目

![docproject.jpg](img/docproject.jpg)

3.点击设置,Mock

![mock.jpg](img/mock.jpg)

4.将Mock Server地址配置在前端faceConfig.js中的faceConfig下的mockServer中

#### 二.配置接口

1.新建分组

![interface.jpg](img/interface.jpg)

2.新建接口

新建接口
![interface2.jpg](img/interface2.jpg)

设置接口名称,接口状态,请求类型,请求地址
![newInterface.jpg](img/newInterface.jpg)

类似postman,可以设置请求头,在body里面设置请求参数,允许设置参数名,参数类型,参数说明,参数默认值,必选等
![interface3.jpg](img/interface3.jpg)

设置接口的返回值,可以使用导入json直接生成结果
![interface4.jpg](img/interface4.jpg)

完成上述步骤之后点击右上角的保存按钮即可

#### 三.调试

1.调试前端

将接口的请求类型设置成 开发中 状态,然后前端用dev-mock启动,直接请求对应接口,会自动返回你设置在接口中的返回值

2.调试后端

类似postman,选中接口,点击右上角的运行按钮
![yunxing.jpg](img/yunxing.jpg)

可以选择请求类型,请求的后端地址,请求路径,请求参数和具体指

再点击右上角的运行,可以在下方查看到后台返回的结果