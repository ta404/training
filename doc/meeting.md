# 视频会议使用办法
- 下载chrome浏览器
- 通过视频会议连接地址访问 https://yinhai.webex.com.cn/yinhai/onstage/g.php?MTID=e61f443fcffbd6fc817f33e6b2b26008e
- 安装chrome相关插件
- 如图填写相应信息，秘密 1234

![会议信息](../resource/meeting-1.jpg)

- 加入会议
- 打开语音选择电脑语音

![会议信息](../resource/meeting-2.jpg)