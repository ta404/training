//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.runqian.report4.model.engine;

import com.runqian.base4.resources.EngineMessage;
import com.runqian.base4.resources.MessageManager;
import com.runqian.base4.util.CustomReportError;
import com.runqian.base4.util.MD5;
import com.runqian.base4.util.NStringMap;
import com.runqian.base4.util.Queue;
import com.runqian.base4.util.ReportError;
import com.runqian.base4.util.Stack;
import com.runqian.base4.util.StringUtils;
import com.runqian.report4.dataset.DataSet;
import com.runqian.report4.dataset.DsValue;
import com.runqian.report4.model.CalcReport;
import com.runqian.report4.model.CellLocation;
import com.runqian.report4.model.Matrix;
import com.runqian.report4.model.NormalCell;
import com.runqian.report4.model.ReportDefine;
import com.runqian.report4.model.Seal;
import com.runqian.report4.model.expression.Expression;
import com.runqian.report4.model.expression.Variant2;
import com.runqian.report4.usermodel.Area;
import com.runqian.report4.usermodel.BackGraphConfig;
import com.runqian.report4.usermodel.Context;
import com.runqian.report4.usermodel.DataSetMetaData;
import com.runqian.report4.usermodel.ExportConfig;
import com.runqian.report4.usermodel.ICell;
import com.runqian.report4.usermodel.IColCell;
import com.runqian.report4.usermodel.INormalCell;
import com.runqian.report4.usermodel.IRecord;
import com.runqian.report4.usermodel.IReport;
import com.runqian.report4.usermodel.IReportListener;
import com.runqian.report4.usermodel.IRowCell;
import com.runqian.report4.usermodel.MacroMetaData;
import com.runqian.report4.usermodel.ParamMetaData;
import com.runqian.report4.usermodel.PrintSetup;
import com.runqian.report4.usermodel.SubReportMetaData;
import com.runqian.report4.usermodel.input.FlowVar;
import com.runqian.report4.usermodel.input.InputProperty;
import com.runqian.report4.util.ByteArrayInputRecord;
import com.runqian.report4.util.ByteArrayOutputRecord;
import com.runqian.report4.util.ICellStyle;
import com.runqian.report4.util.ICellStyleManager;
import com.runqian.report4.util.MacroResolver2;
import com.runqian.report4.util.StyleConfig;
import com.yinhai.ta404.core.utils.ServiceLocator;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.ResourceLoader;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Map.Entry;

public class ExtCellSet implements IReport, Cloneable{
    private static final long serialVersionUID = 3036008453099297658L;
    private static final short _$1 = 1;
    private static final short _$2 = 2;
    private static final short _$3 = 4;
    private static ArrayList _$4 = new ArrayList(20);
    Matrix _$5;
    private Matrix _$6;
    private Queue _$7;
    private lIIIllIIIIlIIlII _$8;
    private Map _$9;
    private ExtCell _$10;
    private ExtCell _$11;
    private Context _$12 = null;
    private llIllIIIIlIIlIII _$13 = new llIllIIIIlIIlIII();
    private Area _$14 = null;
    private ExtCellSet _$15 = null;
    private List _$16 = null;
    private int _$17 = 0;
    private int _$18 = 0;
    private byte _$19 = 0;
    private byte _$20 = 0;
    private short _$21 = 0;
    private volatile boolean _$22 = false;
    private ExtExtendCell _$23 = null;
    private ReportDefine _$24;
    private boolean _$25 = false;
    private boolean _$26 = false;
    private boolean _$27 = false;
    private String _$28 = null;
    private ICellStyleManager _$29 = null;
    private String _$30;
    private static Object _$31 = new Object();
    private static ExtCellSet _$32 = new ExtCellSet();
    private static String _$33 = null;
    private static boolean _$34 = false;
    private static boolean _$35 = false;
    public static final byte LIC_IDE = 0;
    public static final byte LIC_SERVER = 1;
    public static final byte LIC_CSHARP = 2;
    public static final byte QUIEE = 10;
    public static final byte RUNQIAN = 11;
    public static final byte SERVER_TRAIL = 20;
    public static final byte SERVER_DEVELOPMENT = 21;
    public static final byte SERVER_FORMAL = 22;
    private static int _$36 = (int)(System.currentTimeMillis() / 1000L);
    private byte _$37 = 0;
    private byte _$38 = 10;
    private String _$39;
    private String _$40;
    private String _$41;
    private String _$42;
    private String _$43;
    private String _$44;
    private String _$45;
    private String _$46;
    private String _$47;
    private String _$48;
    private String _$49;
    private String _$50;
    private int _$51 = 1;
    private int _$52 = -1;
    private String _$53;
    private int[] _$54;
    private long _$55;
    private short _$56 = 1;
    private String _$57 = "0.0.0.0";
    private short _$58 = 1;
    private short _$59 = 1;
    private long _$60 = 0L;
    private static Random _$61 = new Random();
    private static int _$62 = 3;

    public ExtCellSet() {
    }

    public ExtCellSet(IReport var1) {
        this._$24 = (ReportDefine)var1;
        int var2 = var1.getRowCount() + 1;
        int var3 = var1.getColCount() + 1;
        this._$5 = new Matrix(var2, var3);
        this._$6 = new Matrix(var2, var3);
        this._$23 = new ExtExtendCell(this, 0, (short)0, 1, (short)1, new NormalCell());
        this._$5.set(0, 0, this._$23);
        this._$6.set(0, 0, this._$23.engine);

        for(int var4 = 1; var4 < var2; ++var4) {
            IRowCell var5;
            if ((var5 = var1.getRowCell(var4)).getRowType() == -90) {
                this._$21 = (short)(this._$21 | 2);
            }

            ExtRowCell var6 = new ExtRowCell(this, var4, var5);
            this._$5.set(var4, 0, var6);
            this._$6.set(var4, 0, var6.engine);
        }

        for(short var17 = 1; var17 < var3; ++var17) {
            IColCell var18 = var1.getColCell(var17);
            ExtColCell var7 = new ExtColCell(this, var17, var18);
            this._$5.set(0, var17, var7);
            this._$6.set(0, var17, var7.engine);
        }

        for(int var19 = 1; var19 < var2; ++var19) {
            for(short var20 = 1; var20 < var3; ++var20) {
                INormalCell var8;
                if ((var8 = var1.getCell(var19, var20)) != null) {
                    if (!var8.isMerged()) {
                        ExtNormalCell var21 = new ExtNormalCell(this, var19, var20, 1, (short)1, var8);
                        this._$5.set(var19, var20, var21);
                        this._$6.set(var19, var20, var21.engine);
                    } else {
                        Area var9;
                        int var10 = (var9 = var8.getMergedArea()).getBeginRow();
                        short var11 = var9.getBeginCol();
                        if (var10 == var19 && var11 == var20) {
                            int var12 = var9.getEndRow();
                            short var13 = var9.getEndCol();
                            ExtNormalCell var14 = new ExtNormalCell(this, var19, var20, var12 - var19 + 1, (short)(var13 - var20 + 1), var8);

                            for(int var15 = var10; var15 <= var12; ++var15) {
                                for(short var16 = var11; var16 <= var13; ++var16) {
                                    this._$5.set(var15, var16, var14);
                                    this._$6.set(var15, var16, var14.engine);
                                }
                            }
                        }

                        var20 = (short)(var20 + (var9.getEndCol() - var11));
                    }
                }
            }
        }

        if (var1.isSigned()) {
            this._$21 = (short)(this._$21 | 1);
        }

    }

    private void _$1() {
        int var1 = this._$5.getRowSize();
        int var2 = this._$5.getColSize();

        for(int var3 = 1; var3 < var1; ++var3) {
            for(short var4 = 1; var4 < var2; ++var4) {
                INormalCell var5;
                ICellStyleManager var6;
                String var7;
                if ((var5 = (INormalCell)this._$5.get(var3, var4)) != null && (var6 = this.getCssStyleManager()) != null && var5 != null && (var7 = var5.getCellStyleName()) != null && !"".equalsIgnoreCase(var7.trim())) {
                    ICellStyle var8;
                    if ((var8 = var6.getCellStyle(var7)) == null) {
                        throw new ReportError("单元格样式名为：" + var7 + "的样式不存在！");
                    }

                    this.setCellStyle(var5, var8, this._$24, var3, var4);
                }
            }
        }

    }

    void _$1(int var1) {
        int var2 = this._$5.getColSize();
        int var3 = this._$5.getRowSize();

        for(int var4 = 1; var4 < var2; ++var4) {
            ExtCell var5;
            if ((var5 = (ExtCell)this._$5.get(var1, var4)) != null && var5.getRowMerge() > 1 && var5.getCol() == var4) {
                var5.addRowMerge(-1);
            }
        }

        for(int var8 = var1 + 1; var8 < var3; ++var8) {
            for(int var6 = 0; var6 < var2; ++var6) {
                ExtCell var7;
                if ((var7 = (ExtCell)this._$5.get(var8, var6)) != null) {
                    if (var7.getRow() == var8 && var7.getCol() == var6) {
                        var7.addRow(-1);
                    }

                    var6 += var7.getColMerge() - 1;
                }
            }
        }

        this._$5.deleteRow(var1);
    }

    void _$1(int var1, int var2) {
        this._$5.insertRows(var1, var2);
        this._$17 += var2;
        int var3 = this._$5.getColSize();
        int var4 = this._$5.getRowSize();

        for(int var5 = 0; var5 < var3; ++var5) {
            for(int var6 = var1 + var2; var6 < var4; ++var6) {
                ExtCell var7;
                int var8;
                if ((var7 = (ExtCell)this._$5.get(var6, var5)) != null && (var8 = var7.getRow()) >= var1) {
                    if (var6 == var8 + var2 && var5 == var7.getCol()) {
                        var7.addRow(var2);
                        var8 += var2;
                    }

                    if ((var8 += var7.getRowMerge() - 1) > var6) {
                        var6 = var8;
                    }
                }
            }
        }

    }

    void _$1(int var1, int var2, int var3, int var4, ExtCell var5) {
        for(int var6 = var1; var6 < var3; ++var6) {
            for(int var7 = var2; var7 < var4; ++var7) {
                this._$5.set(var6, var7, var5);
            }
        }

    }

    private short _$1(int var1, short var2, int var3) {
        short var4 = var2;
        boolean var5 = true;

        while(var5) {
            var5 = false;
            ExtNormalCell var6;
            if ((var6 = (ExtNormalCell)this._$5.get(var1 + var3, var4)) != null && var6.isMerged()) {
                short var7 = var4;
                if ((var4 = var6.getCol()) < var7 && (var6 = (ExtNormalCell)this._$5.get(var1, var4)) != null && var6.isMerged()) {
                    var7 = var4;
                    if ((var4 = var6.getCol()) < var7) {
                        var5 = true;
                    }
                }
            }
        }

        return var4;
    }

    private int _$1(int var1, short var2, short var3) {
        int var4 = var1;
        boolean var5 = true;

        while(var5) {
            var5 = false;
            ExtNormalCell var6;
            if ((var6 = (ExtNormalCell)this._$5.get(var4, var2 + var3)) != null && var6.isMerged()) {
                int var7 = var4;
                if ((var4 = var6.getRow()) < var7 && (var6 = (ExtNormalCell)this._$5.get(var4, var2)) != null && var6.isMerged()) {
                    var7 = var4;
                    if ((var4 = var6.getRow()) < var7) {
                        var5 = true;
                    }
                }
            }
        }

        return var4;
    }

    ExtCellSet _$1(int var1, short var2, boolean var3) {
        ExtCellSet var4;
        (var4 = new ExtCellSet())._$24 = this._$24;
        var4._$19 = this._$19;
        var4._$21 = this._$21;
        var4._$6 = this._$6;
        var4._$23 = this._$23;
        var4._$12 = this._$12;
        var4._$5 = new Matrix(var1, var2);
        ExtCell var5 = null;
        var4._$5.set(0, 0, this._$23);

        for(int var6 = 1; var6 < var1; ++var6) {
            var5 = (ExtCell)this._$5.get(var6, 0);
            if (var3) {
                var5 = (ExtCell)((ExtRowCell)var5).deepClone();
            }

            var4._$5.set(var6, 0, var5);
        }

        for(short var7 = 1; var7 < var2; ++var7) {
            var5 = (ExtCell)this._$5.get(0, var7);
            if (var3) {
                var5 = (ExtCell)((ExtColCell)var5).deepClone();
            }

            var4._$5.set(0, var7, var5);
        }

        for(int var8 = 1; var8 < var1; ++var8) {
            for(short var9 = 1; var9 < var2; ++var9) {
                if ((var5 = (ExtCell)this._$5.get(var8, var9)) != null) {
                    int var10 = var5.getRow();
                    int var11 = var5.getRowMerge();
                    short var12 = var5.getCol();
                    short var13 = var5.getColMerge();
                    if (var10 == var8 && var12 == var9) {
                        if (var3) {
                            var5 = (ExtCell)((ExtNormalCell)var5).deepClone();
                        }

                        if (var11 <= 1 && var13 <= 1) {
                            var4._$5.set(var8, var9, var5);
                        } else {
                            for(int var14 = 0; var14 < var11; ++var14) {
                                for(int var15 = 0; var15 < var13; ++var15) {
                                    var4._$5.set(var14 + var8, var15 + var9, var5);
                                }
                            }
                        }
                    }

                    var9 = (short)(var9 + (var13 - 1));
                }
            }
        }

        return var4;
    }

    private int _$1(ExtCell var1) {
        int var2 = var1.getRow();
        int var3 = 0;

        for(int var4 = ((ExtNormalCell)var1).getLeftSubSize(); var3 < var4; ++var3) {
            ExtCell var5 = ((ExtNormalCell)var1).getLeftSubCell(var3);
            int var6;
            if ((var6 = this._$1(var5)) < var2) {
                var2 = var6;
            }
        }

        return var2;
    }

    private void _$1(ExtCellSet var1, Area var2) {
        var1._$15 = this;
        var1._$14 = var2;
        Stack var3;
        (var3 = new Stack(256)).reset();
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();

        for(int var6 = 1; var6 < var4; ++var6) {
            label41:
            for(int var7 = 1; var7 < var5; ++var7) {
                ExtCell var8;
                if ((var8 = (ExtCell)this._$5.get(var6, var7)) != null && var8.getRow() == var6 && var8.getCol() == var7) {
                    while(true) {
                        while(true) {
                            try {
                                var1._$10 = var8;
                                this._$10 = var8;
                                this._$13.setCell(var8, this._$23);
                                var8.calculate(this._$12);
                                if (var3.empty()) {
                                    continue label41;
                                }

                                var8 = (ExtCell)var3.pop();
                            } catch (lllllllIllIIIlII var12) {
                                byte var10 = var12.getState();
                                ExtCell var11 = var12.getCell();
                                var3.push(var8);
                                var8 = var11;
                            }
                        }
                    }
                }
            }
        }

        this._$1(this._$12, true);
    }

    private ExtCell _$1(ExtNormalCell var1) {
        String var2;
        ExtCell var3;
        if ((var2 = var1.getLeftHead()) != null && (var2 = var2.trim()).length() > 0 && (var3 = this.getSource(var2)) != null) {
            if (!var1.isLeftHeadOf(var3)) {
                return var3;
            } else {
                MessageManager var9 = EngineMessage.get();
                Stack var10 = new Stack(256);
                ExtCell var11 = var3;
                var10.push(var1);

                while(var11 != null && var1 != var11) {
                    var10.push(var11);
                    var11 = var11.getCellLeftHead();
                }

                lllllllIllIIIlII var12;
                (var12 = new lllllllIllIIIlII(var1, (byte)3)).setStack(var10, var9);
                throw new ReportError(var9.getMessage("ExtCellSet.cell") + var12.getCell().getSourceId() + var9.getMessage("ExtCellSet.hasError") + var12.getMessage(), var12);
            }
        } else {
            int var8 = var1.getRow();

            for(int var4 = var1.getCol() - 1; var4 >= 1; --var4) {
                ExtNormalCell var5;
                if ((var5 = (ExtNormalCell)this._$5.get(var8, var4)) != null) {
                    byte var6 = var5.getExtensible();
                    if (!var5.isMerged() || var6 == 18 || var5.getRow() == var8) {
                        if (var5.containRow(var1) && var6 == 18) {
                            if (!var1.isLeftHeadOf(var5)) {
                                return var5;
                            }
                        } else {
                            ExtCell var7;
                            if ((var7 = var5.getCellLeftHead()) != null && var7 != var1 && var7 != this._$23 && var7 != this._$10 && var7.getCellLeftHead() == null) {
                                var7.setCellLeftHead(this._$1((ExtNormalCell)var7));
                            }

                            if (var7 != null && !var1.isLeftHeadOf(var7)) {
                                return var7;
                            }
                        }
                    }
                }
            }

            return this._$23;
        }
    }

    private void _$1(Context var1) {
        if (this._$19 <= 4) {
            Stack var2 = new Stack(256);

            try {
                this.prepareCalculate(var1, true);
                this._$10 = null;
                if (!this.isSigned()) {
                    this._$3(var1);
                }

                this._$9 = var1.getAllParamMap();
                Queue var3 = new Queue();
                this._$7 = new Queue();
                this._$8 = new lIIIllIIIIlIIlII();
                this._$17 = this._$5.getRowSize();
                this._$18 = this._$5.getColSize();
                this._$23.setValue((Object)null);
                this._$1(var1, var2);
                this.resetDSCurrent(var1);
                this.setDSCurrent(var1);
                this._$13.reset();
                this._$1(var1, var3, var2);
                this.resetDSCurrent(var1);
                this.setDSCurrent(var1);
                this._$13.reset();
                this._$1(var1, false);
                this._$1((IReport)this._$24, (Context)var1);
                this._$1();
                this._$8();
                IlIIlIlIIlllIIII.perfect(this, var1);
                this.resetDSCurrent(var1);
                this.setDSCurrent(var1);
                this._$13.reset();
                this._$2(var1, var3, var2);
                if (this._$24.getInput() == 1) {
                    this.resetDSCurrent(var1);
                    this.setDSCurrent(var1);
                    this._$13.reset();

                    for(int var9 = 1; var9 < this._$17; ++var9) {
                        for(int var5 = 1; var5 < this._$18; ++var5) {
                            ExtNormalCell var6;
                            if ((var6 = (ExtNormalCell)this._$5.get(var9, var5)) != null && var6.getRow() == var9 && var6.getCol() == var5 && !(var6.getValue() instanceof ExtCellSet)) {
                                this._$10 = var6;
                                this._$13.setCell(var6, this._$23);
                                var6.calcInput(var1);
                            }
                        }
                    }
                }

                this._$19 = 7;
            } catch (ReportError var7) {
                if (this._$10 != null && !(var7 instanceof CustomReportError)) {
                    var7.setCellId(this._$10.getSourceId());
                }

                throw var7;
            } catch (Exception var8) {
                MessageManager var4 = EngineMessage.get();
                if (var8 instanceof lllllllIllIIIlII) {
                    ((lllllllIllIIIlII)var8).setStack(var2, var4);
                    throw new ReportError(var4.getMessage("ExtCellSet.cell") + ((lllllllIllIIIlII)var8).getCell().getSourceId() + var4.getMessage("ExtCellSet.hasError") + var8.getMessage(), var8);
                } else {
                    throw new ReportError(var4.getMessage("ExtCellSet.cell") + (this._$10 != null ? this._$10.getSourceId() : "") + var4.getMessage("ExtCellSet.hasError") + var8.getMessage(), var8);
                }
            }
        }
    }

    private void _$1(Context var1, Queue var2, Stack var3) {
        this._$19 = 5;
        var3.reset();

        for(int var4 = 1; var4 < this._$17; ++var4) {
            boolean var5 = false;
            if (get().getVersion() == 10 && _$62 > 0 && var4 % _$62 == 0) {
                var5 = true;
            }

            label91:
            for(int var7 = 1; var7 < this._$18; ++var7) {
                this._$2();
                ExtCell var8;
                if ((var8 = (ExtCell)this._$5.get(var4, var7)) != null && var8.getExtensible() == 17 && var8.getRow() == var4 && var8.getCol() == var7) {
                    while(true) {
                        while(true) {
                            try {
                                this._$10 = var8;
                                this._$13.setCell(var8, this._$23);
                                String var9;
                                if (var5 && (var9 = (String)((ExtNormalCell)var8).engine.srcPrepare.getExpMap().get((byte)40)) != null && var9.trim().length() > 0) {
                                    var9 = MacroResolver2.replaceMacros(var9, var1);
                                    ((ExtNormalCell)var8).engine.expression = new Expression(((ExtNormalCell)var8).engine.cs, var1, var9);
                                }

                                var8.calculate(var1);
                                if (var3.empty()) {
                                    continue label91;
                                }

                                var8 = (ExtCell)var3.pop();
                            } catch (lllllllIllIIIlII var12) {
                                byte var10 = var12.getState();
                                ExtCell var11 = var12.getCell();
                                if (var10 != 4 && var10 != 5) {
                                    if (var10 == 6) {
                                        var11.reset();
                                        var2.push(var8);
                                        continue label91;
                                    }

                                    if (var10 != 8) {
                                        var3.push(var8);
                                        throw var12;
                                    }

                                    ((ExtNormalCell)var8).setCalState((byte)8);
                                    var11.reset();
                                    if (var11.getSource() == var11 && this._$7.indexOf(var11) < 0) {
                                        ((ExtNormalCell)var11).setCalState((byte)8);
                                        this._$7.push(var11);
                                    }

                                    while(true) {
                                        if (var3.empty()) {
                                            continue label91;
                                        }

                                        (var11 = (ExtCell)var3.pop()).reset();
                                        if (var11.getSource() == var11 && this._$7.indexOf(var11) < 0) {
                                            ((ExtNormalCell)var11).setCalState((byte)8);
                                            this._$7.push(var11);
                                        }
                                    }
                                }

                                var3.push(var8);
                                var8 = var11;
                            }
                        }
                    }
                }
            }
        }

    }

    private void _$1(Context var1, Stack var2) {
        this._$19 = 4;

        int var4;
        for(int var3 = 1; var3 < this._$17; ++var3) {
            label97:
            for(var4 = 1; var4 < this._$18; ++var4) {
                this._$2();
                ExtCell var5;
                if ((var5 = (ExtCell)this._$5.get(var3, var4)) != null && var5.getExtensible() == 19 && var5.getRow() == var3 && var5.getCol() == var4) {
                    while(true) {
                        while(true) {
                            try {
                                this._$10 = var5;
                                this._$13.setCell(var5, this._$23);
                                var5.calculate(var1);
                                if (var2.empty()) {
                                    continue label97;
                                }

                                var5 = (ExtCell)var2.pop();
                            } catch (lllllllIllIIIlII var11) {
                                byte var7 = var11.getState();
                                ExtCell var8 = var11.getCell();
                                if (var7 != 4) {
                                    var2.push(var5);
                                    throw var11;
                                }

                                var2.push(var5);
                                var5 = var8;
                            }
                        }
                    }
                }
            }
        }

        for(var4 = 1; var4 < this._$17; ++var4) {
            label67:
            for(int var12 = 1; var12 < this._$18; ++var12) {
                this._$2();
                ExtCell var6;
                if ((var6 = (ExtCell)this._$5.get(var4, var12)) != null && var6.getExtensible() == 18 && var6.getRow() == var4 && var6.getCol() == var12) {
                    while(true) {
                        while(true) {
                            try {
                                this._$10 = var6;
                                this._$13.setCell(var6, this._$23);
                                var6.calculate(var1);
                                if (var2.empty()) {
                                    continue label67;
                                }

                                var6 = (ExtCell)var2.pop();
                            } catch (lllllllIllIIIlII var10) {
                                byte var13 = var10.getState();
                                ExtCell var9 = var10.getCell();
                                if (var13 != 4) {
                                    var2.push(var6);
                                    throw var10;
                                }

                                var2.push(var6);
                                var6 = var9;
                            }
                        }
                    }
                }
            }
        }

    }

    private void _$1(Context var1, boolean var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();

        for(int var5 = 1; var5 < var3; ++var5) {
            ExtCell var6 = (ExtCell)this._$5.get(var5, 0);
            this._$10 = var6;
            var6.calcProperties(var1, this._$8, this._$7, var2);
        }

        for(int var10 = 1; var10 < var4; ++var10) {
            ExtCell var7 = (ExtCell)this._$5.get(0, var10);
            this._$10 = var7;
            var7.calcProperties(var1, this._$8, this._$7, var2);
        }

        for(int var11 = 1; var11 < var3; ++var11) {
            for(int var8 = 1; var8 < var4; ++var8) {
                this._$2();
                ExtNormalCell var9;
                if ((var9 = (ExtNormalCell)this._$5.get(var11, var8)) != null && var9.getRow() == var11 && var9.getCol() == var8 && !(var9.getValue() instanceof ExtCellSet)) {
                    this._$10 = var9;
                    if (!var2) {
                        this._$13.setCell(var9, this._$23);
                    }

                    var9.calcProperties(var1, this._$8, this._$7, var2);
                } else if (var9 != null && var9.getValue() instanceof ExtCellSet && var9.engine != null && var9.engine.cellGraphExpression != null && var9.getCellGraphConfig() != null) {
                    this._$10 = var9;
                    if (!var2) {
                        this._$13.setCell(var9, this._$23);
                    }

                    var9.calcProperties(var1, this._$8, this._$7, var2);
                }
            }
        }

    }

    private void _$1(IReport var1, Context var2) {
        String var3;
        if ((var3 = var1.getReportStyleName()) != null && var3.indexOf("=") == 0) {
            String var5 = MacroResolver2.replaceMacros(var3.substring(1), var2);
            Object var6 = Variant2.getValue((new Expression(var2, var5)).calculate(var2, false), false, false);
            this._$28 = Variant2.toString(var6);
        }

    }

    private void _$1(InputStream var1) throws Exception {
        MessageManager var14;
        if ((char)_$2(var1) != 'R') {
            var14 = EngineMessage.get();
            throw new RuntimeException(var14.getMessage("ExtCellSet.invalidLicense"));
        } else if ((char)_$2(var1) != 'Q') {
            var14 = EngineMessage.get();
            throw new RuntimeException(var14.getMessage("ExtCellSet.invalidLicense"));
        } else if ((char)_$2(var1) != 'Q') {
            var14 = EngineMessage.get();
            throw new RuntimeException(var14.getMessage("ExtCellSet.invalidLicense"));
        } else if ((char)_$2(var1) != 'R') {
            var14 = EngineMessage.get();
            throw new RuntimeException(var14.getMessage("ExtCellSet.invalidLicense"));
        } else {
            this._$37 = _$2(var1);
            ByteArrayOutputStream var3 = new ByteArrayOutputStream(1024);
            byte[] var4 = new byte[1024];
            int var5 = 1;

            while(var5 > 0) {
                if ((var5 = var1.read(var4)) > 0) {
                    var3.write(var4, 0, var5);
                }
            }

            byte[] var6 = var3.toByteArray();
            byte[] var7;
            byte[] var8;
            ByteArrayInputStream var9;
            if (this._$37 == 0) {
                var7 = RSA2.decrypt1(var6);
                var8 = new byte[16];
                System.arraycopy(var7, 0, var8, 0, 16);
                var4 = new byte[var7.length - 16];
                System.arraycopy(var7, 16, var4, 0, var4.length);
                if (!_$1(MD5.get(var4), var8)) {
                    MessageManager var16 = EngineMessage.get();
                    throw new RuntimeException(var16.getMessage("ExtCellSet.hashValueError"));
                } else {
                    var9 = new ByteArrayInputStream(var4);
                    this._$38 = _$2((InputStream)var9);
                    this._$39 = _$7(var9);
                    this._$40 = _$7(var9);
                    this._$41 = _$7(var9);
                    this._$42 = _$7(var9);
                    this._$43 = _$7(var9);
                    this._$44 = _$7(var9);
                    this._$45 = _$7(var9);
                    this._$46 = _$7(var9);
                    this._$47 = _$7(var9);
                    this._$48 = _$7(var9);
                    this._$1(this._$48);
                    this._$51 = _$4((InputStream)var9);
                    this._$52 = _$4((InputStream)var9);
                    this._$55 = _$5(var9);
                    this._$60 = _$5(var9);
                }
            } else if (this._$37 == 2) {
                MessageManager var15 = EngineMessage.get();
                throw new RuntimeException(var15.getMessage("ExtCellSet.licenseCSharp"));
            } else {
                var7 = new byte[128];
                System.arraycopy(var6, 0, var7, 0, 128);
                var8 = RSA2.decrypt1(var7);
                var9 = new ByteArrayInputStream(var8);
                this._$51 = _$4((InputStream)var9);
                this._$52 = _$4((InputStream)var9);
                byte[] var10 = new byte[16];
                _$1((InputStream)var9, (byte[])var10);
                this._$54 = _$6(var9);
                byte[] var11 = new byte[var6.length - 128];
                System.arraycopy(var6, 128, var11, 0, var6.length - 128);
                byte[] var12;
                if (!_$1(MD5.get(var12 = RSA2.decrypt1(var11)), var10)) {
                    MessageManager var13 = EngineMessage.get();
                    throw new RuntimeException(var13.getMessage("ExtCellSet.hashValueError"));
                } else {
                    var9 = new ByteArrayInputStream(var12);
                    this._$38 = _$2((InputStream)var9);
                    this._$39 = _$7(var9);
                    this._$40 = _$7(var9);
                    this._$41 = _$7(var9);
                    this._$42 = _$7(var9);
                    this._$43 = _$7(var9);
                    this._$44 = _$7(var9);
                    this._$45 = _$7(var9);
                    this._$46 = _$7(var9);
                    this._$47 = _$7(var9);
                    this._$48 = _$7(var9);
                    this._$1(this._$48);
                    this._$53 = _$7(var9);
                    this._$55 = _$5(var9);
                    this._$56 = _$3((InputStream)var9);
                    this._$57 = _$7(var9);
                    this._$58 = _$3((InputStream)var9);
                    this._$59 = _$3((InputStream)var9);
                    this._$60 = _$5(var9);
                }
            }
        }
    }

    private static byte[] _$1(InputStream var0, byte[] var1) throws IOException {
        if (var1.length != var0.read(var1)) {
            throw new IOException("byte array len invalid");
        } else {
            return var1;
        }
    }

    private void _$1(String var1) {
        if (var1 != null) {
            int var2;
            if ((var2 = var1.indexOf(0)) >= 0) {
                this._$48 = var1.substring(0, var2);
                int var3;
                if ((var3 = var1.indexOf(0, var2 + 1)) < 0) {
                    this._$49 = var1.substring(var2 + 1);
                } else {
                    this._$49 = var1.substring(var2 + 1, var3);
                    this._$50 = var1.substring(var3);
                }
            }
        }
    }

    private String _$1(String var1, int var2) {
        StringBuffer var3 = new StringBuffer();

        for(int var4 = 0; var4 < var1.length(); ++var4) {
            var3.append((char)(var1.charAt(var4) + var2));
        }

        return var3.toString();
    }

    void _$1(short var1) {
        int var2 = this._$5.getColSize();
        int var3 = this._$5.getRowSize();

        for(int var4 = 1; var4 < var3; ++var4) {
            ExtCell var5;
            if ((var5 = (ExtCell)this._$5.get(var4, var1)) != null && var5.getColMerge() > 1 && var5.getRow() == var4) {
                var5.addColMerge((short)-1);
            }
        }

        for(int var9 = 0; var9 < var3; ++var9) {
            for(int var6 = var1 + 1; var6 < var2; ++var6) {
                ExtCell var7;
                if ((var7 = (ExtCell)this._$5.get(var9, var6)) != null) {
                    if (var7.getRow() == var9 && var7.getCol() == var6) {
                        var7.addCol((short)-1);
                    }

                    short var8;
                    if ((var8 = var7.getColMerge()) > 1) {
                        if (var7.getCol() == var6 - 1) {
                            var6 += var8 - 1;
                        } else {
                            var6 = var7.getCol() + var8;
                        }
                    }
                }
            }
        }

        this._$5.deleteCol(var1);
    }

    private void _$1(short var1, short var2) {
        this._$1(var1, var2, this._$3(), this._$4(), true);
    }

    void _$1(short var1, short var2, int var3, int var4, boolean var5) {
        this._$5.insertCols(var1, var2, var3, var4, var5);
        this._$18 += var2;
        int var6 = this._$5.getColSize();
        int var7;
        if (var5) {
            for(var7 = var1 + var2; var7 < var6; ++var7) {
                ExtCell var8;
                if ((var8 = (ExtCell)this._$5.get(0, var7)) != null) {
                    var8.addCol(var2);
                }
            }
        }

        for(var7 = var3; var7 <= var4; ++var7) {
            for(int var11 = var1 + var2; var11 < var6; ++var11) {
                ExtCell var9;
                int var10;
                if ((var9 = (ExtCell)this._$5.get(var7, var11)) != null && var9.getRow() == var7 && (var10 = var9.getCol()) >= var1) {
                    if (var11 == var10 + var2 && var7 == var9.getRow()) {
                        var9.addCol(var2);
                        var10 += var2;
                    }

                    if ((var10 += var9.getColMerge() - 1) > var11) {
                        var11 = var10;
                    }
                }
            }
        }

    }

    private static boolean _$1(byte[] var0, byte[] var1) {
        if (var0 != null && var1 != null) {
            if (var0.length != var1.length) {
                return false;
            } else {
                for(int var2 = 0; var2 < var0.length; ++var2) {
                    if (var0[var2] != var1[var2]) {
                        return false;
                    }
                }

                return true;
            }
        } else {
            return false;
        }
    }

    private final byte _$10() {
        int var1 = 0;
        HashMap var2 = new HashMap();
        byte var3 = this.getInput();
        int var4 = this._$6.getColSize();
        ExtCellSet var5;
        if (!(var5 = get()).getSemanticsEnabled() && this._$26) {
            return 9;
        } else {
            int var6 = 1;

            for(int var7 = this._$6.getRowSize(); var6 < var7; ++var6) {
                for(int var8 = 1; var8 < var4; ++var8) {
                    NormalCellEngine var9;
                    if ((var9 = (NormalCellEngine)this._$6.get(var6, var8)) != null && var9.srcCurrent.getRow() == var6 && var9.srcCurrent.getCol() == var8) {
                        int var10;
                        if (var9.expression != null) {
                            if (!var5.getMultiDSEnabled()) {
                                ((Expression)var9.expression).putDataSet(var2);
                                if (var2.size() > 1) {
                                    return 2;
                                }
                            }

                            if (!var5.getCoordinateEnabled()) {
                                if (((Expression)var9.expression).isLayerCo()) {
                                    return 3;
                                }

                                if (((Expression)var9.expression).isLineCo()) {
                                    return 4;
                                }

                                if (((Expression)var9.expression).isPlaneCo()) {
                                    return 5;
                                }
                            }

                            if (!var5.getSpecialFunctionEnabled() && ((Expression)var9.expression).isDelayFun()) {
                                return 7;
                            }

                            if (!var5.getSpecialGroupEnabled() && ((Expression)var9.expression).isSpecialGroup()) {
                                return 8;
                            }

                            if (!var5.getAddedDSEnabled() && var9.addExpression != null) {
                                for(var10 = 0; var10 < var9.addExpression.length; ++var10) {
                                    if (var9.addExpression[var10] != null) {
                                        return 10;
                                    }
                                }
                            }
                        }

                        if (!var5.getMultiInputEnabled() && var3 == 1 && var9.propInput != null) {
                            InputProperty var11;
                            if ((var10 = var9.propInput.getUpdateCount()) > 0 && var9.srcPrepare.getCellType() == -62 && (var11 = var9.srcPrepare.getInputProperty()) != null && var11.getEditStyle() == 7) {
                                --var10;
                            }

                            if ((var1 += var10) > 1) {
                                return 6;
                            }
                        }

                        var8 += var9.srcCurrent.getColMerge() - 1;
                    }
                }
            }

            return 0;
        }
    }

    private static void _$11() {
    }

    private void _$2() {
        if (this._$22) {
            throw new ReportError("Calculation is interrupted!");
        }
    }

    private boolean _$2(int var1) {
        if (var1 == 0) {
            return true;
        } else {
            ExtRowCell var2;
            if ((var2 = (ExtRowCell)this._$5.get(var1, 0)) == null) {
                return true;
            } else {
                byte var3;
                return (var3 = var2.getRowType()) == -91 || var3 == -95 || var3 == -94 || var3 == -90;
            }
        }
    }

    private short _$2(int var1, short var2, int var3) {
        short var4 = var2;
        boolean var5 = true;

        while(var5) {
            var5 = false;
            ExtNormalCell var6;
            if ((var6 = (ExtNormalCell)this._$5.get(var1 + var3, var4)) != null && var6.isMerged()) {
                short var7 = var4;
                if ((var4 = (short)(var6.getCol() + var6.getColMerge() - 1)) > var7 && (var6 = (ExtNormalCell)this._$5.get(var1, var4)) != null && var6.isMerged()) {
                    var7 = var4;
                    if ((var4 = (short)(var6.getCol() + var6.getColMerge() - 1)) > var7) {
                        var5 = true;
                    }
                }
            }
        }

        return var4;
    }

    private int _$2(int var1, short var2, short var3) {
        int var4 = var1;
        boolean var5 = true;

        while(var5) {
            var5 = false;
            ExtNormalCell var6;
            if ((var6 = (ExtNormalCell)this._$5.get(var4, var2 + var3)) != null && var6.isMerged()) {
                int var7 = var4;
                if ((var4 = var6.getRow() + var6.getRowMerge() - 1) > var7 && (var6 = (ExtNormalCell)this._$5.get(var4, var2)) != null && var6.isMerged()) {
                    var7 = var4;
                    if ((var4 = var6.getRow() + var6.getRowMerge() - 1) > var7) {
                        var5 = true;
                    }
                }
            }
        }

        return var4;
    }

    private int _$2(ExtCell var1) {
        int var2 = var1.getRow() + var1.getRowMerge();
        int var3 = 0;

        for(int var4 = ((ExtNormalCell)var1).getLeftSubSize(); var3 < var4; ++var3) {
            ExtCell var5 = ((ExtNormalCell)var1).getLeftSubCell(var3);
            int var6;
            if ((var6 = this._$2(var5)) > var2) {
                var2 = var6;
            }
        }

        return var2;
    }

    void _$2(ExtCellSet var1, Area var2) {
        if (this._$19 == 7 && (this._$21 & 4) != 0) {
            if (this != var1) {
                this._$19 = 8;
            }

            try {
                if (var1._$19 == 7) {
                    var1._$2(this._$12);
                    var1._$19 = 8;
                }

                this._$1(var1, var2);
            } catch (ReportError var5) {
                if (this._$10 != null && !(var5 instanceof CustomReportError)) {
                    var5.setCellId(this._$10.getSourceId());
                }

                throw var5;
            } catch (Throwable var6) {
                if (this._$10 == null) {
                    throw new ReportError(var6.getMessage(), var6.getCause());
                }

                MessageManager var4 = EngineMessage.get();
                throw new ReportError(var4.getMessage("ExtCellSet.cell") + this._$10.getSourceId() + var4.getMessage("ExtCellSet.hasError") + var6.getMessage(), var6);
            }
        }

        this._$9();
    }

    private ExtCell _$2(ExtNormalCell var1) {
        String var2;
        ExtCell var3;
        if ((var2 = var1.getTopHead()) != null && (var2 = var2.trim()).length() > 0 && (var3 = this.getSource(var2)) != null) {
            if (!var1.isTopHeadOf(var3)) {
                return var3;
            } else {
                MessageManager var9 = EngineMessage.get();
                Stack var10 = new Stack(256);
                ExtCell var11 = var3;
                var10.push(var1);

                while(var11 != null && var1 != var11) {
                    var10.push(var11);
                    var11 = var11.getCellTopHead();
                }

                lllllllIllIIIlII var12;
                (var12 = new lllllllIllIIIlII(var1, (byte)3)).setStack(var10, var9);
                throw new ReportError(var9.getMessage("ExtCellSet.cell") + var12.getCell().getSourceId() + var9.getMessage("ExtCellSet.hasError") + var12.getMessage(), var12);
            }
        } else {
            short var8 = var1.getCol();

            for(int var4 = var1.getRow() - 1; var4 >= 1; --var4) {
                ExtNormalCell var5;
                if ((var5 = (ExtNormalCell)this._$5.get(var4, var8)) != null) {
                    byte var6 = var5.getExtensible();
                    if (!var5.isMerged() || var6 == 19 || var5.getCol() == var8) {
                        if (var1.sameRowTypeAs(var5) && var5.containCol(var1) && var6 == 19) {
                            if (!var1.isTopHeadOf(var5)) {
                                return var5;
                            }
                        } else if (var1.sameRowTypeAs(var5)) {
                            ExtCell var7;
                            if ((var7 = var5.getCellTopHead()) != null && var7 != var1 && var7 != this._$23 && var7 != this._$10 && var7.getCellTopHead() == null) {
                                var7.setCellTopHead(this._$2((ExtNormalCell)var7));
                            }

                            if (var7 != null && !var1.isTopHeadOf(var7)) {
                                return var7;
                            }
                        }
                    }
                }
            }

            return this._$23;
        }
    }

    private void _$2(Context var1) {
        int var2 = this._$6.getRowSize();
        int var3 = this._$6.getColSize();

        for(int var4 = 1; var4 < var2; ++var4) {
            RowCellEngine var5;
            if ((var5 = (RowCellEngine)this._$6.get(var4, 0)).srcCurrent != null) {
                var5.srcCurrent.prepProperties(var1);
            }
        }

        for(int var9 = 1; var9 < var3; ++var9) {
            ColCellEngine var6;
            if ((var6 = (ColCellEngine)this._$6.get(0, var9)).srcCurrent != null) {
                var6.srcCurrent.prepProperties(var1);
            }
        }

        for(int var10 = 1; var10 < var2; ++var10) {
            for(int var7 = 1; var7 < var3; ++var7) {
                NormalCellEngine var8;
                if ((var8 = (NormalCellEngine)this._$6.get(var10, var7)) != null && var8.srcCurrent != null && var8.srcCurrent.getSourceRow() == var10 && var8.srcCurrent.getSourceCol() == var7) {
                    if (var8.expression instanceof String) {
                        var8.expression = new Expression(this, var1, (String)var8.expression);
                    } else if (var8.expression instanceof Expression && var8.state == 8) {
                        var8.expression = new Expression(this, var1, ((Expression)var8.expression).toString());
                    }

                    var8.srcCurrent.prepProperties(var1);
                }
            }
        }

    }

    private void _$2(Context var1, Queue var2, Stack var3) {
        this._$19 = 6;
        ExtCell var4 = null;

        while(!var2.empty()) {
            try {
                var4 = (ExtCell)var2.pop();
                this._$10 = var4;
                this._$13.setCell(var4, this._$23);
                var4.calculate(var1);
            } catch (lllllllIllIIIlII var6) {
                if (var6.getState() != 6) {
                    var3.push(var4);
                    throw var6;
                }

                var2.push(var4);
            }
        }

    }

    private static byte _$2(InputStream var0) throws IOException {
        int var1;
        if ((var1 = var0.read()) < 0) {
            throw new EOFException();
        } else {
            return (byte)var1;
        }
    }

    void _$2(short var1, short var2) {
        this._$5.insertCols(var1, var2);
        this._$18 += var2;
        int var3 = this._$5.getColSize();
        int var4 = this._$5.getRowSize();

        for(int var5 = 1; var5 < var4; ++var5) {
            IRowCell var6;
            if ((var6 = (IRowCell)this._$5.get(var5, 0)).getRowType() == -91 || var6.getRowType() == -90 || var6.getRowType() == -94 || var6.getRowType() == -95) {
                for(int var7 = var1 + var2; var7 < var3; ++var7) {
                    ExtCell var8;
                    int var9;
                    if ((var8 = (ExtCell)this._$5.get(var5, var7)) != null && var8.getRow() == var5 && (var9 = var8.getCol()) >= var1) {
                        if (var7 == var9 + var2 && var5 == var8.getRow()) {
                            var8.addCol(var2);
                            var9 += var2;
                        }

                        if ((var9 += var8.getColMerge() - 1) > var7) {
                            var7 = var9;
                        }
                    }
                }
            }
        }

    }

    int _$3() {
        int var1 = this._$5.getRowSize();

        for(int var2 = 1; var2 < var1; ++var2) {
            if (this._$2(var2)) {
                return var2;
            }
        }

        return -1;
    }

    private static final String _$3(int var0) {
        return var0 <= 0 ? "without limit" : (new SimpleDateFormat("yyyy-MM-dd")).format(new Date((long)var0 * 1000L));
    }

    private int _$3(ExtCell var1) {
        int var2 = var1.getCol();
        int var3 = 0;

        for(int var4 = ((ExtNormalCell)var1).getTopSubSize(); var3 < var4; ++var3) {
            ExtCell var5 = ((ExtNormalCell)var1).getTopSubCell(var3);
            int var6;
            if ((var6 = this._$3(var5)) < var2) {
                var2 = var6;
            }
        }

        return var2;
    }

    private ExtExtendCell _$3(ExtNormalCell var1) {
        ExtExtendCell var2;
        (var2 = (ExtExtendCell)this._$23.clone()).row = var1.row;
        var2.rowMerge = var1.rowMerge;
        var2.col = var1.col;
        var2.colMerge = var1.colMerge;
        var2.leftHead = var1.leftHead;
        var2.topHead = var1.topHead;
        var2.cellValue = var1.cellValue;
        var2.engine = var1.engine;
        var2.propSetMap = null;
        var2.engine.srcCurrent = var2;
        var2._$1 = null;
        var2._$2 = null;
        var2._$3 = null;
        var2._$4 = null;
        var2._$5 = null;
        return var2;
    }

    private boolean _$3(Context var1) {
        byte var2;
        if ((var2 = this._$10()) > 1) {
            MessageManager var13 = EngineMessage.get();
            throw new ReportError(var13.getMessage("ExtCellSet.nonsupport") + CalcReport.getNonlinearCause(var2));
        } else {
            ExtCellSet var3;
            if ((var3 = get()).getType() == 0) {
                return true;
            } else {
                short var4;
                if ((var4 = var3.getNonlinearReportNum()) < 0) {
                    return false;
                } else if (this.getNonlinear() <= 1) {
                    return false;
                } else {
                    long var6 = 0L;
                    int var8 = this._$5.getColSize();
                    int var9 = this._$5.getRowSize();

                    for(int var10 = 1; var10 < var9; ++var10) {
                        for(int var11 = 1; var11 < var8; ++var11) {
                            ExtNormalCell var12;
                            if ((var12 = (ExtNormalCell)this._$5.get(var10, var11)) != null && var12.engine.expression != null && var12.getCellType() == -64) {
                                var6 += (long)(((Expression)var12.engine.expression).toString().hashCode() * var10 + var11);
                            }
                        }
                    }

                    Long var14 = new Long(var6);
                    if (_$4.indexOf(var14) == -1) {
                        if (_$4.size() >= var4) {
                            MessageManager var15 = EngineMessage.get();
                            throw new ReportError(var15.getMessage("ExtCellSet.complicated"));
                        }

                        _$4.add(var14);
                    }

                    return true;
                }
            }
        }
    }

    private static short _$3(InputStream var0) throws IOException {
        int var1 = var0.read();
        int var2 = var0.read();
        if ((var1 | var2) < 0) {
            throw new EOFException();
        } else {
            return (short)((var1 << 8) + var2);
        }
    }

    int _$4() {
        for(int var2 = this._$5.getRowSize() - 1; var2 >= 1; --var2) {
            if (this._$2(var2)) {
                return var2;
            }
        }

        return -1;
    }

    private int _$4(ExtCell var1) {
        int var2 = var1.getCol() + var1.getColMerge();
        int var3 = 0;

        for(int var4 = ((ExtNormalCell)var1).getTopSubSize(); var3 < var4; ++var3) {
            ExtCell var5 = ((ExtNormalCell)var1).getTopSubCell(var3);
            int var6;
            if ((var6 = this._$4(var5)) > var2) {
                var2 = var6;
            }
        }

        return var2;
    }

    private static int _$4(InputStream var0) throws IOException {
        int var1 = var0.read();
        int var2 = var0.read();
        int var3 = var0.read();
        int var4 = var0.read();
        if ((var1 | var2 | var3 | var4) < 0) {
            throw new EOFException();
        } else {
            return (var1 << 24) + (var2 << 16) + (var3 << 8) + var4;
        }
    }

    private void _$5() {
        int var1 = this._$6.getRowSize();
        int var2 = this._$6.getColSize();

        for(int var3 = 1; var3 < var1; ++var3) {
            RowCellEngine var4;
            if ((var4 = (RowCellEngine)this._$6.get(var3, 0)).srcCurrent != null) {
                var4.srcCurrent.revertProperties();
            }
        }

        for(int var8 = 1; var8 < var2; ++var8) {
            ColCellEngine var5;
            if ((var5 = (ColCellEngine)this._$6.get(0, var8)).srcCurrent != null) {
                var5.srcCurrent.revertProperties();
            }
        }

        for(int var9 = 1; var9 < var1; ++var9) {
            for(int var6 = 1; var6 < var2; ++var6) {
                NormalCellEngine var7;
                if ((var7 = (NormalCellEngine)this._$6.get(var9, var6)) != null && var7.srcCurrent != null && var7.srcCurrent.getSourceRow() == var9 && var7.srcCurrent.getSourceCol() == var6) {
                    var7.srcCurrent.revertProperties();
                }
            }
        }

    }

    private static long _$5(InputStream var0) throws IOException {
        return ((long)_$4(var0) << 32) + ((long)_$4(var0) & 4294967295L);
    }

    private void _$6() {
        this._$10 = null;
        this._$11 = null;
        int var1 = this._$6.getRowSize();
        int var2 = this._$6.getColSize();

        for(int var3 = 1; var3 < var1; ++var3) {
            ((RowCellEngine)this._$6.get(var3, 0)).srcCurrent.clearProperties(this._$8);
        }

        for(int var4 = 1; var4 < var2; ++var4) {
            ((ColCellEngine)this._$6.get(0, var4)).srcCurrent.clearProperties(this._$8);
        }

        for(int var5 = 1; var5 < var1; ++var5) {
            for(int var6 = 1; var6 < var2; ++var6) {
                NormalCellEngine var7;
                if ((var7 = (NormalCellEngine)this._$6.get(var5, var6)) != null && var7.srcCurrent != null && var7.srcCurrent.getSourceRow() == var5 && var7.srcCurrent.getSourceCol() == var6) {
                    var7.addExpression = null;
                    var7.propInput = null;
                    var7.srcCurrent.clearProperties(this._$8);
                    if (((ExtNormalCell)var7.srcCurrent).getCellType() == -60) {
                        ((ExtCellSet)((ExtNormalCell)var7.srcCurrent).getValue())._$6();
                    }
                }
            }
        }

    }

    private static int[] _$6(InputStream var0) throws IOException {
        short var1;
        if ((var1 = _$3(var0)) == 0) {
            return null;
        } else {
            int[] var2 = new int[var1];

            for(int var3 = 0; var3 < var1; ++var3) {
                var2[var3] = _$4(var0);
            }

            return var2;
        }
    }

    private void _$7() {
        int var1 = this._$5.getRowSize();
        int var2 = this._$5.getColSize();

        for(int var3 = 1; var3 < var1; ++var3) {
            for(int var4 = 1; var4 < var2; ++var4) {
                ExtNormalCell var5;
                ExtCellSet var6;
                if ((var5 = (ExtNormalCell)this._$5.get(var3, var4)) != null && var5.getRow() == var3 && var5.getCol() == var4 && var5.getCellType() == -60 && (var6 = (ExtCellSet)var5.getValue()).getSubRptType() == 2) {
                    var6._$7();
                    (new llIIIllIllIllIlI(this, var5, var6))._$3();
                }
            }
        }

    }

    private static String _$7(InputStream var0) throws IOException {
        short var1;
        if ((var1 = _$3(var0)) == 0) {
            return null;
        } else {
            StringBuffer var2 = new StringBuffer(var1);

            for(int var3 = 0; var3 < var1; ++var3) {
                var2.append((char)_$3(var0));
            }

            return var2.toString();
        }
    }

    private void _$8() {
        int var1 = this._$5.getRowSize();
        int var2 = this._$5.getColSize();
        if (var1 > 1) {
            int var3;
            int var4;
            for(var4 = var2 - 1; var4 > 1; --var4) {
                boolean var5 = false;

                ExtNormalCell var6;
                for(var3 = 1; var3 < var1 && ((var6 = (ExtNormalCell)this._$5.get(var3, var4)) == null || var6.getCellType() == -59); ++var3) {
                    var5 = true;
                }

                if (var3 != var1) {
                    break;
                }
            }

            if (var4 < var2 - 1) {
                this._$5.deleteCols(var4 + 1, var2 - var4 - 1);

                for(var3 = 1; var3 < var1; ++var3) {
                    ExtNormalCell var7;
                    Area var8;
                    if ((var7 = (ExtNormalCell)this._$5.get(var3, var4)) != null && var7.getCellType() != -59 && (var8 = var7.getMergedArea()) != null && var8.getEndCol() > var4) {
                        var8.setEndCol((short)var4);
                        var7.setMergedArea(var8);
                    }
                }
            }
        }

    }

    private void _$9() {
        int var1 = this._$5.getRowSize() - 1;
        int var2 = this._$5.getColSize() - 1;

        for(int var3 = 1; var3 <= var1; ++var3) {
            for(int var4 = 1; var4 <= var2; ++var4) {
                ExtNormalCell var5;
                if ((var5 = (ExtNormalCell)this._$5.get(var3, var4)) != null) {
                    int var6 = var5.getRow();
                    short var7 = var5.getCol();
                    if (var6 == var3 && var7 == var4) {
                        int var8 = var6 + var5.getRowMerge();
                        int var9 = var7 + var5.getColMerge();
                        ExtNormalCell var10;
                        if (var8 <= var1 && (var10 = (ExtNormalCell)this._$5.get(var8, var4)) != null) {
                            if (var10.getTBStyle() != 80) {
                                IRowCell var12 = this.getRowCell(var8);
                                if (this.getPageBorderMode() != 0 || var12.getRowType() != -92) {
                                    var5.setBBColor(var10.getTBColor());
                                    var5.setBBStyle(var10.getTBStyle());
                                    var5.setBBWidth(var10.getTBWidth());
                                }
                            } else if (var5.getBBStyle() != 80) {
                                IRowCell var13 = this.getRowCell(var8);
                                if (this.getPageBorderMode() != 0 || var13.getRowType() != -92) {
                                    var10.setTBColor(var5.getBBColor());
                                    byte var14;
                                    if ((var14 = var5.getRealBBStyle()) == 80) {
                                        var10.setTBStyle(var14);
                                    } else {
                                        byte var15 = var5.getBBStyle();
                                        var10.setTBStyle(var15);
                                    }

                                    var10.setTBWidth(var5.getBBWidth());
                                }
                            }
                        }

                        if (var9 <= var2 && (var10 = (ExtNormalCell)this._$5.get(var3, var9)) != null) {
                            if (var10.getLBStyle() != 80) {
                                var5.setRBColor(var10.getLBColor());
                                var5.setRBStyle(var10.getLBStyle());
                                var5.setRBWidth(var10.getLBWidth());
                            } else if (var5.getRBStyle() != 80) {
                                var10.setLBColor(var5.getRBColor());
                                var10.setLBStyle(var5.getRBStyle());
                                var10.setLBWidth(var5.getRBWidth());
                            }
                        }
                    }
                }
            }
        }

    }

    public void addCol$(short var1) {
        this._$5.addCols(var1);
    }

    public void addCol() {
    }

    public void addCol(short var1) {
    }

    public void addRow$(int var1) {
        this._$5.addRows(var1);
    }

    public void addRow() {
    }

    public void addRow(int var1) {
    }

    public void calculate(Context var1) {
        ExtCellSet var2 = null;

        try {
            var2 = get();
        } catch (Exception var7) {
            ;
        }

        if (var2 != null && var2.getType() != 0 && var2.getVersion() != 20) {
            this._$1(var1);
        } else {
            Object var3 = _$31;
            synchronized(_$31){}

            try {
                this._$1(var1);
            } catch (Throwable var6) {
                throw var6;
            }

        }
    }

    public static final boolean checkDog() {
        if (get().getDogNotEnabled()) {
            return true;
        } else {
            boolean var0 = true;
            if (!_$35) {
                try {
                    System.loadLibrary("CHKIDE");
                    _$35 = true;
                } catch (Throwable var5) {
                    _$35 = false;
                    MessageManager var1 = EngineMessage.get();
                    System.out.println(var1.getMessage("ExtCellSet.loadLibraryFalse"));
                }
            }

            byte[] var7 = new byte[64];
            byte[] var2 = new byte[128];
            _$61.nextBytes(var7);
            var7[0] = 63;

            int var6;
            try {
                var6 = cryptByDog(var7, var2);
            } catch (Error var4) {
                MessageManager var3 = EngineMessage.get();
                System.out.println(var3.getMessage("ExtCellSet.callLibraryError"));
                return false;
            }

            if (var6 < -1) {
                return false;
            } else {
                byte[] var8 = RSA2.decrypt2(var2);
                if (!compare(var7, 1, var8, 0, 63)) {
                    var0 = true;
                    return false;
                } else {
                    return var6 >= -1;
                }
            }
        }
    }

    public final boolean checkExpiration() {
        int var1;
        if ((var1 = (int)(System.currentTimeMillis() / 1000L)) + 86400 < _$36) {
            MessageManager var2 = EngineMessage.get();
            throw new RuntimeException(var2.getMessage("ExtCellSet.timeModified"));
        } else {
            _$36 = var1;
            if (this._$51 > 0 && this._$51 > _$36) {
                return false;
            } else {
                return this._$52 <= 0 || this._$52 >= _$36;
            }
        }
    }

    public final boolean checkIP(String var1) {
        if (StringUtils.isSpaceString(this._$57)) {
            return true;
        } else {
            MessageManager var2;
            if (StringUtils.isSpaceString(var1)) {
                var2 = EngineMessage.get();
                throw new RuntimeException(var2.getMessage("ExtCellSet.license") + var1);
            } else {
                try {
                    if (InetAddress.getByName(var1) == null) {
                        return false;
                    }
                } catch (UnknownHostException var3) {
                    var2 = EngineMessage.get();
                    throw new RuntimeException(var2.getMessage("ExtCellSet.license") + var1);
                }

                return this._$57.indexOf(var1) >= 0;
            }
        }
    }

    public final boolean checkIP(InetAddress var1) {
        return this.checkIP(var1.getHostAddress());
    }

    public static boolean checkKey(String var0) {
        return var0 == null ? false : "8F6D52578B4F50EDACB01537AF4FE739".equals((new MD5()).getMD5ofStr(var0));
    }

    public final boolean checkProperty(int var1) {
        if (this._$54 == null) {
            return true;
        } else {
            for(int var2 = this._$54.length - 1; var2 >= 0; --var2) {
                if (var1 == this._$54[var2]) {
                    return true;
                }
            }

            return false;
        }
    }

    public void clearCellSetInfo() {
        int var1 = this._$5.getRowSize();
        int var2 = this._$5.getColSize();
        ExtCell var3;
        if ((var3 = (ExtCell)this._$5.get(0, 0)) instanceof ExtNormalCell) {
            ((ExtNormalCell)var3).clearCellInfo();
        }

        for(int var4 = 1; var4 < var1; ++var4) {
            for(int var5 = 1; var5 < var2; ++var5) {
                if ((var3 = (ExtCell)this._$5.get(var4, var5)) != null && var3 instanceof ExtNormalCell) {
                    ((ExtNormalCell)var3).clearCellInfo();
                }
            }
        }

        var1 = this._$6.getRowSize();
        var2 = this._$6.getColSize();
        Object var8 = null;

        for(int var6 = 0; var6 < var1; ++var6) {
            for(int var7 = 0; var7 < var2; ++var7) {
                if ((var8 = this._$6.get(var6, var7)) instanceof NormalCellEngine) {
                    ((NormalCellEngine)var8).clearEngineInfo();
                }
            }
        }

    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException var2) {
            throw new ReportError(var2.getMessage(), var2);
        }
    }

    public static boolean compare(byte[] var0, int var1, byte[] var2, int var3, int var4) {
        if (var0 != null && var2 != null) {
            int var5 = var0.length;
            int var6 = var2.length;
            if (var1 + var4 <= var5 && var3 + var4 <= var6) {
                for(int var7 = var4 - 1; var7 >= 0; --var7) {
                    if (var0[var7 + var1] != var2[var7 + var3]) {
                        return false;
                    }
                }

                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private static native int cryptByDog(byte[] var0, byte[] var1);

    public Object deepClone() {
        ExtCellSet var1;
        (var1 = (ExtCellSet)this.clone())._$10 = null;
        var1._$11 = null;
        var1._$17 = 0;
        var1._$18 = 0;
        int var2 = this._$5.getRowSize();
        int var3 = this._$5.getColSize();
        var1._$5 = new Matrix(var2, var3);
        var1._$23 = (ExtExtendCell)this._$23.deepClone();
        var1._$5.set(0, 0, var1._$23);

        for(int var4 = 1; var4 < var2; ++var4) {
            var1._$5.set(var4, 0, ((ICell)this._$5.get(var4, 0)).deepClone());
        }

        for(int var5 = 1; var5 < var3; ++var5) {
            var1._$5.set(0, var5, ((ICell)this._$5.get(0, var5)).deepClone());
        }

        int var7;
        int var9;
        for(int var6 = 1; var6 < var2; ++var6) {
            for(var7 = 1; var7 < var3; ++var7) {
                ExtNormalCell var8;
                if ((var8 = (ExtNormalCell)this._$5.get(var6, var7)) != null) {
                    var9 = var8.getRow();
                    short var10 = var8.getCol();
                    if (var9 == var6 && var10 == var7) {
                        var1._$5.set(var6, var7, var8.deepClone());
                    } else {
                        ExtNormalCell var11;
                        if ((var11 = (ExtNormalCell)var1._$5.get(var9, var10)).getRow() + var11.getRowMerge() > var6 && var11.getCol() + var11.getColMerge() > var7) {
                            var1._$5.set(var6, var7, var11);
                        } else {
                            var1._$5.set(var6, var7, (Object)null);
                        }
                    }
                }
            }
        }

        var7 = this._$6.getRowSize();
        int var19 = this._$6.getColSize();
        var1._$6 = new Matrix(var7, var19);
        var1._$23.engine = (NormalCellEngine)this._$23.engine.clone();
        var1._$23.engine.cs = var1;
        var1._$23.engine.srcCurrent = var1._$23;
        var1._$6.set(0, 0, var1._$23.engine);

        int var12;
        ExtRowCell var13;
        for(var9 = 1; var9 < var7; ++var9) {
            RowCellEngine var20;
            RowCellEngine var22;
            (var22 = (RowCellEngine)(var20 = (RowCellEngine)this._$6.get(var9, 0)).clone()).cs = var1;
            if ((var12 = var20.srcCurrent.getRow()) < var2) {
                var13 = (ExtRowCell)var1._$5.get(var12, 0);
                var22.srcCurrent = var13;
                var13.engine = var22;
            } else {
                var22.srcCurrent = null;
            }

            var1._$6.set(var9, 0, var22);
        }

        ExtColCell var14;
        for(int var21 = 1; var21 < var19; ++var21) {
            ColCellEngine var23;
            ColCellEngine var25;
            (var25 = (ColCellEngine)(var23 = (ColCellEngine)this._$6.get(0, var21)).clone()).cs = var1;
            short var26;
            if ((var26 = var23.srcCurrent.getCol()) < var3) {
                var14 = (ExtColCell)var1._$5.get(0, var26);
                var25.srcCurrent = var14;
                var14.engine = var25;
            } else {
                var25.srcCurrent = null;
            }

            var1._$6.set(0, var21, var25);
        }

        int var15;
        for(int var24 = 1; var24 < var7; ++var24) {
            for(var12 = 1; var12 < var19; ++var12) {
                NormalCellEngine var27;
                if ((var27 = (NormalCellEngine)this._$6.get(var24, var12)) != null && var27.srcCurrent != null) {
                    if (var27.srcCurrent.getSourceRow() == var24 && var27.srcCurrent.getSourceCol() == var12) {
                        NormalCellEngine var29;
                        (var29 = (NormalCellEngine)var27.clone()).cs = var1;
                        var15 = var27.srcCurrent.getRow();
                        short var16 = var27.srcCurrent.getCol();
                        if (var15 < var2 && var16 < var3) {
                            ExtNormalCell var17 = (ExtNormalCell)var1._$5.get(var15, var16);
                            var29.srcCurrent = var17;
                            var17.engine = var29;
                        } else {
                            var29.srcCurrent = null;
                        }

                        var1._$6.set(var24, var12, var29);
                    } else {
                        var1._$6.set(var24, var12, var1._$6.get(var27.srcCurrent.getSourceRow(), var27.srcCurrent.getSourceCol()));
                    }
                }
            }
        }

        var1._$23.updateCell(var1, this._$23);

        for(var12 = 1; var12 < var2; ++var12) {
            var13 = (ExtRowCell)this._$5.get(var12, 0);
            ((ExtRowCell)var1._$5.get(var12, 0)).updateCell(var1, var13);
        }

        for(int var28 = 1; var28 < var3; ++var28) {
            var14 = (ExtColCell)this._$5.get(0, var28);
            ((ExtColCell)var1._$5.get(0, var28)).updateCell(var1, var14);
        }

        for(int var30 = 1; var30 < var2; ++var30) {
            for(var15 = 1; var15 < var3; ++var15) {
                ExtNormalCell var32;
                if ((var32 = (ExtNormalCell)this._$5.get(var30, var15)) != null && var32.getRow() == var30 && var32.getCol() == var15) {
                    ((ExtNormalCell)var1._$5.get(var30, var15)).updateCell(var1, var32);
                }
            }
        }

        if (this._$16 != null) {
            ArrayList var31 = new ArrayList();
            int var33 = 0;

            for(int var34 = this._$16.size(); var33 < var34; ++var33) {
                Seal var18 = (Seal)this._$16.get(var33);
                var31.add(var18.deepClone());
            }
        }

        return var1;
    }

    protected void delCol(int var1) {
        this._$5.deleteCol(var1);
    }

    protected void delRow(int var1) {
        this._$5.deleteRow(var1);
    }

    public void extendCol(ExtCell var1, int var2) {
        if (!this._$2(var1.getRow())) {
            MessageManager var21 = EngineMessage.get();
            throw new ReportError(var21.getMessage("ExtCellSet.entendCol"));
        } else {
            int var3 = this._$3(var1);
            int var4;
            int var5 = (var4 = this._$4(var1)) - var3;
            ExtCell[] var6;
            (var6 = new ExtCell[var2])[0] = var1;
            var1.setExtCells(var6);
            this._$1((short)var4, (short)(var5 * (var2 - 1)));

            int var9;
            for(int var7 = var3; var7 < var4; ++var7) {
                ExtCell var8 = (ExtCell)this._$5.get(0, var7);

                for(var9 = 1; var9 < var2; ++var9) {
                    ExtCell var10;
                    (var10 = (ExtCell)var8.clone()).addCol((short)(var9 * var5));
                    this._$5.set(0, var7 + var9 * var5, var10);
                }
            }

            var1.expandTopHead((short)(var5 * (var2 - 1)));
            int var22 = this._$5.getRowSize();

            int var15;
            int var17;
            for(var9 = 1; var9 < var22; ++var9) {
                this._$2();

                for(short var23 = (short)var3; var23 < var4; ++var23) {
                    ExtCell var11;
                    if ((var11 = (ExtCell)this._$5.get(var9, var23)) != null) {
                        if (var11.getRow() == var9 && var11.getCol() == var23 && var1.isTopHeadOf(var11)) {
                            for(int var12 = 1; var12 < var2; ++var12) {
                                ExtNormalCell var13 = (ExtNormalCell)var11.clone();
                                short var14 = (short)(var12 * var5);
                                var13.addCol(var14);
                                var14 += var23;
                                var15 = var9 + var13.getRowMerge();
                                int var16 = var14 + var13.getColMerge();

                                for(var17 = var9; var17 < var15; ++var17) {
                                    for(int var18 = var14; var18 < var16; ++var18) {
                                        this._$5.set(var17, var18, var13);
                                    }
                                }

                                if (var11 == var1) {
                                    var6[var12] = var13;
                                    var13.setExtCells(var6);
                                }
                            }
                        }

                        var23 = (short)(var23 + (var11.getColMerge() - 1));
                    }
                }
            }

            for(int var25 = 1; var25 < var22; ++var25) {
                this._$2();

                for(int var24 = var3; var24 < var4; ++var24) {
                    ExtCell var26;
                    if ((var26 = (ExtCell)this._$5.get(var25, var24)) != null) {
                        if (var26.getRow() == var25 && var26.getCol() == var24 && var1.isTopHeadOf(var26)) {
                            ExtCell var27 = var26.getCellLeftHead();
                            ExtCell var28;
                            var15 = (var28 = var26.getCellTopHead()).getRow();
                            short var29 = var28.getCol();

                            for(var17 = 1; var17 < var2; ++var17) {
                                ExtCell var30 = (ExtCell)this._$5.get(var25, var24 + var5 * var17);
                                if (var26 != var1) {
                                    ExtCell var19 = (ExtCell)this._$5.get(var15, var29 + var5 * var17);
                                    var30.setCellTopHead(var19);
                                }

                                if (var27 != this._$23) {
                                    short var31;
                                    if ((var31 = var27.getCol()) >= var3 && var31 < var4) {
                                        int var20 = var27.getRow();
                                        var30.setCellLeftHead((ExtCell)this._$5.get(var20, var31 + var5 * var17));
                                    } else {
                                        var30.setCellLeftHead(var27);
                                    }
                                }
                            }
                        }

                        var24 += var26.getColMerge() - 1;
                    }
                }
            }

        }
    }

    public void extendRow(ExtCell var1, int var2) {
        int var3 = this._$1(var1);
        int var4;
        int var5 = (var4 = this._$2(var1)) - var3;
        ExtCell[] var6;
        (var6 = new ExtCell[var2])[0] = var1;
        var1.setExtCells(var6);
        this._$1(var4, var5 * (var2 - 1));
        var1.expandLeftHead(var5 * (var2 - 1));
        int var7 = this._$5.getColSize();

        int var9;
        int var10;
        int var11;
        Object[] var13;
        for(int var8 = 1; var8 < var2; ++var8) {
            this._$2();

            for(var9 = var3; var9 < var4; ++var9) {
                var10 = var8 * var5;
                var11 = var9 + var10;
                Object[] var12 = this._$5.getRow(var9);
                (var13 = this._$5.getRow(var11))[0] = ((ExtCell)var12[0]).clone();
                ((ExtCell)var13[0]).addRow(var10);

                for(short var14 = 1; var14 < var7; ++var14) {
                    ExtCell var15;
                    if ((var15 = (ExtCell)var12[var14]) != null) {
                        if (var15.getRow() == var9 && var15.getCol() == var14 && var1.isLeftHeadOf(var15)) {
                            ExtNormalCell var16;
                            (var16 = (ExtNormalCell)var15.clone()).addRow(var10);
                            int var17 = var11 + var16.getRowMerge();
                            int var18 = var14 + var16.getColMerge();

                            for(int var19 = var11; var19 < var17; ++var19) {
                                for(short var20 = var14; var20 < var18; ++var20) {
                                    this._$5.set(var19, var20, var16);
                                }
                            }

                            if (var15 == var1) {
                                var6[var8] = var16;
                                var16.setExtCells(var6);
                            }
                        }

                        var14 = (short)(var14 + (var15.getColMerge() - 1));
                    }
                }
            }
        }

        for(var9 = 1; var9 < var2; ++var9) {
            this._$2();

            for(var10 = var3; var10 < var4; ++var10) {
                var11 = var9 * var5;
                int var21 = var10 + var11;
                var13 = this._$5.getRow(var10);
                Object[] var22 = this._$5.getRow(var21);

                for(short var23 = 1; var23 < var7; ++var23) {
                    ExtCell var24;
                    if ((var24 = (ExtCell)var13[var23]) != null) {
                        if (var24.getRow() == var10 && var24.getCol() == var23 && var1.isLeftHeadOf(var24)) {
                            ExtCell var25 = (ExtCell)var22[var23];
                            ExtCell var26 = var24.getCellLeftHead();
                            ExtCell var27;
                            if (var24 != var1) {
                                var27 = (ExtCell)this._$5.get(var26.getRow() + var11, var26.getCol());
                                var25.setCellLeftHead(var27);
                            }

                            if ((var27 = var24.getCellTopHead()) != this._$23) {
                                var25.setCellTopHead(var27);
                            }
                        }

                        var23 = (short)(var23 + (var24.getColMerge() - 1));
                    }
                }
            }
        }

    }

    public void fillRecord(byte[] var1) throws IOException, ClassNotFoundException {
        ByteArrayInputRecord var2;
        int var3 = (var2 = new ByteArrayInputRecord(var1)).readInt();
        short var4 = var2.readShort();
        this._$5 = new Matrix(var3, var4);

        for(int var5 = 1; var5 < var3; ++var5) {
            ExtRowCell var6 = (ExtRowCell)var2.readRecord(new ExtRowCell());
            this.setRowCell(var5, var6);
        }

        for(short var19 = 1; var19 < var4; ++var19) {
            ExtColCell var7 = (ExtColCell)var2.readRecord(new ExtColCell());
            this.setColCell(var19, var7);
        }

        int var20 = var2.readInt();
        ArrayList var8 = new ArrayList();
        if (var20 > 0) {
            var8 = new ArrayList(var20);
        }

        for(int var9 = 0; var9 < var20; ++var9) {
            var8.add(var9, (INormalCell)var2.readRecord(new NormalCell()));
        }

        for(int var10 = 1; var10 < var3; ++var10) {
            for(short var11 = 1; var11 < var4; ++var11) {
                ExtNormalCell var12;
                if ((var12 = (ExtNormalCell)var2.readRecord(new ExtNormalCell())) != null) {
                    int var13;
                    if ((var13 = var2.readInt()) >= 0) {
                        ((ExtNormalCell)var12).engine.srcPrepare = (INormalCell)var8.get(var13);
                    }

                    if (!var12.isMerged()) {
                        this.setCell(var10, var11, var12);
                    } else {
                        Area var14;
                        if ((var14 = var12.getMergedArea()).getBeginCol() == var11 && var14.getBeginRow() == var10) {
                            short var15 = var11;

                            for(short var16 = var14.getEndCol(); var15 <= var16; ++var15) {
                                int var17 = var10;

                                for(int var18 = var14.getEndRow(); var17 <= var18; ++var17) {
                                    this.setCell(var17, var15, var12);
                                }
                            }
                        }
                    }
                }
            }
        }

        this._$23 = (ExtExtendCell)var2.readRecord(new ExtExtendCell());
        int var21;
        if (var2.available() > 0 && (var21 = var2.readInt()) > 0) {
            this._$16 = new ArrayList();

            for(int var22 = 0; var22 < var21; ++var22) {
                Seal var23 = (Seal)var2.readRecord(new Seal());
                this._$16.add(var23);
            }
        }

    }

    public static final ExtCellSet get() {
        if (!_$34) {
            return _$32;
        } else {
            Object var0 = null;
            MessageManager var18;

            if (_$33 == null) {
                var18 = EngineMessage.get();
                throw new RuntimeException(var18.getMessage("ExtCellSet.noneLicenseFile"));
            } else {
                try {
                    String str = "classpath:" + _$33.substring(9,_$33.length());
                    var0 = ServiceLocator.getAppContext().getResource(str).getInputStream();
                    //var0 = new FileInputStream(_$33);
                } catch (Exception var16) {
                    ;
                }

                ClassLoader var1;
                if (var0 == null && (var1 = Thread.currentThread().getContextClassLoader()) != null) {
                    try {
                        var0 = var1.getResourceAsStream(_$33);
                    } catch (Exception var15) {
                        ;
                    }
                }

                if (var0 == null) {
                    try {
                       // var0 = (class$com$runqian$report4$model$engine$ExtCellSet != null ? class$com$runqian$report4$model$engine$ExtCellSet : (class$com$runqian$report4$model$engine$ExtCellSet = class$("com.runqian.report4.model.engine.ExtCellSet"))).getResourceAsStream(_$33);
                    } catch (Exception var14) {
                        ;
                    }
                }

                if (var0 == null) {
                    var18 = EngineMessage.get();
                    throw new RuntimeException(var18.getMessage("ExtCellSet.noneFindLicense") + _$33 + var18.getMessage("ExtCellSet.checkLicenseFile"));
                } else {
                    try {
                        _$32._$1((InputStream)var0);
                    } catch (Exception var13) {
                        MessageManager var4 = EngineMessage.get();
                        throw new RuntimeException(var4.getMessage("ExtCellSet.licenseError") + var13.getMessage());
                    } finally {
                        if (var0 != null) {
                            try {
                                ((InputStream)var0).close();
                            } catch (Exception var12) {
                                var12.printStackTrace();
                            }
                        }

                    }

                    _$11();
                    _$34 = false;
                    return _$32;
                }
            }
        }
    }

    public final boolean getAddedDSEnabled() {
        return (this._$60 & 64L) != 0L;
    }

    public final boolean getAddedDSInNonlinear() {
        return (this._$55 & 64L) != 0L;
    }

    public final boolean getAttributeEnabled() {
        return (this._$60 & 16777216L) != 0L;
    }

    public final boolean getAutoStructEnabled() {
        return (this._$60 & 134217728L) != 0L;
    }

    public int getBBColor(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getBBColor();
            }

            if (var1 < var3 - 1 && (var5 = (INormalCell)this._$5.get(var1 + 1, var2)) != null) {
                return var5.getTBColor();
            }
        }

        return 0;
    }

    public byte getBBStyle(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getBBStyle();
            }

            if (var1 < var3 - 1 && (var5 = (INormalCell)this._$5.get(var1 + 1, var2)) != null) {
                return var5.getTBStyle();
            }
        }

        return 80;
    }

    public float getBBWidth(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getBBWidth();
            }

            if (var1 < var3 - 1 && (var5 = (INormalCell)this._$5.get(var1 + 1, var2)) != null) {
                return var5.getTBWidth();
            }
        }

        return 1.0F;
    }

    public BackGraphConfig getBackGraphConfig() {
        return this._$24.getBackGraphConfig();
    }

    public final boolean getBaseOlapEnabled() {
        return (this._$60 & 4194304L) != 0L;
    }

    public final String getBindingIPs() {
        return this._$57;
    }

    public final byte getCalcEnable(ExtCellSet var1) {
        int var2 = 0;
        HashMap var3 = new HashMap();
        byte var4 = this.getInput();
        int var5 = this._$6.getColSize();
        if (!var1.getSemanticsEnabled() && this._$26) {
            return 9;
        } else {
            int var6 = 1;

            for(int var7 = this._$6.getRowSize(); var6 < var7; ++var6) {
                for(int var8 = 1; var8 < var5; ++var8) {
                    NormalCellEngine var9;
                    if ((var9 = (NormalCellEngine)this._$6.get(var6, var8)) != null && var9.srcCurrent.getRow() == var6 && var9.srcCurrent.getCol() == var8) {
                        int var10;
                        if (var9.expression != null) {
                            if (!var1.getMultiDSEnabled()) {
                                ((Expression)var9.expression).putDataSet(var3);
                                if (var3.size() > 1) {
                                    return 2;
                                }
                            }

                            if (!var1.getCoordinateEnabled()) {
                                if (((Expression)var9.expression).isLayerCo()) {
                                    return 3;
                                }

                                if (((Expression)var9.expression).isLineCo()) {
                                    return 4;
                                }

                                if (((Expression)var9.expression).isPlaneCo()) {
                                    return 5;
                                }
                            }

                            if (!var1.getSpecialFunctionEnabled() && ((Expression)var9.expression).isDelayFun()) {
                                return 7;
                            }

                            if (!var1.getSpecialGroupEnabled() && ((Expression)var9.expression).isSpecialGroup()) {
                                return 8;
                            }

                            if (!var1.getAddedDSEnabled() && var9.addExpression != null) {
                                for(var10 = 0; var10 < var9.addExpression.length; ++var10) {
                                    if (var9.addExpression[var10] != null) {
                                        return 10;
                                    }
                                }
                            }
                        }

                        if (!var1.getMultiInputEnabled() && var4 == 1 && var9.propInput != null) {
                            InputProperty var11;
                            if ((var10 = var9.propInput.getUpdateCount()) > 0 && var9.srcPrepare.getCellType() == -62 && (var11 = var9.srcPrepare.getInputProperty()) != null && var11.getEditStyle() == 7) {
                                --var10;
                            }

                            if ((var2 += var10) > 1) {
                                return 6;
                            }
                        }

                        var8 += var9.srcCurrent.getColMerge() - 1;
                    }
                }
            }

            return 0;
        }
    }

    public INormalCell getCell(int var1, short var2) {
        return var2 > 0 && var2 < this._$5.getColSize() && var1 > 0 && var1 < this._$5.getRowSize() ? (INormalCell)this._$5.get(var1, var2) : null;
    }

    public ExtCell getCell00() {
        return this._$23;
    }

    public final byte[] getCipherHash() {
        return this._$24.getCipherHash();
    }

    public boolean getClusterEnabled() {
        return (this._$60 & 8589934592L) != 0L;
    }

    public IColCell getColCell(short var1) {
        return var1 > 0 && var1 < this._$5.getColSize() ? (IColCell)this._$5.get(0, var1) : null;
    }

    public short getColCount() {
        return (short)(this._$5.getColSize() - 1);
    }

    public final short getConcurrentUserNum() {
        return this._$59;
    }

    public final boolean getCoordinateEnabled() {
        return (this._$60 & 8L) != 0L;
    }

    public final boolean getCoordinateInNonlinear() {
        return (this._$55 & 8L) != 0L;
    }

    public final String getCopyright() {
        return this._$44;
    }

    public ICellStyleManager getCssStyleManager() {
        String var1 = this.getReportStyleName();
        if (this._$29 == null && var1 != null && !"".equalsIgnoreCase(var1.trim())) {
            this._$29 = StyleConfig.get().getCellStyleManager(var1);
        }

        return this._$29;
    }

    public ExtCell getCurrent() {
        return this._$10;
    }

    public static int getCurrentNonlinearCount() {
        return _$4.size();
    }

    public ExtCellSet getCurrentPage() {
        return this._$15;
    }

    public Map getCustomProperties() {
        return this._$24.getCustomProperties();
    }

    public final String getCustomerName() {
        return this._$46;
    }

    public final boolean getDataManagerEnabled() {
        return (this._$60 & 512L) != 0L;
    }

    public DataSetMetaData getDataSetMetaData() {
        return this._$24.getDataSetMetaData();
    }

    public int getDispRatio() {
        return this._$24.getDispRatio();
    }

    public static final int getDog() throws Exception {
        boolean var0 = true;
        if (!_$35) {
            try {
                System.loadLibrary("CHKIDE");
                _$35 = true;
            } catch (Throwable var6) {
                _$35 = false;
                MessageManager var1 = EngineMessage.get();
                throw new ReportError(var1.getMessage("ExtCellSet.loadLibraryFalse"));
            }
        }

        byte[] var8 = new byte[64];
        byte[] var2 = new byte[128];
        _$61.nextBytes(var8);
        var8[0] = 63;

        MessageManager var3;
        int var7;
        try {
            var7 = cryptByDog(var8, var2);
        } catch (Error var5) {
            var3 = EngineMessage.get();
            throw new ReportError(var3.getMessage("ExtCellSet.callLibraryError"));
        }

        if (var7 < -1) {
            var3 = EngineMessage.get();
            throw new ReportError(var3.getMessage("ExtCellSet.readDogError"));
        } else {
            byte[] var9 = RSA2.decrypt2(var2);
            if (!compare(var8, 1, var9, 0, 63)) {
                MessageManager var4 = EngineMessage.get();
                throw new ReportError(var4.getMessage("ExtCellSet.checkDogFalse"));
            } else {
                return var7;
            }
        }
    }

    public boolean getDogNotEnabled() {
        return (this._$60 & 137438953472L) != 0L;
    }

    public MacroMetaData getDynamicMacroMetaData() {
        return this._$24.getDynamicMacroMetaData();
    }

    public ParamMetaData getDynamicParamMetaData() {
        return this._$24.getDynamicParamMetaData();
    }

    public final int getEffectTime() {
        return this._$51;
    }

    public final short getEnabledCPUNum() {
        return this._$58;
    }

    public boolean getEsprocEnabled() {
        return (this._$60 & -9223372036854775808L) != 0L;
    }

    public final boolean getExcelImportEnabled() {
        return (this._$60 & 262144L) != 0L;
    }

    public final boolean getExpPreparedEnabled() {
        return (this._$60 & 33554432L) != 0L;
    }

    public final int getExpirationTime() {
        return this._$52;
    }

    public ExportConfig getExportConfig() {
        return this._$24.getExportConfig();
    }

    public final boolean getExportEnabled() {
        return (this._$60 & 256L) != 0L;
    }

    public boolean getFlashEnabled() {
        return (this._$60 & 17179869184L) != 0L;
    }

    public FlowVar getFlowVar() {
        return this._$24.getFlowVar();
    }

    public final boolean getFreezedHeaderEnabled() {
        return (this._$60 & 8192L) != 0L;
    }

    public String getFutureCellNumExp() {
        return this._$24.getFutureCellNumExp();
    }

    public final boolean getGeneralQueryEnabled() {
        return (this._$60 & 1048576L) != 0L;
    }

    public final byte[] getHash() {
        return this._$24.getHash();
    }

    public byte getInput() {
        return this._$24.getInput();
    }

    public final boolean getInputSchedulerEnabled() {
        return (this._$60 & 536870912L) != 0L;
    }

    public final String getIntegratorName() {
        return this._$45;
    }

    public int getLBColor(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getLBColor();
            }

            if (var2 > 1 && (var5 = (INormalCell)this._$5.get(var1, (short)(var2 - 1))) != null) {
                return var5.getRBColor();
            }
        }

        return 0;
    }

    public byte getLBStyle(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getLBStyle();
            }

            if (var2 > 1 && (var5 = (INormalCell)this._$5.get(var1, (short)(var2 - 1))) != null) {
                return var5.getRBStyle();
            }
        }

        return 80;
    }

    public float getLBWidth(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getLBWidth();
            }

            if (var2 > 1 && (var5 = (INormalCell)this._$5.get(var1, (short)(var2 - 1))) != null) {
                return var5.getRBWidth();
            }
        }

        return 1.0F;
    }

    public static final String getLicenseFileName() {
        return _$33;
    }

    public final String getLicenseString() {
        return this.getLicenseString("\n");
    }

    public final String getLicenseString(String var1) {
        StringBuffer var2 = new StringBuffer(1024);
        if (this._$37 == 0) {
            var2.append("type             = IDE").append(var1);
            var2.append("version          = ").append(this._$38).append("( 10:SIMPLE, 11:NORMAL )").append(var1);
            var2.append("product name     = " + this._$39).append(var1);
            var2.append("provider name    = " + this._$40).append(var1);
            var2.append("provider HTTP    = " + this._$41).append(var1);
            var2.append("provider tel     = " + this._$42).append(var1);
            var2.append("provider logo    = " + this._$43).append(var1);
            var2.append("copyright        = " + this._$44).append(var1);
            var2.append("integrator name  = " + this._$45).append(var1);
            var2.append("customer name    = " + this._$46).append(var1);
            var2.append("project name     = " + this._$47).append(var1);
            var2.append("prompt           = " + this._$48).append(var1);
            var2.append("product version  = " + this._$49).append(var1);
            var2.append("effect time      = " + _$3(this._$51)).append(var1);
            var2.append("expiration time  = " + _$3(this._$52)).append(var1);
            var2.append("multi datasets = " + this.getMultiDSEnabled()).append(var1);
            var2.append("special function = " + this.getSpecialFunctionEnabled()).append(var1);
            var2.append("special group = " + this.getSpecialGroupEnabled()).append(var1);
            var2.append("coordinate = " + this.getCoordinateEnabled()).append(var1);
            var2.append("subreport = " + this.getSubReportEnabled()).append(var1);
            var2.append("semantics        = " + this.getSemanticsEnabled()).append(var1);
            var2.append("added dataset    = " + this.getAddedDSEnabled()).append(var1);
            var2.append("multi input = " + this.getMultiInputEnabled()).append(var1);
            var2.append("print            = " + this.getPrintEnabled()).append(var1);
            var2.append("export           = " + this.getExportEnabled()).append(var1);
            var2.append("data manager     = " + this.getDataManagerEnabled()).append(var1);
            var2.append("remote design    = " + this.getRemoteEnabled()).append(var1);
            var2.append("remote privilege = " + this.getRemotePrivilegeEnabled()).append(var1);
            var2.append("freezed header = " + this.getFreezedHeaderEnabled()).append(var1);
            var2.append("special chart = " + this.getSpecialChartEnabled()).append(var1);
            var2.append("special pager = " + this.getSpecialPagerEnabled()).append(var1);
            var2.append("version check = " + this.getVersionCheckEnabled()).append(var1);
            var2.append("dog disabled = " + this.getDogNotEnabled()).append(var1);
            var2.append("esproc = " + this.getEsprocEnabled()).append(var1);
            var2.append("nonlinear        = ");
            if (this.getMultiDSInNonlinear()) {
                var2.append("multi datasets,");
            }

            if (this.getSpecialFunctionInNonlinear()) {
                var2.append("special function,");
            }

            if (this.getSpecialGroupInNonlinear()) {
                var2.append("special group,");
            }

            if (this.getCoordinateInNonlinear()) {
                var2.append("coordinate,");
            }

            if (this.getMultiInputInNonlinear()) {
                var2.append("multi input,");
            }

            if (this.getSemanticsInNonlinear()) {
                var2.append("semantics,");
            }

            if (this.getAddedDSInNonlinear()) {
                var2.append("added datasets,");
            }

            var2.append(var1);
        } else {
            var2.append("type             = SERVER").append(var1);
            var2.append("version          = ").append(this._$38).append(" ( 20: TRAIL; 21: DEVELOPMENT; 22: FORMAL )").append(var1);
            var2.append("product name     = " + this._$39).append(var1);
            var2.append("provider name    = " + this._$40).append(var1);
            var2.append("provider HTTP    = " + this._$41).append(var1);
            var2.append("provider tel     = " + this._$42).append(var1);
            var2.append("provider logo    = " + this._$43).append(var1);
            var2.append("copyright        = " + this._$44).append(var1);
            var2.append("integrator name  = " + this._$45).append(var1);
            var2.append("customer name    = " + this._$46).append(var1);
            var2.append("project name     = " + this._$47).append(var1);
            var2.append("prompt           = " + this._$48).append(var1);
            var2.append("product version  = " + this._$49).append(var1);
            var2.append("effect time      = " + _$3(this._$51)).append(var1);
            var2.append("expiration time  = " + _$3(this._$52)).append(var1);
            var2.append("nonlinear        = ");
            if (this.getMultiDSInNonlinear()) {
                var2.append("multi datasets,");
            }

            if (this.getSpecialFunctionInNonlinear()) {
                var2.append("special function,");
            }

            if (this.getSpecialGroupInNonlinear()) {
                var2.append("special group,");
            }

            if (this.getCoordinateInNonlinear()) {
                var2.append("coordinate,");
            }

            if (this.getMultiInputInNonlinear()) {
                var2.append("multi input,");
            }

            if (this.getSemanticsInNonlinear()) {
                var2.append("semantics,");
            }

            if (this.getAddedDSInNonlinear()) {
                var2.append("added datasets,");
            }

            var2.append(var1);
            var2.append("nonlinear report = ").append(this._$56 < 0 ? "without limit" : Integer.toString(this._$56)).append(var1);
            var2.append("binding IP       = ").append(this._$57).append(var1);
            var2.append("CPU num          = ").append(this._$58 <= 0 ? "without limit" : Integer.toString(this._$58)).append(var1);
            var2.append("concurrent task  = ").append(this._$59 <= 0 ? "without limit" : Integer.toString(this._$59)).append(var1);
            var2.append("OS name          = ").append(this._$53).append(var1);
            var2.append("properties       = ");
            if (this._$54 != null) {
                for(int var3 = 0; var3 < this._$54.length; ++var3) {
                    var2.append(this._$54[var3]);
                    if (var3 != this._$54.length - 1) {
                        var2.append(",");
                    }
                }
            }

            var2.append(var1);
            var2.append("multi datasets      = " + this.getMultiDSEnabled()).append(var1);
            var2.append("special function    = " + this.getSpecialFunctionEnabled()).append(var1);
            var2.append("special group       = " + this.getSpecialGroupEnabled()).append(var1);
            var2.append("coordinate          = " + this.getCoordinateEnabled()).append(var1);
            var2.append("multi input         = " + this.getMultiInputEnabled()).append(var1);
            var2.append("semantics           = " + this.getSemanticsEnabled()).append(var1);
            var2.append("added dataset       = " + this.getAddedDSEnabled()).append(var1);
            var2.append("sub report          = " + this.getSubReportEnabled()).append(var1);
            var2.append("freezed header      = " + this.getFreezedHeaderEnabled()).append(var1);
            var2.append("special pager       = " + this.getSpecialPagerEnabled()).append(var1);
            var2.append("special chart       = " + this.getSpecialChartEnabled()).append(var1);
            var2.append("upload              = " + this.getUploadEnabled()).append(var1);
            var2.append("special input       = " + this.getSpecialInputEnabled()).append(var1);
            var2.append("excel import        = " + this.getExcelImportEnabled()).append(var1);
            var2.append("offline input       = " + this.getOfflineInputEnabled()).append(var1);
            var2.append("general query       = " + this.getGeneralQueryEnabled()).append(var1);
            var2.append("online report       = " + this.getOnlineReportEnabled()).append(var1);
            var2.append("base OLAP           = " + this.getBaseOlapEnabled()).append(var1);
            var2.append("online OLAP         = " + this.getOnlineOlapEnabled()).append(var1);
            var2.append("attribute           = " + this.getAttributeEnabled()).append(var1);
            var2.append("expression prepare  = " + this.getExpPreparedEnabled()).append(var1);
            var2.append("performance         = " + this.getPerformanceEnabled()).append(var1);
            var2.append("auto data struct    = " + this.getAutoStructEnabled()).append(var1);
            var2.append("scheduler           = " + this.getSchedulerEnabled()).append(var1);
            var2.append("input scheduler     = " + this.getInputSchedulerEnabled()).append(var1);
            var2.append("report center       = " + this.getReportCenterEnabled()).append(var1);
            var2.append("report group        = " + this.getReportGroupEnabled()).append(var1);
            var2.append("cluster             = " + this.getClusterEnabled()).append(var1);
            var2.append("flash               = " + this.getFlashEnabled()).append(var1);
            var2.append("local print         = " + this.getLocalPrintEnabled()).append(var1);
            var2.append("version check       = " + this.getVersionCheckEnabled()).append(var1);
        }

        return var2.toString();
    }

    public boolean getLocalPrintEnabled() {
        return (this._$60 & 34359738368L) != 0L;
    }

    public MacroMetaData getMacroMetaData() {
        return this._$24.getMacroMetaData();
    }

    public final boolean getMultiDSEnabled() {
        return (this._$60 & 1L) != 0L;
    }

    public final boolean getMultiDSInNonlinear() {
        return (this._$55 & 1L) != 0L;
    }

    public final boolean getMultiInputEnabled() {
        return (this._$60 & 16L) != 0L;
    }

    public final boolean getMultiInputInNonlinear() {
        return (this._$55 & 16L) != 0L;
    }

    public final byte getNonlinear() {
        if (!this.hasDS()) {
            return 1;
        } else {
            int var1 = 0;
            HashMap var2 = new HashMap();
            byte var3 = this.getInput();
            int var4 = this._$6.getColSize();
            ExtCellSet var5;
            if ((var5 = get()).getSemanticsInNonlinear() && this._$26) {
                return 9;
            } else {
                int var6 = 1;

                for(int var7 = this._$6.getRowSize(); var6 < var7; ++var6) {
                    for(int var8 = 1; var8 < var4; ++var8) {
                        NormalCellEngine var9;
                        if ((var9 = (NormalCellEngine)this._$6.get(var6, var8)) != null && var9.srcCurrent.getRow() == var6 && var9.srcCurrent.getCol() == var8) {
                            int var10;
                            if (var9.expression != null) {
                                if (var5.getMultiDSInNonlinear()) {
                                    ((Expression)var9.expression).putDataSet(var2);
                                    if (var2.size() > 1) {
                                        return 2;
                                    }
                                }

                                if (var5.getCoordinateInNonlinear()) {
                                    if (((Expression)var9.expression).isLayerCo()) {
                                        return 3;
                                    }

                                    if (((Expression)var9.expression).isLineCo()) {
                                        return 4;
                                    }

                                    if (((Expression)var9.expression).isPlaneCo()) {
                                        return 5;
                                    }
                                }

                                if (var5.getSpecialFunctionInNonlinear() && ((Expression)var9.expression).isDelayFun()) {
                                    return 7;
                                }

                                if (var5.getSpecialGroupInNonlinear() && ((Expression)var9.expression).isSpecialGroup()) {
                                    return 8;
                                }

                                if (var5.getAddedDSInNonlinear() && var9.addExpression != null) {
                                    for(var10 = 0; var10 < var9.addExpression.length; ++var10) {
                                        if (var9.addExpression[var10] != null) {
                                            return 10;
                                        }
                                    }
                                }
                            }

                            if (var5.getMultiInputInNonlinear() && var3 == 1 && var9.propInput != null) {
                                InputProperty var11;
                                if ((var10 = var9.propInput.getUpdateCount()) > 0 && var9.srcPrepare.getCellType() == -62 && (var11 = var9.srcPrepare.getInputProperty()) != null && var11.getEditStyle() == 7) {
                                    --var10;
                                }

                                if ((var1 += var10) > 1) {
                                    return 6;
                                }
                            }

                            var8 += var9.srcCurrent.getColMerge() - 1;
                        }
                    }
                }

                return 0;
            }
        }
    }

    public final long getNonlinearControl() {
        return this._$55;
    }

    public final short getNonlinearReportNum() {
        return this._$56;
    }

    public String getNotes() {
        return this._$24.getNotes();
    }

    public final String getOSName() {
        return this._$53;
    }

    public final boolean getOfflineInputEnabled() {
        return (this._$60 & 524288L) != 0L;
    }

    public final boolean getOnlineOlapEnabled() {
        return (this._$60 & 8388608L) != 0L;
    }

    public final boolean getOnlineReportEnabled() {
        return (this._$60 & 2097152L) != 0L;
    }

    public Area getPage() {
        return this._$14;
    }

    public byte getPageBorderMode() {
        return this._$24.getPageBorderMode();
    }

    public ParamMetaData getParamMetaData() {
        return this._$24.getParamMetaData();
    }

    public final boolean getPerformanceEnabled() {
        return (this._$60 & 67108864L) != 0L;
    }

    public final boolean getPrintEnabled() {
        return (this._$60 & 128L) != 0L;
    }

    public PrintSetup getPrintSetup() {
        return this._$24.getPrintSetup();
    }

    public final String getProductName() {
        return this._$39;
    }

    public final String getProductVersion() {
        return this._$49;
    }

    public final String getProjectName() {
        return this._$47;
    }

    public final String getPrompt() {
        return this._$48;
    }

    public final int[] getProperties() {
        return this._$54;
    }

    public final String getProviderHTTP() {
        return this._$41;
    }

    public final String getProviderLogo() {
        return this._$43;
    }

    public final String getProviderName() {
        return this._$40;
    }

    public final String getProviderTel() {
        return this._$42;
    }

    public int getRBColor(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getRBColor();
            }

            if (var2 < var4 - 1 && (var5 = (INormalCell)this._$5.get(var1, (short)(var2 + 1))) != null) {
                return var5.getLBColor();
            }
        }

        return 0;
    }

    public byte getRBStyle(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getRBStyle();
            }

            if (var2 < var4 - 1 && (var5 = (INormalCell)this._$5.get(var1, (short)(var2 + 1))) != null) {
                return var5.getLBStyle();
            }
        }

        return 80;
    }

    public float getRBWidth(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getRBWidth();
            }

            if (var2 < var4 - 1 && (var5 = (INormalCell)this._$5.get(var1, (short)(var2 + 1))) != null) {
                return var5.getLBWidth();
            }
        }

        return 1.0F;
    }

    public final boolean getRemoteEnabled() {
        return (this._$60 & 1024L) != 0L;
    }

    public final boolean getRemotePrivilegeEnabled() {
        return (this._$60 & 2048L) != 0L;
    }

    public final boolean getReportCenterEnabled() {
        return (this._$60 & 1073741824L) != 0L;
    }

    public ReportDefine getReportDefine() {
        return this._$24;
    }

    public boolean getReportGroupEnabled() {
        return (this._$60 & 4294967296L) != 0L;
    }

    public IReportListener getReportListener() {
        try {
            return (IReportListener)Class.forName(this._$30).newInstance();
        } catch (Exception var1) {
            return null;
        }
    }

    public String getReportListenerName() {
        return this._$30;
    }

    public String getReportStyleName() {
        return this._$28 != null ? this._$28 : this._$24.getReportStyleName();
    }

    public byte getReportType() {
        return this._$24.getReportType();
    }

    public IRowCell getRowCell(int var1) {
        return var1 > 0 && var1 < this._$5.getRowSize() ? (IRowCell)this._$5.get(var1, 0) : null;
    }

    public int getRowCount() {
        return this._$5.getRowSize() - 1;
    }

    public ExtCell getSavedCurrent() {
        return this._$11;
    }

    public final boolean getSchedulerEnabled() {
        return (this._$60 & 268435456L) != 0L;
    }

    public List getSealList() {
        return this._$16;
    }

    public final boolean getSemanticsEnabled() {
        return (this._$60 & 32L) != 0L;
    }

    public final boolean getSemanticsInNonlinear() {
        return (this._$55 & 32L) != 0L;
    }

    public ExtCell getSource(int var1, short var2) {
        NormalCellEngine var3;
        return (var1 > 0 && var1 < this._$6.getRowSize() && var2 > 0 && var2 < this._$6.getColSize() || var1 == 0 && var2 == 0) && (var3 = (NormalCellEngine)this._$6.get(var1, var2)) != null ? var3.srcCurrent : null;
    }

    public ExtCell getSource(String var1) {
        CellLocation var2;
        if ((var2 = CellLocation.parse(var1)) != null) {
            int var3 = var2.getRow();
            short var4 = var2.getCol();
            NormalCellEngine var5;
            if ((var3 > 0 && var3 < this._$6.getRowSize() && var4 > 0 && var4 < this._$6.getColSize() || var3 == 0 && var4 == 0) && (var5 = (NormalCellEngine)this._$6.get(var3, var4)) != null) {
                return var5.srcCurrent;
            }
        }

        return null;
    }

    public final boolean getSpecialChartEnabled() {
        return (this._$60 & 32768L) != 0L;
    }

    public final boolean getSpecialFunctionEnabled() {
        return (this._$60 & 2L) != 0L;
    }

    public final boolean getSpecialFunctionInNonlinear() {
        return (this._$55 & 2L) != 0L;
    }

    public final boolean getSpecialGroupEnabled() {
        return (this._$60 & 4L) != 0L;
    }

    public final boolean getSpecialGroupInNonlinear() {
        return (this._$55 & 4L) != 0L;
    }

    public final boolean getSpecialInputEnabled() {
        return (this._$60 & 131072L) != 0L;
    }

    public final boolean getSpecialPagerEnabled() {
        return (this._$60 & 16384L) != 0L;
    }

    public byte getState() {
        return this._$19;
    }

    public MacroMetaData getStaticMacroMetaData() {
        return this._$24.getStaticMacroMetaData();
    }

    public ParamMetaData getStaticParamMetaData() {
        return this._$24.getStaticParamMetaData();
    }

    public final boolean getSubReportEnabled() {
        return (this._$60 & 4096L) != 0L;
    }

    public SubReportMetaData getSubReportMetaData() {
        return this._$24.getSubReportMetaData();
    }

    public byte getSubRptType() {
        return this._$20;
    }

    public byte getSubmit() {
        return this._$24.getSubmit();
    }

    public int getTBColor(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getTBColor();
            }

            if (var1 > 1 && (var5 = (INormalCell)this._$5.get(var1 - 1, var2)) != null) {
                return var5.getBBColor();
            }
        }

        return 0;
    }

    public byte getTBStyle(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getTBStyle();
            }

            if (var1 > 1 && (var5 = (INormalCell)this._$5.get(var1 - 1, var2)) != null) {
                return var5.getBBStyle();
            }
        }

        return 80;
    }

    public float getTBWidth(int var1, short var2) {
        int var3 = this._$5.getRowSize();
        int var4 = this._$5.getColSize();
        if (var1 > 0 && var1 < var3 && var2 > 0 && var2 < var4) {
            INormalCell var5;
            if ((var5 = (INormalCell)this._$5.get(var1, var2)) != null) {
                return var5.getTBWidth();
            }

            if (var1 > 1 && (var5 = (INormalCell)this._$5.get(var1 - 1, var2)) != null) {
                return var5.getBBWidth();
            }
        }

        return 1.0F;
    }

    public String getTip() {
        return this._$24.getTip();
    }

    public final byte getType() {
        return this._$37;
    }

    public byte getUnit() {
        return this._$24.getUnit();
    }

    public final boolean getUploadEnabled() {
        return (this._$60 & 65536L) != 0L;
    }

    public final byte getVersion() {
        return this._$38;
    }

    public boolean getVersionCheckEnabled() {
        return (this._$60 & 68719476736L) != 0L;
    }

    public boolean hasDS() {
        return this._$27;
    }

    public final boolean hasGroupHeader() {
        return (this._$21 & 2) != 0;
    }

    public boolean hasThirdExp() {
        return (this._$21 & 4) != 0;
    }

    public void insertCol(short var1) {
    }

    public void insertCol(short var1, short var2) {
    }

    public void insertRow(int var1) {
    }

    public void insertRow(int var1, int var2) {
    }

    public void interrupt() {
        this._$22 = true;
    }

    public boolean isLoaded() {
        return this._$25;
    }

    public boolean isMainChanged() {
        return this._$13.isChanged();
    }

    public final boolean isQuiee() {
        return this._$38 == 10;
    }

    public final boolean isRunQian() {
        return this._$38 == 11;
    }

    public final boolean isSigned() {
        return (this._$21 & 1) != 0;
    }

    public boolean isSource(String var1) {
        CellLocation var2;
        if ((var2 = CellLocation.parse(var1)) != null) {
            int var3 = var2.getRow();
            short var4 = var2.getCol();
            if (var3 > 0 && var3 < this._$6.getRowSize() && var4 > 0 && var4 < this._$6.getColSize() || var3 == 0 && var4 == 0) {
                return true;
            }
        }

        return false;
    }

    public void prepareCalculate(Context var1, boolean var2) {
        int var3 = this._$5.getColSize();
        int var4 = this._$5.getRowSize();
        Object var5;
        boolean var6 = ((Map)(var5 = var1.getMacroMap(true))).size() > 0;
        Map var7;
        if (((var7 = var1.getDataSetMap(false)) == null || var7.size() == 0) && !var6) {
            String var8 = this._$1("P]O]55", 2);
            String var9 = this._$1("ep_nf", 2);
            if (var5 == null) {
                var5 = new NStringMap();
            }

            ((Map)var5).put(var8, var9);
            var6 = true;
        }

        this._$19 = 2;

        int var16;
        for(int var15 = 1; var15 < var4; ++var15) {
            for(var16 = 1; var16 < var3; ++var16) {
                ExtNormalCell var10;
                if ((var10 = (ExtNormalCell)this._$5.get(var15, var16)) != null) {
                    if (var10.getRow() == var15 && var10.getCol() == var16) {
                        this._$10 = var10;
                        if (var10.checkExtend(var1)) {
                            ExtExtendCell var17 = this._$3(var10);
                            this._$5.set(var15, var16, var17);
                        }
                    } else {
                        this._$5.set(var15, var16, this._$5.get(var10.getRow(), var10.getCol()));
                    }
                }
            }
        }

        this._$19 = 3;
        int var19;
        if (var2) {
            for(var16 = 1; var16 < var4; ++var16) {
                ExtCell var18 = (ExtCell)this._$5.get(var16, 0);
                this._$10 = var18;
                var18.prepProperties(var1);
            }

            for(var19 = 1; var19 < var3; ++var19) {
                ExtCell var11 = (ExtCell)this._$5.get(0, var19);
                this._$10 = var11;
                var11.prepProperties(var1);
            }
        }

        for(var16 = 1; var16 < var4; ++var16) {
            for(var19 = 1; var19 < var3; ++var19) {
                ExtNormalCell var20;
                if ((var20 = (ExtNormalCell)this._$5.get(var16, var19)) != null && var20.getRow() == var16 && var20.getCol() == var19) {
                    this._$10 = var20;
                    var20.setCellLeftHead(this._$1(var20));
                    var20.setCellTopHead(this._$2(var20));
                    var20.prepareCalculate(var1, (Map)var5);
                    if (var2) {
                        var20.prepProperties(var1);
                    }
                }
            }
        }

        for(var19 = 1; var19 < var4; ++var19) {
            for(int var21 = 1; var21 < var3; ++var21) {
                ExtCell var12;
                if ((var12 = (ExtCell)this._$5.get(var19, var21)) != null && var12.getRow() == var19 && var12.getCol() == var21) {
                    ExtCell var13;
                    if (!((var13 = var12.getCellLeftHead()) instanceof ExtExtendCell)) {
                        for(var13 = var13.getCellLeftHead(); !(var13 instanceof ExtExtendCell) && var13 != var12; var13 = var13.getCellLeftHead()) {
                            ;
                        }

                        if (var12 == var13) {
                            var12.setCellLeftHead(this._$23);
                        } else {
                            var12.setCellLeftHead(var13);
                        }
                    }

                    ExtCell var14;
                    if (!((var14 = var12.getCellTopHead()) instanceof ExtExtendCell)) {
                        for(var14 = var14.getCellTopHead(); !(var14 instanceof ExtExtendCell) && var14 != var12; var14 = var14.getCellTopHead()) {
                            ;
                        }

                        if (var12 == var14) {
                            var12.setCellTopHead(this._$23);
                        }

                        var12.setCellTopHead(var14);
                    }
                }
            }
        }

    }

    public void readExternal(ObjectInput var1) throws IOException, ClassNotFoundException {
        this._$25 = true;
        byte var2 = var1.readByte();
        this._$9 = (Map)var1.readObject();
        this._$12 = new Context();
        this._$12.setParamMap(this._$9);
        this._$20 = 0;
        this._$24 = (ReportDefine)var1.readObject();
        this._$6 = (Matrix)var1.readObject();
        this._$5 = (Matrix)var1.readObject();
        int var3 = 1;

        for(int var4 = this._$5.getRowSize(); var3 < var4; ++var3) {
            int var5 = 1;

            for(int var6 = this._$5.getColSize(); var5 < var6; ++var5) {
                ExtNormalCell var7;
                if ((var7 = (ExtNormalCell)this._$5.get(var3, var5)) != null && !var7.isMerged()) {
                    var7.setCol((short)var5);
                    var7.setRow(var3);
                }
            }
        }

        this._$10 = null;
        this._$11 = null;
        this._$17 = 0;
        this._$18 = 0;
        this._$19 = var1.readByte();
        this._$21 = var1.readShort();
        this._$23 = (ExtExtendCell)var1.readObject();
        this._$13 = new llIllIIIIlIIlIII();
        if (var2 > 1) {
            this._$16 = (List)var1.readObject();
        }

    }

    public void reflush() {
        this._$6();
        this._$7();
        this._$8();
        this._$12 = new Context();
        this._$12.setParamMap(this._$9);
        if (this._$7.size() > 0 || this._$8.size() > 0) {
            this._$21 = (short)(this._$21 | 4);
        }

        this._$7.reset();
        this._$8.reset();
        this._$5();
        this._$9();
    }

    public void removeCol(short var1) {
    }

    public void removeCol(short var1, short var2) {
    }

    public void removeRow(int var1) {
    }

    public void removeRow(int var1, int var2) {
    }

    public void resetDSCurrent(Context var1) {
        this._$13.setChanged(false);
        Map var2;
        if ((var2 = var1.getDataSetMap(false)) != null) {
            Iterator var3 = var2.entrySet().iterator();

            while(var3.hasNext()) {
                DataSet var5;
                if ((var5 = (DataSet)((Entry)var3.next()).getValue()) != null) {
                    var5.setCurrent((DsValue)null, true);
                }
            }
        }

    }

    public void resumeCurrent() {
        this._$10 = this._$11;
    }

    public void saveCurrent() {
        this._$11 = this._$10;
    }

    public byte[] serialize() throws IOException {
        ByteArrayOutputRecord var1 = new ByteArrayOutputRecord();
        int var2 = this._$5.getRowSize();
        short var3 = (short)this._$5.getColSize();
        var1.writeInt(var2);
        var1.writeShort(var3);

        for(int var4 = 1; var4 < var2; ++var4) {
            IRowCell var5 = this.getRowCell(var4);
            var1.writeRecord(var5);
        }

        for(short var15 = 1; var15 < var3; ++var15) {
            IColCell var6 = this.getColCell(var15);
            var1.writeRecord(var6);
        }

        ArrayList var16 = new ArrayList();

        for(int var7 = 1; var7 < var2; ++var7) {
            for(short var8 = 1; var8 < var3; ++var8) {
                INormalCell var9 = this.getCell(var7, var8);
                INormalCell var10 = null;
                if (var9 != null) {
                    if (!var9.isMerged()) {
                        var10 = ((ExtNormalCell)var9).engine.srcPrepare;
                    } else {
                        Area var11;
                        if ((var11 = var9.getMergedArea()).getBeginCol() == var8 && var11.getBeginRow() == var7) {
                            var10 = ((ExtNormalCell)var9).engine.srcPrepare;
                        }
                    }
                }

                if (var10 != null && !var16.contains(var10)) {
                    var16.add(var10);
                }
            }
        }

        int var17 = var16.size();
        var1.writeInt(var17);

        for(int var18 = 0; var18 < var17; ++var18) {
            var1.writeRecord((INormalCell)var16.get(var18));
        }

        for(int var19 = 1; var19 < var2; ++var19) {
            for(short var20 = 1; var20 < var3; ++var20) {
                INormalCell var12;
                if ((var12 = this.getCell(var19, var20)) == null) {
                    var1.writeRecord((IRecord)null);
                } else if (!var12.isMerged()) {
                    var1.writeRecord(var12);
                    INormalCell var23 = ((ExtNormalCell)var12).engine.srcPrepare;
                    var1.writeInt(var16.indexOf(var23));
                } else {
                    Area var13;
                    if ((var13 = var12.getMergedArea()).getBeginCol() == var20 && var13.getBeginRow() == var19) {
                        var1.writeRecord(var12);
                        INormalCell var14 = ((ExtNormalCell)var12).engine.srcPrepare;
                        var1.writeInt(var16.indexOf(var14));
                    } else {
                        var1.writeRecord((IRecord)null);
                    }
                }
            }
        }

        var1.writeRecord(this._$23);
        int var21 = this._$16 == null ? 0 : this._$16.size();
        var1.writeInt(var21);

        for(int var22 = 0; var22 < var21; ++var22) {
            Seal var24 = (Seal)this._$16.get(var22);
            var1.writeRecord(var24);
        }

        return var1.toByteArray();
    }

    public void setBBColor(int var1, short var2, int var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8 = var6.getRow() + var6.getRowMerge() - 1;
            short var9 = (short)(var7 + var6.getColMerge() - 1);
            if (var8 < var4 - 1) {
                if (var7 > 1) {
                    var7 = this._$1(var8, var7, (int)1);
                }

                if (var9 < var5 - 1) {
                    var9 = this._$2(var8, var9, (int)1);
                }

                for(short var10 = var7; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8, var10)) != null) {
                        var6.setBBColor(var3);
                    }
                }

                for(short var11 = var7; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8 + 1, var11)) != null) {
                        var6.setTBColor(var3);
                    }
                }

                return;
            }

            var6.setBBColor(var3);
        }

    }

    public void setBBStyle(int var1, short var2, byte var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8 = var6.getRow() + var6.getRowMerge() - 1;
            short var9 = (short)(var7 + var6.getColMerge() - 1);
            if (var8 < var4 - 1) {
                if (var7 > 1) {
                    var7 = this._$1(var8, var7, (int)1);
                }

                if (var9 < var5 - 1) {
                    var9 = this._$2(var8, var9, (int)1);
                }

                for(short var10 = var7; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8, var10)) != null) {
                        var6.setBBStyle(var3);
                    }
                }

                for(short var11 = var7; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8 + 1, var11)) != null) {
                        var6.setTBStyle(var3);
                    }
                }

                return;
            }

            var6.setBBStyle(var3);
        }

    }

    public void setBBWidth(int var1, short var2, float var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8 = var6.getRow() + var6.getRowMerge() - 1;
            short var9 = (short)(var7 + var6.getColMerge() - 1);
            if (var8 < var4 - 1) {
                if (var7 > 1) {
                    var7 = this._$1(var8, var7, (int)1);
                }

                if (var9 < var5 - 1) {
                    var9 = this._$2(var8, var9, (int)1);
                }

                for(short var10 = var7; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8, var10)) != null) {
                        var6.setBBWidth(var3);
                    }
                }

                for(short var11 = var7; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8 + 1, var11)) != null) {
                        var6.setTBWidth(var3);
                    }
                }

                return;
            }

            var6.setBBWidth(var3);
        }

    }

    public void setBackGraphConfig(BackGraphConfig var1) {
        this._$24.setBackGraphConfig(var1);
    }

    public void setCell(int var1, short var2, INormalCell var3) {
        if (var1 > 0 && var2 > 0 && var1 < this._$5.getRowSize() && var2 < this._$5.getColSize()) {
            this._$5.set(var1, var2, var3);
        }

    }

    public void setCellStyle(INormalCell var1, ICellStyle var2, ReportDefine var3, int var4, short var5) {
        if (var2.getHAlign() != null) {
            var1.setHAlign(var2.getHAlign());
        }

        if (var2.getVAlign() != null) {
            var1.setVAlign(var2.getVAlign());
        }

        if (var2.getIndent() != null) {
            var1.setIndent(var2.getIndent());
        }

        if (var2.getFontName() != null) {
            var1.setFontName(var2.getFontName());
        }

        if (var2.getFontSize() != null) {
            var1.setFontSize(var2.getFontSize());
        }

        if (var2.isBold() != null) {
            var1.setBold(var2.isBold());
        }

        if (var2.isItalic() != null) {
            var1.setItalic(var2.isItalic());
        }

        if (var2.isUnderline() != null) {
            var1.setUnderline(var2.isUnderline());
        }

        if (var2.isVisible() != null) {
            var1.setVisible(var2.isVisible());
        }

        if (var2.getForeColor() != null) {
            var1.setForeColor(var2.getForeColor());
        }

        if (var2.getBackColor() != null) {
            var1.setBackColor(var2.getBackColor());
        }

        if (var2.getLBStyle() != null) {
            var3.setLBStyle(var4, var5, var2.getLBStyle());
        }

        if (var2.getLBWidth() != null) {
            var3.setLBWidth(var4, var5, var2.getLBWidth());
        }

        if (var2.getLBColor() != null) {
            var3.setLBColor(var4, var5, var2.getLBColor());
        }

        if (var2.getRBStyle() != null) {
            var3.setRBStyle(var4, var5, var2.getRBStyle());
        }

        if (var2.getRBWidth() != null) {
            var3.setRBWidth(var4, var5, var2.getRBWidth());
        }

        if (var2.getRBColor() != null) {
            var3.setRBColor(var4, var5, var2.getRBColor());
        }

        if (var2.getTBStyle() != null) {
            var3.setTBStyle(var4, var5, var2.getTBStyle());
        }

        if (var2.getTBWidth() != null) {
            var3.setTBWidth(var4, var5, var2.getTBWidth());
        }

        if (var2.getTBColor() != null) {
            var3.setTBColor(var4, var5, var2.getTBColor());
        }

        if (var2.getBBStyle() != null) {
            var3.setBBStyle(var4, var5, var2.getBBStyle());
        }

        if (var2.getBBWidth() != null) {
            var3.setBBWidth(var4, var5, var2.getBBWidth());
        }

        if (var2.getBBColor() != null) {
            var3.setBBColor(var4, var5, var2.getBBColor());
        }

    }

    public void setCellValue(int var1, int var2, Object var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            var6.setValue(var3);
        }

    }

    public void setColCell(short var1, IColCell var2) {
        if (var1 > 0 && var1 < this._$5.getColSize()) {
            this._$5.set(0, var1, var2);
        }

    }

    public void setColWidth(int var1, int var2) {
        this.getColCell((short)var1).setColWidth(unUnitTransfer((float)var2, this.getUnit()));
    }

    public void setColWidth(int var1, int var2, int var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            byte var7 = this.getUnit();
            if (var3 == 0) {
                return;
            }

            Area var9;
            short var10 = (var9 = var6.getMergedArea()).getBeginCol();
            short var11 = var9.getEndCol();
            if (var6.isMerged()) {
                int var12 = var11 - var10 + 1;

                for(int var13 = var10; var13 <= var11; ++var13) {
                    if (!this.getColCell((short)var13).getColVisible()) {
                        --var12;
                    } else {
                        short var15 = 1;

                        for(short var16 = this.getColCount(); var15 <= var16; ++var15) {
                            INormalCell var17;
                            if ((var17 = this.getCell(var13, var15)) != null && var17.getColHidden()) {
                                --var12;
                                break;
                            }
                        }
                    }
                }

                int var14 = (int)Math.ceil((double)var3 / (double)var12);

                for(int var21 = var10; var21 <= var11; ++var21) {
                    IColCell var22;
                    if ((var22 = this.getColCell((short)var21)).getColVisible()) {
                        boolean var23 = true;
                        short var18 = 1;

                        for(short var19 = this.getColCount(); var18 <= var19; ++var18) {
                            INormalCell var20;
                            if ((var20 = this.getCell(var21, var18)) != null && var20.getRowHidden()) {
                                var23 = false;
                                break;
                            }
                        }

                        if (var23) {
                            var22.setColWidth(unUnitTransfer((float)var14, var7));
                        }
                    }
                }

                return;
            }

            this.getColCell((short)var2).setColWidth(unUnitTransfer((float)var3, this.getUnit()));
        }

    }

    public void setCurrent(ExtCell var1) {
        this._$10 = var1;
    }

    public void setCustomProperties(Map var1) {
    }

    public void setDSCurrent(Context var1) {
        Map var2;
        if ((var2 = var1.getDataSetMap(false)) != null) {
            Iterator var3 = var2.entrySet().iterator();

            while(var3.hasNext()) {
                DataSet var5;
                if ((var5 = (DataSet)((Entry)var3.next()).getValue()) != null && var5.getCurrent() == null) {
                    var5.setCurrent(var5.getRootGroup(), false);
                }
            }
        }

    }

    public void setDataSetMetaData(DataSetMetaData var1) {
        this._$24.setDataSetMetaData(var1);
    }

    public void setDispRatio(int var1) {
        this._$24.setDispRatio(var1);
    }

    public void setDispValue(int var1, int var2, String var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            var6.setDispValue(var3);
        }

    }

    public void setExportConfig(ExportConfig var1) {
        this._$24.setExportConfig(var1);
    }

    public void setFlowVar(FlowVar var1) {
        this._$24.setFlowVar(var1);
    }

    public void setFutureCellNumExp(String var1) {
        this._$24.setFutureCellNumExp(var1);
    }

    public void setHasDS(boolean var1) {
        this._$27 = var1;
    }

    public void setInput(byte var1) {
        this._$24.setInput(var1);
    }

    public void setLBColor(int var1, short var2, int var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8;
            int var9 = (var8 = var6.getRow()) + var6.getRowMerge() - 1;
            if (var7 > 1) {
                if (var8 > 1) {
                    var8 = this._$1(var8, var7, (short)-1);
                }

                if (var9 < var4 - 1) {
                    var9 = this._$2(var9, var7, (short)-1);
                }

                for(int var10 = var8; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var10, var7)) != null) {
                        var6.setLBColor(var3);
                    }
                }

                for(int var11 = var8; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var11, var7 - 1)) != null) {
                        var6.setRBColor(var3);
                    }
                }

                return;
            }

            var6.setLBColor(var3);
        }

    }

    public void setLBStyle(int var1, short var2, byte var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8;
            int var9 = (var8 = var6.getRow()) + var6.getRowMerge() - 1;
            if (var7 > 1) {
                if (var8 > 1) {
                    var8 = this._$1(var8, var7, (short)-1);
                }

                if (var9 < var4 - 1) {
                    var9 = this._$2(var9, var7, (short)-1);
                }

                for(int var10 = var8; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var10, var7)) != null) {
                        var6.setLBStyle(var3);
                    }
                }

                for(int var11 = var8; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var11, var7 - 1)) != null) {
                        var6.setRBStyle(var3);
                    }
                }

                return;
            }

            var6.setLBStyle(var3);
        }

    }

    public void setLBWidth(int var1, short var2, float var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8;
            int var9 = (var8 = var6.getRow()) + var6.getRowMerge() - 1;
            if (var7 > 1) {
                if (var8 > 1) {
                    var8 = this._$1(var8, var7, (short)-1);
                }

                if (var9 < var4 - 1) {
                    var9 = this._$2(var9, var7, (short)-1);
                }

                for(int var10 = var8; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var10, var7)) != null) {
                        var6.setLBWidth(var3);
                    }
                }

                for(int var11 = var8; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var11, var7 - 1)) != null) {
                        var6.setRBWidth(var3);
                    }
                }

                return;
            }

            var6.setLBWidth(var3);
        }

    }

    public static final void setLicenseFileName(String var0) {
        _$33 = var0;
        _$34 = true;
        if (var0 == null) {
            _$32 = new ExtCellSet();
            _$34 = false;
        }

    }

    public void setMacroMetaData(MacroMetaData var1) {
        this._$24.setMacroMetaData(var1);
    }

    public void setNotes(String var1) {
        this._$24.setNotes(var1);
    }

    public void setPageBorderMode(byte var1) {
    }

    public void setParamMetaData(ParamMetaData var1) {
        this._$24.setParamMetaData(var1);
    }

    public void setPrintSetup(PrintSetup var1) {
        this._$24.setPrintSetup(var1);
    }

    public void setRBColor(int var1, short var2, int var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = (short)(var6.getCol() + var6.getColMerge() - 1);
            int var8;
            int var9 = (var8 = var6.getRow()) + var6.getRowMerge() - 1;
            if (var7 < var5 - 1) {
                if (var8 > 1) {
                    var8 = this._$1(var8, var7, (short)1);
                }

                if (var9 < var4 - 1) {
                    var9 = this._$2(var9, var7, (short)1);
                }

                for(int var10 = var8; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var10, var7)) != null) {
                        var6.setRBColor(var3);
                    }
                }

                for(int var11 = var8; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var11, var7 + 1)) != null) {
                        var6.setLBColor(var3);
                    }
                }

                return;
            }

            var6.setRBColor(var3);
        }

    }

    public void setRBStyle(int var1, short var2, byte var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = (short)(var6.getCol() + var6.getColMerge() - 1);
            int var8;
            int var9 = (var8 = var6.getRow()) + var6.getRowMerge() - 1;
            if (var7 < var5 - 1) {
                if (var8 > 1) {
                    var8 = this._$1(var8, var7, (short)1);
                }

                if (var9 < var4 - 1) {
                    var9 = this._$2(var9, var7, (short)1);
                }

                for(int var10 = var8; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var10, var7)) != null) {
                        var6.setRBStyle(var3);
                    }
                }

                for(int var11 = var8; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var11, var7 + 1)) != null) {
                        var6.setLBStyle(var3);
                    }
                }

                return;
            }

            var6.setRBStyle(var3);
        }

    }

    public void setRBWidth(int var1, short var2, float var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = (short)(var6.getCol() + var6.getColMerge() - 1);
            int var8;
            int var9 = (var8 = var6.getRow()) + var6.getRowMerge() - 1;
            if (var7 < var5 - 1) {
                if (var8 > 1) {
                    var8 = this._$1(var8, var7, (short)1);
                }

                if (var9 < var4 - 1) {
                    var9 = this._$2(var9, var7, (short)1);
                }

                for(int var10 = var8; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var10, var7)) != null) {
                        var6.setRBWidth(var3);
                    }
                }

                for(int var11 = var8; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var11, var7 + 1)) != null) {
                        var6.setLBWidth(var3);
                    }
                }

                return;
            }

            var6.setRBWidth(var3);
        }

    }

    public void setReportDefine(ReportDefine var1) {
        this._$24 = var1;
    }

    public void setReportListener(String var1) {
        this._$30 = var1;
    }

    public void setReportStyleName(String var1) {
    }

    public void setReportType(byte var1) {
        this._$24.setReportType(var1);
    }

    public void setRowCell(int var1, IRowCell var2) {
        if (var1 > 0 && var1 < this._$5.getRowSize()) {
            this._$5.set(var1, 0, var2);
        }

    }

    public void setRowHeight(int var1, int var2) {
        this.getRowCell(var1).setRowHeight(unUnitTransfer((float)var2, this.getUnit()));
    }

    public void setRowHeight(int var1, int var2, int var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            byte var7 = this.getUnit();
            if (var3 == 0) {
                return;
            }

            Area var9;
            int var10 = (var9 = var6.getMergedArea()).getBeginRow();
            int var11 = var9.getEndRow();
            if (var6.isMerged()) {
                int var12 = var11 - var10 + 1;

                for(int var13 = var10; var13 <= var11; ++var13) {
                    if (!this.getRowCell(var13).getRowVisible()) {
                        --var12;
                    } else {
                        short var15 = 1;

                        for(short var16 = this.getColCount(); var15 <= var16; ++var15) {
                            INormalCell var17;
                            if ((var17 = this.getCell(var13, var15)) != null && var17.getRowHidden()) {
                                --var12;
                                break;
                            }
                        }
                    }
                }

                int var14 = (int)Math.ceil((double)var3 / (double)var12);

                for(int var21 = var10; var21 <= var11; ++var21) {
                    IRowCell var22;
                    if ((var22 = this.getRowCell(var21)).getRowVisible()) {
                        boolean var23 = true;
                        short var18 = 1;

                        for(short var19 = this.getColCount(); var18 <= var19; ++var18) {
                            INormalCell var20;
                            if ((var20 = this.getCell(var21, var18)) != null && var20.getRowHidden()) {
                                var23 = false;
                                break;
                            }
                        }

                        if (var23) {
                            var22.setRowHeight(unUnitTransfer((float)var14, var7));
                        }
                    }
                }

                return;
            }

            this.getRowCell(var10).setRowHeight(unUnitTransfer((float)var3, var7));
        }

    }

    public void setSealList(List var1) {
        this._$16 = var1;
    }

    public final void setSemanticsExpFlag(boolean var1) {
        this._$26 = var1;
    }

    public void setState(byte var1) {
        this._$19 = var1;
    }

    public void setSubReportMetaData(SubReportMetaData var1) {
        this._$24.setSubReportMetaData(var1);
    }

    public void setSubRptType(byte var1) {
        this._$20 = var1;
    }

    public void setSubmit(byte var1) {
        this._$24.setSubmit(var1);
    }

    public void setTBColor(int var1, short var2, int var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8 = var6.getRow();
            short var9 = (short)(var7 + var6.getColMerge() - 1);
            if (var8 > 1) {
                if (var7 > 1) {
                    var7 = this._$1(var8, var7, (int)-1);
                }

                if (var9 < var5 - 1) {
                    var9 = this._$2(var8, var9, (int)-1);
                }

                for(short var10 = var7; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8, var10)) != null) {
                        var6.setTBColor(var3);
                    }
                }

                for(short var11 = var7; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8 - 1, var11)) != null) {
                        var6.setBBColor(var3);
                    }
                }

                return;
            }

            var6.setTBColor(var3);
        }

    }

    public void setTBStyle(int var1, short var2, byte var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8 = var6.getRow();
            short var9 = (short)(var7 + var6.getColMerge() - 1);
            if (var8 > 1) {
                if (var7 > 1) {
                    var7 = this._$1(var8, var7, (int)-1);
                }

                if (var9 < var5 - 1) {
                    var9 = this._$2(var8, var9, (int)-1);
                }

                for(short var10 = var7; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8, var10)) != null) {
                        var6.setTBStyle(var3);
                    }
                }

                for(short var11 = var7; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8 - 1, var11)) != null) {
                        var6.setBBStyle(var3);
                    }
                }

                return;
            }

            var6.setTBStyle(var3);
        }

    }

    public void setTBWidth(int var1, short var2, float var3) {
        int var4 = this._$5.getRowSize();
        int var5 = this._$5.getColSize();
        ExtNormalCell var6;
        if (var1 > 0 && var1 < var4 && var2 > 0 && var2 < var5 && (var6 = (ExtNormalCell)this._$5.get(var1, var2)) != null) {
            short var7 = var6.getCol();
            int var8 = var6.getRow();
            short var9 = (short)(var7 + var6.getColMerge() - 1);
            if (var8 > 1) {
                if (var7 > 1) {
                    var7 = this._$1(var8, var7, (int)-1);
                }

                if (var9 < var5 - 1) {
                    var9 = this._$2(var8, var9, (int)-1);
                }

                for(short var10 = var7; var10 <= var9; ++var10) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8, var10)) != null) {
                        var6.setTBWidth(var3);
                    }
                }

                for(short var11 = var7; var11 <= var9; ++var11) {
                    if ((var6 = (ExtNormalCell)this._$5.get(var8 - 1, var11)) != null) {
                        var6.setBBWidth(var3);
                    }
                }

                return;
            }

            var6.setTBWidth(var3);
        }

    }

    public void setTip(String var1) {
        this._$24.setTip(var1);
    }

    public void setUnit(byte var1) {
        this._$24.setUnit(var1);
    }

    public static float unUnitTransfer(float var0, byte var1) {
        if (var1 == 2) {
            var0 /= 72.0F;
        } else if (var1 == 1) {
            var0 = var0 * 25.4F / 72.0F;
        }

        return var0;
    }

    public static float unitTransfer(float var0, byte var1) {
        if (var1 == 2) {
            var0 *= 72.0F;
        } else if (var1 == 1) {
            var0 = var0 * 72.0F / 25.4F;
        }

        return var0;
    }

    public void writeExternal(ObjectOutput var1) throws IOException {
        var1.writeByte(2);
        var1.writeObject(this._$9);
        var1.writeObject(this._$24);
        var1.writeObject(this._$6);
        var1.writeObject(this._$5);
        var1.writeByte(this._$19);
        var1.writeShort(this._$21);
        var1.writeObject(this._$23);
        var1.writeObject(this._$16);
    }
}
