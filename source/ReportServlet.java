package com.runqian.report4.view;

import com.runqian.base4.resources.MessageManager;
import com.runqian.base4.resources.ServerMessage;
import com.runqian.base4.util.ArgumentTokenizer;
import com.runqian.base4.util.DBTypes;
import com.runqian.base4.util.Logger;
import com.runqian.base4.util.Native2Ascii;
import com.runqian.base4.util.ObjectCache;
import com.runqian.base4.util.PwdUtils;
import com.runqian.base4.util.ReadJavaScriptServlet;
import com.runqian.base4.util.SegmentSet;
import com.runqian.base4.util.Sentence;
import com.runqian.base4.util.StringUtils;
import com.runqian.report4.cache.CacheManager;
import com.runqian.report4.cache.CacheMonitor;
import com.runqian.report4.cache.Cluster;
import com.runqian.report4.cache.Member;
import com.runqian.report4.control.ControlUtils;
import com.runqian.report4.dataset.JNDIConnectionFactory;
import com.runqian.report4.ide.base.DataSource;
import com.runqian.report4.input.AjaxFormat;
import com.runqian.report4.input.SaveDataServlet;
import com.runqian.report4.input.SaveGroupServlet;
import com.runqian.report4.input.SaveSheetsServlet;
import com.runqian.report4.input.SaveToLocalServlet;
import com.runqian.report4.input.SelectExcelFileServlet;
import com.runqian.report4.input.SelectUploadFile;
import com.runqian.report4.input.Tree;
import com.runqian.report4.input.UploadExcelServlet;
import com.runqian.report4.input.UploadFile2DBServlet;
import com.runqian.report4.model.engine.ExtCellSet;
import com.runqian.report4.model.engine.ExtNormalCell;
import com.runqian.report4.model.expression.FunctionLib;
import com.runqian.report4.semantics.SemanticsManager;
import com.runqian.report4.tag.CommonQuerySaveAction;
import com.runqian.report4.tag.DefaultTagProps;
import com.runqian.report4.tag.HtmlTag;
import com.runqian.report4.usermodel.Context;
import com.runqian.report4.usermodel.DataSourceConfig;
import com.runqian.report4.usermodel.IConnectionFactory;
import com.runqian.report4.usermodel.ILetterSpacing;
import com.runqian.report4.usermodel.PerfMonitor;
import com.runqian.report4.usermodel.ReportGroup;
import com.runqian.report4.util.DMUtil;
import com.runqian.report4.util.ReportUtils2;
import com.runqian.report4.util.StyleConfig;
import com.runqian.report4.view.excel.ConfigExcelServlet;
import com.runqian.report4.view.excel.ExcelReportServlet;
import com.runqian.report4.view.excel.SaveAsExcelServlet;
import com.runqian.report4.view.html.GraphPool;
import com.runqian.report4.view.html.GraphServlet;
import com.runqian.report4.view.html.HtmlReport;
import com.runqian.report4.view.olap.InputBox;
import com.runqian.report4.view.olap.ListOlapStatus;
import com.runqian.report4.view.olap.OlapLet;
import com.runqian.report4.view.olap.OlapSaveAction;
import com.runqian.report4.view.olap.ShowOlapDetail;
import com.runqian.report4.view.olap.ShowOlapGraph;
import com.runqian.report4.view.olap.expexcel.OlapExpExcelAction;
import com.runqian.report4.view.paj.PajReportServlet;
import com.runqian.report4.view.pdf.ConfigPdfServlet;
import com.runqian.report4.view.pdf.PdfReportServlet;
import com.runqian.report4.view.text.TextFileServlet;
import com.runqian.report4.view.word.WordReportServlet;
import com.yinhai.ta404.core.utils.ServiceLocator;
import org.springframework.context.ResourceLoaderAware;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Properties;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReportServlet extends HttpServlet {
    public static final int ACTION_APPLET = 1;
    public static final int ACTION_PRINT = 2;
    public static final int ACTION_EXCELREPORT = 3;
    public static final int ACTION_CONFIGEXCEL = 4;
    public static final int ACTION_CONFIGPDF = 25;
    public static final int ACTION_SAVEASEXCEL = 5;
    public static final int ACTION_PDFREPORT = 6;
    public static final int ACTION_WORDREPORT = 7;
    public static final int ACTION_SETREPORTPARAMS = 8;
    public static final int ACTION_GRAPH = 9;
    public static final int ACTION_READJS = 10;
    public static final int ACTION_SAVEDATA = 11;
    public static final int ACTION_SAVETOLOCAL = 12;
    public static final int ACTION_SELECTUPLOADFILE = 13;
    public static final int ACTION_UPLOADFILE2DB = 14;
    public static final int ACTION_SAVESHEETS = 15;
    public static final int ACTION_UPLOADEXCEL = 16;
    public static final int ACTION_SELECTEXCELFILE = 17;
    public static final int ACTION_TEXTFILE = 18;
    public static final int ACTION_OLAPDETAIL = 19;
    public static final int ACTION_OLAPGRAPH = 20;
    public static final int ACTION_SAVEGROUP = 21;
    public static final int ACTION_OLAPEXPORTEXCEL = 22;
    public static final int ACTION_OLAPSAVE = 23;
    public static final int ACTION_INPUTBOX = 24;
    public static final int ACTION_SAVECOMMONQUERY = 26;
    public static final int ACTION_AJAXFORMAT = 27;
    public static final int ACTION_PAJFILE = 28;
    public static final int ACTION_DIRECTPRINT = 30;
    public static String appUrlPrefix = null;
    private static boolean _$5 = true;
    public static boolean releaseTag = true;
    private static CacheMonitor _$4;
    private static CleanTimeoutParams _$3;
    private static Cluster _$2 = null;
    public static String jreInstallName = "/j2re-1_4_1-windows-i586-i.exe#Version=1,4,1,0";
    public static String jreVersion = "1.4";
    public static String windir;
    public static String tempDir;
    private static boolean _$1;
    public static String flashGraphDir = "/flashGraph";
    public static String jsDomain = "";
    public static String serverEncode = null;
    public static boolean recalcWhileTimeout = false;
    public static String errorPage4export = null;

    public ReportServlet() {
    }


    public void init() throws ServletException {
        String headless = this.getServletConfig().getInitParameter("headless");
        if (headless == null || headless.trim().length() == 0) {
            headless = "true";
        }

        if (!"none".equalsIgnoreCase(headless)) {
            System.setProperty("java.awt.headless", headless);
        }

        System.out.println("Report System initing......");
        ServletContext application = this.getServletContext();
        String configFilePath = this.getServletConfig().getInitParameter("configFile");
        application.setAttribute("___reportConfigFile", configFilePath);
        loadConfig(application, false);
        System.out.println("Report System initialized......");
    }

    public static void loadConfig(ServletContext application, boolean isReload) throws ServletException {
        Context initContext = new Context();
        initContext.setApplication(application);
        String configFilePath = (String)application.getAttribute("___reportConfigFile");
        if (!"".equals(configFilePath) && configFilePath != null) {
            loadConfig(application, configFilePath, initContext, isReload);
        } else {
            throw new ServletException("No report config file!");
        }
    }

    public static void loadConfig(ServletContext application, String configFilePath, Context initContext, boolean isReload) throws ServletException {
        IReportConfigManager config = ReportConfigManagerImpl.getInstance();
        Object is = null;

        try {
            try {
                Class<?> cla = null;
                cla = Class.forName("com.runqianapp.ngr.data.IOManagerUtils");
                Class<?> c = Class.forName("com.runqianapp.utils.IDataReader");
                Method m = cla.getMethod("getReader");
                Object idr = m.invoke(cla);
                Method method = c.getMethod("getInputStream", String.class);
                Object[] params = new Object[]{configFilePath};
                is = (InputStream)method.invoke(idr, params);
            } catch (Throwable var93) {
                ;
            }

/*            if (is == null) {
                is = application.getResourceAsStream(configFilePath);
            }*/
            if(is == null){
                Resource resource = ServiceLocator.getAppContext().getResource(configFilePath);
                is = resource.getInputStream();
            }
            if (is == null) {
                is = new FileInputStream(configFilePath);
            }

            config.setInputStream((InputStream)is);
        } catch (Throwable var95) {
            Logger.debug(var95);
            throw new ServletException(var95);
        } finally {
            try {
                ((InputStream)is).close();
            } catch (Exception var76) {
                ;
            }

        }

        Context.setReportConfigModel(config.getReportConfigModel());
        String logConfig = config.getInitParameter("logConfig");
        if (logConfig != null && logConfig.trim().length() > 0) {
            //InputStream lcis = application.getResourceAsStream(logConfig);
            InputStream lcis = null;
            try {
                lcis = ServiceLocator.getAppContext().getResource("classpath:runqian/"+logConfig).getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (lcis != null) {
                Properties p = new Properties();

                try {
                    p.load(lcis);
                    Logger.setPropertyConfig(p);
                    lcis.close();
                } catch (Exception var92) {
                    ;
                }
            }
        }

        windir = config.getInitParameter("windir");
        String license = config.getInitParameter("license");

      //  license=resourceLoader.getResource(license).getFilename();
/*        if (license == null || license.trim().length() == 0) {
            license = "/WEB-INF/xrq_license_server.lic";
        }

        if (!(new File(license)).exists() && license.startsWith("/")) {
            license = application.getRealPath(license);
        }*/

        MessageManager mm = ServerMessage.get();
        ExtCellSet.setLicenseFileName(license);

        String olapDsc;
        String timeout;
        String jndiPrefix;
        String dsc;
        try {
            ExtCellSet lc = ExtCellSet.get();
            if (lc.getType() != 1) {
                throw new Exception(mm.getMessage("license.notserver"));
            }

            jndiPrefix = lc.getOSName();
            dsc = System.getProperty("os.name").toLowerCase();
            if (jndiPrefix.equalsIgnoreCase("other")) {
                if (dsc.indexOf("windows") >= 0 || dsc.indexOf("linux") >= 0) {
                    throw new Exception(mm.getMessage("license.other"));
                }
            } else if (dsc.indexOf(jndiPrefix.toLowerCase()) < 0) {
                throw new Exception(mm.getMessage("license.win_linux", jndiPrefix));
            }

            if (lc.getVersion() == 22) {
                olapDsc = "127.0.0.1";

                try {
                    olapDsc = InetAddress.getLocalHost().getHostAddress();
                } catch (Throwable var91) {
                    ;
                }

                if (!lc.checkIP(olapDsc)) {
                    throw new Exception(mm.getMessage("license.ip", olapDsc));
                }
            }

            if (lc.getVersion() == 22 || lc.getVersion() == 21) {
                int[] codes = lc.getProperties();
                if (codes != null && codes.length > 0) {
                    int bindcode = ExtNormalCell.getBindCode();
                    boolean rightCode = false;
                    timeout = "";

                    for(int i = 0; i < codes.length; ++i) {
                        if (bindcode == codes[i]) {
                            rightCode = true;
                        }

                        if (timeout.length() > 0) {
                            timeout = timeout + ",";
                        }

                        timeout = timeout + codes[i];
                    }

                    if (!rightCode) {
                        throw new Exception(mm.getMessage("license.bindcode", String.valueOf(bindcode), timeout));
                    }
                }
            }
        } catch (Throwable var94) {
            Logger.info(mm.getMessage("license.error") + "\n" + var94.getMessage());
            throw new ServletException(var94.getMessage());
        }

        String reportFileHome = config.getInitParameter("reportFileHome");
        if (reportFileHome == null) {
            reportFileHome = "/";
        }

        if (reportFileHome != null) {
            reportFileHome = StringUtils.replace(reportFileHome, "\\", "/");
            File tmpFile = new File(reportFileHome);
            if (!tmpFile.exists() && !reportFileHome.startsWith("/")) {
                reportFileHome = "/" + reportFileHome;
            }
        }

        _$1 = isDMInstalled();
        Context.setMainDir(reportFileHome);
        jndiPrefix = config.getInitParameter("JNDIPrefix");
        if (jndiPrefix == null) {
            jndiPrefix = "";
        }

        if (jndiPrefix.trim().length() > 0 && !jndiPrefix.endsWith("/")) {
            jndiPrefix = jndiPrefix.trim() + "/";
        }

        dsc = config.getInitParameter("dataSource");
        if (dsc != null && dsc.trim().length() > 0) {
            _$1(dsc, jndiPrefix, initContext, mm);
        }

        _$1(initContext, config.getReportConfigModel());
        olapDsc = config.getInitParameter("olapDataSource");
        if (olapDsc != null && olapDsc.trim().length() > 0) {
            ;
        }

        Context.setInitCtx(initContext);
        String jspCharset = config.getInitParameter("jspCharset");
        if (jspCharset != null && jspCharset.trim().length() > 0) {
            Context.setJspCharset(jspCharset);
        }

        String svrEncode = config.getInitParameter("serverEncode");
        if (svrEncode != null && svrEncode.trim().length() > 0) {
            serverEncode = svrEncode;
        }

        timeout = config.getInitParameter("cachedParamsTimeout");
        if (timeout != null && timeout.trim().length() > 0) {
            try {
                ParamsPool.timeOut = Integer.parseInt(timeout);
            } catch (Exception var90) {
                ;
            }
        }

        if (Logger.isAllLevel()) {
            CacheManager.setLogMode(true);
        }

        String tempdir = config.getInitParameter("cachedReportDir");
        String sysTempdir = System.getProperty("java.io.tmpdir");
        sysTempdir = (new File(sysTempdir)).getAbsolutePath();
        if (tempdir != null && tempdir.trim().length() > 0) {
            File tmpfile = new File(tempdir);
            if (!tmpfile.exists()) {
                if (!tmpfile.mkdirs()) {
                    Logger.info(mm.getMessage("config.cachdir1", sysTempdir));
                    tempdir = sysTempdir;
                }
            } else if (!tmpfile.isDirectory()) {
                Logger.info(mm.getMessage("config.cachdir2", sysTempdir));
                tempdir = sysTempdir;
            } else if (!tmpfile.canWrite()) {
                Logger.info(mm.getMessage("config.cachdir3", sysTempdir));
                tempdir = sysTempdir;
            }
        } else {
            Logger.info(mm.getMessage("config.cachdir4", sysTempdir));
            tempdir = sysTempdir;
        }

        CacheManager manager = CacheManager.getInstance();
        manager.setCachePath(tempdir);
        GraphPool.dir = tempdir;
        tempDir = tempdir;
        String cachIdPrefix = config.getInitParameter("cachedIdPrefix");
        if (cachIdPrefix != null && cachIdPrefix.trim().length() > 0) {
            cachIdPrefix = cachIdPrefix.trim();
            manager.setIDPrefix(cachIdPrefix);
        }

        manager.deleteTempFiles();
        long time = 3600000L;
        timeout = config.getInitParameter("cachedReportTimeout");
        if (timeout != null && timeout.trim().length() > 0) {
            try {
                time = (long)(Integer.parseInt(timeout) * 60 * 1000);
            } catch (Exception var89) {
                ;
            }
        }

        String s_interval = config.getInitParameter("cacheMonitorInterval");
        long interval = 300000L;
        if (s_interval != null && s_interval.trim().length() > 0) {
            try {
                interval = (long)(Integer.parseInt(s_interval) * 1000);
            } catch (Exception var88) {
                ;
            }
        }

        _$4 = new CacheMonitor(time, interval);
        _$4.start();
        _$3 = new CleanTimeoutParams();
        _$3.start();
        String maxCellNum = config.getInitParameter("maxCellNum");
        if (maxCellNum != null && maxCellNum.trim().length() > 0) {
            try {
                PerfMonitor.setCellNumLimitEnabled(true);
                PerfMonitor.setMaxCellNum(Integer.parseInt(maxCellNum));
            } catch (Exception var87) {
                ;
            }
        }

        String maxConcurrentForReport = config.getInitParameter("maxConcurrentForReport");
        if (maxConcurrentForReport != null && maxConcurrentForReport.trim().length() > 0) {
            try {
                PerfMonitor.setMaxConcurrentTaskNum(Integer.parseInt(maxConcurrentForReport));
            } catch (Exception var86) {
                ;
            }
        }

        String maxWaitForReport = config.getInitParameter("maxWaitForReport");
        if (maxWaitForReport != null && maxWaitForReport.trim().length() > 0) {
            try {
                PerfMonitor.setMaxWaitingTaskName(Integer.parseInt(maxWaitForReport));
            } catch (Exception var85) {
                ;
            }
        }

        String maxWaitTimeForReport = config.getInitParameter("maxWaitTimeForReport");
        if (maxWaitTimeForReport != null && maxWaitTimeForReport.trim().length() > 0) {
            try {
                PerfMonitor.setWaitTimeAfterOutOfMemory(Integer.parseInt(maxWaitTimeForReport) * 1000);
            } catch (Exception var84) {
                ;
            }
        }

        String urlPrefix = config.getInitParameter("appUrlPrefix");
        if (urlPrefix != null && urlPrefix.trim().length() > 0) {
            appUrlPrefix = urlPrefix;
            if (urlPrefix.indexOf("/APPMAP") >= 0) {
                _$5 = false;
            }
        }

        String alwaysReloadDefine = config.getInitParameter("alwaysReloadDefine");
        if (alwaysReloadDefine != null && alwaysReloadDefine.trim().length() > 0) {
            manager.setRealtimeReload("yes".equalsIgnoreCase(alwaysReloadDefine));
        }

        String semanticsFile = config.getInitParameter("semanticsFile");
        if (semanticsFile != null && semanticsFile.trim().length() > 0) {
            if (!semanticsFile.startsWith("/")) {
                semanticsFile = "/" + semanticsFile;
            }

            try {
                SemanticsManager sm = new SemanticsManager();
                sm.readXMLStream(application.getResourceAsStream(semanticsFile));
                Context.setDefSemanticsManager(sm);
            } catch (Throwable var83) {
                Logger.debug(var83.getMessage(), var83);
            }
        }

        String treeImagesPath = config.getInitParameter("treeImagesPath");
        if (treeImagesPath != null && treeImagesPath.trim().length() > 0) {
            Tree.IMAGEPATH = treeImagesPath;
        }

        String errorPage = config.getInitParameter("errorPage");
        if (errorPage != null && errorPage.trim().length() > 0) {
            DefaultTagProps.errorPage = errorPage;
        }

        errorPage = config.getInitParameter("errorPage4export");
        if (errorPage != null && errorPage.trim().length() > 0) {
            errorPage4export = errorPage;
        }

        String recalc = config.getInitParameter("recalcWhileTimeout");
        if (recalc != null && recalc.trim().length() > 0) {
            recalcWhileTimeout = "yes".equalsIgnoreCase(recalc);
        }

        String webServerType = config.getInitParameter("webServerType");
        if (webServerType != null && webServerType.trim().length() > 0) {
            releaseTag = !"resin".equalsIgnoreCase(webServerType);
        }

        String tmp = config.getInitParameter("jreInstallName");
        if (tmp != null && tmp.trim().length() > 0) {
            jreInstallName = tmp;
            tmp = config.getInitParameter("jreVersion");
            if (tmp != null && tmp.trim().length() > 0) {
                jreVersion = tmp;
            }
        }

        String members = config.getInitParameter("clusterMember");
        String olapDir;
        if (members != null && members.trim().length() > 0) {
            _$2 = new Cluster();
            ArgumentTokenizer at = new ArgumentTokenizer(members, ';');

            while(at.hasMoreTokens()) {
                olapDir = at.nextToken().trim();
                if (olapDir.length() != 0) {
                    Member member = new Member();
                    ArgumentTokenizer at1 = new ArgumentTokenizer(olapDir, ',');
                    member.setName(at1.nextToken().trim());
                    member.setHost(at1.nextToken().trim());
                    member.setPort(Integer.parseInt(at1.nextToken()));
                    _$2.addMemeber(member);
                }
            }

            olapDir = config.getInitParameter("isCachedFileShared");
            if (olapDir != null && olapDir.trim().length() > 0) {
                _$2.setFileShared("yes".equalsIgnoreCase(olapDir.trim()));
            }

            try {
                _$2.start();
            } catch (Throwable var82) {
                Logger.error(var82.getMessage(), var82);
                throw new ServletException(var82.getMessage(), var82);
            }

            manager.setCluster(_$2);
        }

        String ogp = config.getInitParameter("olapGraphProfile");
        if (ogp != null && ogp.trim().length() > 0) {
            ShowOlapGraph.profile = ogp.trim();
        }

        olapDir = config.getInitParameter("olapDir");
        if (olapDir != null && olapDir.trim().length() > 0) {
            OlapLet.olapDir = olapDir;
        }

        String picFileExistTime = config.getInitParameter("picFileExistTime");
        if (picFileExistTime != null && picFileExistTime.trim().length() > 0) {
            try {
                GraphPool.saveTime = Integer.parseInt(picFileExistTime);
            } catch (Exception var81) {
                ;
            }
        }

        String promptReplace = config.getInitParameter("uploadPromptReplace");
        if (promptReplace != null && "yes".equalsIgnoreCase(promptReplace)) {
            SelectUploadFile.promptReplace = true;
        }

        String reportEnterUrl = config.getInitParameter("reportEnterUrl");
        if (reportEnterUrl != null && reportEnterUrl.trim().length() > 0) {
            HtmlTag.generalReportEnterUrl = reportEnterUrl;
        }

        String fgd = config.getInitParameter("flashGraphDir");
        if (fgd != null && fgd.trim().length() > 0) {
            flashGraphDir = fgd.trim();
            if (!flashGraphDir.startsWith("/")) {
                flashGraphDir = "/" + flashGraphDir;
            }
        }

        String jsd = config.getInitParameter("jsDomain");
        if (jsd != null && jsd.trim().length() > 0) {
            jsDomain = jsd.trim();
        }

        int wrapWidth = ControlUtils.getWrapInchingWidth();
        boolean wrapByChar = false;
        String wrapInchingWidth = config.getInitParameter("wrapInchingWidth");
        if (wrapInchingWidth != null && wrapInchingWidth.trim().length() > 0) {
            try {
                wrapWidth = Integer.parseInt(wrapInchingWidth);
            } catch (Exception var80) {
                ;
            }
        }

        String swrapByChar = config.getInitParameter("wrapByChar");
        if (swrapByChar != null && swrapByChar.trim().length() > 0) {
            wrapByChar = "yes".equalsIgnoreCase(swrapByChar);
        }

        ControlUtils.setWrapProperty(wrapWidth, wrapByChar);
        String numberFormatClass = config.getInitParameter("numberFormatClass");
        if (numberFormatClass != null && numberFormatClass.trim().length() > 0) {
            try {
                ObjectCache.setNumberFormatClass(Class.forName(numberFormatClass));
            } catch (Exception var79) {
                var79.printStackTrace();
            }
        }

        String CSSFile = config.getInitParameter("CSSFile");
        if (CSSFile != null && CSSFile.trim().length() > 0) {
            if (!CSSFile.startsWith("/")) {
                CSSFile = "/" + CSSFile;
            }

            StyleConfig.setConfigName(application.getRealPath(CSSFile));
        }

        String defaultCellNum = config.getInitParameter("defaultCellNum");
        if (defaultCellNum != null && defaultCellNum.trim().length() > 0) {
            try {
                PerfMonitor.setDefFutureCellNumPerReport(Integer.parseInt(defaultCellNum));
            } catch (Exception var78) {
                ;
            }
        }

        String letterSpacingClass = config.getInitParameter("letterSpacingClass");
        if (letterSpacingClass != null && letterSpacingClass.trim().length() > 0) {
            try {
                HtmlReport.ils = (ILetterSpacing)Class.forName(letterSpacingClass).newInstance();
            } catch (Throwable var77) {
                var77.printStackTrace();
            }
        }

        String customFunction = config.getInitParameter("customFunction");
        if (customFunction != null && customFunction.trim().length() > 0) {
            if (!(new File(customFunction)).exists() && customFunction.startsWith("/")) {
                customFunction = application.getRealPath(customFunction);
            }

            FunctionLib.setCustomFileName(customFunction);
        } else {
            FunctionLib.setCustomFileName("/config/customFunctions.properties");
        }

        ServletMappings.application = application;
        readServletMappings(application);
    }

    private static void _$1(String dsc, String jndiPrefix, Context ctx, MessageManager mm) {
        ArgumentTokenizer at = new ArgumentTokenizer(dsc, ';');
        String defaultDsc = at.nextToken();
        _$1(defaultDsc, jndiPrefix, ctx, mm, true);

        while(at.hasMoreTokens()) {
            String dsc1 = at.nextToken();
            _$1(dsc1, jndiPrefix, ctx, mm, false);
        }

    }

    public static void addJNDIDsConfig(String dsconfig, String jndiPrefix, boolean isDefault) {
        if (jndiPrefix.trim().length() > 0 && !jndiPrefix.endsWith("/")) {
            jndiPrefix = jndiPrefix.trim() + "/";
        }

        _$1(dsconfig, jndiPrefix, Context.getInitCtx(), ServerMessage.get(), isDefault);
    }

    private static void _$1(String dsc, String jndiPrefix, Context ctx, MessageManager mm, boolean isDefault) {
        if (dsc != null && dsc.trim().length() != 0) {
            ArgumentTokenizer at = new ArgumentTokenizer(dsc, ',');
            int count = at.countTokens();
            String dsName = at.nextToken().trim();
            if (isDefault) {
                ctx.setDefDataSourceName(dsName);
            }

            String sDbType = at.nextToken().trim();
            int dbtype = DBTypes.getDBType(sDbType);
            DataSourceConfig dsConfig = new DataSourceConfig(dbtype);
            boolean needTranContent = true;
            boolean needTranSentence = false;
            String clientCharset = "GBK";
            String dbCharset = "GBK";
            String commit;
            if (at.hasMoreTokens()) {
                commit = at.nextToken().trim();
                if ("1".equals(commit)) {
                    dsConfig.setNeedTranContent(true);
                    if (count < 5) {
                        Logger.error(mm.getMessage("config.ds.error1", dsc));
                        return;
                    }
                } else {
                    needTranContent = false;
                }

                if (at.hasMoreTokens()) {
                    dbCharset = at.nextToken().trim();
                    dsConfig.setDBCharset(dbCharset);
                }

                if (at.hasMoreTokens()) {
                    clientCharset = at.nextToken().trim();
                    dsConfig.setClientCharset(clientCharset);
                    if (isDefault) {
                        Context.setJspCharset(clientCharset);
                    }
                }
            }

            if (at.hasMoreTokens()) {
                commit = at.nextToken().trim();
                needTranSentence = "1".equals(commit);
                dsConfig.setNeedTranSentence(needTranSentence);
            }

            commit = null;
            if (at.hasMoreTokens()) {
                commit = at.nextToken().trim();
                dsConfig.setAutoCommit(!"false".equalsIgnoreCase(commit));
            }

            ctx.setDataSourceConfig(dsName, dsConfig);

            try {
                IConnectionFactory conFactory = new JNDIConnectionFactory(jndiPrefix + dsName, commit);
                ctx.setConnectionFactory(dsName, conFactory);
            } catch (Exception var17) {
                Logger.error(var17.getMessage(), var17);
            }

            if (_$1) {
                DMUtil.setJndiDataSource(dsName, jndiPrefix, dbtype, clientCharset, dbCharset, needTranContent, needTranSentence);
            }

        }
    }

    private static void _$1(Context __context, ReportConfigModel _config) {
        if (_config != null) {
            String[] jcmKeys = _config.listDsModelKeys();

            for(int i = 0; i < jcmKeys.length; ++i) {
                String jcmKey = jcmKeys[i];
                if (jcmKey != null && !"".equals(jcmKey)) {
                    JDBCDsConfigModel jcm = _config.getDsValue(jcmKey);
                    if (jcm != null) {
                        DataSource ds = new DataSource();
                        ds.setName(jcmKey);
                        String url = jcm.getUrl();
                        ds.setUrl(url);
                        String driver = jcm.getDriver();
                        ds.setDriver(driver);
                        String un = jcm.getUserName();
                        if (un == null) {
                            un = "";
                        }

                        ds.setUser(un);
                        String pass = jcm.getPassword();
                        if (pass == null) {
                            pass = "";
                        }

                        String exs;
                        if (pass.length() > 0) {
                            exs = jcm.getDecryptClass();
                            if (exs != null && exs.length() > 0) {
                                try {
                                    IDecrypt idec = (IDecrypt)Class.forName(exs).newInstance();
                                    pass = idec.decrypt(pass);
                                } catch (Exception var21) {
                                    Logger.info(var21);
                                }
                            }
                        }

                        ds.setPassword(PwdUtils.encrypt(pass));
                        ds.setAutoCommit(jcm.getAutoCommit());
                        exs = jcm.getExtendProperties();
                        ds.setExtend(exs);
                        String dbTypeName = jcm.getDbType();
                        String dc = jcm.getDbCharset();
                        String cc = jcm.getClientCharset();
                        String defDn = __context.getDefDataSourceName();
                        __context.setConnectionFactory(jcmKey, ds);
                        boolean var16 = true;

                        int dbType;
                        try {
                            dbType = Integer.parseInt(dbTypeName);
                        } catch (Throwable var20) {
                            dbType = DBTypes.getDBType(dbTypeName);
                        }

                        DataSourceConfig dsoc = new DataSourceConfig(dbType, true, dc, cc, false);
                        __context.setDataSourceConfig(jcmKey, dsoc);
                        if ((defDn == null || "".equals(defDn)) && i == 0) {
                            __context.setDefDataSourceName(jcmKey);
                        }

                        if (_$1) {
                            try {
                                DMUtil.setDataSource(jcmKey, dbType, dc, cc, false, true, driver, url, un, PwdUtils.encrypt(pass), false, false, exs, false);
                            } catch (Exception var19) {
                                var19.printStackTrace();
                            }
                        }
                    }
                }
            }
        }

    }

    public void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            if (serverEncode == null) {
                request.setCharacterEncoding(Context.getJspCharset());
            } else {
                request.setCharacterEncoding(serverEncode);
            }

            String tmp = request.getParameter("action");
            boolean var4 = true;

            int action;
            try {
                action = Integer.parseInt(tmp);
            } catch (Throwable var46) {
                throw new ServletException(ServerMsg.getMessage(request, "actionError"));
            }

            ServletContext application = this.getServletContext();
            String archive;
            switch(action) {
            case 1:
                AppletData ad = new AppletData();
                ad.service(application, request, response);
                break;
            case 2:
            case 30:
                String localPrint = request.getParameter("localPrint");
                boolean inNS = request.getHeader("user-agent").toLowerCase().indexOf("msie") < 0;
                String name = request.getParameter("name");
                String isGroup = request.getParameter("isGroup");
                String cachedId = request.getParameter("cachedId");
                String reportFileName = request.getParameter("reportFileName");
                reportFileName = Native2Ascii.decode(reportFileName);
                String srcType = request.getParameter("srcType");
                String width = request.getParameter("width");
                String height = request.getParameter("height");
                String reportParamsId = request.getParameter("reportParamsId");
                String appRoot = getUrlPrefix(request);
                String sessionId = request.getSession().getId();
                String dataServlet = ServletMappings.getMapping("com.runqian.report4.view.ReportServlet") + ";jsessionid=" + sessionId + "?action=" + 1;
                String userUnitName = "润乾报表 -- 试用版";

                try {
                    ExtCellSet llo = ExtCellSet.get();
                    userUnitName = llo.getPrompt();
                } catch (Exception var45) {
                    ;
                }

                String savePrintSetup = request.getParameter("savePrintSetup");
                String appletJarName = request.getParameter("appletJarName");
                String useJinit = request.getParameter("useJinit");
                String needSelectPrinter = request.getParameter("needSelectPrinter");
                String serverPagedPrint = request.getParameter("serverPagedPrint");
                String mirror = request.getParameter("mirror");
                String printerName = request.getParameter("printerName");
                if (printerName != null) {
                    printerName = Native2Ascii.decode(printerName);
                }

                String key = request.getParameter("key");
                String promptString = null;
                ExtCellSet.get();
                if (ExtCellSet.checkKey(key)) {
                    promptString = userUnitName;
                }

                StringBuffer reportTitles = null;
                if (isGroup != null) {
                    ReportGroup rg = ReportUtils2.readReportGroup(reportFileName, new Context());
                    reportTitles = new StringBuffer();

                    for(int i = 0; i < rg.getItemCount(); ++i) {
                        if (reportTitles.length() > 0) {
                            reportTitles.append(",");
                        }

                        reportTitles.append(rg.getItem(i).getTitle());
                    }
                }

                String updateServerUrl;
                String copies;
                if (reportParamsId == null) {
                    String paramString = request.getParameter("paramString");
                    if (paramString != null) {
                        Hashtable hash = new Hashtable();
                        SegmentSet seg = new SegmentSet(paramString, true, ';');
                        Iterator it = seg.keySet().iterator();

                        while(it.hasNext()) {
                            updateServerUrl = (String)it.next();
                            copies = seg.get(updateServerUrl);
                            hash.put(updateServerUrl, copies);
                        }

                        reportParamsId = ParamsPool.createParamsId();
                        ParamsPool.put(reportParamsId, hash);
                    }
                }

                StringBuffer sb = new StringBuffer();
                sb.append("<html><body>");
                ArgumentTokenizer at;
                if (action == 2) {
                    if (localPrint != null) {
                        String printUrl = "RQLocalPrint://fileName=" + Native2Ascii.encode(reportFileName) + "&srcType=" + srcType + "&savePrintSetup=" + savePrintSetup + "&appletJarName=" + URLEncoder.encode(appletJarName) + "&serverPagedPrint=" + serverPagedPrint + "&mirror=" + mirror;
                        if (reportParamsId != null) {
                            printUrl = printUrl + "&reportParamsId=" + reportParamsId;
                        }

                        if (cachedId != null) {
                            printUrl = printUrl + "&cachedId=" + cachedId;
                        }

                        if (isGroup != null) {
                            printUrl = printUrl + "&isGroup=" + isGroup;
                            printUrl = printUrl + "&reportTitles=" + reportTitles;
                        }

                        if (needSelectPrinter != null) {
                            printUrl = printUrl + "&needSelectPrinter=" + needSelectPrinter;
                        }

                        archive = request.getParameter("directPrint");
                        if (archive != null) {
                            printUrl = printUrl + "&directPrint=1";
                        }

                        String needSave = request.getParameter("needSave");
                        if (needSave != null) {
                            printUrl = printUrl + "&needSave=yes";
                            updateServerUrl = request.getParameter("saveAsName");
                            if (updateServerUrl != null) {
                                printUrl = printUrl + "&saveAsName=" + updateServerUrl;
                            }
                        }

                        updateServerUrl = request.getParameter("updateServerUrl");
                        if (updateServerUrl != null) {
                            printUrl = printUrl + "&updateServer=" + updateServerUrl + "&updateServerMsg=no";
                        }

                        printUrl = printUrl + "&appRoot=" + appRoot + "&dataServlet=" + dataServlet + "&sessionId=" + sessionId + "&unitName=" + Native2Ascii.encode(userUnitName);
                        sb.append("<script language=javascript>\n");
                        sb.append("//校验是否安装\n");
                        sb.append("document.location = \"" + printUrl + "\";\n");
                        sb.append("</script>\n");
                    } else {
                        if (width == null) {
                            width = "0";
                        }

                        if (height == null) {
                            height = "0";
                        }

                        (new StringBuilder()).append(";jsessionid=").append(sessionId).toString();
                        archive = "";

                        for(at = new ArgumentTokenizer(appletJarName, ','); at.hasNext(); archive = archive + appRoot + "/" + at.nextToken()) {
                            if (archive.length() > 0) {
                                archive = archive + ",";
                            }
                        }

                        if (!inNS) {
                            if (useJinit == null) {
                                sb.append("<object classid=\"clsid:8AD9C840-044E-11D1-B3E9-00805F499D93\"");
                                sb.append("\tcodebase=\"" + appRoot + jreInstallName + "\"");
                                sb.append("\twidth=\"0\" height=\"0\" id=\"" + name + "_printApplet\">");
                                sb.append("\t<param name=\"type\" value=\"application/x-java-applet;version=" + jreVersion + "\">");
                            } else {
                                sb.append("<object classid=\"clsid:CAFECAFE-0013-0001-0018-ABCDEFABCDEF\"");
                                sb.append("\tcodebase=\"" + appRoot + "/jinit13118.exe#Version=1,3,1,18\"");
                                sb.append("\twidth=\"100\" height=\"100\" id=\"" + name + "_printApplet\">");
                                sb.append("\t<param name=\"type\" value=\"application/x-jinit-applet;version=1.3.1.18\">");
                            }

                            sb.append("\t<param name=\"name\" value=\"" + name + "_printApplet\">");
                            sb.append("\t<param name=\"code\" value=\"com.runqian.report4.view.applet.HtmlPrintApplet.class\">");
                            sb.append("\t<param name=\"archive\" value=\"" + archive + "\">");
                            sb.append("\t<param name=\"appRoot\" value=\"" + appRoot + "\">");
                            sb.append("\t<param name=\"dataServlet\" value=\"" + dataServlet + "\">");
                            sb.append("\t<param name=\"fileName\" value=\"" + reportFileName + "\">");
                            sb.append("\t<param name=\"srcType\" value=\"" + srcType + "\">");
                            sb.append("\t<param name=\"unitName\" value=\"" + userUnitName + "\">");
                            sb.append("\t<param name=\"savePrintSetup\" value=\"" + savePrintSetup + "\">");
                            sb.append("\t<param name=\"serverPagedPrint\" value=\"" + serverPagedPrint + "\">");
                            sb.append("\t<param name=\"mirror\" value=\"" + mirror + "\">");
                            if (reportParamsId != null) {
                                sb.append("\t<param name=\"reportParamsId\" value=\"" + reportParamsId + "\">");
                            }

                            if (cachedId != null) {
                                sb.append("\t<param name=\"cachedId\" value=\"" + cachedId + "\">");
                            }

                            if (isGroup != null) {
                                sb.append("\t<param name=\"isGroup\" value=\"1\">");
                                sb.append("\t<param name=\"reportTitles\" value=\"" + reportTitles.toString() + "\">");
                                sb.append("\t<param name=\"cachedIds\" value=\"" + request.getParameter("cachedIds") + "\">");
                            }

                            if (needSelectPrinter != null) {
                                sb.append("\t<param name=\"needSelectPrinter\" value=\"" + needSelectPrinter + "\">");
                            }

                            if (printerName != null) {
                                sb.append("\t<param name=\"printerName\" value=\"" + printerName + "\">");
                            }

                            if (promptString != null) {
                                sb.append("\t<param name=\"promptString\" value=\"" + promptString + "\">");
                            }

                            sb.append("\t<param name=\"wrapInchingWidth\" value=\"" + ControlUtils.getWrapInchingWidth() + "\">");
                            sb.append("\t<param name=\"wrapByChar\" value=\"" + ControlUtils.getWrapChar() + "\">");
                            sb.append("\t<param name=\"scriptable\" value=\"true\">");
                            sb.append("</object>");
                        } else {
                            if (useJinit == null) {
                                sb.append("<embed type=\"application/x-java-applet;version=1.4\"");
                            } else {
                                sb.append("<embed type=\"application/x-jinit-applet;version=1.3.1.18\"");
                            }

                            sb.append("\tpluginspage=\"" + appRoot + jreInstallName);
                            sb.append("\twidth=\"0\"");
                            sb.append("\theight=\"0\"");
                            sb.append("\tid=\"" + name + "_printApplet\"");
                            sb.append("\tname=\"" + name + "_printApplet\"");
                            sb.append("\tcode=\"com.runqian.report4.view.applet.HtmlPrintApplet.class\"");
                            sb.append("\tarchive=\"" + archive + "\"");
                            sb.append("\tappRoot=\"" + appRoot + "\"");
                            sb.append("\tdataServlet=\"" + dataServlet + "\"");
                            sb.append("\tfileName=\"" + reportFileName + "\"");
                            sb.append("\tsrcType=\"" + srcType + "\"");
                            sb.append("\tunitName=\"" + userUnitName + "\"");
                            sb.append("\tsavePrintSetup=\"" + savePrintSetup + "\"");
                            sb.append("\tserverPagedPrint=\"" + serverPagedPrint + "\"");
                            sb.append("\tmirror=\"" + mirror + "\"");
                            if (reportParamsId != null) {
                                sb.append("\treportParamsId=\"" + reportParamsId + "\"");
                            }

                            if (cachedId != null) {
                                sb.append("\tcachedId=\"" + cachedId + "\"");
                            }

                            if (isGroup != null) {
                                sb.append("\tisGroup=\"1\"");
                                sb.append("\treportTitles=\"" + reportTitles.toString() + "\"");
                                sb.append("\tcachedIds=\"" + request.getParameter("cachedIds") + "\"");
                            }

                            if (needSelectPrinter != null) {
                                sb.append("\tneedSelectPrinter=\"" + needSelectPrinter + "\"");
                            }

                            if (printerName != null) {
                                sb.append("\tprinterName=\"" + printerName + "\"");
                            }

                            if (promptString != null) {
                                sb.append("\tpromptString=\"" + promptString + "\"");
                            }

                            sb.append("\twrapInchingWidth=\"" + ControlUtils.getWrapInchingWidth() + "\"");
                            sb.append("\twrapByChar=\"" + ControlUtils.getWrapChar() + "\"");
                            sb.append("\tscriptable=\"true\">");
                            sb.append("</embed>");
                        }
                    }
                } else if (action == 30) {
                    (new StringBuilder()).append(";jsessionid=").append(sessionId).toString();
                    archive = "";

                    for(at = new ArgumentTokenizer(appletJarName, ','); at.hasNext(); archive = archive + appRoot + "/" + at.nextToken()) {
                        if (archive.length() > 0) {
                            archive = archive + ",";
                        }
                    }

                    updateServerUrl = request.getParameter("needPrintPrompt");
                    copies = request.getParameter("copies");
                    String pages = request.getParameter("pages");
                    if (!inNS) {
                        sb.append("<object classid=\"clsid:8AD9C840-044E-11D1-B3E9-00805F499D93\"");
                        sb.append("\tcodebase=\"" + appRoot + jreInstallName + "\"");
                        sb.append("\twidth=\"10\" height=\"10\" id=\"" + name + "_directPrintApplet\">");
                        sb.append("\t<param name=\"type\" value=\"application/x-java-applet;version=" + jreVersion + "\">");
                        sb.append("\t<param name=\"name\" value=\"" + name + "_directPrintApplet\">");
                        sb.append("\t<param name=\"code\" value=\"com.runqian.report4.view.applet.DirectPrintWithoutShow.class\">");
                        sb.append("\t<param name=\"archive\" value=\"" + archive + "\">");
                        sb.append("\t<param name=\"appRoot\" value=\"" + appRoot + "\">");
                        sb.append("\t<param name=\"dataServlet\" value=\"" + dataServlet + "\">");
                        sb.append("\t<param name=\"fileName\" value=\"" + reportFileName + "\">");
                        sb.append("\t<param name=\"srcType\" value=\"" + srcType + "\">");
                        if (reportParamsId != null) {
                            sb.append("\t<param name=\"reportParamsId\" value=\"" + reportParamsId + "\">");
                        }

                        if (cachedId != null) {
                            sb.append("\t<param name=\"cachedId\" value=\"" + cachedId + "\">");
                        }

                        sb.append("\t<param name=\"scriptable\" value=\"true\">");
                        sb.append("\t<param name=\"needPrintPrompt\" value=\"" + updateServerUrl + "\">");
                        sb.append("\t<param name=\"needSelectPrinter\" value=\"" + needSelectPrinter + "\">");
                        sb.append("\t<param name=\"savePrintSetup\" value=\"" + savePrintSetup + "\">");
                        sb.append("\t<param name=\"wrapInchingWidth\" value=\"" + ControlUtils.getWrapInchingWidth() + "\">");
                        sb.append("\t<param name=\"wrapByChar\" value=\"" + ControlUtils.getWrapChar() + "\">");
                        if (printerName != null && printerName.trim().length() > 0) {
                            sb.append("\t<param name=\"printerName\" value=\"" + printerName + "\">");
                        }

                        if (copies != null && copies.trim().length() > 0) {
                            sb.append("\t<param name=\"copies\" value=\"" + copies + "\">");
                        }

                        if (pages != null && pages.trim().length() > 0) {
                            sb.append("\t<param name=\"pages\" value=\"" + pages + "\">");
                        }

                        sb.append("</object>");
                    } else {
                        sb.append("<embed type=\"application/x-java-applet;version=1.4\"");
                        sb.append("\tpluginspage=\"" + appRoot + jreInstallName);
                        sb.append("\twidth=\"10\"");
                        sb.append("\theight=\"10\"");
                        sb.append("\tid=\"" + name + "_directPrintApplet\"");
                        sb.append("\tname=\"" + name + "_directPrintApplet\"");
                        sb.append("\tcode=\"com.runqian.report4.view.applet.DirectPrintWithoutShow.class\"");
                        sb.append("\tarchive=\"" + archive + "\"");
                        sb.append("\tappRoot=\"" + appRoot + "\"");
                        sb.append("\tdataServlet=\"" + dataServlet + "\"");
                        sb.append("\tfileName=\"" + reportFileName + "\"");
                        sb.append("\tsrcType=\"" + srcType + "\"");
                        if (reportParamsId != null) {
                            sb.append("\treportParamsId=\"" + reportParamsId + "\"");
                        }

                        if (cachedId != null) {
                            sb.append("\tcachedId=\"" + cachedId + "\"");
                        }

                        sb.append("\tscriptable=\"true\"");
                        sb.append("\tneedPrintPrompt=\"" + updateServerUrl + "\"");
                        sb.append("\tneedSelectPrinter=\"" + needSelectPrinter + "\"");
                        sb.append("\tsavePrintSetup=\"" + savePrintSetup + "\"");
                        sb.append("\twrapInchingWidth=\"" + ControlUtils.getWrapInchingWidth() + "\"");
                        sb.append("\twrapByChar=\"" + ControlUtils.getWrapChar() + "\"");
                        if (printerName != null && printerName.trim().length() > 0) {
                            sb.append("\tprinterName=\"" + printerName + "\"");
                        }

                        if (copies != null && copies.trim().length() > 0) {
                            sb.append("\tcopies=\"" + copies + "\"");
                        }

                        if (pages != null && pages.trim().length() > 0) {
                            sb.append("\tpages=\"" + pages + "\"");
                        }

                        sb.append("></embed>");
                    }
                }

                sb.append("</body></html>");
                PrintWriter pw = null;

                try {
                    try {
                        response.setContentType("text/html; charset=UTF-8");
                        pw = response.getWriter();
                        if (request.getParameter("toDownload") != null) {
                            pw.println("你尚未安装支持报表打印的Java Plug-in.<br>");
                            pw.println("请下载并安装后再打印报表。<p>");
                            pw.println("Windows系统，<a href=\"" + appRoot + "/j2re-1_4_1-windows-i586-i.exe\">下载</a><br>");
                            pw.println("Linux系统，<a href=\"" + appRoot + "/j2re-1_4_2_08-linux-i586.rpm\">下载</a><br>");
                            pw.println("其他操作系统，<a href=\"http://java.sun.com/j2se/1.4.2/download.html\">下载</a><br>");
                        } else {
                            pw.print(sb.toString());
                        }

                        pw.flush();
                    } catch (Exception var44) {
                        var44.printStackTrace();
                    }
                    break;
                } finally {
                    if (pw != null) {
                        pw.close();
                    }

                }
            case 3:
                (new ExcelReportServlet()).service(request, response, application);
                break;
            case 4:
                (new ConfigExcelServlet()).service(request, response);
                break;
            case 5:
                (new SaveAsExcelServlet()).service(request, response, application);
                break;
            case 6:
                (new PdfReportServlet()).service(request, response, application);
                break;
            case 7:
                (new WordReportServlet()).service(request, response, application);
                break;
            case 8:
                (new SetReportParamsServlet()).service(request, response, application);
                break;
            case 9:
                (new GraphServlet()).service(request, response);
                break;
            case 10:
                (new ReadJavaScriptServlet()).service(request, response);
                break;
            case 11:
                (new SaveDataServlet()).service(request, response, application);
                break;
            case 12:
                (new SaveToLocalServlet()).service(request, response);
                break;
            case 13:
                (new SelectUploadFile()).service(request, response);
                break;
            case 14:
                (new UploadFile2DBServlet()).service(request, response, this.getServletConfig());
                break;
            case 15:
                (new SaveSheetsServlet()).service(request, response, application);
                break;
            case 16:
                (new UploadExcelServlet()).service(request, response, this.getServletConfig(), application);
                break;
            case 17:
                (new SelectExcelFileServlet()).service(request, response);
                break;
            case 18:
                (new TextFileServlet()).service(request, response, application);
                break;
            case 19:
                (new ShowOlapDetail()).service(request, response);
                break;
            case 20:
                (new ShowOlapGraph()).service(request, response);
                break;
            case 21:
                (new SaveGroupServlet()).service(request, response, application);
                break;
            case 22:
                (new OlapExpExcelAction()).service(request, response);
                break;
            case 23:
                archive = request.getParameter("list");
                if (archive != null) {
                    (new ListOlapStatus()).service(application, request, response);
                } else {
                    (new OlapSaveAction()).service(application, request, response);
                }
                break;
            case 24:
                (new InputBox()).service(request, response);
                break;
            case 25:
                (new ConfigPdfServlet()).service(request, response);
                break;
            case 26:
                (new CommonQuerySaveAction()).service(request, response);
                break;
            case 27:
                (new AjaxFormat()).service(request, response);
                break;
            case 28:
                (new PajReportServlet()).service(request, response, application);
            case 29:
            }

        } catch (Exception var48) {
            throw new ServletException(var48);
        }
    }

    public static String getUrlPrefix(HttpServletRequest request) {
        if (appUrlPrefix != null) {
            if (!_$5) {
                appUrlPrefix = Sentence.replace(appUrlPrefix, "/APPMAP", request.getContextPath(), 0);
                _$5 = true;
            }

            return appUrlPrefix;
        } else {
            String appRoot = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath();
            return appRoot;
        }
    }

    public static void reloadConfig(ServletContext application) throws Exception {
        if (Context.getInitCtx() == null) {
            System.out.println("Reload Report Config.............");
            loadConfig(application, true);
        }
    }

    public static void reloadConfig(ServletContext application, String configFilePath) throws ServletException {
        if (Context.getInitCtx() == null) {
            System.out.println("Report System initing......");
            Context initContext = new Context();
            initContext.setApplication(application);
            loadConfig(application, configFilePath, initContext, true);
            System.out.println("Report System initialized......");
        }
    }

    static void readServletMappings(ServletContext application) {
        try {
            InputStream is = null;

            try {
                is = application.getResourceAsStream("/WEB-INF/web.xml");
            } catch (Exception var3) {
                ;
            }

            if (is == null) {
                is = application.getResourceAsStream("/web-inf/web.xml");
            }

            ServletMappings.read(is);
            is.close();
        } catch (Throwable var4) {
            Logger.info(var4.getMessage(), var4);
        }

    }

    static boolean isDMInstalled() {
        String prj = "/com/raq/ide/prjx/PRJX.class";
        InputStream is = ReportServlet.class.getResourceAsStream(prj);
        return is != null;
    }

    public void destroy() {
        CleanTimeoutParams.shutDown = true;
        _$4.shutdown();
        _$3.interrupt();
        _$4.interrupt();
        CacheManager.getInstance().deleteTempFiles();
        if (_$2 != null) {
            _$2.stop();
        }

    }
}
